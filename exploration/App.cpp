#include "App.h"

static App* app;




void handleWindowResize(int width, int height) {
    app->handleWindowResize(width, height);
}

void display() {
    app->display();
}



void handleKeyPress(unsigned char key, int x, int y) {
    app->handleKeyPress(key, x, y);
}

void handleMousePress(int button, int state, int x, int y) {}

void handleMouseMove(int x, int y) {}

void handleJoystick(unsigned int buttonmask, int x, int y, int z) {}

void update(int value) {
    app->update(value);
    glutTimerFunc(50, update, 0);
}

void idleFunc() {}

App::App(int argc, char** argv) {
    glutInit(&argc, argv);
    glutInitContextVersion(2, 1);
    glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);
    glutInitContextProfile(GLUT_CORE_PROFILE);
    glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);
    glutInitWindowSize(window_size.x, window_size.y);
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
    WindowHandle = glutCreateWindow("Pierwsza aplikacja");
    if (WindowHandle < 1) {
        throw std::logic_error("Error: Could not create a window\n");
    }
    glutReshapeFunc(::handleWindowResize);
    glutDisplayFunc(::display);
    glutKeyboardFunc(::handleKeyPress);
    glutMouseFunc(::handleMousePress);
    glutMotionFunc(::handleMouseMove);
    /*glutJoystickFunc ( handleJoystick, 16 );*/
    glutTimerFunc(0, ::update, 0);
    /*glutIdleFunc ( idleFunc );*/
    GetGLProcAddresses();

    if (USE_GL3W and !gl3wIsSupported(4, 2)) {
        ExitOnError("Initialise: OpenGL version 4.2 not supported\n");
    }

    icosahedron = std::make_unique<Icosahedron>();
}

void App::handleKeyPress(unsigned char key, int x, int y) {
    if (key == ESCAPE) {
        icosahedron = nullptr;
        glutDestroyWindow(WindowHandle);
        glutLeaveMainLoop();
    }
    static KeyMap keybindings = initBindings();
    auto it = keybindings.find(key);
    if (it != keybindings.end()) {
        it->second();
        redraw();
    }
}

void App::display() {
    glClearColor(1.0, 1.0, 1.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    icosahedron->draw(option);
    glFlush();
    glutSwapBuffers();
    if (opti > 0) {
        opti--;
        redraw();
    }
}

App::KeyMap App::initBindings() {
    KeyMap map;
    map.emplace('w', [&] { option = ProgramState::Walls; });
    map.emplace('v', [&] { option = ProgramState::Vertices; });
    map.emplace('e', [&] { option = ProgramState::Edges; });
    map.emplace('+', [&] {
        icosahedron->near++;
        icosahedron->reapplyProjectionMatrix(Vector2i());
    });
    map.emplace('-', [&] {
        icosahedron->near--;
        icosahedron->reapplyProjectionMatrix(Vector2i());
    });
    map.emplace('x', [&] { icosahedron->axis = Axis::X; });
    map.emplace('y', [&] { icosahedron->axis = Axis::Y; });
    map.emplace('z', [&] { icosahedron->axis = Axis::Z; });

    // for all letters, add also uppercase variants
    for (auto&[c, state] : map) {
        if ('a' <= c and c <= 'z') {
            map.emplace(c - 'a' + 'A', state);
        }
    }
    return map;
}

void App::handleWindowResize(int width, int height) {
    window_size = {width, height};
    glViewport(0, 0, width, height); // whole window
    icosahedron->reapplyProjectionMatrix(window_size);
    opti = 4;
    std::clog << "Window was resized to (" << window_size.x << ", " << window_size.y << ").\n";
    ExitIfGLError("handleWindowResize");
}

void App::setCurrentApp(App& new_app) {
    app = &new_app;
}
