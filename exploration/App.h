#ifndef EXPLORATION_APP_H
#define EXPLORATION_APP_H

#include <map>
#include <functional>
#include "utilities/utilities.h"
#include "Icosahedron.h"



class App {
    int WindowHandle;

    const unsigned char ESCAPE = 0x1B;

    ProgramState option = ProgramState::Walls;

    int opti = 0;

    std::unique_ptr<Icosahedron> icosahedron;

    Vector2i window_size = {800, 600};
public:
    void redraw() {
        glutPostWindowRedisplay(WindowHandle);
    }

    void handleWindowResize(int width, int height);

    void display();


    void handleKeyPress(unsigned char key, int x, int y);

    void update(int value) {
        icosahedron->update();
        redraw();
        ExitIfGLError("update");
    }

    App(int argc, char** argv);

    using KeyMap = std::map<char, std::function<void()>>;

    KeyMap initBindings();

    static void setCurrentApp(App& new_app);
};



#endif //EXPLORATION_APP_H
