#include "Icosahedron.h"

Icosahedron::Icosahedron() : model_matrix(Matrix::identity()), view_matrix(Matrix::translation({0.0, 0.0, -10.0f})) {
    loadShaders();
    apply_model_matrix();
    apply_view_matrix();
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glGenBuffers(3, vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
    glBufferData(GL_ARRAY_BUFFER, 12 * 3 * sizeof(GLfloat), vertex_positions, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*) nullptr);
    setColors(false);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_UNSIGNED_BYTE, GL_TRUE, 3 * sizeof(GLubyte), (GLvoid*) nullptr);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[2]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 62 * sizeof(GLubyte), vertind, GL_STATIC_DRAW);
}

void Icosahedron::setColors(bool black) const {
    if (black) {
        static const std::vector<Vector3<GLubyte>> vertex_colors(0, {0, 0, 0});
        glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
        glBufferData(GL_ARRAY_BUFFER, 12 * 3 * sizeof(GLubyte), vertex_colors.data(), GL_STATIC_DRAW);
    } else {
        glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
        glBufferData(GL_ARRAY_BUFFER, 12 * 3 * sizeof(GLubyte), vertex_colors, GL_STATIC_DRAW);
    }
}

void Icosahedron::drawEdges() const {
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[2]);
    glDrawElements(GL_LINE_STRIP, 25, GL_UNSIGNED_BYTE, (GLvoid*) nullptr);
    glDrawElements(GL_LINE_STRIP, 3, GL_UNSIGNED_BYTE, (GLvoid*) (25 * sizeof(GLubyte)));
    glDrawElements(GL_LINES, 8, GL_UNSIGNED_BYTE, (GLvoid*) (28 * sizeof(GLubyte)));
}

void Icosahedron::draw(ProgramState state) {
    glUseProgram(program_id);
    glBindVertexArray(vao);
    switch (state) {
        case ProgramState::Vertices:
            glPointSize(5.0);
            glDrawArrays(GL_POINTS, 0, 12);
            break;
        case ProgramState::Edges:
            drawEdges();
            break;
        case ProgramState::Walls:
            setColors(false);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[2]);
            glDrawElements(GL_TRIANGLE_FAN, 7, GL_UNSIGNED_BYTE, (GLvoid*) (36 * sizeof(GLubyte)));
            glDrawElements(GL_TRIANGLE_FAN, 7, GL_UNSIGNED_BYTE, (GLvoid*) (43 * sizeof(GLubyte)));
            glDrawElements(GL_TRIANGLE_STRIP, 12, GL_UNSIGNED_BYTE, (GLvoid*) (50 * sizeof(GLubyte)));
            setColors(true);
            drawEdges();
            break;
    }
}

void Icosahedron::loadShaders() {
    static const char* filename[] =
      {"app1.glsl.vert", "app1.glsl.frag"};
    static const GLchar* UTBNames[] =
      {"TransBlock", "TransBlock.mm", "TransBlock.vm", "TransBlock.pm"};
    GLuint ind[3];

    shader_id[0] = CompileShaderFiles(GL_VERTEX_SHADER, {filename[0]});
    shader_id[1] = CompileShaderFiles(GL_FRAGMENT_SHADER, {filename[1]});
    program_id = LinkShaderProgram(2, shader_id);
    transform_uniform_block_index = glGetUniformBlockIndex(program_id, UTBNames[0]);
    glGetActiveUniformBlockiv(program_id, transform_uniform_block_index, GL_UNIFORM_BLOCK_DATA_SIZE, &trans_block_size);
    glGetUniformIndices(program_id, 3, &UTBNames[1], ind);
    glGetActiveUniformsiv(program_id, 3, ind, GL_UNIFORM_OFFSET, trans_block_offsets);
    glUniformBlockBinding(program_id, transform_uniform_block_index, 0);
    glGenBuffers(1, &trans_block_uniform_buffer);
    glBindBufferBase(GL_UNIFORM_BUFFER, 0, trans_block_uniform_buffer);
    glBufferData(GL_UNIFORM_BUFFER, trans_block_size, nullptr, GL_DYNAMIC_DRAW);
    ExitIfGLError("LoadMyShaders");
}

Icosahedron::~Icosahedron() {
    glUseProgram(0);
    for (unsigned int i : shader_id) {
        glDeleteShader(i);
    }
    glDeleteProgram(program_id);
    glDeleteBuffers(1, &trans_block_uniform_buffer);
    glDeleteVertexArrays(1, &vao);
    glDeleteBuffers(3, vbo);
}

void Icosahedron::reapplyProjectionMatrix(Vector2i window_size) {
    float lr = 0.5533f * (float) window_size.x / (float) window_size.y;
    auto m = M4x4ToStandardCubeFrustum({-lr, lr, -0.5533f, 0.5533f, near, far});
    glBindBuffer(GL_UNIFORM_BUFFER, trans_block_uniform_buffer);
    glBufferSubData(GL_UNIFORM_BUFFER, trans_block_offsets[2], 16 * sizeof(GLfloat), m);
}

void Icosahedron::update() {
    const auto angular_velocity = 0.05f;
//    auto& rot = rotation[static_cast<int>(axis)];
//    rot += angular_velocity;
//    rot = static_cast<float>(std::fmod(rot, 2 * M_PI));
//    model_matrix = Matrix::identity();
//    for (int i = 0; i < 3; ++i) {
//        model_matrix *= Matrix::rotation(static_cast<Axis>(i), rotation[i]);
//    }
    model_matrix *= Matrix::rotation(axis, angular_velocity);

    apply_model_matrix();
}
