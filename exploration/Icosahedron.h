#ifndef EXPLORATION_ICOSAHEDRON_H
#define EXPLORATION_ICOSAHEDRON_H


#include "utilities/utilities.h"


class Icosahedron {
    GLuint vao, vbo[3];
    GLuint shader_id[2];

    GLuint program_id;

    GLuint transform_uniform_block_index, trans_block_uniform_buffer;

    GLint trans_block_size, trans_block_offsets[3];

    Matrix view_matrix, model_matrix, projection_matrix;
public:
    float near = 5.f;

    const float far = 15.f;

    Vector3f rotation;

    Axis axis = Axis::Y;
private:
    static constexpr float A = 0.52573115f;
    static constexpr float B = 0.85065085f;
    static constexpr GLfloat vertex_positions[12][3] = {
      {-A,  0.0, -B},
      {A,   0.0, -B},
      {0.0, -B,  -A},
      {-B,  -A,  0.0},
      {-B,  A,   0.0},
      {0.0, B,   -A},
      {A,   0.0, B},
      {-A,  0.0, B},
      {0.0, -B,  A},
      {B,   -A,  0.0},
      {B,   A,   0.0},
      {0.0, B,   A}
    };
    static constexpr GLubyte vertind[62] =
      {0, 1, 2, 0, 3, 4, 0, 5, 1, 9, 2, 8, 3, /* lamana, od 0 */
       7, 4, 11, 5, 10, 9, 6, 8, 7, 6, 11, 7,
       1, 10, 6,                              /* lamana, od 25 */
       2, 3, 4, 5, 8, 9, 10, 11,              /* 4 odcinki, od 28 */
       0, 1, 2, 3, 4, 5, 1,                   /* wachlarz, od 36 */
       6, 7, 8, 9, 10, 11, 7,                 /* wachlarz, od 43  */
       1, 9, 2, 8, 3, 7, 4, 11, 5, 10, 1, 9}; /* tasma, od 50 */

    static constexpr GLubyte vertex_colors[12][3] =
      {{255, 0,   0},
       {255, 127, 0},
       {255, 255, 0},
       {127, 255, 0},
       {0,   255, 0},
       {0,   255, 127},
       {0,   255, 255},
       {0,   127, 255},
       {0,   0,   255},
       {127, 0,   255},
       {255, 0,   255},
       {255, 0,   127}};

    void apply_model_matrix() {
        glBindBuffer(GL_UNIFORM_BUFFER, trans_block_uniform_buffer);
        glBufferSubData(GL_UNIFORM_BUFFER, trans_block_offsets[0], 16 * sizeof(GLfloat), model_matrix);
    }
    void apply_view_matrix() {
        glBindBuffer(GL_UNIFORM_BUFFER, trans_block_uniform_buffer);
        glBufferSubData(GL_UNIFORM_BUFFER, trans_block_offsets[1], 16 * sizeof(GLfloat), view_matrix);
    }

public:
    Icosahedron();

    ~Icosahedron();

    void setColors(bool black = false) const;

    void drawEdges() const;

    void draw(ProgramState state);

    void loadShaders();

    void reapplyProjectionMatrix(Vector2i window_size);

    void update();
};


#endif //EXPLORATION_ICOSAHEDRON_H
