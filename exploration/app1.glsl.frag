#version 420

in Vertex {
  vec4 Colour;
} In;

out vec4 out_Colour;

void main() {
  out_Colour = In.Colour;
}
