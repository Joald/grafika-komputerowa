#include "utilities/utilities.h"
#include "App.h"


int main(int argc, char* argv[]) {
    App app(argc, argv);
    App::setCurrentApp(app);
    try {
        glutMainLoop();
    } catch (std::exception& e) {
        std::cerr << e.what() << "\n";
    }
}
