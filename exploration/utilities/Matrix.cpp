#include <iostream>
#include "Matrix.h"

std::optional<PALU> Matrix::PALUDecompose() const {

    Matrix lu = *this;
    Vector3i p;
    for (int j = 0; j < 3; j++) {
        GLfloat d = std::abs(lu.ind(j, j));
        p[j] = j;
        for (int i = j + 1; i < 4; i++) { // Check if we need to permute rows.
            auto row_value = std::abs(lu.ind(i, j));
            if (row_value > d) {
                d = row_value;
                p[j] = i;
            }
        }
        if (d == 0.0) { // the whole column was 0, matrix is not invertible
            return {};
        }
        if (p[j] != j) { // permutation is not identity
            int i = p[j];
            for (int k = 0; k < 4; k++) {
                std::swap(lu.ind(i, k), lu.ind(j, k));
            }
        }
        for (int i = j + 1; i < 4; i++) {
            d = lu.ind(i, j) /= lu.ind(j, j);
            for (int k = j + 1; k < 4; k++) {
                lu.ind(i, k) -= d * lu.ind(j, k);
            }
        }
    }
    if (lu[15] == 0.0) {
        return {};
    } else {
        return std::make_optional<PALU>(lu, p, transposed);
    }
}

std::optional<Matrix> Matrix::invert() const {
    auto op = PALUDecompose();
    if (!op.has_value()) {
        return {};
    }
    auto& palu = op.value();
    Matrix res;
    for (int i = 0; i < 4; i++) {
        auto e = Vector4GLf::e(i + 1);
        palu.solve(res.column(i), e);
    }
    return {res};
}


std::ostream& operator<<(std::ostream& stream, const Matrix& m) {
    stream << "[";
    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 4; ++j) {
            stream << m[i * 4 + j] << (j != 3 ? ", " : "");
        }
        if (i != 3) {
            stream << "; ";
        }
    }
    return stream << "]\n";
}
