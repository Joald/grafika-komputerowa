#pragma once

#include <ostream>
#include <string>
#include <optional>
#include <vector>
#include <GL/gl.h>
#include <stdexcept>
#include <algorithm>
#include "Vectors.h"
#include "math.h"


struct PALU;

enum class Axis {
    X, Y, Z
};

class Matrix {
    static const int N = 16;
    std::vector<GLfloat> data;
    bool transposed = false;

    void real_transpose() {
        if (transposed) {
            transposed = false;
            auto& A = *this;
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < i; j++) {
                    std::swap(A.ind(i, j), A.ind(j, i));
                }
            }
        }
    }

    void real_transpose() const {
        // this does not change the logical state,
        // only the internal representation
        const_cast<Matrix*>(this)->real_transpose();
    }

public:

    Matrix() : data(N) {}

    Matrix(const GLfloat arr[N]) : data() {
        std::copy(arr, arr + N, std::back_inserter(data));
    }

    Matrix(std::initializer_list<GLfloat> list) : data() {
        if (list.size() != N) {
            throw std::invalid_argument("Matrices are 4x4, please specify 16 floats!");
        }
        std::copy(list.begin(), list.end(), std::back_inserter(data));
    }

    Matrix(std::vector<GLfloat> v) : data(std::move(v)) {
        if (data.size() != N) {
            throw std::invalid_argument("Matrices are 4x4, please specify 16 floats!");
        }
    }

    Matrix(std::initializer_list<Vector4GLf> list) : data() {
        if (list.size() != N / 4) {
            throw std::invalid_argument("Matrices are 4x4, please specify 4 vectors!");
        }
        data.reserve(N);
        for (auto& i : list) {
            std::copy(i.begin(), i.end(), std::back_inserter(data));
        }
    }

    Matrix(Matrix&& other) noexcept: data(std::move(other.data)) {}

    Matrix& operator=(Matrix&& other) noexcept {
        data = std::move(other.data);
        return *this;
    }

    Matrix(const Matrix& other) : data(other.data) {}

    Matrix& operator=(const Matrix& other) {
        if (this != &other) {
            transposed = other.transposed;
            data = other.data;
        }
        return *this;
    }

    operator std::vector<GLfloat>&() {
        real_transpose();
        return data;
    }

    operator GLfloat*() {
        real_transpose();
        return data.data();
    }

    operator const GLfloat*() const {
        real_transpose();
        return data.data();
    }

    decltype(data.size()) size() const {
        return data.size();
    }

    GLfloat& operator[](int i) {
        real_transpose();
        return data[i];
    }

    const GLfloat& operator[](int i) const {
        real_transpose();
        return data[i];
    }

    GLfloat* column(int i) {
        real_transpose();
        return data.begin().base() + i * 4;
    }

    GLfloat& ind(int i, int j) {
        return data[i + 4 * j];
    }

    const GLfloat& ind(int i, int j) const {
        return data[i + 4 * j];
    }

    std::optional<PALU> PALUDecompose() const;

    std::optional<Matrix> invert() const;

    Matrix& transpose() {
        transposed = !transposed;
        return *this;
    }

    Matrix& operator*=(const Matrix& B) {
        *this = *this * B;
        return *this;
    }

    Matrix operator*(const Matrix& B) {
        real_transpose();
        auto& A = *this;
        auto ab = Matrix();
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                ab.ind(i, j) = A.ind(i, 0) * B.ind(0, j);
                for (int k = 1; k < 4; k++) {
                    ab.ind(i, j) += A.ind(i, k) * B.ind(k, j);
                }
            }
        }
        return ab;
    }

    Vector4GLf operator*(const Vector4GLf& v) {
        auto& A = *this;
        Vector4GLf Av;
        if (transposed) {
            for (int i = 0; i < 4; i++) {
                Av[i] = A.ind(0, i) * v[0];
                for (int j = 1; j < 4; j++) {
                    Av[i] += A.ind(j, i) * v[j];
                }
            }
        } else {
            for (int i = 0; i < 4; i++) {
                Av[i] = A.ind(i, 0) * v[0];
                for (int j = 1; j < 4; j++) {
                    Av[i] += A.ind(i, j) * v[j];
                }
            }
        }
        return Av;
    }

    bool operator==(const Matrix& other) const {
        return data == other.data;
    }



    /////////////////////////////////////////////////////////////////////////////
    ///                            Static functions                           ///
    /////////////////////////////////////////////////////////////////////////////


    /**
     * @brief Returns the identity matrix.
     * @return the identity matrix
     */
    static Matrix identity() {
        return {Vector4GLf::e(1), Vector4GLf::e(2), Vector4GLf::e(3), Vector4GLf::e(4)};
    }

    /**
     * @brief Returns a matrix that translates a vector
     * @param translation_vector - the vector to translate by
     * @return the matrix
     */
    static Matrix translation(const Vector3GLf& translation_vector) {
        auto a = Matrix::identity();
        a[12] = translation_vector.x;
        a[13] = translation_vector.y;
        a[14] = translation_vector.z;
        return a;
    }

    /**
     * @brief Returns a matrix that scales a vector
     * @param scale_vector - the vector to scale by
     * @return the matrix
     */
    static Matrix M4x4Scale(Vector3f scale_vector) {
        auto a = Matrix::identity();
        a[0] = scale_vector.x;
        a[5] = scale_vector.y;
        a[10] = scale_vector.z;
        return a;
    } /*M4x4Scale*/

    /**
     * @brief Returns a matrix that rotates a vector around the given axis
     * @param phi - angly to rotate by
     * @param axis - the axis
     * @return the matrix
     */
    static Matrix rotation(Axis axis, float phi) {
        auto a = Matrix::identity();
        switch (axis) {
            case Axis::X:
                a[5] = a[10] = std::cos(phi);
                a[9] = -(a[6] = std::sin(phi));
                break;
            case Axis::Y:
                a[0] = a[10] = std::cos(phi);
                a[2] = -(a[8] = std::sin(phi));
                break;
            case Axis::Z:
                a[0] = a[5] = std::cos(phi);
                a[4] = -(a[1] = std::sin(phi));
                break;
        }
        return a;
    } /*M4x4Rotation*/
};


std::ostream& operator<<(std::ostream& stream, const Matrix& m);


struct PALU {
    const Matrix lu;
    const Vector3i p;
    bool transposed;

    PALU(Matrix lu, Vector3i p, bool transposed = false) :
      lu(std::move(lu)), p(p), transposed(transposed) {}

    PALU& transpose() {
        transposed = !transposed;
        return *this;
    }


    /**
     * @brief Given a PA=LU decomposition of a 4x4 matrix and a vector b, solve
     *  the linear equation system Ax=b
     * @param solution: RandomAccessIterator pointing to the beginning
     *  of a size 4 column vector of GLfloats to solve.
     * @param b - a vector of GLfloats of size 4 - the right side of the Ax=b equation
     */
    template<class RandomAccessIterator, class F>
    void solve(RandomAccessIterator solution, Vector4<F> b) {
        if (transposed) {
            for (int i = 0; i < 4; ++i) {
                solution[i] = b[i];
            }
            for (int i = 0; i <= 3; i++) {
                for (int j = 0; j < i; j++) {
                    solution[i] -= lu.ind(j, i) * solution[j];
                }
                solution[i] /= lu.ind(i, i);
            }
            for (int i = 2; i >= 0; i--) {
                for (int j = i + 1; j < 4; j++) {
                    solution[i] -= lu.ind(j, i) * solution[j];
                }
            }
            for (int i = 2; i >= 0; i--) {
                if (p[i] != i) {
                    std::swap(solution[i], solution[p[i]]);
                }
            }
        } else { // not transposed
            for (int i = 0; i < 4; ++i) {
                solution[i] = b[i];
            }
            for (int i = 0; i < 3; i++) {
                if (p[i] != i) {
                    std::swap(solution[i], solution[p[i]]);
                }
            }
            for (int i = 1; i < 4; i++) {
                for (int j = 0; j < i; j++) {
                    solution[i] -= lu.ind(i, j) * solution[j];
                }
            }
            for (int i = 3; i >= 0; i--) {
                for (int j = i + 1; j < 4; j++) {
                    solution[i] -= lu.ind(i, j) * solution[j];
                }
                solution[i] /= lu.ind(i, i);
            }
        }
    }
};
