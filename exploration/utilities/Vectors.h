#ifndef OPENGL_VECTOR_H
#define OPENGL_VECTOR_H


#include <type_traits>
#include <GL/gl.h>
#include <stdexcept>
#include <cmath>
#include <cassert>
#include <ostream>
#include <tuple>

template<class T>
struct Vector2 {
    static_assert(std::is_trivially_copy_constructible_v<T>);

    using iterator = T*;

    T x, y;
    Vector2() = default;
    Vector2(const Vector2<T>& other) = default;

    Vector2(T x, T y) : x(x), y(y) {}

    T& operator[](int i) {
        if (i == 0) {
            return x;
        } else if (i == 1) {
            return y;
        }
        throw std::invalid_argument("Vector2 only has 2 elements: 0 and 1");
    }

    iterator begin() {
        return &x;
    }

    iterator end() {
        return &y + 1;
    }
};

using Vector2f = Vector2<float>;
using Vector2GLf = Vector2<GLfloat>;
using Vector2i = Vector2<int>;
using Vector2ui = Vector2<unsigned int>;

static_assert(std::is_trivially_copy_constructible_v<Vector2f>);

template<class T>
struct Vector4;

template<class T>
struct Vector3 {
    static_assert(std::is_trivially_copy_constructible_v<T>);

    using iterator = T*;
    using const_iterator = const T*;
    T x, y, z;

    constexpr Vector3() = default;
    constexpr Vector3(const Vector3<T>& other) = default;

    Vector3(std::initializer_list<T> list) : Vector3<T>() {
        auto it = list.begin();
        for (int i = 0; i < 3 and it != list.end(); ++i, ++it) {
            (*this)[i] = *it;
        }
    }

    Vector3(T x, T y, T z) : x(x), y(y), z(z) {}

    Vector3(T x) : x(x), y(x), z(x) {}

    T& operator[](int i) {
        return const_cast<T&>( // cast back to non-const
          static_cast<const Vector3<T>&>(*this)[i] // calls const overload
        ); // getter/setter
    }

    const T& operator[](int i) const {
        if (i == 0) {
            return x;
        } else if (i == 1) {
            return y;
        } else if (i == 2) {
            return z;
        }
        throw std::invalid_argument("Vector3 only has 3 elements: 0, 1 and 2");
    }

    template<class F = T>
    std::enable_if_t<std::is_floating_point_v<F>, T> length() const {
        return std::sqrt(length_sq());
    }

    template<class F = T>
    std::enable_if_t<std::is_floating_point_v<F>, T> length_sq() const {
        T res = 0;
        for (auto i : *this) {
            res += i * i;
        }
        return res;
    }

    template<class F = T>
    std::enable_if_t<std::is_floating_point_v<F>, Vector3<T>&> normalize() {
        T len = length();
        for (auto& i : *this) {
            i /= len;
        }
        return *this;
    }

    const_iterator begin() const {
        return &x;
    }

    const_iterator end() const {
        return &z + 1;
    }

    iterator begin() {
        return &x;
    }

    iterator end() {
        return &z + 1;
    }

    /**
     * @brief Returns a new vector that is reflected across a given plane.
     * @param p0 - the point that gives the plane
     * @param n - the normal vector of the plane
     * @return reflected point
     */
    Vector3<T> reflect(Vector3<T> p0, Vector3<T> normal_vector) {
        Vector3<T> r, q = *this;

        for (auto i : {0, 1, 2}) {
            r[i] = q[i] - p0[i];
        }
        float g = 2.0f * normal_vector.dot(r) / normal_vector.length_sq();
        for (auto i : {0, 1, 2}) {
            r[i] = q[i] - g * normal_vector[i];
        }
        return r;
    }

    float dot(Vector3<T> other) const {
        T res = 0;
        for (int i = 0; i < 3; ++i) {
            res += (*this)[i] * other[i];
        }
        return res;
    }

    Vector3<T> cross(const Vector3<T>& v2) const {
        auto& v1 = *this;

        return {v1[1] * v2[2] - v1[2] * v2[1],
                v1[2] * v2[0] - v1[0] * v2[2],
                v1[0] * v2[1] - v1[1] * v2[0]};

    }

    Vector3<T> operator*(const Vector3<T>& rhs) const {
        return cross(rhs);
    }
    Vector3<T> operator/(const Vector3<T>& rhs) const {
        return {x / rhs.x, y / rhs.y, z / rhs.z};
    }
    Vector3<T> operator/(const T& t) const {
        return {x / t, y / t, z / t};
    }

    Vector3<T> operator-() const {
        return {-x, -y, -z};
    }

    Vector4<T> to_vector4() const {
        return {x, y, z, 0};
    }
    Vector4<T> to_uniform() const {
        return {x, y, z, 1};
    }

    operator const T*() const {
        return begin();
    }

    operator T*() {
        return begin();
    }
};

using Vector3f = Vector3<float>;
using Vector3GLf = Vector3<GLfloat>;
using Vector3i = Vector3<int>;
using Vector3ui = Vector3<unsigned int>;


template<class T>
struct Vector4 {
    static_assert(std::is_trivially_copy_constructible_v<T>);

    using iterator = T*;
    using const_iterator = const T*;

    T x, y, z, w;

    Vector4() = default;
    Vector4(const Vector4<T>& other) = default;

    Vector4(const Vector3<T>& other) : Vector4(other.to_vector4()) {}

    explicit Vector4(const T* o) : x(*o++), y(*o++), z(*o++), w(*o++) {}

    Vector4<T>& operator=(const Vector4<T>& other) = default;

    Vector4<T>& operator=(Vector4<T>&& other) noexcept = default;

    Vector4<T>& operator=(const Vector3<T>& other) {
        *this = other.to_vector4();
    }

    Vector4(std::initializer_list<T> list) : Vector4<T>() {
        for (int i = 0; i < 4 and i < list.size(); ++i) {
            (*this)[i] = list.begin()[i];
        }
    }

    Vector4(T x, T y, T z, T w) : x(x), y(y), z(z), w(w) {}

    T& operator[](int i) {
        return const_cast<T&>( // cast back to non-const
          static_cast<const Vector4<T>&>(*this)[i] // calls const overload
        );
    }

    const T& operator[](int i) const {
        return *(begin() + i);
//        if (i == 0) {
//            return x;
//        } else if (i == 1) {
//            return y;
//        } else if (i == 2) {
//            return z;
//        } else if (i == 3) {
//            return w;
//        }
//        throw std::invalid_argument("Vector4 only has 4 elements: 0, 1, 2 and 3");
    }

    template<class F = T>
    std::enable_if_t<std::is_floating_point_v<F>, T> length() const {
        return std::sqrt(length_sq());
    }

    template<class F = T>
    std::enable_if_t<std::is_floating_point_v<F>, T> length_sq() const {
        T res = 0;
        for (auto i : *this) {
            res += i * i;
        }
        return res;
    }

    template<class F = T>
    std::enable_if_t<std::is_floating_point_v<F>> normalize() {
        T len = length();
        for (auto& i : *this) {
            i /= len;
        }
    }

    iterator begin() {
        return &x;
    }

    iterator end() {
        return &w + 1;
    }

    const_iterator begin() const {
        return &x;
    }

    const_iterator end() const {
        return &w + 1;
    }

    void zero_out() {
        x = y = z = w = 0;
    }

    Vector3<T> to_vector3() const {
        return {x, y, z};
    }

    static Vector4<T> e(int dim) {
        if (0 < dim and dim < 5) {
            Vector4<T> res{};
            res[dim - 1] = 1;
            return res;
        }
        throw std::invalid_argument("dim must be 1-4");
    }

    operator const T*() const {
        return begin();
    }

    operator T*() {
        return begin();
    }

    bool operator==(const Vector4<T> other) {
        return std::tie(x, y, z, w) == std::tie(x, y, z, w);
    }

    Vector3<T> to_carthesian() {
        return to_vector3() / w;
    }
};

using Vector4f = Vector4<float>;
using Vector4GLf = Vector4<GLfloat>;
using Vector4i = Vector4<int>;
using Vector4ui = Vector4<unsigned int>;
static_assert(sizeof(Vector4GLf) == 4 * sizeof(GLfloat), "Your compiler doesn't pack GLfloats tightly");

template<class T>
std::ostream& operator<<(std::ostream& stream, const Vector4<T>& m) {
    for (auto it = m.begin(); it != m.end(); ++it) {
        stream << *it << " ";
    }
    return stream << "\n";
}


#endif //OPENGL_VECTOR_H
