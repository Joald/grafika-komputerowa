#include <unistd.h>
#include <sys/times.h>

#include "utilities.h"

static clock_t tick0, tick1, tick2;
static double  ticks_per_sec = 100.0;
double         app_time, tic_time, toc_time;

void TimerInit ( void )
{
  struct tms clk;

  ticks_per_sec = (double)sysconf ( _SC_CLK_TCK );
  tick0 = tick1 = tick2 = times ( &clk );
  app_time = toc_time = 0.0;
} /*TimerInit*/

double TimerTic ( void )
{
  struct tms clk;

  tick1 = tick2 = times ( &clk );
  toc_time = 0.0;
  return app_time = tic_time = (double)(tick2-tick0)/ticks_per_sec;
} /*TimerTic*/

double TimerToc ( void )
{
  struct tms clk;

  tick2 = times ( &clk );
  app_time = (double)(tick2-tick0)/ticks_per_sec;
  return toc_time = (double)(tick2-tick1)/ticks_per_sec;
} /*TimerToc*/

