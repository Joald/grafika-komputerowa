#include <sstream>
#include "utilities.h"
#include <GL/glu.h>

#ifdef ENABLE_SPIRV

PFNGLSPECIALIZESHADERARB glSpecializeShaderARB = nullptr;

#endif

namespace {

std::string err_convert(GLenum err) {
    switch (err) {
        case GL_NO_ERROR:
            return "No error";
        case GL_INVALID_ENUM:
            return "Invalid enum";
        case GL_INVALID_VALUE:
            return "Invalid value";
        case GL_INVALID_OPERATION:
            return "Invalid operation";
        case GL_OUT_OF_MEMORY:
            return "out of memory";
        default:
            break;
    }
    return "cannot convert error";
}

}

/* ////////////////////////////////////////////////////////////////////////// */

void PrintGLVersion() {
    std::cout << "OpenGL " << glGetString(GL_VERSION) << ", GLSL " << glGetString(GL_SHADING_LANGUAGE_VERSION) << "\n";
} /*PrintGLVersion*/

void ExitOnError(const std::string& msg) {
    std::stringstream s;
    s << "Error: " << msg << "\n";
    throw std::logic_error(s.str());
} /*ExitOnError*/

void ExitIfGLError(const std::string& msg) {
    GLenum err = glGetError();
    if (err != GL_NO_ERROR) {
        std::stringstream s;
        s << "Error " << err << ": " << err_convert(err) << " (" << msg << ")\n";
        throw std::logic_error(s.str());
    }
} /*ExitIfGLError*/

void GetGLProcAddresses() {
#ifdef USE_GL3W
    if (gl3wInit()) {
        ExitOnError("GetGLProcAddresses (gl3w)\n");
    }
#else
    auto err = glewInit();

    if (err != GLEW_OK) {
        std::cerr << "Error: " << glewGetErrorString(err) << "\n";
        exit(1);
    }
#endif
} /*GetGLProcAddresses*/

/* ////////////////////////////////////////////////////////////////////////// */

GLuint LinkShaderProgram(int shader_count, const GLuint* shaders) {
    GLuint program_id = glCreateProgram();
    ExitIfGLError("LinkShaderProgram - cannot create program");

    for (int i = 0; i < shader_count; i++) {
        glAttachShader(program_id, shaders[i]);
        ExitIfGLError("LinkShaderProgram - cannot attach shader no. " + std::to_string(i) + " with id " +
                      std::to_string(shaders[i]));
    }

    glLinkProgram(program_id);
    ExitIfGLError("LinkShaderProgram - cannot link program");

    GLint log_size;
    glGetProgramiv(program_id, GL_INFO_LOG_LENGTH, &log_size);
    if (!log_size) {
        ExitIfGLError("LinkShaderProgram - log is empty");
        return program_id;
    }
    GLchar log[log_size + 1];
    if (log_size > 1) {
        glGetProgramInfoLog(program_id, log_size, &log_size, log);
        std::cerr << log << std::endl;
    }

    ExitIfGLError("LinkShaderProgram");
    return program_id;
} /*LinkShaderProgram*/

/* ////////////////////////////////////////////////////////////////////////// */
#ifdef ENABLE_SPIRV

GLuint CreateSPIRVShader(GLenum shader_type, size_t size, const GLuint* spirv) {
    GLuint shader_id = glCreateShader(shader_type);
    if (shader_id) {
        glShaderBinary(1, &shader_id, GL_SHADER_BINARY_FORMAT_SPIR_V_ARB, spirv, size);
        glSpecializeShaderARB(shader_id, "main", 0, nullptr, nullptr);
        GLint status;
        glGetShaderiv(shader_id, GL_COMPILE_STATUS, &status);
        if (!status) {
            glDeleteShader(shader_id);
            shader_id = 0;
        }
    }
    ExitIfGLError("CompileSPIRVString");
    return shader_id;
} /*CreateSPIRVShader*/

GLuint LoadSPIRVFile(GLenum shader_type, const char* filename) {
    GLuint shader_id = 0;
    FILE* f = fopen(filename, "rb");
    GLuint* spirv;

    if (!f) {
        return 0;
    }
    ::fseek(f, 0, SEEK_END);
    auto size = static_cast<size_t>(::ftell(f));
    ::rewind(f);
    spirv = new GLuint[size];
    if (fread(spirv, sizeof(char), size, f) == size) {
        shader_id = CreateSPIRVShader(shader_type, size, spirv);
    }
    fclose(f);
    delete spirv;
    return shader_id;
} /*LoadSPIRVFile*/
#endif

/* ////////////////////////////////////////////////////////////////////////// */
GLuint NewUniformBindingPoint() {
    static GLint max_uniform_binding_point = 0;
    static GLuint next_uniform_binding_point = 0;
    if (!max_uniform_binding_point) {
        glGetIntegerv(GL_MAX_UNIFORM_BUFFER_BINDINGS, &max_uniform_binding_point);
    }
    if (next_uniform_binding_point < max_uniform_binding_point) {
        return next_uniform_binding_point++;
    } else {
        ExitOnError("NewUniformBindingPoint");
    }
    return 0;
} /*NewUniformBindingPoint*/

void GetAccessToUniformBlock(
  GLuint prog, int n, const GLchar** names, GLuint* ind, GLint* size, GLint* ofs,
  GLuint* binding_point) {
    const int MAX_UNIFORM_FIELDS = 32;
    GLuint ufi[MAX_UNIFORM_FIELDS];

    *ind = glGetUniformBlockIndex(prog, names[0]);
    glGetActiveUniformBlockiv(prog, *ind, GL_UNIFORM_BLOCK_DATA_SIZE, size);
    if (n > 0) {
        glGetUniformIndices(prog, n, &names[1], ufi);
        glGetActiveUniformsiv(prog, n, ufi, GL_UNIFORM_OFFSET, ofs);
    }
    *binding_point = NewUniformBindingPoint();
    glUniformBlockBinding(prog, *ind, *binding_point);
    ExitIfGLError("GetAccessToUniformBlock");
} /*GetAccessToUniformBlock*/

GLuint NewUniformBlockObject(GLint size, GLuint binding_point) {
    GLuint buffer;
    glGenBuffers(1, &buffer);
    glBindBufferBase(GL_UNIFORM_BUFFER, binding_point, buffer);
    glBufferData(GL_UNIFORM_BUFFER, size, nullptr, GL_DYNAMIC_DRAW);
    ExitIfGLError("NewUniformBlockObject");
    return buffer;
} /*NewUniformBlockObject*/

void AttachUniformBlockToBindingPoint(GLuint program_id, const GLchar* name, GLuint binding_point) {
    GLuint ind = glGetUniformBlockIndex(program_id, name);
    glUniformBlockBinding(program_id, ind, binding_point);
    ExitIfGLError("AttachUniformBlockToBindingPoint");
} /*AttachUniformBlockToBindingPoint*/

void GetAccessToStorageBlock(GLuint prog, int n, const GLchar** names, GLuint* ind, GLint* size, GLint* ofs,
                             GLuint* bpoint) {
    ind[0] = glGetProgramResourceIndex(prog, GL_SHADER_STORAGE_BLOCK, names[0]);
    GLuint prop = GL_BUFFER_DATA_SIZE;
    glGetProgramResourceiv(prog, GL_SHADER_STORAGE_BLOCK, ind[0], 1, &prop, 1, nullptr, size);
    prop = GL_BUFFER_BINDING;
    glGetProgramResourceiv(prog, GL_SHADER_STORAGE_BLOCK, ind[0], 1, &prop, 1, nullptr, (GLint*) bpoint);
    if (n > 0) {
        prop = GL_OFFSET;
        for (int i = 1; i <= n; i++) {
            ind[i] = glGetProgramResourceIndex(prog, GL_BUFFER_VARIABLE, names[i]);
            glGetProgramResourceiv(prog, GL_BUFFER_VARIABLE, ind[i], 1, &prop, 1, nullptr, &ofs[i - 1]);
        }
    }
    ExitIfGLError("GetAccessToStorageBlock");
} /*GetAccessToStorageBlock*/

/* ////////////////////////////////////////////////////////////////////////// */




Matrix M4x4CustomRotation(Vector3f v, float phi) {
    auto a = Matrix::identity();
    v.normalize();
    float s = std::sin(phi);
    float c = std::cos(phi);
    float c1 = 1.0f - c;
    a[0] = v.x * v.x * c1 + c;
    a[1] = a[4] = v.x * v.x * c1;
    a[5] = v.x * v.x * c1 + c;
    a[2] = a[8] = v.x * v.z * c1;
    a[6] = a[9] = v.x * v.z * c1;
    a[10] = v.z * v.z * c1 + c;
    a[6] += s * v.x;
    a[2] -= s * v.x;
    a[1] += s * v.z;
    a[9] -= s * v.x;
    a[8] += s * v.x;
    a[4] -= s * v.z;
    return a;
} /*M4x4CustomRotation*/

/**
 * @brief
 * @param ai
 * @param a
 */
Matrix M4x4InvertAffineIsometry(const Matrix& a) {
    Matrix ai;
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            ai.ind(i, j) = a.ind(j, i);
        }
    }
    for (int i = 0; i < 3; i++) {
        ai.ind(i, 3) = -(ai.ind(i, 0) * a.ind(0, 3) + ai.ind(i, 1) * a.ind(1, 3) + ai.ind(i, 2) * a.ind(2, 3));
    }
    ai[3] = ai[7] = ai[11] = 0.0;
    ai[15] = 1.0;
    return ai;
} /*M4x4InvertAffineIsometry*/


/* ////////////////////////////////////////////////////////////////////////// */
Matrix M4x4ToStandardCubeFrustum(const ProjectionParams& params) {
    // Formerly M4x4Frustumf
    float left, right, bottom, top, near, far;
    std::tie(left, right, bottom, top, near, far) = params.tuple();
    Matrix a;
    float rl = right - left;
    float tb = top - bottom;
    float nf = near - far;
    float nn = near + near;
    a[0] = nn / rl;
    a[8] = (right + left) / rl;
    a[5] = nn / tb;
    a[9] = (top + bottom) / tb;
    a[10] = (far + near) / nf;
    a[14] = far * nn / nf;
    a[11] = -1.0f;
    return a;
}

Matrix M4x4FromStandardCubeFrustum(const ProjectionParams& params) {
    float left, right, bottom, top, near, far;
    std::tie(left, right, bottom, top, near, far) = params.tuple();
    Matrix ai;
    float rl = right - left;
    float tb = top - bottom;
    float nf = near - far;
    float nn = near + near;
    ai[0] = rl / nn;
    ai[12] = (right + left) / nn;
    ai[5] = tb / nn;
    ai[13] = (top + bottom) / nn;
    ai[14] = -1.0f;
    ai[11] = nf / (far * nn);
    ai[15] = (far + near) / (far * nn);
    return ai;
} /*M4x4ToStandardCubeFrustum*/

Matrix M4x4ToStandardCubeParallel(float left, float right, float bottom, float top, float near, float far) {
    // formerly M4x4Orthof
    Matrix a;
    float rl = right - left;
    float tb = top - bottom;
    float fn = far - near;
    a[0] = 2.0f / rl;
    a[12] = -(right + left) / rl;
    a[5] = 2.0f / tb;
    a[13] = -(top + bottom) / tb;
    a[10] = -2.0f / fn;
    a[14] = (far + near) / fn;
    a[15] = 1.0;
    return a;
} /*M4x4ToStandardCubeParallel*/

Matrix M4x4FromStandardCubeParallel(float left, float right, float bottom, float top, float near, float far) {
    Matrix ai;
    float rl = right - left;
    float tb = top - bottom;
    float fn = far - near;
    ai[0] = 0.5f * rl;
    ai[12] = 0.5f * (right + left);
    ai[5] = 0.5f * tb;
    ai[13] = 0.5f * (top + bottom);
    ai[10] = -0.5f * fn;
    ai[14] = 0.5f * (far + near);
    ai[15] = 1.0;
    return ai;
} /*M4x4FromStandardCubeParallel*/

/* ////////////////////////////////////////////////////////////////////////// */

std::pair<float, Vector3f> V3ComposeRotations(const Vector3f& v2, float phi2, const Vector3f& v1, float phi1) {
    float s2 = std::sin(0.5f * phi2);
    float c2 = std::cos(0.5f * phi2);
    float s1 = std::sin(0.5f * phi1);
    float c1 = std::cos(0.5f * phi1);
    auto v2v1 = v2.dot(v1);
    auto v2xv1 = v2 * v1;
    float c = c2 * c1 - v2v1 * s2 * s1;
    Vector3f v(v2[0] * s2 * c1 + v1[0] * s1 * c2 + v2xv1[0] * s2 * s1,
               v2[1] * s2 * c1 + v1[1] * s1 * c2 + v2xv1[1] * s2 * s1,
               v2[2] * s2 * c1 + v1[2] * s1 * c2 + v2xv1[2] * s2 * s1);
    auto s = std::sqrt(v.length_sq());
    float phi;
    if (s > 0.0) {
        v[0] /= s, v[1] /= s, v[2] /= s;
        phi = 2.0f * std::atan2(s, c);
    } else {
        v[0] = 1.0, v[1] = v[2] = phi = 0.0;
    }
    return {phi, v};
} /*V3ComposeRotations*/

/* ////////////////////////////////////////////////////////////////////////// */


GLuint CompileShaderFiles(GLenum shader_type, const std::vector<const char*>& filenames) {
    std::basic_stringstream<GLchar> shaderData;
    for (auto filename : filenames) {
        std::ifstream shaderFile(filename);
        if (!shaderFile) {
            ExitOnError("CompileShaderFiles: Can't open shader file!");
        }
        shaderData << shaderFile.rdbuf();  //Loads the entire string into a string stream.
    }

    const auto& shaderString = shaderData.str();
    const auto* ptr = shaderString.c_str();
    GLuint shader_id = glCreateShader(shader_type);
    if (shader_id != 0) {
        glShaderSource(shader_id, 1, &ptr, nullptr);
        glCompileShader(shader_id);
        GLint log_size;
        glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &log_size);
        if (log_size > 1) {
            GLchar log[log_size + 1];
            glGetShaderInfoLog(shader_id, log_size, &log_size, log);
            std::cerr << log << std::endl;
        }
    } else {
        ExitIfGLError("CompileShaderStrings");
        ExitOnError("shader id was zero");
    }
    return shader_id;
} /*CompileShaderFiles*/





