
extern GLuint shader_id[2]; 
extern GLuint program_id;
extern GLuint trbi, trbuf;
extern GLint  trbsize, trbofs[3];

extern GLuint icos_vao, icos_vbo[3];
extern int opcja;


extern int WindowHandle;


void InitModelMatrix ( void );
void InitViewMatrix ( void );
void ConstructIcosahedronVAO ( void );
void DrawIcosahedron ( int opt );


void ReshapeFunc ( int width, int height );
void DisplayFunc ( void );
void KeyboardFunc ( unsigned char key, int x, int y );
void MouseFunc ( int button, int state, int x, int y );
void MotionFunc ( int x, int y );
void TimerFunc ( int value );
void IdleFunc ( void );

void LoadMyShaders ( void );
void InitMyObject ( void );
void Cleanup ( void );

void Initialise ( int argc, char *argv[] );

