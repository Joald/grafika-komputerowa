#include <stdlib.h>
#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include "myglheader.h"
#include <GL/glx.h>

#include "../utilities/utilities.h"
#include "initglxctx.h"
#include "app1.h"

Window xmywin;
char   terminate;

int opcja = 2;

void DestroyMyShaders ( void )
{
} /*DestroyMyShaders*/

void DestroyMyObject ( void )
{
} /*DestroyMyObject*/

void InitMyGLXWindow ( int argc, char **argv,
                       int major, int minor, int width, int height )
{
  XSetWindowAttributes swa;

  if ( !(xdisplay = XOpenDisplay ( "" )) )
    exit ( 1 );
  xscreen = DefaultScreen ( xdisplay );
  xrootwin = RootWindow ( xdisplay, xscreen );
  InitGLContext ( major, minor );
  if ( !(xcolormap = XCreateColormap ( xdisplay, xrootwin,
                                       xvisual, AllocNone )) )
    exit ( 1 );
  swa.colormap = xcolormap;
  swa.event_mask = ExposureMask | StructureNotifyMask| ButtonPressMask |
                   ButtonReleaseMask | PointerMotionMask | KeyPressMask ;
  xmywin = XCreateWindow ( xdisplay, xrootwin, 0, 0, width, height,
                           0, 24, InputOutput, xvisual,
                           CWColormap | CWEventMask, &swa );
  XMapWindow ( xdisplay, xmywin );
  if ( !glXMakeCurrent ( xdisplay, xmywin, glxcontext ) ) {
    printf ( "Error: glXMakeCurrent failed\n" );
    exit ( 1 );
  }
  GetGLProcAddresses ();
#ifdef USE_GL3W
  if ( !gl3wIsSupported ( 4, 2 ) )
    ExitOnError ( "Initialise: OpenGL version 4.2 not supported\n" );
#endif
} /*InitMyGLXWindow*/

void DestroyMyGLXWindow ( void )
{
  XCloseDisplay ( xdisplay );
} /*DestroyMyGLXWindow*/

void Initialise ( int argc, char **argv )
{
  InitMyGLXWindow ( argc, argv, 4, 2, 480, 360 );
  LoadMyShaders ();
  InitMyObject ();
} /*Initialise*/

void MyWinExpose ( void )
{
  glClearColor ( 1.0, 1.0, 1.0, 1.0 );
  glClear ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
  glEnable ( GL_DEPTH_TEST );
  DrawIcosahedron ( opcja );
  glFlush ();
  glXSwapBuffers ( xdisplay, xmywin );
} /*MyWinExpose*/

void MyWinConfigureNotify ( XEvent *event )
{
  int     width, height;
  GLfloat m[16];
  float   lr;

  width = event->xconfigure.width;
  height = event->xconfigure.height;
  glViewport ( 0, 0, width, height );  /* klatka jest calym oknem */
  lr = 0.5533*(float)width/(float)height;
  M4x4Frustumf ( m, NULL, -lr, lr, -0.5533, 0.5533, 5.0, 20.0 );
  glBindBuffer ( GL_UNIFORM_BUFFER, trbuf );
  glBufferSubData ( GL_UNIFORM_BUFFER, trbofs[2], 16*sizeof(GLfloat), m );
  ExitIfGLError ( "ReshapeFunc" );
} /*MyWinConfigureNotify*/

void MyWinButtonPress ( XEvent *event )
{
} /*MyWinButtonPress*/

void MyWinButtonRelease ( XEvent *event )
{
} /*MyWinButtonRelease*/

void MyWinMotionNotify ( XEvent *event )
{
} /*MyWinMotionNotify*/

void MyWinKeyPress ( XEvent *event )
{
  terminate = 1;
} /*MyWinKeyPress*/

void MyWinClientMessage ( XEvent *event )
{
} /*MyWinClientMessage*/

void MyWinMessageProc ( XEvent *event )
{
  switch ( event->xany.type ) {
case Expose:
    if ( event->xexpose.count == 0 )
      MyWinExpose ();
    break;
case ConfigureNotify:  MyWinConfigureNotify ( event );  break;
case ButtonPress:      MyWinButtonPress ( event );      break;
case ButtonRelease:    MyWinButtonRelease ( event );    break;
case MotionNotify:     MyWinMotionNotify ( event );     break;
case KeyPress:         MyWinKeyPress ( event );         break;
case ClientMessage:    MyWinClientMessage ( event );    break;
           default:    break;
  }
} /*MyWinMessageProc*/

void MessageLoop ( void )
{
  XEvent event;

  terminate = 0;
  do {
    XNextEvent ( xdisplay, &event );
    if ( event.xany.window == xmywin )
      MyWinMessageProc ( &event );
  } while ( !terminate );
} /*MessageLoop*/

int main ( int argc, char **argv )
{
  Initialise ( argc, argv );
  MessageLoop ();
  Cleanup ();
  exit ( 0 );
} /*main*/
