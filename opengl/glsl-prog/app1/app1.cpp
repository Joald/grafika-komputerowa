#include <cstdlib>
#include <cstdio>
#include <iostream>

#include "../utilities/utilities.h"

int WindowHandle;

enum class Option {
    VERTICES, EDGES, WALLS
} option = Option::WALLS;

int opti = 0;

GLuint shader_id[2];

GLuint program_id;

GLuint trans_block_index, trbuf;

GLint trans_block_size, trbofs[3];

GLuint icosahedron_vao, icosahedron_vbo[3];


///////////////////////////////////////////////////////////


void LoadMyShaders() {
    static const GLchar* UTBNames[] = {"TransBlock", "TransBlock.mm", "TransBlock.vm", "TransBlock.pm"};
    shader_id[0] = CompileShaderFiles(GL_VERTEX_SHADER, {"app1.glsl.vert"});
    ExitIfGLError("LoadMyShaders9");
    shader_id[1] = CompileShaderFiles(GL_FRAGMENT_SHADER, {"app1.glsl.frag"});
    ExitIfGLError("LoadMyShaders8");
    program_id = LinkShaderProgram(2, shader_id);
    ExitIfGLError("LoadMyShaders7.5");
    trans_block_index = glGetUniformBlockIndex(program_id, UTBNames[0]);
    ExitIfGLError("LoadMyShaders7");
    glGetActiveUniformBlockiv(program_id, trans_block_index, GL_UNIFORM_BLOCK_DATA_SIZE, &trans_block_size);
    ExitIfGLError("LoadMyShaders6");
    GLuint uniform_indices[3];
    glGetUniformIndices(program_id, 3, &UTBNames[1], uniform_indices);
    ExitIfGLError("LoadMyShaders5");
    glGetActiveUniformsiv(program_id, 3, uniform_indices, GL_UNIFORM_OFFSET, trbofs);
    ExitIfGLError("LoadMyShaders4");
    glUniformBlockBinding(program_id, trans_block_index, 0);
    ExitIfGLError("LoadMyShaders3");
    glGenBuffers(1, &trbuf);
    ExitIfGLError("LoadMyShaders2");
    glBindBufferBase(GL_UNIFORM_BUFFER, 0, trbuf);
    ExitIfGLError("LoadMyShaders1");
    glBufferData(GL_UNIFORM_BUFFER, trans_block_size, nullptr, GL_DYNAMIC_DRAW);
} /*LoadMyShaders*/

void InitModelMatrix() {
    auto m = M4x4Identity();
    glBindBuffer(GL_UNIFORM_BUFFER, trbuf);
    glBufferSubData(GL_UNIFORM_BUFFER, trbofs[0], 16 * sizeof(GLfloat), m);
    ExitIfGLError("InitModelMatrix");
} /*InitModelMatrix*/

void InitViewMatrix() {
    auto m = M4x4Translate({0, 0, -10.0f});
    glBindBuffer(GL_UNIFORM_BUFFER, trbuf);
    glBufferSubData(GL_UNIFORM_BUFFER, trbofs[1], 16 * sizeof(GLfloat), m);
    ExitIfGLError("InitViewMatrix");
} /*InitViewMatrix*/

void ConstructIcosahedronVAO() {
#define A 0.52573115f
#define B 0.85065085f
    static const GLfloat vertpos[12][3] = {
      {-A,  0.0, -B},
      {A,   0.0, -B},
      {0.0, -B,  -A},
      {-B,  -A,  0.0},
      {-B,  A,   0.0},
      {0.0, B,   -A},
      {A,   0.0, B},
      {-A,  0.0, B},
      {0.0, -B,  A},
      {B,   -A,  0.0},
      {B,   A,   0.0},
      {0.0, B,   A}
    };
    static const GLubyte vertcol[12][3] = {
      {255, 0,   0},
      {255, 127, 0},
      {255, 255, 0},
      {127, 255, 0},
      {0,   255, 0},
      {0,   255, 127},
      {0,   255, 255},
      {0,   127, 255},
      {0,   0,   255},
      {127, 0,   255},
      {255, 0,   255},
      {255, 0,   127}
    };
    static const GLubyte vertind[62] = {
      0, 1, 2, 0, 3, 4, 0, 5, 1, 9, 2, 8, 3,
      7, 4, 11, 5, 10, 9, 6, 8, 7, 6, 11, 7, 1, /* lamana, od 0 do 24*/
      10, 6,                              /* lamana, od 25 */
      2, 3, 4, 5, 8, 9, 10, 11,              /* 4 odcinki, od 28 */
      0, 1, 2, 3, 4, 5, 1,                   /* wachlarz, od 36 */
      6, 7, 8, 9, 10, 11, 7,                 /* wachlarz, od 43  */
      1, 9, 2, 8, 3, 7, 4, 11, 5, 10, 1, 9
    }; /* tasma, od 50 */

    glGenVertexArrays(1, &icosahedron_vao);
    glBindVertexArray(icosahedron_vao);
    glGenBuffers(3, icosahedron_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, icosahedron_vbo[0]);
    glBufferData(GL_ARRAY_BUFFER, 12 * 3 * sizeof(GLfloat), vertpos, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*) nullptr);
    glBindBuffer(GL_ARRAY_BUFFER, icosahedron_vbo[1]);
    glBufferData(GL_ARRAY_BUFFER, 12 * 3 * sizeof(GLubyte), vertcol, GL_STATIC_DRAW);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_UNSIGNED_BYTE, GL_TRUE, 3 * sizeof(GLubyte), (GLvoid*) nullptr);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, icosahedron_vbo[2]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 62 * sizeof(GLubyte), vertind, GL_STATIC_DRAW);
    ExitIfGLError("ConstructIcosahedronVAO");
} /*ConstructIcosahedronVAO*/


void InitMyObject() {
    InitModelMatrix();
    InitViewMatrix();
    ConstructIcosahedronVAO();
} /*InitMyObject*/


void DrawIcosahedron(Option opt) {
    glUseProgram(program_id);
    glBindVertexArray(icosahedron_vao);
    switch (opt) {
        case Option::VERTICES:    /* wierzcholki */
            glPointSize(5.0);
            glDrawArrays(GL_POINTS, 0, 12);
            break;
        case Option::EDGES:    /* krawedzie */
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, icosahedron_vbo[2]);
            glDrawElements(GL_LINE_STRIP, 25, GL_UNSIGNED_BYTE, (GLvoid*) nullptr);
            glDrawElements(GL_LINE_STRIP, 3, GL_UNSIGNED_BYTE, (GLvoid*) (25 * sizeof(GLubyte)));
            glDrawElements(GL_LINES, 8, GL_UNSIGNED_BYTE, (GLvoid*) (28 * sizeof(GLubyte)));
            break;
        case Option::WALLS:   /* sciany */
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, icosahedron_vbo[2]);
            glDrawElements(GL_TRIANGLE_FAN, 7, GL_UNSIGNED_BYTE, (GLvoid*) (36 * sizeof(GLubyte)));
            glDrawElements(GL_TRIANGLE_FAN, 7, GL_UNSIGNED_BYTE, (GLvoid*) (43 * sizeof(GLubyte)));
            glDrawElements(GL_TRIANGLE_STRIP, 12, GL_UNSIGNED_BYTE, (GLvoid*) (50 * sizeof(GLubyte)));
            break;
    }
} /*DrawIcosahedron*/

void Cleanup() {
    glUseProgram(0);
    for (auto i : shader_id) {
        glDeleteShader(i);
    }
    glDeleteProgram(program_id);
    glDeleteBuffers(1, &trbuf);
    glDeleteVertexArrays(1, &icosahedron_vao);
    glDeleteBuffers(3, icosahedron_vbo);
    ExitIfGLError("Cleanup");
    glutDestroyWindow(WindowHandle);
} /*Cleanup*/

///////////////////////////////////////////////////////////


void ReshapeFunc(int width, int height) {
    glViewport(0, 0, width, height);  /* klatka jest calym oknem */
    float lr = 0.5533f * width / height;
    auto m = M4x4ToStandardCubeFrustum(-lr, lr, -0.5533f, 0.5533f, 5.0f, 15.0f);
    glBindBuffer(GL_UNIFORM_BUFFER, trbuf);
    glBufferSubData(GL_UNIFORM_BUFFER, trbofs[2], 16 * sizeof(GLfloat), m);
    opti = 4;
    ExitIfGLError("ReshapeFunc");
} /*ReshapeFunc*/

void DisplayFunc() {
    glClearColor(1.0, 1.0, 1.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    DrawIcosahedron(option);
    glFlush();
    glutSwapBuffers();
    if (opti > 0) {
        opti--;
        glutPostWindowRedisplay(WindowHandle);
    }
} /*DisplayFunc*/

void KeyboardFunc(unsigned char key, int x, int y) {
    UNUSED(x);
    UNUSED(y);
    switch (key) {
        case 0x1B:            /* klawisz Esc - zatrzymanie programu */
            Cleanup();
            glutLeaveMainLoop();
            break;
        case 'W':
        case 'w':  /* przelaczamy na wierzcholki */
            option = Option::VERTICES;
            glutPostWindowRedisplay(WindowHandle);
            break;
        case 'K':
        case 'k':  /* przelaczamy na krawedzie */
            option = Option::EDGES;
            glutPostWindowRedisplay(WindowHandle);
            break;
        case 'S':
        case 's':  /* przelaczamy na sciany */
            option = Option::WALLS;
            glutPostWindowRedisplay(WindowHandle);
            break;
        default:              /* ignorujemy wszystkie inne klawisze */
            break;
    }
} /*KeyboardFunc*/

void MouseFunc(int button, int state, int x, int y) {
    UNUSED(button);
    UNUSED(state);
    UNUSED(x);
    UNUSED(y);
}

void MotionFunc(int x, int y) {
    UNUSED(x);
    UNUSED(y);
}

void JoystickFunc(unsigned int buttonmask, int x, int y, int z) {
    UNUSED(buttonmask);
    UNUSED(x);
    UNUSED(y);
    UNUSED(z);
}

void TimerFunc(int value) { UNUSED(value); }

void IdleFunc() {}

void Initialise(int argc, char* argv[]) {
    glutInit(&argc, argv);
    glutInitContextVersion(2, 1);
    glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);
    glutInitContextProfile(GLUT_CORE_PROFILE);
    glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);
    glutInitWindowSize(480, 360);
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
    WindowHandle = glutCreateWindow("NAPIERDALAAAAMY");
    if (WindowHandle < 1) {
        std::cerr << "Error: Could not create a window\n";
        exit(1);
    }
    glutReshapeFunc(ReshapeFunc);
    glutDisplayFunc(DisplayFunc);
    glutKeyboardFunc(KeyboardFunc);
    glutMouseFunc(MouseFunc);
    glutMotionFunc(MotionFunc);
    /*glutJoystickFunc ( JoystickFunc, 16 );*/
    /*glutTimerFunc ( 0, TimerFunc, 0 );*/
    /*glutIdleFunc ( IdleFunc );*/
    GetGLProcAddresses();
#ifdef USE_GL3W
    if ( !gl3wIsSupported ( 4, 2 ) )
      ExitOnError ( "Initialise: OpenGL version 4.2 not supported\n" );
#endif
    LoadMyShaders();
    InitMyObject();
} /*Initialise*/

int main(int argc, char* argv[]) {
    Initialise(argc, argv);
    glutMainLoop();
    exit(0);
} /*main*/
