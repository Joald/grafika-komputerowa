#include <cstdlib>
#include <unistd.h>
#include <cstdio>
#include <sys/times.h>
#include "myglheader.h"      /* najpierw ten */
#include <GL/freeglut.h>  /* potem ten */

#include "../utilities/utilities.h"
#include "app1a.h"

int WindowHandle;

int win_width, win_height;

int last_xi, last_eta;

float left, right, bottom, top, near, far;

int app_state = STATE_NOTHING;

float viewer_rvec[3] = {1.0, 0.0, 0.0};

float viewer_rangle = 0.0;

const float viewer_pos0[4] = {0.0, 0.0, 10.0, 1.0};

int option = 2;

char animate = false;

clock_t app_clock0;

float clocks_per_sec, app_time, app_time0;

int opti = 0;

void ReshapeFunc(int width, int height) {
    float lr;

    glViewport(0, 0, width, height);      /* klatka jest calym oknem */
    lr = 0.5533f * (float) width / (float) height;  /* przyjmujemy aspekt rowny 1 */
    Matrix m = M4x4ToStandardCubeFrustum(-lr, lr, -0.5533f, 0.5533, 5.0, 15.0);
    glBindBuffer(GL_UNIFORM_BUFFER, trbuf);
    glBufferSubData(GL_UNIFORM_BUFFER, trbofs[2], 16 * sizeof(GLfloat), m);
    win_width = width, win_height = height;
    left = -(right = lr);
    bottom = -(top = 0.5533);
    near = 5.0;
    far = 15.0;
    opti = 4;
    ExitIfGLError("ReshapeFunc");
} /*ReshapeFunc*/

void DisplayFunc() {
    glClearColor(1.0, 1.0, 1.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    /*
    glEnable ( GL_CULL_FACE );
    glCullFace ( GL_FRONT );
    glFrontFace ( GL_CW );
    */
    DrawIcosahedron(option);
    glUseProgram(0);
    glFlush();
    glutSwapBuffers();
    if (opti > 0) {
        opti--;
        glutPostWindowRedisplay(WindowHandle);
    }
} /*DisplayFunc*/

void ToggleAnimation() {
    struct tms clk{};

    if ((animate = !animate)) {
        app_time0 = app_time = (float) (times(&clk) - app_clock0) / clocks_per_sec;
        model_rot_angle = model_rot_angle0;
        glutIdleFunc(IdleFunc);
    } else {
        glutIdleFunc(nullptr);
        model_rot_angle0 = model_rot_angle;
    }
} /*ToggleAnimation*/

void KeyboardFunc(unsigned char key, int x, int y) {
    switch (key) {
        case 0x1B:            /* klawisz Esc - zatrzymanie programu */
            Cleanup();
            glutLeaveMainLoop();
            break;
        case 'W':
        case 'w':  /* przelaczamy na wierzcholki */
            option = 0;
            glutPostWindowRedisplay(WindowHandle);
            break;
        case 'K':
        case 'k':  /* przelaczamy na krawedzie */
            option = 1;
            glutPostWindowRedisplay(WindowHandle);
            break;
        case 'S':
        case 's':  /* przelaczamy na sciany */
            option = 2;
            glutPostWindowRedisplay(WindowHandle);
            break;
        case ' ':             /* wlaczamy albo wylaczamy animacje */
            ToggleAnimation();
            break;
        default:              /* ignorujemy wszystkie inne klawisze */
            break;
    }
} /*KeyboardFunc*/

void MouseFunc(int button, int state, int x, int y) {
    switch (app_state) {
        case STATE_NOTHING:
            if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
                last_xi = x, last_eta = y;
                app_state = STATE_TURNING;
            }
            break;
        case STATE_TURNING:
            if (button == GLUT_LEFT_BUTTON && state != GLUT_DOWN) {
                app_state = STATE_NOTHING;
            }
            break;
        default:break;
    }
} /*MouseFunc*/

void MotionFunc(int x, int y) {
    switch (app_state) {
        case STATE_TURNING:
            if (x != last_xi || y != last_eta) {
                RotateViewer(x - last_xi, y - last_eta);
                last_xi = x, last_eta = y;
                glutPostWindowRedisplay(WindowHandle);
            }
            break;
        default:break;
    }
} /*MotionFunc*/

void JoystickFunc(unsigned int buttonmask, int x, int y, int z) {}

void IdleFunc() {
    struct tms clk{};

    app_time = (float) (times(&clk) - app_clock0) / clocks_per_sec;
    model_rot_angle = model_rot_angle0 + 0.78539816 * (app_time - app_time0);
    SetupModelMatrix(model_rot_axis, model_rot_angle);
    glutPostWindowRedisplay(WindowHandle);
} /*IdleFunc*/

void TimerFunc(int value) {}

void InitMyObject() {
    struct tms clk{};

    clocks_per_sec = (float) sysconf(_SC_CLK_TCK);
    app_clock0 = times(&clk);
    app_time0 = app_time = 0.0;
    SetupModelMatrix(model_rot_axis, model_rot_angle);
    InitViewMatrix();
    ConstructIcosahedronVAO();
} /*InitMyObject*/

void Initialise(int argc, char* argv[]) {
    glutInit(&argc, argv);
    glutInitContextVersion(2, 1);
    glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);
    glutInitContextProfile(GLUT_CORE_PROFILE);
    glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE,
                  GLUT_ACTION_GLUTMAINLOOP_RETURNS);
    glutInitWindowSize(480, 360);
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
    WindowHandle = glutCreateWindow("aplikacja 1A");
    if (WindowHandle < 1) {
        fprintf(stderr, "Error: Could not create a window\n");
        exit(1);
    }
    glutReshapeFunc(ReshapeFunc);
    glutDisplayFunc(DisplayFunc);
    glutKeyboardFunc(KeyboardFunc);
    glutMouseFunc(MouseFunc);
    /*glutJoystickFunc ( JoystickFunc, 16 );*/
    glutMotionFunc(MotionFunc);
    /*glutTimerFunc ( 0, TimerFunc, 0 );*/
    GetGLProcAddresses();
#ifdef USE_GL3W
    if ( !gl3wIsSupported ( 4, 2 ) )
      ExitOnError ( "Initialise: OpenGL version 4.2 not supported\n" );
#endif
    LoadMyShaders();
    InitMyObject();
} /*Initialise*/

int main(int argc, char* argv[]) {
    Initialise(argc, argv);
    glutMainLoop();
    exit(0);
} /*main*/
