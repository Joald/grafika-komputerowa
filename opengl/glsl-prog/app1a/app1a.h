
#define STATE_NOTHING 0
#define STATE_TURNING 1

extern GLuint shader_id[2]; 
extern GLuint program_id;
extern GLuint trans_block_index, trbuf;
extern GLint  trans_block_size, trbofs[3];
extern GLuint icosahedron_vao, icosahedron_vbo[3];
extern float model_rot_axis[3], model_rot_angle0, model_rot_angle;


extern int   WindowHandle;
extern int   win_width, win_height;
extern int   last_xi, last_eta;
extern float left, right, bottom, top, near, far;
extern int   app_state;
extern float viewer_rvec[3];
extern float viewer_rangle;
extern const float viewer_pos0[4];
extern int   option;
extern char  animate;
extern clock_t app_clock0;
extern float clocks_per_sec, app_time, app_time0;


void SetupModelMatrix ( float axis[3], float angle );
void InitViewMatrix ( void );
void RotateViewer ( int delta_xi, int delta_eta );
void ConstructIcosahedronVAO ( void );
void DrawIcosahedron ( int opt );

void LoadMyShaders ( void );
void Cleanup ( void );


void ReshapeFunc ( int width, int height );
void DisplayFunc ( void );
void KeyboardFunc ( unsigned char key, int x, int y );
void MouseFunc ( int button, int state, int x, int y );
void MotionFunc ( int x, int y );
void TimerFunc ( int value );
void IdleFunc ( void );

void InitMyObject ( void );
void Initialise ( int argc, char *argv[] );

