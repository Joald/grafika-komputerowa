#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <sys/times.h>
#include "myglheader.h"   /* najpierw ten */
#include <GL/freeglut.h>  /* potem ten */
#include <iostream>

#include "../utilities/utilities.h"
#include "app1a.h"

GLuint shader_id[2];

GLuint program_id;

GLuint trans_block_index, trbuf;

GLint trans_block_size, trbofs[3];

GLuint icosahedron_vao, icosahedron_vbo[3];

float model_rot_axis[3] = {0.0, 1.0, 0.0},
  model_rot_angle0 = 0.0, model_rot_angle;

void LoadMyShaders() {
    static const char* filename[] =
      {"app1.glsl.vert", "app1.glsl.frag"};
    static const GLchar* UTBNames[] =
      {"TransBlock", "TransBlock.mm", "TransBlock.vm", "TransBlock.pm"};
    GLuint ind[3];

    shader_id[0] = CompileShaderFiles(GL_VERTEX_SHADER, 1, &filename[0]);
    shader_id[1] = CompileShaderFiles(GL_FRAGMENT_SHADER, 1, &filename[1]);

    program_id = LinkShaderProgram(2, shader_id);
    trans_block_index = glGetUniformBlockIndex(program_id, UTBNames[0]);
    glGetActiveUniformBlockiv(program_id, trans_block_index,
                              GL_UNIFORM_BLOCK_DATA_SIZE, &trans_block_size);
    glGetUniformIndices(program_id, 3, &UTBNames[1], ind);
    glGetActiveUniformsiv(program_id, 3, ind, GL_UNIFORM_OFFSET, trbofs);
    glUniformBlockBinding(program_id, trans_block_index, 0);
    glGenBuffers(1, &trbuf);
    glBindBufferBase(GL_UNIFORM_BUFFER, 0, trbuf);
    glBufferData(GL_UNIFORM_BUFFER, trans_block_size, nullptr, GL_DYNAMIC_DRAW);
    ExitIfGLError("LoadMyShaders");
} /*LoadMyShaders*/

void SetupModelMatrix(float axis[3], float angle) {
    GLfloat m[16];

    M4x4CustomRotation(axis[2], angle);
    glBindBuffer(GL_UNIFORM_BUFFER, trbuf);
    glBufferSubData(GL_UNIFORM_BUFFER, trbofs[0], 16 * sizeof(GLfloat), m);
    ExitIfGLError("SetupModelMatrix");
} /*SetupModelMatrix*/

void InitViewMatrix() {
    GLfloat m[16];

    M4x4Translate(-viewer_pos0[2]);
    glBindBuffer(GL_UNIFORM_BUFFER, trbuf);
    glBufferSubData(GL_UNIFORM_BUFFER, trbofs[1], 16 * sizeof(GLfloat), m);
    ExitIfGLError("InitViewMatrix");
} /*InitViewMatrix*/

void RotateViewer(int delta_xi, int delta_eta) {
    float vi[3], lgt, angi, vk[3], angk;
    GLfloat tm[16], rm[16], vm[16];

    if (delta_xi == 0 && delta_eta == 0) {
        return;
    }  /* natychmiast uciekamy - nie chcemy dzielic przez 0 */
    vi[0] = (float) delta_eta * (right - left) / (float) win_height;
    vi[1] = (float) delta_xi * (top - bottom) / (float) win_width;
    vi[2] = 0.0;
    lgt = sqrt(V3DotProductf(vi, vi));
    vi[0] /= lgt;
    vi[1] /= lgt;
    angi = -0.052359878;  /* -3 stopnie */
    V3ComposeRotations(viewer_rvec, viewer_rangle, vi, angi);
    memcpy(viewer_rvec, vk, 3 * sizeof(float));
    viewer_rangle = angk;
    M4x4Translate(-viewer_pos0[2]);
    M4x4CustomRotation(viewer_rvec[2],
                       -viewer_rangle);
    M4x4Multiplication(vm, tm, rm);
    glBindBuffer(GL_UNIFORM_BUFFER, trbuf);
    glBufferSubData(GL_UNIFORM_BUFFER, trbofs[1], 16 * sizeof(GLfloat), vm);
    ExitIfGLError("RotateViewer");
} /*RotateViewer*/

void ConstructIcosahedronVAO() {
#define A 0.52573115
#define B 0.85065085
    static const GLfloat vertpos[12][3] =
      {{-A,  0.0, -B},
       {A,   0.0, -B},
       {0.0, -B,  -A},
       {-B,  -A,  0.0},
       {-B,  A,   0.0},
       {0.0, B,   -A},
       {A,   0.0, B},
       {-A,  0.0, B},
       {0.0, -B,  A},
       {B,   -A,  0.0},
       {B,   A,   0.0},
       {0.0, B,   A}};
    static const GLubyte vertcol[12][3] =
      {{255, 0,   0},
       {255, 127, 0},
       {255, 255, 0},
       {127, 255, 0},
       {0,   255, 0},
       {0,   255, 127},
       {0,   255, 255},
       {0,   127, 255},
       {0,   0,   255},
       {127, 0,   255},
       {255, 0,   255},
       {255, 0,   127}};
    static const GLubyte vertind[62] =
      {0, 1, 2, 0, 3, 4, 0, 5, 1, 9, 2, 8, 3, /* lamana, od 0 */
       7, 4, 11, 5, 10, 9, 6, 8, 7, 6, 11, 7,
       1, 10, 6,                              /* lamana, od 25 */
       2, 3, 4, 5, 8, 9, 10, 11,              /* 4 odcinki, od 28 */
       0, 1, 2, 3, 4, 5, 1,                   /* wachlarz, od 36 */
       6, 7, 8, 9, 10, 11, 7,                 /* wachlarz, od 43  */
       1, 9, 2, 8, 3, 7, 4, 11, 5, 10, 1, 9}; /* tasma, od 50 */

    glGenVertexArrays(1, &icosahedron_vao);
    glBindVertexArray(icosahedron_vao);
    glGenBuffers(3, icosahedron_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, icosahedron_vbo[0]);
    glBufferData(GL_ARRAY_BUFFER,
                 12 * 3 * sizeof(GLfloat), vertpos, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
                          3 * sizeof(GLfloat), (GLvoid*) nullptr);
    glBindBuffer(GL_ARRAY_BUFFER, icosahedron_vbo[1]);
    glBufferData(GL_ARRAY_BUFFER,
                 12 * 3 * sizeof(GLubyte), vertcol, GL_STATIC_DRAW);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_UNSIGNED_BYTE, GL_TRUE,
                          3 * sizeof(GLubyte), (GLvoid*) nullptr);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, icosahedron_vbo[2]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                 62 * sizeof(GLubyte), vertind, GL_STATIC_DRAW);
    ExitIfGLError("ConstructIcosahedronVAO");
} /*ConstructIcosahedronVAO*/

void DrawIcosahedron(int opt) {
    glUseProgram(program_id);
    glBindVertexArray(icosahedron_vao);
    switch (opt) {
        case 0:    /* wierzcholki */
            glPointSize(5.0);
            glDrawArrays(GL_POINTS, 0, 12);
            break;
        case 1:    /* krawedzie */
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, icosahedron_vbo[2]);
            glDrawElements(GL_LINE_STRIP, 25,
                           GL_UNSIGNED_BYTE, (GLvoid*) nullptr);
            glDrawElements(GL_LINE_STRIP, 3,
                           GL_UNSIGNED_BYTE, (GLvoid*) (25 * sizeof(GLubyte)));
            glDrawElements(GL_LINES, 8,
                           GL_UNSIGNED_BYTE, (GLvoid*) (28 * sizeof(GLubyte)));
            break;
        default:   /* sciany */
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, icosahedron_vbo[2]);
            glDrawElements(GL_TRIANGLE_FAN, 7,
                           GL_UNSIGNED_BYTE, (GLvoid*) (36 * sizeof(GLubyte)));
            glDrawElements(GL_TRIANGLE_FAN, 7,
                           GL_UNSIGNED_BYTE, (GLvoid*) (43 * sizeof(GLubyte)));
            glDrawElements(GL_TRIANGLE_STRIP, 12,
                           GL_UNSIGNED_BYTE, (GLvoid*) (50 * sizeof(GLubyte)));
            break;
    }
} /*DrawIcosahedron*/

void Cleanup() {
    int i;

    glUseProgram(0);
    for (i = 0; i < 2; i++)
        glDeleteShader(shader_id[i]);
    glDeleteProgram(program_id);
    glDeleteBuffers(1, &trbuf);
    glDeleteVertexArrays(1, &icosahedron_vao);
    glDeleteBuffers(3, icosahedron_vbo);
    ExitIfGLError("Cleanup");
    glutDestroyWindow(WindowHandle);
} /*Cleanup*/

