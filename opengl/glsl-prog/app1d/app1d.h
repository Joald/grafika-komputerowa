
#define STATE_NOTHING 0
#define STATE_TURNING 1

struct TransBl {
    GLfloat mm[16], vm[16], pm[16];
    GLfloat eye_position[4];
};

extern GLuint shader_id[17];

extern GLuint program_id[5];

extern GLuint trans_block_index, trbuf, trbbp;

extern GLint trans_block_size, trbofs[6];

extern GLuint icosahedron_vao, icosahedron_vbo[4];

extern float model_rot_axis[3], model_rot_angle0, model_rot_angle;

extern const float viewer_pos0[4];

extern TransBl trans;

extern myTextObject* vptext;

extern myFont* font;


extern int WindowHandle;

extern int win_width, win_height;

extern int last_xi, last_eta;

extern float left, right, bottom, top, near, far;

extern int app_state;

extern float viewer_rvec[3];

extern float viewer_rangle;

extern int option;

extern char animate, enlight, tesselate;

extern clock_t app_clock0;

extern float clocks_per_sec, app_time, app_time0;


void SetupMVPMatrix();
void SetupModelMatrix(float axis[3], float angle);
void InitViewMatrix();
void RotateViewer(int delta_xi, int delta_eta);
void ConstructIcosahedronVAO();
void DrawIcosahedron(int option, char enlight);
void DrawTesselatedIcosahedron(int option, char enlight);
void InitLights();
void NotifyViewerPos();

void LoadMyShaders();
void InitMyObject();
void Cleanup();


void ToggleAnimation();
void ToggleLight();
void ToggleTesselation();

void ReshapeFunc(int width, int height);
void DisplayFunc();
void KeyboardFunc(unsigned char key, int x, int y);
void MouseFunc(int button, int state, int x, int y);
void MotionFunc(int x, int y);
void TimerFunc(int value);
void IdleFunc();

void Initialise(int argc, char* argv[]);

