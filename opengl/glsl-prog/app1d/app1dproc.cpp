#include <cstdlib>
#include <unistd.h>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <sys/times.h>

#include "myglheader.h"   /* najpierw ten */
#include <GL/freeglut.h>  /* potem ten */

#include "../utilities/utilities.h"
#include "lights.h"
#include "mygltext.h"
#include "app1d.h"

GLuint shader_id[17];

GLuint program_id[5];

GLuint trans_block_index, trbuf, trbbp;

GLint trans_block_size, trbofs[6];

GLuint icosahedron_vao, icosahedron_vbo[4];

float model_rot_axis[3] = {0.0, 1.0, 0.0},
  model_rot_angle0 = 0.0, model_rot_angle;

const float viewer_pos0[4] = {0.0, 0.0, 10.0, 1.0};

TransBl trans;

myTextObject* vptext;

myFont* font;

void LoadMyShaders() {
    static const char* filename[12] =
      {"app1d0.glsl.vert", "app1d1.glsl.vert", "app1d2.glsl.vert",
       "app1d2.glsl.tesc", "app1d3.glsl.tesc",
       "app1d2.glsl.tese", "app1d3.glsl.tese", "app1d4.glsl.tese",
       "app1d1.glsl.geom",
       "app1d0.glsl.frag", "app1d1.glsl.frag", "app1d2.glsl.frag"};
    static const GLuint shtype[12] =
      {GL_VERTEX_SHADER, GL_VERTEX_SHADER, GL_VERTEX_SHADER,
       GL_TESS_CONTROL_SHADER, GL_TESS_CONTROL_SHADER,
       GL_TESS_EVALUATION_SHADER, GL_TESS_EVALUATION_SHADER,
       GL_TESS_EVALUATION_SHADER,
       GL_GEOMETRY_SHADER,
       GL_FRAGMENT_SHADER, GL_FRAGMENT_SHADER, GL_FRAGMENT_SHADER};
    static const GLchar* UTBNames[] =
      {"TransBlock", "TransBlock.mm", "TransBlock.mmti", "TransBlock.vm",
       "TransBlock.pm", "TransBlock.mvpm", "TransBlock.eye_position"};
    static const GLchar* ULSNames[] =
      {"LSBlock", "LSBlock.light_count", "LSBlock.mask",
       "LSBlock.lightSourceParameters[0].ambient", "LSBlock.lightSourceParameters[0].direct",
       "LSBlock.lightSourceParameters[0].position",
       "LSBlock.lightSourceParameters[0].attenuation", "LSBlock.lightSourceParameters[1].ambient"};
    GLuint sh[4];
    int i;

    for (i = 0; i < 12; i++) {
        shader_id[i] = CompileShaderFiles(shtype[i], 1, &filename[i]);
    }
    sh[0] = shader_id[0];
    sh[1] = shader_id[9];
    program_id[0] = LinkShaderProgram(2, sh);
    sh[0] = shader_id[1];
    sh[1] = shader_id[8];
    sh[2] = shader_id[10];
    program_id[1] = LinkShaderProgram(3, sh);
    sh[0] = shader_id[2];
    sh[1] = shader_id[3];
    sh[2] = shader_id[5];
    sh[3] = shader_id[11];
    program_id[2] = LinkShaderProgram(4, sh);
    sh[0] = shader_id[2];
    sh[1] = shader_id[4];
    sh[2] = shader_id[6];
    sh[3] = shader_id[11];
    program_id[3] = LinkShaderProgram(4, sh);
    sh[0] = shader_id[2];
    sh[1] = shader_id[4];
    sh[2] = shader_id[7];
    sh[3] = shader_id[10];
    program_id[4] = LinkShaderProgram(4, sh);
    GetAccessToUniformBlock(program_id[1], 6, &UTBNames[0],
                            &trans_block_index, &trans_block_size, trbofs, &trbbp);
    GetAccessToUniformBlock(program_id[1], 7, &ULSNames[0],
                            &light_source_block_index, &light_source_block_size, light_source_block_offsets,
                            &light_source_buffer_binding_point);
    AttachUniformBlockToBindingPoint(program_id[0], UTBNames[0], trbbp);
    AttachUniformBlockToBindingPoint(program_id[2], UTBNames[0], trbbp);
    AttachUniformBlockToBindingPoint(program_id[3], UTBNames[0], trbbp);
    AttachUniformBlockToBindingPoint(program_id[4], UTBNames[0], trbbp);
    AttachUniformBlockToBindingPoint(program_id[4], ULSNames[0], light_source_buffer_binding_point);
    trbuf = NewUniformBlockObject(trans_block_size, trbbp);
    light_source_UBO_id = NewUniformBlockObject(light_source_block_size, light_source_buffer_binding_point);
    ExitIfGLError("LoadMyShaders");
} /*LoadMyShaders*/

void SetupMVPMatrix() {
    GLfloat m[16], mvp[16];

    M4x4Multiplication(m, trans.vm, trans.mm);
    M4x4Multiplication(mvp, trans.pm, m);
    glBindBuffer(GL_UNIFORM_BUFFER, trbuf);
    glBufferSubData(GL_UNIFORM_BUFFER, trbofs[4], 16 * sizeof(GLfloat), mvp);
    ExitIfGLError("SetupNVPMatrix");
} /*SetupMVPMatrix*/

void SetupModelMatrix(float axis[3], float angle) {
    M4x4CustomRotation(axis[2], angle);
    glBindBuffer(GL_UNIFORM_BUFFER, trbuf);
    glBufferSubData(GL_UNIFORM_BUFFER, trbofs[0], 16 * sizeof(GLfloat), trans.mm);
    glBufferSubData(GL_UNIFORM_BUFFER, trbofs[1], 16 * sizeof(GLfloat), trans.mm);
    ExitIfGLError("SetupModelMatrix");
    SetupMVPMatrix();
} /*SetupModelMatrix*/

void InitViewMatrix() {
    memcpy(trans.eye_position, viewer_pos0, 4 * sizeof(GLfloat));
    M4x4Translate(-viewer_pos0[2]);
    glBindBuffer(GL_UNIFORM_BUFFER, trbuf);
    glBufferSubData(GL_UNIFORM_BUFFER, trbofs[2], 16 * sizeof(GLfloat), trans.vm);
    /* !!!! */
    glBufferSubData(GL_UNIFORM_BUFFER, trbofs[5], 4 * sizeof(GLfloat), trans.eye_position);
    ExitIfGLError("InitViewMatrix");
    SetupMVPMatrix();
} /*InitViewMatrix*/

void RotateViewer(int delta_xi, int delta_eta) {
    float vi[3], lgt, angi, vk[3], angk;
    GLfloat tm[16], rm[16];

    if (delta_xi == 0 && delta_eta == 0) {
        return;
    }  /* natychmiast uciekamy - nie chcemy dzielic przez 0 */
    vi[0] = (float) delta_eta * (right - left) / (float) win_height;
    vi[1] = (float) delta_xi * (top - bottom) / (float) win_width;
    vi[2] = 0.0;
    lgt = sqrt(V3DotProductf(vi, vi));
    vi[0] /= lgt;
    vi[1] /= lgt;
    angi = -0.052359878;  /* -3 stopnie */
    V3ComposeRotations(viewer_rvec, viewer_rangle, vi, angi);
    memcpy(viewer_rvec, vk, 3 * sizeof(float));
    viewer_rangle = angk;
    M4x4Translate(-viewer_pos0[2]);
    M4x4CustomRotation(viewer_rvec[2],
                       -viewer_rangle);
    M4x4Multiplication(trans.vm, tm, rm);
    glBindBuffer(GL_UNIFORM_BUFFER, trbuf);
    glBufferSubData(GL_UNIFORM_BUFFER, trbofs[2], 16 * sizeof(GLfloat), trans.vm);
    M4x4Transposef(tm, rm);
    M4x4MultMVf(trans.eye_position, tm, viewer_pos0);
    glBufferSubData(GL_UNIFORM_BUFFER, trbofs[5], 4 * sizeof(GLfloat), trans.eye_position);
    ExitIfGLError("RotateViewer");
    SetupMVPMatrix();
} /*RotateViewer*/

void ConstructIcosahedronVAO() {
#define A 0.52573115
#define B 0.85065085
    static const GLfloat vertpos[12][3] =
      {{-A,  0.0, -B},
       {A,   0.0, -B},
       {0.0, -B,  -A},
       {-B,  -A,  0.0},
       {-B,  A,   0.0},
       {0.0, B,   -A},
       {A,   0.0, B},
       {-A,  0.0, B},
       {0.0, -B,  A},
       {B,   -A,  0.0},
       {B,   A,   0.0},
       {0.0, B,   A}};
    static const GLubyte vertcol[12][3] =
      {{255, 0,   0},
       {255, 127, 0},
       {255, 255, 0},
       {127, 255, 0},
       {0,   255, 0},
       {0,   255, 127},
       {0,   255, 255},
       {0,   127, 255},
       {0,   0,   255},
       {127, 0,   255},
       {255, 0,   255},
       {255, 0,   127}};
    static const GLubyte vertind[96] =
      {0, 1, 2, 0, 3, 4, 0, 5, 1, 9, 2, 8, 3, /* lamana, od 0 */
       7, 4, 11, 5, 10, 9, 6, 8, 7, 6, 11, 7,
       1, 10, 6,                              /* lamana, od 25 */
       2, 3, 4, 5, 8, 9, 10, 11,              /* 4 odcinki, od 28 */
       0, 1, 2, 0, 2, 3, 0, 3, 4, 0, 4, 5,    /* trojkatne platy, od 36 */
       0, 5, 1, 6, 7, 8, 6, 8, 9, 6, 9, 10,
       6, 10, 11, 6, 11, 7, 1, 9, 2, 9, 8, 2,
       2, 8, 3, 8, 7, 3, 3, 7, 4, 7, 11, 4,
       4, 11, 5, 11, 10, 5, 10, 1, 5, 10, 9, 1};

    glGenVertexArrays(1, &icosahedron_vao);
    glBindVertexArray(icosahedron_vao);
    glGenBuffers(3, icosahedron_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, icosahedron_vbo[0]);
    glBufferData(GL_ARRAY_BUFFER,
                 12 * 3 * sizeof(GLfloat), vertpos, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
                          3 * sizeof(GLfloat), (GLvoid*) nullptr);
    glBindBuffer(GL_ARRAY_BUFFER, icosahedron_vbo[1]);
    glBufferData(GL_ARRAY_BUFFER,
                 12 * 3 * sizeof(GLubyte), vertcol, GL_STATIC_DRAW);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_UNSIGNED_BYTE, GL_TRUE,
                          3 * sizeof(GLubyte), (GLvoid*) nullptr);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, icosahedron_vbo[2]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                 96 * sizeof(GLubyte), vertind, GL_STATIC_DRAW);
    ExitIfGLError("ConstructIcosahedronVAO");
} /*ConstructIcosahedronVAO*/

void DrawIcosahedron(int option, char enlight) {
    glBindVertexArray(icosahedron_vao);
    switch (option) {
        case 0:    /* wierzcholki */
            glUseProgram(program_id[0]);
            glPointSize(5.0);
            glDrawArrays(GL_POINTS, 0, 12);
            break;
        case 1:    /* krawedzie */
            glUseProgram(program_id[0]);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, icosahedron_vbo[2]);
            glDrawElements(GL_LINE_STRIP, 25,
                           GL_UNSIGNED_BYTE, (GLvoid*) nullptr);
            glDrawElements(GL_LINE_STRIP, 3,
                           GL_UNSIGNED_BYTE, (GLvoid*) (25 * sizeof(GLubyte)));
            glDrawElements(GL_LINES, 8,
                           GL_UNSIGNED_BYTE, (GLvoid*) (28 * sizeof(GLubyte)));
            break;
        default:   /* sciany */
            glUseProgram(program_id[enlight ? 1 : 0]);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, icosahedron_vbo[2]);
            glDrawElements(GL_TRIANGLES, 60,
                           GL_UNSIGNED_BYTE, (GLvoid*) (36 * sizeof(GLubyte)));
            break;
    }
} /*DrawIcosahedron*/

void DrawTesselatedIcosahedron(int option, char enlight) {
    glBindVertexArray(icosahedron_vao);
    switch (option) {
        case 0:break;
        case 1:glUseProgram(program_id[2]);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, icosahedron_vbo[2]);
            glPatchParameteri(GL_PATCH_VERTICES, 2);
            glDrawElements(GL_PATCHES, 24, GL_UNSIGNED_BYTE, (GLvoid*) nullptr);
            glDrawElements(GL_PATCHES, 26, GL_UNSIGNED_BYTE, (GLvoid*) (1 * sizeof(GLubyte)));
            glDrawElements(GL_PATCHES, 10, GL_UNSIGNED_BYTE, (GLvoid*) (26 * sizeof(GLubyte)));
            break;
        default:glUseProgram(program_id[enlight ? 4 : 3]);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, icosahedron_vbo[2]);
            glPatchParameteri(GL_PATCH_VERTICES, 3);
            glDrawElements(GL_PATCHES, 60, GL_UNSIGNED_BYTE, (GLvoid*) (36 * sizeof(GLubyte)));
    }
    ExitIfGLError("DrawTesselatedIcosahedron");
} /*DrawTesselatedIcosahedron*/

void InitLights() {
    GLfloat amb0[4] = {0.2, 0.2, 0.3, 1.0};
    GLfloat dif0[4] = {0.8, 0.8, 0.8, 1.0};
    GLfloat pos0[4] = {0.0, 1.0, 1.0, 0.0};
    GLfloat atn0[3] = {1.0, 0.0, 0.0};

    SetLightAmbient(0, amb0);
    SetLightDiffuse(0, dif0);
    SetLightPosition(0, pos0);
    SetLightAttenuation(0, atn0);
    SetLightOnOff(0, true);
} /*InitLights*/

void NotifyViewerPos() {
    GLchar s[60];

    sprintf(s, "x = %5.2f, y = %5.2f, z = %5.2f",
            trans.eye_position[0], trans.eye_position[1], trans.eye_position[2]);
    SetTextObjectContents(vptext, s, 0, 17, font);
} /*NotifyViewerPos*/

void InitMyObject() {
    struct tms clk{};

    clocks_per_sec = (float) sysconf(_SC_CLK_TCK);
    app_clock0 = times(&clk);
    app_time0 = app_time = 0.0;
    font = NewFont18x10();
    /*  font = NewFont12x6 ();*/
    vptext = NewTextObject(60);
    memset(&trans, 0, sizeof(TransBl));
    memset(&light, 0, sizeof(LightBlock));
    SetupModelMatrix(model_rot_axis, model_rot_angle);
    InitViewMatrix();
    NotifyViewerPos();
    ConstructIcosahedronVAO();
    InitLights();
} /*InitMyObject*/

void Cleanup() {
    int i;

    glUseProgram(0);
    for (i = 0; i < 5; i++)
        glDeleteProgram(program_id[i]);
    for (i = 0; i < 12; i++)
        glDeleteShader(shader_id[i]);
    glDeleteBuffers(1, &trbuf);
    glDeleteBuffers(1, &light_source_UBO_id);
    glDeleteVertexArrays(1, &icosahedron_vao);
    glDeleteBuffers(3, icosahedron_vbo);
    DeleteTextObject(vptext);
    DeleteFontObject(font);
    ExitIfGLError("Cleanup");
    glutDestroyWindow(WindowHandle);
} /*Cleanup*/

