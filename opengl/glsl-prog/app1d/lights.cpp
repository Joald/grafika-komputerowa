#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <cmath>
#include "myglheader.h"

#include "../utilities/utilities.h"
#include "lights.h"

GLuint light_source_block_index, light_source_UBO_id;

GLint light_source_block_size, light_source_block_offsets[7];

LightBlock light;

void SetLightAmbient(int l, GLfloat ambient[4]) {
    GLint ofs;

    if (l < 0 || l >= MAX_LIGHT_COUNT) {
        return;
    }
    std::memcpy(light.lightSourceParameters[l].ambient, ambient, 4 * sizeof(GLfloat));
    ofs = l * (light_source_block_offsets[6] - light_source_block_offsets[2]) + light_source_block_offsets[2];
    glBindBuffer(GL_UNIFORM_BUFFER, light_source_UBO_id);
    glBufferSubData(GL_UNIFORM_BUFFER, ofs, 4 * sizeof(GLfloat), ambient);
    ExitIfGLError("SetLightAmbient");
} /*SetLightAmbient*/

void SetLightDiffuse(int l, Vector4GLf& dif) {
    if (l < 0 || l >= MAX_LIGHT_COUNT) {
        return;
    }
    std::memcpy(light.lightSourceParameters[l].diffuse, dif, 4 * sizeof(GLfloat));
    GLint offset = l * (light_source_block_offsets[6] - light_source_block_offsets[2]) + light_source_block_offsets[3];
    glBindBuffer(GL_UNIFORM_BUFFER, light_source_UBO_id);
    glBufferSubData(GL_UNIFORM_BUFFER, offset, 4 * sizeof(GLfloat), dif);
    ExitIfGLError("SetLightDiffuse");
} /*SetLightDiffuse*/

void SetLightPosition(int l, Vector4GLf& pos) {
    if (l < 0 || l >= MAX_LIGHT_COUNT) {
        return;
    }
    memcpy(light.lightSourceParameters[l].position, pos, 4 * sizeof(GLfloat));
    GLint offset = l * (light_source_block_offsets[6] - light_source_block_offsets[2]) + light_source_block_offsets[4];
    glBindBuffer(GL_UNIFORM_BUFFER, light_source_UBO_id);
    glBufferSubData(GL_UNIFORM_BUFFER, offset, 4 * sizeof(GLfloat), pos);
    ExitIfGLError("SetLightPosition");
} /*SetLightPosition*/

void SetLightAttenuation(int l, Vector3GLf& at3) {
    if (l < 0 || l >= MAX_LIGHT_COUNT) {
        return;
    }
    memcpy(light.lightSourceParameters[l].attenuation, at3, 3 * sizeof(GLfloat));
    GLint ofs = l * (light_source_block_offsets[6] - light_source_block_offsets[2]) + light_source_block_offsets[5];
    glBindBuffer(GL_UNIFORM_BUFFER, light_source_UBO_id);
    glBufferSubData(GL_UNIFORM_BUFFER, ofs, 3 * sizeof(GLfloat), at3);
    ExitIfGLError("SetLightAttenuation");
} /*SetLightAttenuation*/

void SetLightOnOff(int l, bool on) {
    if (l < 0 || l >= MAX_LIGHT_COUNT) {
        return;
    }
    GLuint mask = 0x01u << l;
    if (on) {
        light.mask |= mask;
        if (l >= light.light_count) {
            light.light_count = l + 1u;
        }
    } else {
        light.mask &= ~mask;
        for (mask = 0x01u << (light.light_count - 1); mask; mask >>= 1) {
            if (light.mask & mask) {
                break;
            } else {
                light.light_count--;
            }
        }
    }
    glBindBuffer(GL_UNIFORM_BUFFER, light_source_UBO_id);
    glBufferSubData(GL_UNIFORM_BUFFER, light_source_block_offsets[0], sizeof(GLuint), &light.light_count);
    glBufferSubData(GL_UNIFORM_BUFFER, light_source_block_offsets[1], sizeof(GLuint), &light.mask);
    ExitIfGLError("SetLightOnOff");
} /*SetLightOnOff*/

