#ifndef LIGHTS_H
#define LIGHTS_H

#include "myglheader.h"

const int MAX_LIGHT_COUNT = 8;

struct LightSourceParameters {
    GLfloat ambient[4];
    GLfloat diffuse[4];
    GLfloat position[4];
    GLfloat attenuation[3];
};

struct LightBlock {
    GLuint light_count, mask;
    LightSourceParameters lightSourceParameters[MAX_LIGHT_COUNT];
};

extern GLuint light_source_block_index, light_source_UBO_id, light_source_buffer_binding_point;

extern GLint light_source_block_size, light_source_block_offsets[7];

extern LightBlock light;

void SetLightAmbient(int l, GLfloat ambient[4]);
void SetLightDiffuse(int l, Vector4GLf& dif);
void SetLightPosition(int l, Vector4GLf& pos);
void SetLightAttenuation(int l, Vector3GLf& at3);
void SetLightOnOff(int l, bool on);

#endif // LIGHTS_H