#version 420 core

layout(location=0) in vec4 in_Position;

out VertInstance {
    int instance;
  } Out;

void main ( void )
{
  Out.instance = gl_InstanceID;
  gl_Position = in_Position;
} /*main*/
