#version 420 core

layout (vertices=4) out;

in VertInstance {
    int instance;
  } In[];

out TCInstance {
    int instance;
  } Out[];

uniform int BezTessLevel;

void main ( void )
{
  if ( gl_InvocationID == 0 ) {
    gl_TessLevelOuter[0] = gl_TessLevelOuter[1] =
    gl_TessLevelOuter[2] = gl_TessLevelOuter[3] = BezTessLevel;
    gl_TessLevelInner[0] = gl_TessLevelInner[1] = BezTessLevel;
    Out[gl_InvocationID].instance = In[gl_InvocationID].instance;
  }
  gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;
} /*main*/
