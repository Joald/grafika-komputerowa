#version 420 core

#define MAX_NLIGHTS 8

layout(early_fragment_tests) in;

in NVertex {
    vec4 Colour;
    vec3 Position;
    vec3 Normal;
  } In;

out vec4 out_Colour;

uniform TransBlock {
    mat4 mm, mmti, vm, pm, mvpm;
    vec4 eyepos;
  } trb;

struct LSPar {
    vec4 ambient;
    vec4 direct;
    vec4 position;
    vec3 attenuation;
  };

uniform LSBlock {
    uint  nls;              /* liczba zrodel swiatla */
    uint  mask;             /* maska wlaczonych zrodel */
    LSPar ls[MAX_NLIGHTS];  /* poszczegolne zrodla swiatla */
  } light;

struct Material {
    vec4  ambref;     /* odbijanie swiatla rozproszonego */
    vec4  dirref;     /* odbijanie swiatla kierunkowego */
    vec4  specref;    /* odbijanie zwierciadlane swiatla */
    float shininess, wa, we;  /* polysk */
  };

uniform MatBlock {
    Material mat;
  } mat;

uniform int ColourSource;

subroutine void LightingProc ( void );

subroutine uniform LightingProc Lighting;

Material mm;

void GetColours ( void )
{
  switch ( ColourSource ) {
default:
    mm.ambref = mm.dirref = In.Colour;
    mm.specref = vec4 ( 0.25 );
    mm.shininess = 20.0;  mm.wa = mm.we = 1.0;
    break;
case 1:
    mm = mat.mat;
    break;
  }
} /*GetColours*/

vec3 posDifference ( vec4 p, vec3 pos, out float dist )
{
  vec3 v;

  if ( p.w != 0.0 ) {
    v = p.xyz/p.w-pos.xyz;
    dist = sqrt ( dot ( v, v ) );
  }
  else
    v = p.xyz;
  return normalize ( v );
} /*posDifference*/

float attFactor ( vec3 att, float dist )
{
  return 1.0/(((att.z*dist)+att.y)*dist+att.x);
} /*attFactor*/

subroutine (LightingProc) void LambertLighting ( void )
{
  vec3  normal, lv, vv;
  float d, e, dist;
  uint  i, mask;

  normal = normalize ( In.Normal );
  vv = posDifference ( trb.eyepos, In.Position, dist );
  e = dot ( vv, normal );

  out_Colour = vec4(0.0);
  for ( i = 0, mask = 0x00000001;  i < light.nls;  i++, mask <<= 1 )
    if ( (light.mask & mask) != 0 ) {
      out_Colour += light.ls[i].ambient * mm.ambref;
      lv = posDifference ( light.ls[i].position, In.Position, dist );
      d = dot ( lv, normal );
      if ( e > 0.0 ) {
        if ( d > 0.0 ) {
          if ( light.ls[i].position.w != 0.0 )
            d *= attFactor ( light.ls[i].attenuation, dist );
          out_Colour += (d * light.ls[i].direct) * mm.dirref;
        }
      }
      else {
        if ( d < 0.0 ) {
          if ( light.ls[i].position.w != 0.0 )
            d *= attFactor ( light.ls[i].attenuation, dist );
          out_Colour -= (d * light.ls[i].direct) * mm.dirref;
        }
      }
    }
  out_Colour = vec4 ( clamp ( out_Colour.rgb, 0.0, 1.0 ), 1.0 );
} /*LambertLighting*/

float wFactor ( float lvn, float wa, float we )
{
  return 1.0+(wa-1.0)*pow ( 1.0-lvn, we );
} /*wFactor*/

subroutine (LightingProc) void BlinnPhongLighting ( void )
{
  vec3  normal, lv, vv, hv;
  float a, d, e, f, dist;
  uint  i, mask;

  normal = normalize ( In.Normal );
  vv = posDifference ( trb.eyepos, In.Position, dist );
  e = dot ( vv, normal );

  out_Colour = vec4(0.0);
  for ( i = 0, mask = 0x00000001;  i < light.nls;  i++, mask <<= 1 )
    if ( (light.mask & mask) != 0 ) {
      out_Colour += light.ls[i].ambient * mm.ambref;
      lv = posDifference ( light.ls[i].position, In.Position, dist );
      d = dot ( lv, normal );
      if ( e > 0.0 ) {
        if ( d > 0.0 ) {
          if ( light.ls[i].position.w != 0.0 )
            a = attFactor ( light.ls[i].attenuation, dist );
          else
            a = 1.0;
          out_Colour += (a * d * light.ls[i].direct) * mm.dirref;
          hv = normalize ( lv+vv );
          f = pow ( dot ( hv, normal ), mm.shininess );
          out_Colour += (a*f*wFactor(d,mm.wa,mm.we))*mm.specref;
        }
      }
      else {
        if ( d < 0.0 ) {
          if ( light.ls[i].position.w != 0.0 )
            a = attFactor ( light.ls[i].attenuation, dist );
          else
            a = 1.0;
          out_Colour -= (a * d * light.ls[i].direct) * mm.dirref;
          hv = normalize ( lv+vv );
          f = pow ( -dot ( hv, normal ), mm.shininess );
          out_Colour += (a*f*wFactor(-d,mm.wa,mm.we))*mm.specref;
        }
      }
    }
  out_Colour = vec4 ( clamp ( out_Colour.rgb, 0.0, 1.0 ), 1.0 );
} /*BlinnPhongLighting*/

void main ( void )
{
  GetColours ();
  Lighting ();
} /*main*/
