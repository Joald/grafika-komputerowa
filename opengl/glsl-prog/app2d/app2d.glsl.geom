#version 450

layout(triangles) in;
layout(triangle_strip,max_vertices=3) out;

in GVertex {
  vec4 Colour;
  vec3 Position;
  vec3 Normal;
  vec2 TexCoord;
} In[];

out NVertex {
  vec4 Colour;
  vec3 Position;
  vec3 Normal;
  vec2 TexCoord;
} Out;

uniform bool BezNormals;

void main ( void )
{
  int i;
  vec3 v1, v2, nv;

  v1 = In[1].Position - In[0].Position;
  v2 = In[2].Position - In[0].Position;
  nv = normalize ( cross ( v1, v2 ) );
  for ( i = 0; i < 3; i++ ) {
    gl_Position = gl_in[i].gl_Position;
    Out.Position = In[i].Position;
    if ( !BezNormals || dot ( nv, nv ) < 1.0e-10 )
      Out.Normal = nv;
    else
      Out.Normal = In[i].Normal;
    Out.Colour = In[i].Colour;
    Out.TexCoord = In[i].TexCoord;
    EmitVertex ();
  }
  EndPrimitive ();
} /*main*/
