#version 450 core

#define MAX_DEG      10

layout(quads,equal_spacing,cw) in;

in TCInstance {
    int instance;
  } In[];

out GVertex {
    vec4 Colour;
    vec3 Position;
    vec3 Normal;
    vec2 TexCoord;
  } Out;

uniform bool BezNormals;
uniform int  ColourSource;

uniform CPoints {
    float cp[1];
  } cp;

uniform CPIndices {
    int cpi[1];
  } cpi;

uniform BezPatchTexCoord {
    vec4 txc[1];
  } txc;

uniform BezPatch {
    int  dim, udeg, vdeg;
    int  stride_u, stride_v, stride_p, stride_q, nq;
    bool use_ind;
    vec4 Colour;
  } bezp;

uniform TransBlock {
    mat4 mm, mmti, vm, pm, mvpm;
    vec4 eyepos;
  } trb;

int inst;

void BCHorner2f ( int n, vec2 bcp[MAX_DEG+1], float t, out vec2 p )
{
  int   i, b;
  float s, d;
  vec2  q0, q1;

  s = 1.0-t;  d = t;  b = n;
  q0 = bcp[0];
  for ( i = 1; i <= n; i++ ) {
    q0 = s*q0 + (b*d)*bcp[i];
    d *= t;  b = (b*(n-i))/(i+1);
  }
  p = q0;
} /*BCHorner2f*/

void BPHorner2f ( float u, float v, out vec4 pos, out vec4 nv )
{
  vec2 p[MAX_DEG+1], q[MAX_DEG+1], r;
  int  i, j, k, l, i0;
  vec4 Pos, Normal;

  if ( bezp.use_ind )
    i0 = In[0].instance*bezp.stride_p;
  else {
    i = In[0].instance / bezp.nq;
    j = In[0].instance % bezp.nq;
    i0 = i*bezp.stride_p + j*bezp.stride_q;
  }
  for ( i = k = 0;  i <= bezp.udeg;  i++ ) {
    if ( bezp.use_ind ) {
      for ( j = 0;  j <= bezp.vdeg;  j++, k++ ) {
        l = 2*cpi.cpi[i0+k];
        p[j] = vec2 ( cp.cp[l], cp.cp[l+1] );
      }
    }
    else {
      for ( j = 0, l = i0+i*bezp.stride_u;  j <= bezp.vdeg;  j++, l += bezp.stride_v )
        p[j] = vec2 ( cp.cp[l], cp.cp[l+1] );
    }
    BCHorner2f ( bezp.vdeg, p, v, q[i] );
  }
  BCHorner2f ( bezp.udeg, q, u, r );
  pos = vec4 ( r.xy, 0.0, 1.0 );
  nv = vec4 ( 0.0, 0.0, 1.0, 0.0 );
} /*BPHorner2f*/

void BCHorner3f ( int n, vec3 bcp[MAX_DEG+1], float t,
                  out vec3 p, out vec3 dp )
{
  int   i, b;
  float s, d, bd;
  vec3  q0, q1;

  n --;
  s = 1.0-t;  d = t;  b = n;
  q0 = bcp[0];  q1 = bcp[1];
  for ( i = 1; i <= n; i++ ) {
    bd = b*d;
    q0 = s*q0 + bd*bcp[i];
    q1 = s*q1 + bd*bcp[i+1];
    d *= t;  b = (b*(n-i))/(i+1);
  }
  p = s*q0 + t*q1;
  dp = q1 - q0;
} /*BCHorner3f*/

void BPHorner3f ( float u, float v, out vec4 pos, out vec4 nv )
{
  vec3 p[MAX_DEG+1], q0[MAX_DEG+1], q1[MAX_DEG+1], r, ru, rv, ruv;
  int  i, j, k, l, i0;

  if ( bezp.use_ind )
    i0 = In[0].instance*bezp.stride_p;
  else {
    i = In[0].instance / bezp.nq;
    j = In[0].instance % bezp.nq;
    i0 = i*bezp.stride_p + j*bezp.stride_q;
  }
  for ( i = k = 0;  i <= bezp.udeg;  i++ ) {
    if ( bezp.use_ind ) {
      for ( j = 0;  j <= bezp.vdeg;  j++, k++ ) {
        l = 3*cpi.cpi[i0+k];
        p[j] = vec3 ( cp.cp[l], cp.cp[l+1], cp.cp[l+2] );
      }
    }
    else {
      for ( j = 0, l = i0+i*bezp.stride_u;  j <= bezp.vdeg;  j++, l += bezp.stride_v )
        p[j] = vec3 ( cp.cp[l], cp.cp[l+1], cp.cp[l+2] );
      k += bezp.stride_u;
    }
    BCHorner3f ( bezp.vdeg, p, v, q0[i], q1[i] );
  }
  BCHorner3f ( bezp.udeg, q0, u, r, ru );
  BCHorner3f ( bezp.udeg, q1, u, rv, ruv );
  pos = vec4 ( r, 1.0 );
  nv = vec4 ( cross ( ru, rv ), 0.0 );
} /*BPHorner3f*/

vec4 cross ( vec4 v0, vec4 v1, vec4 v2 )
{
  float a01, a02, a03, a12, a13, a23;

  a01 = v0.x*v1.y - v0.y*v1.x;  a02 = v0.x*v1.z - v0.z*v1.x;
  a03 = v0.x*v1.w - v0.w*v1.x;  a12 = v0.y*v1.z - v0.z*v1.y;
  a13 = v0.y*v1.w - v0.w*v1.y;  a23 = v0.z*v1.w - v0.w*v1.z;
  return vec4 ( -a23*v2.y+a13*v2.z-a12*v2.w, a23*v2.x-a03*v2.z+a02*v2.w,
                -a13*v2.x+a03*v2.y-a01*v2.w, a12*v2.x-a02*v2.y+a01*v2.z );
} /*cross*/

void BCHorner4f ( int n, vec4 bcp[MAX_DEG+1], float t,
                  out vec4 p, out vec4 dp )
{
  int   i, b;
  float s, d, bd;
  vec4  q0, q1;

  n --;
  s = 1.0-t;  d = t;  b = n;
  q0 = bcp[0];  q1 = bcp[1];
  for ( i = 1; i <= n; i++ ) {
    bd = b*d;
    q0 = s*q0 + bd*bcp[i];
    q1 = s*q1 + bd*bcp[i+1];
    d *= t;  b = (b*(n-i))/(i+1);
  }
  p = s*q0 + t*q1;
  dp = q1 - q0;
} /*BCHorner4f*/

void BPHorner4f ( float u, float v, out vec4 pos, out vec4 nv )
{
  vec4 p[MAX_DEG+1], q0[MAX_DEG+1], q1[MAX_DEG+1], ru, rv;
  int  i, j, k, l, i0;

  if ( bezp.use_ind )
    i0 = In[0].instance*bezp.stride_p;
  else {
    i = In[0].instance / bezp.nq;
    j = In[0].instance % bezp.nq;
    i0 = i*bezp.stride_p + j*bezp.stride_q;
  }
  for ( i = k = 0;  i <= bezp.udeg;  i++ ) {
    if ( bezp.use_ind ) {
      for ( j = 0;  j <= bezp.vdeg;  j++, k++ ) {
        l = 4*cpi.cpi[i0+k];
        p[j] = vec4 ( cp.cp[l], cp.cp[l+1], cp.cp[l+2], cp.cp[l+3] );
      }
    }
    else {
      for ( j = 0, l = i0+i*bezp.stride_u;  j <= bezp.vdeg;  j++, l += bezp.stride_v )
        p[j] = vec4 ( cp.cp[l], cp.cp[l+1], cp.cp[l+2], cp.cp[l+3] );
    }
    BCHorner4f ( bezp.vdeg, p, v, q0[i], q1[i] );
  }
  BCHorner4f ( bezp.udeg, q0, u, pos, ru );
  BCHorner4f ( bezp.udeg, q1, u, rv, nv );
  nv = cross ( pos, ru, rv );
  pos /= pos.w;
} /*BPHorner4f*/

void main ( void )
{
  vec4 pos, nv;
  int  i;

  inst = In[0].instance;
  pos = nv = vec4 ( 0.0 );
  switch ( bezp.dim ) {
case 2: BPHorner2f ( gl_TessCoord.x, gl_TessCoord.y, pos, nv );  break;
case 3: BPHorner3f ( gl_TessCoord.x, gl_TessCoord.y, pos, nv );  break;
case 4: BPHorner4f ( gl_TessCoord.x, gl_TessCoord.y, pos, nv );  break;
  }
  gl_Position = trb.mvpm*pos;
  Out.Position = (trb.mm*pos).xyz;
  if ( !BezNormals || dot ( nv, nv ) < 1.0e-10 )
    Out.Normal = vec3 ( 0.0 );
  else {
    nv = trb.mmti*nv;
    Out.Normal = normalize ( nv.xyz );
  }
  Out.Colour = bezp.Colour;
  if ( ColourSource == 2 )
    Out.TexCoord = mix ( txc.txc[inst].xy, txc.txc[inst].zw, gl_TessCoord.yx );
} /*main*/
