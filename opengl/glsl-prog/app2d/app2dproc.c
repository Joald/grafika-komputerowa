#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <sys/times.h> 
#include <math.h>
#include "myglheader.h"
#include <GLFW/glfw3.h>

#include "../utilities/utilities.h"
#include "texture.h"
#include "lights.h"
#include "bezpatches.h"
#include "teapot.h"
#include "sproduct.h"
#include "app2d.h"

GLuint shader_id[7];
GLuint program_id[2];

GLuint trbi, trbuf, trbbp;       /* dostep do przeksztalcen */
GLint  trbsize, trbofs[6];
GLuint ubeznloc, ubeztloc;
GLint  LightProcLoc, LightProcInd,
       LambertProcInd, BlinnPhongProcInd;
GLint  ColourSourceLoc;

BezierPatchObjf *myteapot, *mytorus;
GLuint mytexture;

float   model_rot_axis[3] = {0.0,0.0,1.0},
        teapot_rot_angle0 = 0.0, teapot_rot_angle = 0.0,
        torus_rot_angle0 = 0.0, torus_rot_angle = 0.0;
GLfloat teapot_mmatrix[16], teapot_mmti[16], torus_mmatrix[16], torus_mmti[16];
TransBl trans;

clock_t zero_time, start_time;
float   clocks_per_sec;
char    cnet = false, skeleton = false;

void LoadMyShaders ( void )
{
  static const char *filename[] =
    { "app2.glsl.vert", "app2.glsl.tesc", "app2d.glsl.tese",
      "app2d.glsl.geom", "app2d.glsl.frag",
      "app2a1.glsl.vert", "app2a1.glsl.frag" };
  static const GLuint shtype[9] =
    { GL_VERTEX_SHADER, GL_TESS_CONTROL_SHADER, GL_TESS_EVALUATION_SHADER,
      GL_GEOMETRY_SHADER, GL_FRAGMENT_SHADER,
      GL_VERTEX_SHADER, GL_FRAGMENT_SHADER };
  static const GLchar *UTBNames[] =
    { "TransBlock", "TransBlock.mm", "TransBlock.mmti", "TransBlock.vm",
      "TransBlock.pm", "TransBlock.mvpm", "TransBlock.eyepos" };
  static const GLchar *ULSNames[] =
    { "LSBlock", "LSBlock.nls", "LSBlock.mask",
      "LSBlock.ls[0].ambient", "LSBlock.ls[0].direct", "LSBlock.ls[0].position",
      "LSBlock.ls[0].attenuation", "LSBlock.ls[1].ambient" };
  static const GLchar *UCPNames[] =
    { "CPoints", "CPoints.cp" };
  static const GLchar *UCPINames[] =
    { "CPIndices", "CPIndices.cpi" };
  static const GLchar *UBezPatchNames[] =
    { "BezPatch", "BezPatch.dim", "BezPatch.udeg", "BezPatch.vdeg",
      "BezPatch.stride_u", "BezPatch.stride_v",
      "BezPatch.stride_p", "BezPatch.stride_q", "BezPatch.nq",
      "BezPatch.use_ind", "BezPatch.Colour" };
  static const GLchar *UVarNames[] =
    { "BezNormals", "BezTessLevel", "ColourSource" };
  static const GLchar *UMatNames[] =
    { "MatBlock", "MatBlock.mat.ambref", "MatBlock.mat.dirref",
      "MatBlock.mat.specref", "MatBlock.mat.shininess",
      "MatBlock.mat.wa", "MatBlock.mat.we" };
  static const GLchar *UTexCoordNames[] =
    { "BezPatchTexCoord", "BezPatchTexCoord.txc" };
  static const GLchar LightProcName[] = "Lighting";
  static const GLchar LambertProcName[] = "LambertLighting";
  static const GLchar BlinnPhongProcName[] = "BlinnPhongLighting";

  GLint i;

  for ( i = 0; i < 7; i++ )
    shader_id[i] = CompileShaderFiles ( shtype[i], 1, &filename[i] );
  program_id[0] = LinkShaderProgram ( 5, shader_id );
  program_id[1] = LinkShaderProgram ( 2, &shader_id[5] );
  GetAccessToUniformBlock ( program_id[0], 6, &UTBNames[0],
                            &trbi, &trbsize, trbofs, &trbbp );
  GetAccessToUniformBlock ( program_id[0], 7, &ULSNames[0],
                            &lsbi, &lsbsize, lsbofs, &lsbbp );
  GetAccessToUniformBlock ( program_id[0], 1, &UCPNames[0],
                            &cpbi, &i, cpbofs, &cpbbp );
  GetAccessToUniformBlock ( program_id[0], 1, &UCPINames[0],
                            &cpibi, &i, cpibofs, &cpibbp );
  GetAccessToUniformBlock ( program_id[0], 10, &UBezPatchNames[0],
                            &bezpbi, &bezpbsize, bezpbofs, &bezpbbp );
  GetAccessToUniformBlock ( program_id[0], 1, &UTexCoordNames[0],
                            &txcbi, &i, txcofs, &txcbp );
  GetAccessToUniformBlock ( program_id[0], 6, &UMatNames[0],
                            &matbi, &matbsize, matbofs, &matbbp );
  ubeznloc = glGetUniformLocation ( program_id[0], UVarNames[0] );
  ubeztloc = glGetUniformLocation ( program_id[0], UVarNames[1] );
  ColourSourceLoc = glGetUniformLocation ( program_id[0], UVarNames[2] );
  LightProcLoc = glGetSubroutineUniformLocation ( program_id[0],
                        GL_FRAGMENT_SHADER, LightProcName );
  LambertProcInd = glGetSubroutineIndex ( program_id[0], GL_FRAGMENT_SHADER,
                                          LambertProcName );
  BlinnPhongProcInd = glGetSubroutineIndex ( program_id[0], GL_FRAGMENT_SHADER,
                                             BlinnPhongProcName );
  LightProcInd = LambertProcInd;
  trbuf = NewUniformBlockObject ( trbsize, trbbp );
  lsbuf = NewUniformBlockObject ( lsbsize, lsbbp );
  matbuf[1] = NewUniformBlockObject ( matbsize, matbbp );
  matbuf[0] = NewUniformBlockObject ( matbsize, matbbp );

  AttachUniformBlockToBP ( program_id[1], UTBNames[0], trbbp );
  AttachUniformBlockToBP ( program_id[1], UCPNames[0], cpbbp );
  AttachUniformBlockToBP ( program_id[1], UCPINames[0], cpibbp );
  AttachUniformBlockToBP ( program_id[1], UBezPatchNames[0], bezpbbp );
  ExitIfGLError ( "LoadMyShaders" );
} /*LoadMyShaders*/

void SetupMVPMatrix ( void )
{
  GLfloat m[16], mvp[16];

  M4x4Multf ( m, trans.vm, trans.mm );
  M4x4Multf ( mvp, trans.pm, m );
  glBindBuffer ( GL_UNIFORM_BUFFER, trbuf );
  glBufferSubData ( GL_UNIFORM_BUFFER, trbofs[4], 16*sizeof(GLfloat), mvp );
  ExitIfGLError ( "SetupNVPMatrix" );
} /*SetupMVPMatrix*/

void SetModelMatrix ( GLfloat mm[16], GLfloat mmti[16] )
{
  memcpy ( trans.mm, mm, 16*sizeof(GLfloat) );
  memcpy ( trans.mmti, mmti, 16*sizeof(GLfloat) );
  glBindBuffer ( GL_UNIFORM_BUFFER, trbuf );
  glBufferSubData ( GL_UNIFORM_BUFFER, trbofs[0], 16*sizeof(GLfloat), trans.mm );
  glBufferSubData ( GL_UNIFORM_BUFFER, trbofs[1], 16*sizeof(GLfloat), trans.mmti );
  ExitIfGLError ( "SetModelMatrix" );
  SetupMVPMatrix ();
} /*SetModelMatrix*/

void SetupTeapotMatrix ( void )
{
#define SCF (1.0/3.0)
  GLfloat ms[16], mr[16], mt[16], ma[16];

  M4x4Scalef ( ms, SCF, SCF, SCF*4.0/3.0 );
  M4x4Translatef ( mt, 0.0, 0.0, -0.6 );
  M4x4Multf ( ma, mt, ms );
  M4x4RotateVf ( mr, model_rot_axis[0], model_rot_axis[1], model_rot_axis[2],
                 animate ? teapot_rot_angle : teapot_rot_angle0 );
  M4x4Multf ( teapot_mmatrix, mr, ma );
  M4x4TInvertf ( teapot_mmti, teapot_mmatrix );
#undef SCF
} /*SetupTeapotMatrix*/

void SetupTorusMatrix ( void )
{
  GLfloat a[16], b[16], c[16], p[4] = {3.1,0.0,2.9,1.0}, q[4];

  M4x4Scalef ( a, 0.1, 0.1, 0.1 );
  M4x4RotateXf ( b, 0.5*PI );
  M4x4Multf ( c, b, a );
  M4x4RotateZf ( a, torus_rot_angle );
  M4x4Multf ( b, a, c );
  M4x4MultMVf ( q, teapot_mmatrix, p );
  M4x4Translatef ( a, q[0], q[1], q[2] );
  M4x4Multf ( torus_mmatrix, a, b );
  M4x4TInvertf ( torus_mmti, torus_mmatrix );
} /*SetupTorusMatrix*/

void InitViewMatrix ( void )
{
  memcpy ( trans.eyepos, viewer_pos0, 4*sizeof(GLfloat) );
  M4x4Translatef ( trans.vm, -viewer_pos0[0], -viewer_pos0[1], -viewer_pos0[2] );
  glBindBuffer ( GL_UNIFORM_BUFFER, trbuf );
  glBufferSubData ( GL_UNIFORM_BUFFER, trbofs[2], 16*sizeof(GLfloat), trans.vm );
  glBufferSubData ( GL_UNIFORM_BUFFER, trbofs[5], 4*sizeof(GLfloat), trans.eyepos );
  ExitIfGLError ( "InitViewMatrix" );
  SetupMVPMatrix ();
} /*InitViewMatrix*/

void RotateViewer ( double delta_xi, double delta_eta )
{
  float   vi[3], lgt, angi, vk[3], angk;
  GLfloat tm[16], rm[16];

  if ( delta_xi == 0 && delta_eta == 0 )
    return;  /* natychmiast uciekamy - nie chcemy dzielic przez 0 */
  vi[0] = (float)delta_eta*(right-left)/(float)win_height;
  vi[1] = (float)delta_xi*(top-bottom)/(float)win_width;
  vi[2] = 0.0;
  lgt = sqrt ( V3DotProductf ( vi, vi ) );
  vi[0] /= lgt;
  vi[1] /= lgt;
  angi = -0.052359878;  /* -3 stopnie */
  V3CompRotationsf ( vk, &angk, viewer_rvec, viewer_rangle, vi, angi );
  memcpy ( viewer_rvec, vk, 3*sizeof(float) );
  viewer_rangle = angk;
  M4x4Translatef ( tm, -viewer_pos0[0], -viewer_pos0[1], -viewer_pos0[2] );
  M4x4RotateVf ( rm, viewer_rvec[0], viewer_rvec[1], viewer_rvec[2],
                 -viewer_rangle );
  M4x4Multf ( trans.vm, tm, rm );
  glBindBuffer ( GL_UNIFORM_BUFFER, trbuf );
  glBufferSubData ( GL_UNIFORM_BUFFER, trbofs[2], 16*sizeof(GLfloat), trans.vm );
  M4x4Transposef ( tm, rm );
  M4x4MultMVf ( trans.eyepos, tm, viewer_pos0 );
  glBufferSubData ( GL_UNIFORM_BUFFER, trbofs[5], 4*sizeof(GLfloat), trans.eyepos );
  ExitIfGLError ( "RotateViewer" );
  SetupMVPMatrix ();
} /*RotateViewer*/

void InitMyObject ( void )
{
  struct tms clk;

  clocks_per_sec = (float)sysconf(_SC_CLK_TCK);
  app_clock0 = times ( &clk );
  teapot_time0 = app_time = 0.0;
  memset ( &trans, 0, sizeof(TransBl) );
  memset ( &light, 0, sizeof(LightBl) );
  SetupTeapotMatrix ();
  SetupTorusMatrix ();
  InitViewMatrix ();
  ConstructBezierPatchDomain ();
  ConstructMyTeapot ();
  ConstructMyTorus ();
  SetBezPatchOptions ( BezNormals, TessLevel );
  InitLights ();
  glUseProgram ( program_id[0] );
  glUniform1i ( ColourSourceLoc, 0 );
  LoadMyTextures ();
} /*InitMyObject*/

void LoadMyTextures ( void )
{
  mytexture = CreateMyTexture ( 1024 );
  LoadMyTextureImage ( mytexture, 1024, 1024,   0,   0, "jaszczur.tif" );
  LoadMyTextureImage ( mytexture, 1024, 1024, 512,   0, "salamandra.tif" );
  LoadMyTextureImage ( mytexture, 1024, 1024,   0, 512, "lis.tif" );
  LoadMyTextureImage ( mytexture, 1024, 1024, 512, 512, "kwiatki.tif" );
  SetupMyTextureMipmaps ( mytexture );
} /*LoadMyTextures*/

void InitLights ( void )
{
  GLfloat amb0[4] = { 0.2, 0.2, 0.3, 1.0 };
  GLfloat dif0[4] = { 0.8, 0.8, 0.8, 1.0 };
  GLfloat pos0[4] = { 0.0, 1.0, 1.0, 0.0 };
  GLfloat atn0[3] = { 1.0, 0.0, 0.0 };

  SetLightAmbient ( 0, amb0 );
  SetLightDiffuse ( 0, dif0 );
  SetLightPosition ( 0, pos0 );
  SetLightAttenuation ( 0, atn0 );
  SetLightOnOff ( 0, 1 );
} /*InitLights*/

void SetBezPatchOptions ( GLint normals, GLint tesslevel )
{
  glUseProgram ( program_id[0] );
  glUniform1i ( ubeznloc, normals );
  glUniform1i ( ubeztloc, tesslevel );
  ExitIfGLError ( "SetBezPatchOptions" );
} /*SetBezPatchOptions*/

void ConstructMyTeapot ( void )
{
  const GLfloat MyColour[4] = { 1.0, 1.0, 1.0, 1.0 };
  const GLfloat ambr[4]  = { 0.75, 0.65, 0.3, 1.0 };
  const GLfloat diffr[4] = { 0.75, 0.65, 0.3, 1.0 };
  const GLfloat specr[4] = { 0.7, 0.7, 0.6, 1.0 };
  const GLfloat shn = 60.0, wa = 5.0, we = 5.0;
  const GLfloat txc[32][4] =
    {{-1.0,0.0,-1.0,0.0},{-1.0,0.0,-1.0,0.0},{-1.0,0.0,-1.0,0.0},{-1.0,0.-1,0.0,0.0},
     {0.5,0.5,0.0,0.0},{0.5,1.0,0.0,0.5},{1.0,0.5,0.5,0.0},{1.0,1.0,0.5,0.5},
     {-1.0,0.0,-1.0,0.0},{-1.0,0.0,-1.0,0.0},{-1.0,0.0,-1.0,0.0},{-1.0,0.0,-1.0,0.0},
     {-1.0,0.0,-1.0,0.0},{-1.0,0.0,-1.0,0.0},{-1.0,0.0,-1.0,0.0},{-1.0,0.0,-1.0,0.0},
     {-1.0,0.0,-1.0,0.0},{-1.0,0.0,-1.0,0.0},{-1.0,0.0,-1.0,0.0},{-1.0,0.0,-1.0,0.0},
     {-1.0,0.0,-1.0,0.0},{-1.0,0.0,-1.0,0.0},{-1.0,0.0,-1.0,0.0},{-1.0,0.0,-1.0,0.0},
     {-1.0,0.0,-1.0,0.0},{-1.0,0.0,-1.0,0.0},{-1.0,0.0,-1.0,0.0},{-1.0,0.0,-1.0,0.0},
     {-1.0,0.0,-1.0,0.0},{-1.0,0.0,-1.0,0.0},{-1.0,0.0,-1.0,0.0},{-1.0,0.0,-1.0,0.0}};

  myteapot = ConstructTheTeapot ( MyColour );
  SetupMaterial ( 0, ambr, diffr, specr, shn, wa, we );
  myteapot->buf[3] = NewUniformBlockObject ( 32*4*sizeof(GLfloat), txcbp );
  glBufferSubData ( GL_UNIFORM_BUFFER, txcofs[0], 32*4*sizeof(GLfloat), txc );
  ExitIfGLError ( "ConstructMyTeapot" );
} /*ConstructMyTeapot*/

void DrawMyTeapot ( void )
{
  if ( skeleton ) {
    glLineWidth ( 2.0 );
    glPolygonMode ( GL_FRONT_AND_BACK, GL_LINE );
  }
  else
    glPolygonMode ( GL_FRONT_AND_BACK, GL_FILL );
  glUseProgram ( program_id[0] );
  glUniformSubroutinesuiv ( GL_FRAGMENT_SHADER, 1, (GLuint*)&LightProcInd );
  glUniform1i ( ColourSourceLoc, colour_source );
  ChooseMaterial ( 0 );
  if ( colour_source == 2 ) {
    glBindBufferBase ( GL_UNIFORM_BUFFER, txcbp, myteapot->buf[3] );
    glBindTexture ( GL_TEXTURE_2D, mytexture );
    DrawBezierPatches ( myteapot );
    glBindTexture ( GL_TEXTURE_2D, 0 );
  }
  else
    DrawBezierPatches ( myteapot );
} /*DrawMyTeapot*/

void DrawMyTeapotCNet ( void )
{
  glUseProgram ( program_id[1] );
  glLineWidth ( 1.0 );
  DrawBezierNets ( myteapot );   
} /*DrawMyTeapotCNet*/

void ConstructMyTorus ( void )
{
  GLfloat MyColour[4] = { 0.2, 0.3, 1.0, 1.0 };
  const GLfloat ambr[4]  = { 0.0, 1.0, 1.0, 1.0 };
  const GLfloat diffr[4] = { 0.0, 0.4, 1.0, 1.0 };
  const GLfloat specr[4] = { 0.7, 0.7, 0.7, 1.0 };
  const GLfloat shn = 20.0, wa = 2.0, we = 5.0;

  mytorus = EnterTorus ( 1.0, 0.5, MyColour );
  SetupMaterial ( 1, ambr, diffr, specr, shn, wa, we );
} /*ConstructMyTorus*/

void DrawMyTorus ( void )
{
  if ( skeleton ) {
    glLineWidth ( 1.0 );
    glPolygonMode ( GL_FRONT_AND_BACK, GL_LINE );
  }
  else
    glPolygonMode ( GL_FRONT_AND_BACK, GL_FILL );
  glUseProgram ( program_id[0] );
  glUniformSubroutinesuiv ( GL_FRAGMENT_SHADER, 1, (GLuint*)&LightProcInd );
  glUniform1i ( ColourSourceLoc, colour_source > 0 ? 1 : 0 );
  ChooseMaterial ( 1 );
  DrawBezierPatches ( mytorus );
} /*DrawMyTorus*/

void DrawMyTorusCNet ( void )
{
  glUseProgram ( program_id[1] );
  glLineWidth ( 1.0 );
  DrawBezierNets ( mytorus );
} /*DrawMyTorusCNet*/

