
GLuint CreateMyTexture ( int wh );
char LoadMyTextureImage ( GLuint tex, int txwidth, int txheight,
                          int x, int y, const char *filename );
char SetupMyTextureMipmaps ( GLuint tex );

