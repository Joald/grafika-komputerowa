#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/times.h>
#include "myglheader.h"
#include <GLFW/glfw3.h>

#include "../utilities/utilities.h"
#include "lights.h"
#include "bezpatches.h"
#include "mirror.h"
#include "app2e.h"

GLFWwindow   *mywindow;
int     win_width, win_height;
double  last_xi, last_eta;
float   left, right, bottom, top, near, far;

int     app_state = STATE_NOTHING;
float   viewer_rvec[3] = {1.0,0.0,0.0};
float   viewer_rangle = 0.0;
const   float viewer_pos0[4] = {0.0,0.0,10.0,1.0};

GLint   BezNormals = GL_TRUE;
GLint   TessLevel = 10;
char    redraw;
char    animate = false, colour_source = 0;
clock_t app_clock0;
float   clocks_per_sec, app_time, teapot_time0;

void myGLFWErrorHandler ( int error, const char *description )
{
  fprintf ( stderr, "GLFW error: %s\n", description );
  exit ( 1 );
} /*myGLFWErrorHandler*/

void ReshapeFunc ( GLFWwindow *win, int width, int height )
{
  float lr;

  win_width = width,  win_height = height;
  lr = 0.5533*(float)width/(float)height;
  M4x4Frustumf ( trans.wpm, NULL, -lr, lr, -0.5533, 0.5533, 5.0, 15.0 );
  SetupMirrorVPMatrices ( trans.eyepos, trans.reyepos, trans.mvm, trans.mpm );
  left = -(right = lr);  bottom = -(top = 0.5533);  near = 5.0;  far = 15.0;
  redraw = true;
} /*ReshapeFunc*/

void Redraw ( GLFWwindow *win )
{
  DrawSceneToWindow ();
  glUseProgram ( 0 );
  glFlush ();
  glfwSwapBuffers ( win );
  ExitIfGLError ( "Redraw" );
  redraw = false;
} /*Redraw*/

void DisplayFunc ( GLFWwindow *win )
{
  redraw = true;
} /*DisplayFunc*/

void ToggleAnimation ( void )
{
  if ( (animate = !animate) ) {
    teapot_time0 = app_time;
    teapot_rot_angle = teapot_rot_angle0;
  }
  else
    teapot_rot_angle0 = teapot_rot_angle;
} /*ToggleAnimation*/

void KeyFunc ( GLFWwindow *win, int key, int scancode, int action, int mode )
{
  if ( action == GLFW_PRESS || action == GLFW_REPEAT ) {
    switch ( key ) {
  case GLFW_KEY_ESCAPE:
      glfwSetWindowShouldClose ( mywindow, 1 );
      break;
  default:
      break;
    }
  }
} /*KeyFunc*/

void CharFunc ( GLFWwindow *win, unsigned int charcode )
{
  switch ( charcode ) {
case '+':
    if ( TessLevel < MAX_TESS_LEVEL ) {
      SetBezierPatchOptions ( program_id[0], BezNormals, ++TessLevel );
      redraw = true;
    }
    break;
case '-':
    if ( TessLevel > MIN_TESS_LEVEL ) {
      SetBezierPatchOptions ( program_id[0], BezNormals, --TessLevel );
      redraw = true;
    }
    break;
case 'N':  case 'n':
    BezNormals = BezNormals == 0;
    SetBezierPatchOptions ( program_id[0], BezNormals, TessLevel );
    redraw = true;
    break;
case ' ':
    ToggleAnimation ();
    break;
case 'C':  case 'c':
    cnet = !cnet;
    redraw = true;
    break;
case 'L':  case 'l':
    skeleton = true;
    redraw = true;
    break;
case 'S':  case 's':
    skeleton = false;
    redraw = true;
    break;
case 'B':  case 'b':
    LightProcInd = (LightProcInd == LambertProcInd) ?
                   BlinnPhongProcInd : LambertProcInd;
    redraw = true;
    break;
case '0':
    glUseProgram ( program_id[0] );
    colour_source = 0;
    redraw = true;
    break;
case '1':
    glUseProgram ( program_id[0] );
    colour_source = 1;
    redraw = true;
    break;
case '2':
    glUseProgram ( program_id[0] );
    colour_source = 2;
    redraw = true;
    break;
default:    /* ignorujemy wszystkie inne klawisze */
    break;
  }
} /*CharFunc*/

void MouseFunc ( GLFWwindow *win, int button, int action, int mods )
{
  switch ( app_state ) {
case STATE_NOTHING:
    if ( button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS ) {
      glfwGetCursorPos ( win, &last_xi, &last_eta );
      app_state = STATE_TURNING;
    }
    break;
case STATE_TURNING:
    if ( button == GLFW_MOUSE_BUTTON_LEFT && action != GLFW_PRESS )
      app_state = STATE_NOTHING;
    break;
default:
    break;
  }
} /*MouseFunc*/

void MotionFunc ( GLFWwindow *win, double x, double y )
{
  switch ( app_state ) {
case STATE_TURNING:
    if ( x != last_xi || y != last_eta ) {
      RotateViewer ( x-last_xi, y-last_eta );
      last_xi = x,  last_eta = y;
      redraw = true;
    }
    break;
default:
    break;
  }
} /*MotionFunc*/

void IdleFunc ( void )
{
  struct tms clk;

  app_time = (float)(times ( &clk )-app_clock0)/clocks_per_sec;
  teapot_rot_angle = teapot_rot_angle0 + 0.78539816*(app_time-teapot_time0);
  torus_rot_angle = torus_rot_angle0 - 2.0*PI*app_time;
  SetupTeapotMatrix ();
  SetupTorusMatrix ();
  redraw = true;
} /*IdleFunc*/

void Initialise ( int argc, char **argv )
{
  glfwSetErrorCallback ( myGLFWErrorHandler );
  if ( !glfwInit () ) {
    fprintf ( stderr, "Error: glfwInit failed\n" );
    exit ( 1 );
  }
  glfwWindowHint ( GLFW_SAMPLES, 8 );
  if ( !(mywindow = glfwCreateWindow ( 480, 360,
                                       "aplikacja 2E", NULL, NULL )) ) {
    glfwTerminate ();
    fprintf ( stderr, "Error: glfwCreateWindow failed\n" );
    exit ( 1 );
  }
  glfwMakeContextCurrent ( mywindow );
  GetGLProcAddresses ();
#ifdef USE_GL3W
  if ( !gl3wIsSupported ( 4, 5 ) )
    ExitOnError ( "Initialise: OpenGL version 4.2 not supported\n" );
#endif
  PrintGLVersion ();
  glfwSetWindowSizeCallback ( mywindow, ReshapeFunc );
  glfwSetWindowRefreshCallback ( mywindow, DisplayFunc );
  glfwSetKeyCallback ( mywindow, KeyFunc );
  glfwSetCharCallback ( mywindow, CharFunc );
  glfwSetMouseButtonCallback ( mywindow, MouseFunc );
  glfwSetCursorPosCallback ( mywindow, MotionFunc );
  LoadMyShaders ();
  InitMyObject ();
  ReshapeFunc ( mywindow, 480, 360 );
  redraw = true;
} /*Initialise*/

void Cleanup ( void )
{
  int i;

  glUseProgram ( 0 );
  for ( i = 0; i < 9; i++ )
    glDeleteShader ( shader_id[i] );
  for ( i = 0; i < 3; i++ )
    glDeleteProgram ( program_id[i] );
  glDeleteBuffers ( 1, &trbuf );
  glDeleteBuffers ( 1, &lsbuf );
  DeleteBezierPatchDomain ();
  DeleteBezierPatches ( myteapot );
  DeleteBezierPatches ( mytorus );
  for ( i = 0; i < 2; i++ )
    DeleteMaterial ( i );
  glDeleteTextures ( 1, &mytexture );
  DestroyMirrorVAO ();
  DestroyMirrorFBO ();
  glfwDestroyWindow ( mywindow );
  glfwTerminate ();
} /*Cleanup*/

static void (*idlefunc)(void) = NULL;

void SetIdleFunc ( void(*IdleFunc)(void) )
{
  idlefunc = IdleFunc;
} /*SetIdleFunc*/

void MessageLoop ( void )
{
  do {
    if ( idlefunc ) {
      idlefunc ();
      glfwPollEvents ();
    }
    else
      glfwWaitEvents ();
    if ( redraw )
      Redraw ( mywindow );
  } while ( !glfwWindowShouldClose ( mywindow ) );
} /*MessageLoop*/

int main ( int argc, char **argv )
{
  Initialise ( argc, argv );
  SetIdleFunc ( IdleFunc );
  MessageLoop ();
  Cleanup ();
  exit ( 0 );
} /*main*/

