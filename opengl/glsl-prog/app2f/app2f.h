
#define STATE_NOTHING 0
#define STATE_TURNING 1

#define MIN_TESS_LEVEL  3
#define MAX_TESS_LEVEL 32

typedef struct TransBl {
          GLfloat mm[16], mmti[16],  /* model matrix */
                  wvm[16], wpm[16],  /* window view and projection matrices */
                  mvm[16], mpm[16];  /* mirror view and projection matrices */
          GLfloat eyepos[4], reyepos[4];
        } TransBl;

extern GLuint shader_id[9];
extern GLuint program_id[3];
extern GLuint trbi, trbuf, trbbp;       /* dostep do przeksztalcen */
extern GLint  trbsize, trbofs[6];
extern GLint  LightProcLoc, LightProcInd,
              LambertProcInd, BlinnPhongProcInd;
extern GLint  ColourSourceLoc, NormalSourceLoc;
              
extern BezierPatchObjf *myteapot, *mytorus;
extern GLuint mytexture;
extern GLuint mirror_vao, mirror_vbo;

extern float   model_rot_axis[3], teapot_rot_angle0, teapot_rot_angle,
               torus_rot_angle0, torus_rot_angle;
extern GLfloat teapot_mmatrix[16], teapot_mmti[16],
               torus_mmatrix[16], torus_mmti[16],
               mirror_matrix[16];

extern TransBl trans;

extern GLFWwindow  *mywindow;
extern int         win_width, win_height;
extern double      last_xi, last_eta;
extern float       left, right, bottom, top, near, far;
extern int         app_state;
extern float       viewer_rvec[3];
extern float       viewer_rangle;
extern const float viewer_pos0[4];
extern GLint       BezNormals;
extern GLint       TessLevel;
extern char        animate, colour_source, normal_source;
extern char        cnet, skeleton;
extern clock_t app_clock0;
extern float clocks_per_sec, app_time, teapot_time0;

void SetupVPMatrix ( char mirror );
void SetModelMatrix ( GLfloat mm[16], GLfloat mmti[16] );

void SetupTeapotMatrix ( void );
void SetupTorusMatrix ( void );
void InitViewMatrix ( void );
void RotateViewer ( double deltaxi, double deltaeta );

void LoadMyTextures ( void );

void InitLights ( void );
void SetBezPatchOptions ( GLint normals, GLint tesslevel );

void ConstructMyTeapot ( void );
void DrawMyTeapot ( void );
void DrawMyTeapotCNet ( void );

void ConstructMyTorus ( void );
void DrawMyTorus ( void );
void DrawMyTorusCNet ( void );

void ConstructMirror ( void );
void DrawMirror ( void );

void DrawScene ( void );
void DrawSceneToMirror ( void );
void DrawSceneToWindow ( void );

void LoadMyShaders ( void );
void InitMyObject ( void );


void myGLFWErrorHandler ( int error, const char *description );
void ReshapeFunc ( GLFWwindow *win, int width, int height );
void DisplayFunc ( GLFWwindow *win );
void MouseFunc ( GLFWwindow *win, int button, int action, int mods );
void MotionFunc ( GLFWwindow *win, double x, double y );
void IdleFunc ( void );
void CharFunc ( GLFWwindow *win, unsigned int charcode );

void Initialise ( int argc, char **argv );
void Cleanup ( void );
void SetIdleFunc ( void(*IdleFunc)(void) );
void MessageLoop ( void );
