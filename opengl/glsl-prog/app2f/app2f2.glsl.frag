#version 450 core

in GVertex {
    vec2 TexCoord;
  } In;

out vec4 out_Colour;

uniform sampler2D tex;

void main ( void )
{
  if ( gl_FrontFacing )
    out_Colour = vec4(0.3);
  else
    out_Colour = texture ( tex, In.TexCoord );
} /*main*/

