#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include "myglheader.h"

#include "../utilities/utilities.h"
#include "lights.h"

GLuint  lsbi, lsbuf, lsbbp;
GLint   lsbsize, lsbofs[9];
LightBl light;

GLuint matbi, matbuf[MAX_MATERIALS], matbbp;
GLint  matbsize, matbofs[6];

void SetLightAmbient ( int l, GLfloat amb[4] )
{
  GLint ofs;

  if ( l < 0 || l >= MAX_NLIGHTS )
    return;
  memcpy ( light.ls[l].ambient, amb, 4*sizeof(GLfloat) );
  ofs = l*(lsbofs[8]-lsbofs[3]) + lsbofs[3];
  glBindBuffer ( GL_UNIFORM_BUFFER, lsbuf );
  glBufferSubData ( GL_UNIFORM_BUFFER, ofs, 4*sizeof(GLfloat), amb );
  ExitIfGLError ( "SetLightAmbient" );
} /*SetLightAmbient*/

void SetLightDiffuse ( int l, GLfloat dif[4] )
{
  GLint ofs;

  if ( l < 0 || l >= MAX_NLIGHTS )
    return;
  memcpy ( light.ls[l].diffuse, dif, 4*sizeof(GLfloat) );
  ofs = l*(lsbofs[8]-lsbofs[3]) + lsbofs[4];
  glBindBuffer ( GL_UNIFORM_BUFFER, lsbuf );
  glBufferSubData ( GL_UNIFORM_BUFFER, ofs, 4*sizeof(GLfloat), dif );
  ExitIfGLError ( "SetLightDiffuse" );
} /*SetLightDiffuse*/

void SetLightPosition ( int l, GLfloat pos[4] )
{
  GLint ofs;

  if ( l < 0 || l >= MAX_NLIGHTS )
    return;
  memcpy ( light.ls[l].position, pos, 4*sizeof(GLfloat) );
  ofs = l*(lsbofs[8]-lsbofs[3]) + lsbofs[5];
  glBindBuffer ( GL_UNIFORM_BUFFER, lsbuf );
  glBufferSubData ( GL_UNIFORM_BUFFER, ofs, 4*sizeof(GLfloat), pos );
  ExitIfGLError ( "SetLightPosition" );
} /*SetLightPosition*/

void SetLightAttenuation ( int l, GLfloat atn[3] )
{
  GLint ofs;

  if ( l < 0 || l >= MAX_NLIGHTS )
    return;
  memcpy ( light.ls[l].attenuation, atn, 3*sizeof(GLfloat) );
  ofs = l*(lsbofs[8]-lsbofs[3]) + lsbofs[6];
  glBindBuffer ( GL_UNIFORM_BUFFER, lsbuf );
  glBufferSubData ( GL_UNIFORM_BUFFER, ofs, 3*sizeof(GLfloat), atn );
  ExitIfGLError ( "SetLightAttenuation" );
} /*SetLightAttenuation*/

void SetLightOnOff ( int l, char on )
{
  GLuint mask;

  if ( l < 0 || l >= MAX_NLIGHTS )
    return;
  mask = 0x01 << l;
  if ( on ) {
    light.mask |= mask;
    if ( l >= light.nls )
      light.nls = l+1;
  }
  else {
    light.mask &= ~mask;
    for ( mask = 0x01 << (light.nls-1); mask; mask >>= 1 ) {
      if ( light.mask & mask )
        break;
      else
        light.nls --;
    }
  }
  glBindBuffer ( GL_UNIFORM_BUFFER, lsbuf );
  glBufferSubData ( GL_UNIFORM_BUFFER, lsbofs[0], sizeof(GLuint), &light.nls );
  glBufferSubData ( GL_UNIFORM_BUFFER, lsbofs[1], sizeof(GLuint), &light.mask );
  ExitIfGLError ( "SetLightOnOff" );
} /*SetLightOnOff*/

void SetupMaterial ( int m, const GLfloat ambr[4], const GLfloat diffr[4],
                     const GLfloat specr[4],
                     GLfloat shn, GLfloat wa, GLfloat we )
{
  if ( m >= 0 && m < MAX_MATERIALS ) {
    matbuf[m] = NewUniformBlockObject ( matbsize, matbbp );
    glBindBuffer ( GL_UNIFORM_BUFFER, matbuf[m] );
    glBufferSubData ( GL_UNIFORM_BUFFER, matbofs[0], 4*sizeof(GLfloat), ambr );
    glBufferSubData ( GL_UNIFORM_BUFFER, matbofs[1], 4*sizeof(GLfloat), diffr );
    glBufferSubData ( GL_UNIFORM_BUFFER, matbofs[2], 4*sizeof(GLfloat), specr );
    glBufferSubData ( GL_UNIFORM_BUFFER, matbofs[3], sizeof(GLfloat), &shn );
    glBufferSubData ( GL_UNIFORM_BUFFER, matbofs[4], sizeof(GLfloat), &wa );
    glBufferSubData ( GL_UNIFORM_BUFFER, matbofs[5], sizeof(GLfloat), &we );
    ExitIfGLError ( "SetupMaterial" );
  }
} /*SetupMaterial*/

void ChooseMaterial ( int m )
{
  if ( m >= 0 && m < MAX_MATERIALS ) {
    glBindBufferBase ( GL_UNIFORM_BUFFER, matbbp, matbuf[m] );
    ExitIfGLError ( "ChooseMaterial" );
  }
} /*ChooseMaterial*/

void DeleteMaterial ( int m )
{
  if ( m >= 0 && m < MAX_MATERIALS ) {
    glDeleteBuffers ( 1, &matbuf[m] );
    ExitIfGLError ( "DeleteMaterial" );
  }
} /*DeleteMaterial*/

/* ////////////////////////////////////////////////////////////////////////// */
extern GLuint program_id[];

void ConstructShadowTxtFBO ( int l )
{
  GLuint fbo, txt;

  if ( l < 0 || l >= MAX_NLIGHTS )
    return;
  glGenTextures ( 1, &txt );
  glGenFramebuffers ( 1, &fbo );
  if ( (light.ls[l].shadow_txt = txt) &&
       (light.ls[l].shadow_fbo = fbo) ) {
    glActiveTexture ( GL_TEXTURE2+l );
    glBindTexture ( GL_TEXTURE_2D, txt );
    glTexImage2D ( GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32,
                   SHADOW_MAP_SIZE, SHADOW_MAP_SIZE, 0,
                   GL_DEPTH_COMPONENT, GL_FLOAT, NULL );
    glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE,
                      GL_COMPARE_REF_TO_TEXTURE );
    glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL );
    glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
    glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
    glBindTexture ( GL_TEXTURE_2D, 0 );
    glBindFramebuffer ( GL_FRAMEBUFFER, fbo );
    glFramebufferTexture ( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                           light.ls[l].shadow_txt, 0 );
    glDrawBuffer ( GL_NONE );
    light.shmask |= 0x01 << l;
    glBindBuffer ( GL_UNIFORM_BUFFER, lsbuf );
    glBufferSubData ( GL_UNIFORM_BUFFER, lsbofs[2], sizeof(GLuint), &light.shmask );
    glBindFramebuffer ( GL_FRAMEBUFFER, 0 );
    ExitIfGLError ( "ConstructShadowTxtFBO" );
  }
} /*ConstructShadowTxtFBO*/

static void SetupShadowViewerTrans ( GLfloat org[3], GLfloat zv[3],
                                     GLfloat vmat[16] )
{
  GLfloat v[3], a[16], g, r, s;
  int     i, j;

        /* skonstruuj odbicie Householdera */
  memcpy ( v, zv, 3*sizeof(float) );
  r = sqrt ( V3DotProductf ( v, v ) );
  v[2] += v[2] > 0.0 ? r : -r;
  g = 2.0/V3DotProductf ( v, v );
        /* odbij kolumny macierzy jednostkowej */
  M4x4Identf ( a );
  for ( i = 0; i < 2; i++ ) {
    s = v[i]*g;
    for ( j = 0; j < 3; j++ )
      a[4*i+j] -= s*v[j];
  }
  a[8] = zv[0]/r;  a[9] = zv[1]/r;  a[10] = zv[2]/r;
  memcpy ( &a[12], org, 3*sizeof(GLfloat) );
  M4x4InvertAffineIsometryf ( vmat, a );
} /*SetupShadowViewerTrans*/

void UpdateShadowMatrix ( int l )
{
  int           ofs;
  GLfloat       lvpm[16], a[16];
  const GLfloat b[16] = {0.5,0.0,0.0,0.0,0.0,0.5,0.0,0.0,
                         0.0,0.0,0.5,0.0,0.5,0.5,0.5,1.0};

  M4x4Multf ( a, light.ls[l].shadow_proj, light.ls[l].shadow_view );
  M4x4Multf ( lvpm, b, a );
  ofs = l*(lsbofs[8]-lsbofs[3]) + lsbofs[7];
  glBindBuffer ( GL_UNIFORM_BUFFER, lsbuf );
  glBufferSubData ( GL_UNIFORM_BUFFER, ofs, 16*sizeof(GLfloat), lvpm );
  ExitIfGLError ( "UpdateShadowMatrix" );
} /*UpdateShadowMatrix*/

void SetupShadowTxtTransformations ( int l, float sc[3], float R )
{
  GLfloat *lvm, *lpm;
  GLfloat lpos[3], v[3], d, n, s, t;
  int     i;

  if ( l < 0 || l >= MAX_NLIGHTS )
    return;
  if ( light.ls[l].shadow_txt ) {
    lvm = light.ls[l].shadow_view;
    lpm = light.ls[l].shadow_proj;
    if ( light.ls[l].position[3] != 0.0 ) {
            /* zrodlo swiatla w skonczonej odleglosci */
      for ( i = 0; i < 3; i++ ) {
        lpos[i] = light.ls[l].position[i]/light.ls[l].position[3];
        v[i] = lpos[i] - sc[i];
      }
      SetupShadowViewerTrans ( lpos, v, lvm );
      d = V3DotProductf ( v, v );
      if ( d > 2.0*R*R ) {
        d = sqrt ( d );  n = d-R;
        s = R/d;                  /* sin(alpha) */
        t = s /sqrt ( 1.0-s*s );  /* tg(alpha)*/
        M4x4Frustumf ( lpm, NULL, -n*t, n*t, -n*t, n*t, n, d+R );
      }
      else {
        n = 0.4142*R;
        M4x4Frustumf ( lpm, NULL, -n, n, -n, n, n, 2.4143*R );
      }
    }
    else {  /* zrodlo swiatla w odleglosci nieskonczonej */
      memcpy ( lpos, sc, 3*sizeof(GLfloat) );
      memcpy ( v, light.ls[l].position, 3*sizeof(GLfloat) );
      SetupShadowViewerTrans ( lpos, v, lvm );
      M4x4Orthof ( lpm, NULL, -R, R, -R, R, -R, R );
    }
  }
} /*SetupShadowTxtTransformations*/

void BindShadowTxtFBO ( int l )
{
  if ( l < 0 || l >= MAX_NLIGHTS )
    return;
  if ( (light.ls[l].shadow_txt == 0) )
    return;
  SetVPMatrix ( light.ls[l].shadow_view, light.ls[l].shadow_proj,
                light.ls[l].position );
  glBindFramebuffer ( GL_FRAMEBUFFER, light.ls[l].shadow_fbo );
  ExitIfGLError ( "BindShadowTxtFBO" );
} /*BindShadowTxtFBO*/

void DestroyShadowFBO ( void )
{
  int l;

  glBindFramebuffer ( GL_FRAMEBUFFER, 0 );
  for ( l = 0; l < MAX_NLIGHTS; l++ ) {
    if ( light.ls[l].shadow_fbo != 0 )
      glDeleteFramebuffers ( 1, &light.ls[l].shadow_fbo );
    if ( light.ls[l].shadow_txt != 0 )
      glDeleteTextures ( 1, &light.ls[l].shadow_txt );
  }
  ExitIfGLError ( "DestroyShadowFBO" );
} /*DestroyShadowFBO*/

