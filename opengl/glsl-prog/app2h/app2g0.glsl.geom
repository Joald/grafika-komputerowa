#version 450

#define MAX_NLIGHTS 8

layout(triangles) in;
layout(triangle_strip,max_vertices=3) out;

in GVertex {
  int  instance;
  vec4 Colour;
  vec3 Position;
  vec3 pu, pv, Normal;
  vec2 PatchCoord, TexCoord;
  vec4 ShadowPos[MAX_NLIGHTS];
} In[];

out NVertex {
  float instance;
  vec4  Colour;
  vec3  Position;
  vec3  pu, pv, Normal;
  vec2  PatchCoord, TexCoord;
  vec4 ShadowPos[MAX_NLIGHTS];
} Out;

struct LSPar {
    vec4 ambient;
    vec4 direct;
    vec4 position;
    vec3 attenuation;
    mat4 shadow_vpm;
  };

uniform LSBlock {
    uint  nls;              /* liczba zrodel swiatla */
    uint  mask;             /* maska wlaczonych zrodel */
    uint  shmask;           /* maska tekstur cienia */
    LSPar ls[MAX_NLIGHTS];  /* poszczegolne zrodla swiatla */
  } light;

uniform BezPatch {
    int  dim, udeg, vdeg;
    int  stride_u, stride_v, stride_p, stride_q, nq;
    bool use_ind;
    vec4 Colour;
    int  TessLevel;
    bool BezNormals;
  } bezp;

void main ( void )
{
  uint i, l, mask;
  vec3 v1, v2, nv;

  v1 = In[1].Position - In[0].Position;
  v2 = In[2].Position - In[0].Position;
  nv = normalize ( cross ( v1, v2 ) );
  for ( i = 0; i < 3; i++ ) {
    gl_Position = gl_in[i].gl_Position;
    Out.Position = In[i].Position;
    if ( !bezp.BezNormals || dot ( nv, nv ) < 1.0e-10 ) {
      Out.pu = In[i].pu - dot ( In[i].pu, nv )*nv;
      Out.pv = In[i].pv - dot ( In[i].pv, nv )*nv;
      Out.Normal = nv;
    }
    else {
      Out.pu = In[i].pu;
      Out.pv = In[i].pv;
      Out.Normal = In[i].Normal;
    }
    Out.Colour = In[i].Colour;
    Out.PatchCoord = In[i].PatchCoord;
    Out.TexCoord = In[i].TexCoord;
    Out.instance = In[i].instance;
    for ( l = 0, mask = 0x00000001;  l < light.nls;  l++, mask <<= 1 )
      if ( (light.mask & mask) != 0 )
        Out.ShadowPos[l] = In[i].ShadowPos[l];
    EmitVertex ();
  }
  EndPrimitive ();
} /*main*/
