#include <cstdlib>
#include <unistd.h>
#include <cstdio>
#include <sys/times.h>
#include "myglheader.h"

#include "../utilities/utilities.h"
#include "linkage.h"
#include "lights.h"
#include "bezpatches.h"
#include "mirror.h"
#include "app2h.h"

GLFWwindow* mywindow;

int win_width, win_height;

double last_xi, last_eta;

float left, right, bottom, top, near, far;

int app_state = STATE_NOTHING;

Vector3f viewer_rvec = {1.0, 0.0, 0.0};

float viewer_rangle = 0.0;

const Vector4f viewer_pos0 = {0.0, 0.0, 10.0, 1.0};

GLint BezNormals = GL_TRUE;

GLint TessLevel = 10;

char redraw;

char animate = false, colour_source = 0, normal_source = 0;

clock_t app_clock0;

float clocks_per_sec, app_time, app_time0, teapot_time0;

void myGLFWErrorHandler(int error, const char* description) {
    UNUSED(error);
    fprintf(stderr, "GLFW error: %s\n", description);
    exit(1);
} /*myGLFWErrorHandler*/

void ReshapeFunc(GLFWwindow* win, int width, int height) {
    UNUSED(win);
    win_width = width, win_height = height;
    float lr = 0.5533f * (float) width / (float) height;

    trans.wpm = M4x4ToStandardCubeFrustum(-lr, lr, -0.5533f, 0.5533, 5.0, 15.0);
    SetupMirrorVPMatrices(trans.eyepos, trans.reyepos, trans.mvm, trans.mpm);
    left = -(right = lr);
    bottom = -(top = 0.5533);
    near = 5.0;
    far = 15.0;
    redraw = true;
} /*ReshapeFunc*/

void Redraw(GLFWwindow* win) {
    DrawSceneToWindow();
    glUseProgram(0);
    glFlush();
    glfwSwapBuffers(win);
    ExitIfGLError("Redraw");
    redraw = false;
} /*Redraw*/

void DisplayFunc(GLFWwindow* win) {
    redraw = true;
} /*DisplayFunc*/

void ToggleAnimation() {
    if ((animate = !animate)) {
        teapot_time0 = app_time;
        teapot_rot_angle = teapot_rot_angle0;
    } else {
        teapot_rot_angle0 = teapot_rot_angle;
    }
} /*ToggleAnimation*/

void KeyFunc(GLFWwindow* win, int key, int scancode, int action, int mode) {
    if (action == GLFW_PRESS || action == GLFW_REPEAT) {
        switch (key) {
            case GLFW_KEY_ESCAPE:glfwSetWindowShouldClose(mywindow, 1);
                break;
            default:break;
        }
    }
} /*KeyFunc*/

void CharFunc(GLFWwindow* win, unsigned int charcode) {
    switch (charcode) {
        case '+':
            if (TessLevel < MAX_TESS_LEVEL) {
                SetBezierPatchOptions(myteapots[0], BezNormals, ++TessLevel);
                SetBezierPatchOptions(mytoruses[0], BezNormals, ++TessLevel);
                break;
            } else {
                return;
            }
        case '-':
            if (TessLevel > MIN_TESS_LEVEL) {
                SetBezierPatchOptions(myteapots[0], BezNormals, --TessLevel);
                SetBezierPatchOptions(mytoruses[0], BezNormals, --TessLevel);
                break;
            } else {
                return;
            }
        case 'N':
        case 'n':BezNormals = BezNormals == 0;
            SetBezierPatchOptions(myteapots[0], BezNormals, TessLevel);
            SetBezierPatchOptions(mytoruses[0], BezNormals, TessLevel);
            break;
        case ' ':ToggleAnimation();
            return;
        case 'C':
        case 'c':cnet = !cnet;
            break;
        case 'L':
        case 'l':skeleton = true;
            break;
        case 'S':
        case 's':skeleton = false;
            break;
        case 'B':
        case 'b':
            LightProcInd = (LightProcInd == LambertProcInd) ?
                           BlinnPhongProcInd : LambertProcInd;
            break;
        case '0':colour_source = 0;
            break;
        case '1':colour_source = 1;
            break;
        case '2':colour_source = 2;
            break;
        case 'D':
        case 'd':
            normal_source = (normal_source + 1) % 3;
            break;
        default:    /* ignorujemy wszystkie inne klawisze */
            return;
    }
    redraw = true;
} /*CharFunc*/

void MouseFunc(GLFWwindow* win, int button, int action, int mods) {
    switch (app_state) {
        case STATE_NOTHING:
            if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
                glfwGetCursorPos(win, &last_xi, &last_eta);
                app_state = STATE_TURNING;
            }
            break;
        case STATE_TURNING:
            if (button == GLFW_MOUSE_BUTTON_LEFT && action != GLFW_PRESS) {
                app_state = STATE_NOTHING;
            }
            break;
        default:break;
    }
} /*MouseFunc*/

void MotionFunc(GLFWwindow* win, double x, double y) {
    switch (app_state) {
        case STATE_TURNING:
            if (!are_close(x, last_xi) || !are_close(y, last_eta)) {
                RotateViewer(x - last_xi, y - last_eta);
                last_xi = x, last_eta = y;
                redraw = true;
            }
            break;
        default:break;
    }
} /*MotionFunc*/

void IdleFunc() {
    struct tms clk{};

    app_time = (float) (times(&clk) - app_clock0) / clocks_per_sec;
    ArticulateMyLinkage(app_time);
    redraw = true;
} /*IdleFunc*/

void Initialise(int argc, char** argv) {
    glfwSetErrorCallback(myGLFWErrorHandler);
    if (!glfwInit()) {
        ExitOnError("glfwInit failed");
    }
    glfwWindowHint(GLFW_SAMPLES, 8);
    if (!(mywindow = glfwCreateWindow(480, 360,
                                      "aplikacja 2H", nullptr, nullptr))) {
        glfwTerminate();
        ExitOnError("glfwCreateWindow failed");
    }
    glfwMakeContextCurrent(mywindow);
    GetGLProcAddresses();
#ifdef USE_GL3W
    if ( !gl3wIsSupported ( 4, 5 ) )
      ExitOnError ( "Initialise: OpenGL version 4.2 not supported\n" );
#endif
    PrintGLVersion();
    glfwSetWindowSizeCallback(mywindow, ReshapeFunc);
    glfwSetWindowRefreshCallback(mywindow, DisplayFunc);
    glfwSetKeyCallback(mywindow, KeyFunc);
    glfwSetCharCallback(mywindow, CharFunc);
    glfwSetMouseButtonCallback(mywindow, MouseFunc);
    glfwSetCursorPosCallback(mywindow, MotionFunc);
    LoadMyShaders();
    InitMyObject();
    ReshapeFunc(mywindow, 480, 360);
    redraw = true;
} /*Initialise*/

void Cleanup() {
    glUseProgram(0);
    for (unsigned int i : shader_id)
        glDeleteShader(i);
    for (unsigned int i : program_id)
        glDeleteProgram(i);
    glDeleteBuffers(1, &trbuf);
    glDeleteBuffers(1, &lsbuf);
    DeleteBezierPatchDomain();
    DeleteBezierPatches(myteapots[0]);
    glDeleteBuffers(1, &myteapots[1]->buf[1]);
    free(myteapots[1]);
    DeleteBezierPatches(mytoruses[0]);
    glDeleteBuffers(1, &mytoruses[1]->buf[1]);
    free(mytoruses[1]);
    glDeleteBuffers(2, lktrbuf);
    kl_DestroyLinkage(mylinkage());
    for (int i = 0; i < 2; i++) {
        DeleteMaterial(i);
    }
    glDeleteTextures(1, &mytexture);
    DestroyMirrorVAO();
    DestroyMirrorFBO();
    DestroyShadowFBO();
    glfwDestroyWindow(mywindow);
    glfwTerminate();
} /*Cleanup*/

static void (* idlefunc)() = nullptr;

void SetIdleFunc(void(* IdleFunc)()) {
    idlefunc = IdleFunc;
} /*SetIdleFunc*/

void MessageLoop() {
    do {
        if (idlefunc) {
            idlefunc();
            glfwPollEvents();
        } else {
            glfwWaitEvents();
        }
        if (redraw) {
            Redraw(mywindow);
        }
    } while (!glfwWindowShouldClose(mywindow));
} /*MessageLoop*/

int main(int argc, char** argv) {
    Initialise(argc, argv);
    SetIdleFunc(IdleFunc);
    MessageLoop();
    Cleanup();
    exit(0);
} /*main*/

