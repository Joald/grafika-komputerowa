#ifndef APP2H_H
#define APP2H_H

#include "../utilities/utilities.h"
#include "bezpatches.h"
#include "linkage.h"
#include <GLFW/glfw3.h>

#define STATE_NOTHING 0
#define STATE_TURNING 1

#define MIN_TESS_LEVEL  3
#define MAX_TESS_LEVEL 32

struct TransBl {
    Matrix mm, mmti,  /* model matrix */
      wvm, wpm,  /* window view and projection matrices */
      mvm, mpm;  /* mirror view and projection matrices */
    Vector4GLf eyepos, reyepos;
};

extern GLuint shader_id[13];

extern GLuint program_id[6], progid0[3], progid1[3];

extern GLuint trans_block_index, trbuf, trbbp;       /* access to transformations */
extern GLint trans_block_size, trbofs[6];

extern GLint LightProcLoc, LightProcInd,
  LambertProcInd, BlinnPhongProcInd;

extern GLuint ucs_loc, uns_loc, dim_loc, ncp_loc, trnum_loc;

extern GLuint ccpbi, ctrbi, ctrbi, ctrbbp, ctribi, ctribbp;

extern BezierPatchObjf* myteapots[2], * mytoruses[2];

extern GLuint lktrbuf[2];

extern GLuint mytexture;

extern GLuint mirror_vao, mirror_vbo;

extern float model_rot_axis[3], teapot_rot_angle0, teapot_rot_angle,
  torus_rot_angle0, torus_rot_angle;

extern Matrix ident_matrix;

extern TransBl trans;

extern int win_width, win_height;

extern double last_xi, last_eta;

extern float left, right, bottom, top, near, far;

extern int app_state;

extern Vector3f viewer_rvec;

extern float viewer_rangle;

extern const Vector4f viewer_pos0;

extern GLint BezNormals;

extern GLint TessLevel;

extern char animate, colour_source, normal_source;

extern char cnet, skeleton;

extern clock_t app_clock0;

extern float clocks_per_sec,
  app_time, app_time0, teapot_time0;

void SetVPMatrix(Matrix& vm, Matrix& pm, Vector4GLf& ep);
void SetModelMatrix(Matrix& mm, Matrix& mmti);

void InitViewMatrix();
void RotateViewer(double deltaxi, double deltaeta);

void LoadMyTextures();
void InitLights();

void ConstructMyTeapot(BezierPatchObjf* teapots[2]);
void DrawMyTeapot(char final, GLuint program);
void DrawMyTeapotCNet(GLuint program);

void ConstructMyTorus(BezierPatchObjf* toruses[2]);
void DrawMyTorus(char final, GLuint program);
void DrawMyTorusCNet(GLuint program);

kl_linkage* ConstructMyLinkage();
GLfloat TeapotRotAngle1(float time);
GLfloat TeapotRotAngle2(float time);
GLfloat SpoutAngle(float time);
GLfloat LidAngle(float time);
GLfloat TorusRotAngle1(float time);
GLfloat TorusRotAngle2(float time);
void ArticulateMyLinkage(float time);

void ConstructMirror();

void DrawSceneToMirror();
void DrawSceneToShadows();
void DrawSceneToWindow();

void LoadMyShaders();
void InitMyObject();


void myGLFWErrorHandler(int error, const char* description);
void ReshapeFunc(GLFWwindow* win, int width, int height);
void DisplayFunc(GLFWwindow* win);
void MouseFunc(GLFWwindow* win, int button, int action, int mods);
void MotionFunc(GLFWwindow* win, double x, double y);
void IdleFunc();
void CharFunc(GLFWwindow* win, unsigned int charcode);

void Initialise(int argc, char** argv);
void Cleanup();
void SetIdleFunc(void(* IdleFunc)());
void MessageLoop();


kl_linkage* mylinkage();

#endif // APP2H_H