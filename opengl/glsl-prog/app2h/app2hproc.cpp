#include <cstdlib>
#include <unistd.h>
#include <cstring>
#include <cstdio>
#include <sys/times.h>
#include <cmath>
#include "myglheader.h"
#include <GLFW/glfw3.h>

#include "../utilities/utilities.h"
#include "linkage.h"
#include "texture.h"
#include "lights.h"
#include "bezpatches.h"
#include "teapot.h"
#include "sproduct.h"
#include "mirror.h"
#include "app2h.h"

GLuint shader_id[13];

GLuint program_id[6], progid0[3], progid1[3];

GLuint trans_block_index, trbuf, trbbp;       /* access to transformations */
GLint trans_block_size, trbofs[6];

GLint LightProcLoc, LightProcInd,
  LambertProcInd, BlinnPhongProcInd;

GLuint ucs_loc, uns_loc, dim_loc, ncp_loc, trnum_loc;

GLuint ccpbi, ctrbi, ctrbbp, ctribi, ctribbp;

BezierPatchObjf* myteapots[2], * mytoruses[2];

GLuint lktrbuf[2];

GLuint mytexture;

float model_rot_axis[3] = {0.0, 0.0, 1.0},
  teapot_rot_angle0 = 0.0, teapot_rot_angle = 0.0;

Matrix ident_matrix = Matrix::identity();

TransBl trans;

char cnet = false, skeleton = false;

void LoadMyShaders() {
    static const char* filename[] =
      {"app2g0.glsl.vert", "app2g0.glsl.tesc", "app2g0.glsl.tese",
       "app2g0.glsl.geom", "app2g0.glsl.frag",
       "app2g1.glsl.vert", "app2g1.glsl.frag",
       "app2g2.glsl.vert", "app2g2.glsl.frag",
       "app2g3.glsl.tese", "app2g3.glsl.frag",
       "app2g4.glsl.vert", "app2h.glsl.comp"};
    static const GLuint shtype[] =
      {GL_VERTEX_SHADER, GL_TESS_CONTROL_SHADER, GL_TESS_EVALUATION_SHADER,
       GL_GEOMETRY_SHADER, GL_FRAGMENT_SHADER,
       GL_VERTEX_SHADER, GL_FRAGMENT_SHADER,
       GL_VERTEX_SHADER, GL_FRAGMENT_SHADER,
       GL_TESS_EVALUATION_SHADER, GL_FRAGMENT_SHADER,
       GL_VERTEX_SHADER, GL_COMPUTE_SHADER};
    static const GLchar* UTBNames[] =
      {"TransBlock", "TransBlock.mm", "TransBlock.mmti", "TransBlock.vm",
       "TransBlock.pm", "TransBlock.vpm", "TransBlock.eyepos"};
    static const GLchar* ULSNames[] =
      {"LSBlock", "LSBlock.light_count", "LSBlock.mask", "LSBlock.shmask",
       "LSBlock.ls[0].ambient", "LSBlock.ls[0].direct", "LSBlock.ls[0].position",
       "LSBlock.ls[0].attenuation", "LSBlock.ls[0].shadow_vpm",
       "LSBlock.ls[1].ambient"};
    static const GLchar* UCPNames[] =
      {"CPoints", "CPoints.cp"};
    static const GLchar* UCPINames[] =
      {"CPIndices", "CPIndices.cpi"};
    static const GLchar* UBezPatchNames[] =
      {"BezPatch", "BezPatch.dim", "BezPatch.udeg", "BezPatch.vdeg",
       "BezPatch.stride_u", "BezPatch.stride_v",
       "BezPatch.stride_p", "BezPatch.stride_q", "BezPatch.nq",
       "BezPatch.use_ind", "BezPatch.Colour",
       "BezPatch.TessLevel", "BezPatch.BezNormals"};
    static const GLchar* UMatNames[] =
      {"MatBlock", "MatBlock.mat.ambref", "MatBlock.mat.dirref",
       "MatBlock.mat.specref", "MatBlock.mat.shininess",
       "MatBlock.mat.wa", "MatBlock.mat.we"};
    static const GLchar* UTexCoordNames[] =
      {"BezPatchTexCoord", "BezPatchTexCoord.txc"};
    static const GLchar LightProcName[] = "Lighting";
    static const GLchar LambertProcName[] = "LambertLighting";
    static const GLchar BlinnPhongProcName[] = "BlinnPhongLighting";
    static const GLchar ColourSourceName[] = "ColourSource";
    static const GLchar NormalSourceName[] = "NormalSource";
    static const GLchar* UCompTrNames[] =
      {"Tr", "Tr.tr"};
    static const GLchar* UCompTrIndNames[] =
      {"TrInd", "TrInd.ind"};
    static const GLchar DimName[] = "dim";
    static const GLchar NcpName[] = "ncp";
    static const GLchar TrNumName[] = "trnum";

    GLint i;
    GLuint shid[5];

    for (i = 0; i < 13; i++) {
        shader_id[i] = CompileShaderFiles < 1 > (shtype[i], &filename[i]);
    }
    program_id[0] = LinkShaderProgram(5, shader_id);
    program_id[1] = LinkShaderProgram(2, &shader_id[5]);
    program_id[2] = LinkShaderProgram(2, &shader_id[7]);
    shid[0] = shader_id[0];
    shid[1] = shader_id[1];
    shid[2] = shader_id[9];
    shid[3] = shader_id[10];
    program_id[3] = LinkShaderProgram(4, shid);
    shid[0] = shader_id[11];
    shid[1] = shader_id[10];
    program_id[4] = LinkShaderProgram(2, shid);
    progid0[0] = program_id[3];
    progid0[1] = program_id[1];
    progid0[2] = program_id[4];
    progid1[0] = program_id[0];
    progid1[1] = program_id[1];
    progid1[2] = program_id[2];
    program_id[5] = LinkShaderProgram(1, &shader_id[12]);
    GetAccessToUniformBlock(program_id[0], 6, &UTBNames[0],
                            &trans_block_index, &trans_block_size, trbofs, &trbbp);
    GetAccessToUniformBlock(program_id[0], 9, &ULSNames[0],
                            &lsbi, &lsbsize, lsbofs, &lsbbp);
    GetAccessToUniformBlock(program_id[0], 1, &UCPNames[0],
                            &cpbi, &i, cpbofs, &cpbbp);
    GetAccessToUniformBlock(program_id[0], 1, &UCPINames[0],
                            &cpibi, &i, cpibofs, &cpibbp);
    GetAccessToUniformBlock(program_id[0], 12, &UBezPatchNames[0],
                            &bezpbi, &bezpbsize, bezpbofs, &bezpbbp);
    GetAccessToUniformBlock(program_id[0], 1, &UTexCoordNames[0],
                            &txcbi, &i, txcofs, &txcbp);
    GetAccessToUniformBlock(program_id[0], 6, &UMatNames[0],
                            &matbi, &matbsize, matbofs, &matbbp);
    ucs_loc = glGetUniformLocation(program_id[0], ColourSourceName);
    uns_loc = glGetUniformLocation(program_id[0], NormalSourceName);
    LightProcLoc = glGetSubroutineUniformLocation(program_id[0],
                                                  GL_FRAGMENT_SHADER, LightProcName);
    LambertProcInd = glGetSubroutineIndex(program_id[0], GL_FRAGMENT_SHADER,
                                          LambertProcName);
    BlinnPhongProcInd = glGetSubroutineIndex(program_id[0], GL_FRAGMENT_SHADER,
                                             BlinnPhongProcName);
    LightProcInd = LambertProcInd;
    GetAccessToUniformBlock(program_id[5], 1, &UCompTrNames[0],
                            &ctrbi, &i, &i, &ctrbbp);
    GetAccessToUniformBlock(program_id[5], 1, &UCompTrIndNames[0],
                            &ctribi, &i, &i, &ctribbp);
    dim_loc = glGetUniformLocation(program_id[5], DimName);
    ncp_loc = glGetUniformLocation(program_id[5], NcpName);
    trnum_loc = glGetUniformLocation(program_id[5], TrNumName);

    trbuf = NewUniformBlockObject(trans_block_size, trbbp);
    lsbuf = NewUniformBlockObject(lsbsize, lsbbp);
    matbuf[1] = NewUniformBlockObject(matbsize, matbbp);
    matbuf[0] = NewUniformBlockObject(matbsize, matbbp);

    AttachUniformBlockToBindingPoint(program_id[1], UTBNames[0], trbbp);
    AttachUniformBlockToBindingPoint(program_id[1], UCPNames[0], cpbbp);
    AttachUniformBlockToBindingPoint(program_id[1], UCPINames[0], cpibbp);
    AttachUniformBlockToBindingPoint(program_id[1], UBezPatchNames[0], bezpbbp);
    AttachUniformBlockToBindingPoint(program_id[2], UTBNames[0], trbbp);
    AttachUniformBlockToBindingPoint(program_id[3], UTBNames[0], trbbp);
    AttachUniformBlockToBindingPoint(program_id[3], UCPNames[0], cpbbp);
    AttachUniformBlockToBindingPoint(program_id[3], UCPINames[0], cpibbp);
    AttachUniformBlockToBindingPoint(program_id[3], UBezPatchNames[0], bezpbbp);
    AttachUniformBlockToBindingPoint(program_id[4], UTBNames[0], trbbp);
    AttachUniformBlockToBindingPoint(program_id[5], UCPNames[0], cpbbp);
    ExitIfGLError("LoadMyShaders");
} /*LoadMyShaders*/

void SetVPMatrix(Matrix& vm, Matrix& pm, Vector4GLf& ep) {
    Matrix vpm = pm * vm;
    glBindBuffer(GL_UNIFORM_BUFFER, trbuf);
    glBufferSubData(GL_UNIFORM_BUFFER, trbofs[2], 16 * sizeof(GLfloat), vm);
    glBufferSubData(GL_UNIFORM_BUFFER, trbofs[3], 16 * sizeof(GLfloat), pm);
    glBufferSubData(GL_UNIFORM_BUFFER, trbofs[4], 16 * sizeof(GLfloat), vpm);
    glBufferSubData(GL_UNIFORM_BUFFER, trbofs[5], 4 * sizeof(GLfloat), ep.begin());
    ExitIfGLError("SetVPMatrix");
} /*SetVPMatrix*/

void SetModelMatrix(Matrix& mm, Matrix& mmti) {
    trans.mm = mm;
    trans.mmti = mmti;
    glBindBuffer(GL_UNIFORM_BUFFER, trbuf);
    glBufferSubData(GL_UNIFORM_BUFFER, trbofs[0], 16 * sizeof(GLfloat), trans.mm);
    glBufferSubData(GL_UNIFORM_BUFFER, trbofs[1], 16 * sizeof(GLfloat), trans.mmti);
    ExitIfGLError("SetModelMatrix");
} /*SetModelMatrix*/

void InitViewMatrix() {
    trans.eyepos = viewer_pos0;
    trans.wvm = M4x4Translate(-viewer_pos0.to_vector3());
    SetupMirrorVPMatrices(trans.eyepos, trans.reyepos, trans.mvm, trans.mpm);
} /*InitViewMatrix*/

void RotateViewer(double delta_xi, double delta_eta) {
    if (are_close(delta_xi, 0) && are_close(delta_eta, 0)) {
        return;
    }  /* natychmiast uciekamy - nie chcemy dzielic przez 0 */
    Vector3f vi = {(float) delta_eta * (right - left) / (float) win_height,
                   (float) delta_xi * (top - bottom) / (float) win_width, 0.0};
    float length = vi.length();
    vi[0] /= length;
    vi[1] /= length;

    float angi = -0.052359878f;  /* -3 stopnie */

    std::tie(viewer_rvec, viewer_rangle) =
      V3ComposeRotations(viewer_rvec, viewer_rangle, vi, angi);
    auto rm = M4x4CustomRotation(viewer_rvec[2],-viewer_rangle);
    trans.wvm = M4x4Translate(-viewer_pos0.to_vector3()) * rm;
    trans.eyepos = rm.transpose() * viewer_pos0;
    SetupMirrorVPMatrices(trans.eyepos, trans.reyepos, trans.mvm, trans.mpm);
} /*RotateViewer*/

void InitMyObject() {
    struct tms clk{};
    SetModelMatrix(ident_matrix, ident_matrix);
    InitViewMatrix();
    ConstructBezierPatchDomain();
    mylinkage = ConstructMyLinkage();
    ConstructMirrorFBO();
    ConstructMirrorVAO();
    InitLights();
    LoadMyTextures();
    clocks_per_sec = (float) sysconf(_SC_CLK_TCK);
    app_clock0 = times(&clk);
    app_time0 = teapot_time0 = 0.0;
    ArticulateMyLinkage(app_time0);
} /*InitMyObject*/

void LoadMyTextures() {
    mytexture = CreateMyTexture(1024);
    LoadMyTextureImage(mytexture, 1024, 1024, 0, 0, "jaszczur.tif");
    LoadMyTextureImage(mytexture, 1024, 1024, 512, 0, "salamandra.tif");
    LoadMyTextureImage(mytexture, 1024, 1024, 0, 512, "lis.tif");
    LoadMyTextureImage(mytexture, 1024, 1024, 512, 512, "kwiatki.tif");
    SetupMyTextureMipmaps(mytexture);
} /*LoadMyTextures*/

void InitLights() {
    GLfloat amb0[4] = {0.2, 0.2, 0.3, 1.0};
    GLfloat dif0[4] = {0.8, 0.8, 0.8, 1.0};
    GLfloat pos0[4] = {-0.2, 1.0, 1.0, 0.0};
    GLfloat atn0[3] = {1.0, 0.0, 0.0};
    GLfloat csc[3] = {0.0, 0.0, 0.0};

    memset(&light, 0, sizeof(LightBl));
    SetLightAmbient(0, amb0);
    SetLightDiffuse(0, dif0);
    SetLightPosition(0, pos0);
    SetLightAttenuation(0, atn0);
    SetLightOnOff(0, 1);
    ConstructShadowTxtFBO(0);
    SetupShadowTxtTransformations(0, csc, 2.2);
    UpdateShadowMatrix(0);
} /*InitLights*/

void DrawScene(char final, GLuint programs[3]) {
    DrawMyTeapot(final, programs[0]);
    if (cnet) {
        DrawMyTeapotCNet(programs[1]);
    }
    DrawMyTorus(final, programs[0]);
    if (cnet) {
        DrawMyTorusCNet(programs[1]);
    }
} /*DrawScene*/

void DrawSceneToMirror() {
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, mirror_fbo);
    glViewport(0, 0, MIRRORTXT_W, MIRRORTXT_H);
    SetVPMatrix(trans.mvm, trans.mpm, trans.reyepos);
    glClearColor(0.95, 0.95, 0.95, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    DrawScene(1, progid1);
    glFlush();
} /*DrawSceneToMirror*/

void DrawSceneToShadows() {
    int l;
    GLuint mask;

    glViewport(0, 0, SHADOW_MAP_SIZE, SHADOW_MAP_SIZE);
    glEnable(GL_POLYGON_OFFSET_FILL);
    glPolygonOffset(2.0f, 4.0f);
    for (l = 0, mask = 0x00000001; l < light.light_count; l++, mask <<= 1) {
        if (light.shmask & mask) {
            BindShadowTxtFBO(l);
            glClear(GL_DEPTH_BUFFER_BIT);
            DrawScene(0, progid0);
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            DrawMirror(0, progid0[2]);
        }
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glDisable(GL_POLYGON_OFFSET_FILL);

    for (l = 0, mask = 0x00000001; l < light.light_count; l++, mask <<= 1) {
        if (light.shmask & mask) {
            glActiveTexture(GL_TEXTURE2 + l);
            glBindTexture(GL_TEXTURE_2D, light.ls[l].shadow_txt);
        }
    }
} /*DrawSceneToShadows*/

void DrawSceneToWindow() {
    glEnable(GL_DEPTH_TEST);
    DrawSceneToShadows();
    DrawSceneToMirror();
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glViewport(0, 0, win_width, win_height);
    SetVPMatrix(trans.wvm, trans.wpm, trans.eyepos);
    glClearColor(1.0, 1.0, 1.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    DrawMirror(1, progid1[2]);
    DrawScene(1, progid1);
}

kl_linkage* mylinkage() {
    static kl_linkage linkage;
    return &linkage;
}
/*DrawSceneToWindow*/

