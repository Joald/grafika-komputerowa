#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <cmath>
#include "myglheader.h"

#include "../utilities/utilities.h"
#include "lights.h"

GLuint lsbi, lsbuf, lsbbp;

GLint lsbsize, lsbofs[9];

LightBl light;

GLuint matbi, matbuf[MAX_MATERIALS], matbbp;

GLint matbsize, matbofs[6];

void SetLightAmbient(int l, Vector4GLf& amb) {
    if (l < 0 || l >= MAX_NLIGHTS) {
        return;
    }
    light.ls[l].ambient = amb;
    GLint ofs = l * (lsbofs[8] - lsbofs[3]) + lsbofs[3];
    glBindBuffer(GL_UNIFORM_BUFFER, lsbuf);
    glBufferSubData(GL_UNIFORM_BUFFER, ofs, 4 * sizeof(GLfloat), amb);
    ExitIfGLError("SetLightAmbient");
} /*SetLightAmbient*/

void SetLightDiffuse(int l, Vector4GLf& dif) {
    if (l < 0 || l >= MAX_NLIGHTS) {
        return;
    }
    light.ls[l].diffuse = dif;
    GLint ofs = l * (lsbofs[8] - lsbofs[3]) + lsbofs[4];
    glBindBuffer(GL_UNIFORM_BUFFER, lsbuf);
    glBufferSubData(GL_UNIFORM_BUFFER, ofs, 4 * sizeof(GLfloat), dif);
    ExitIfGLError("SetLightDiffuse");
} /*SetLightDiffuse*/

void SetLightPosition(int l, Vector4GLf& pos) {
    if (l < 0 || l >= MAX_NLIGHTS) {
        return;
    }
    light.ls[l].position = pos;
    GLint ofs = l * (lsbofs[8] - lsbofs[3]) + lsbofs[5];
    glBindBuffer(GL_UNIFORM_BUFFER, lsbuf);
    glBufferSubData(GL_UNIFORM_BUFFER, ofs, 4 * sizeof(GLfloat), pos);
    ExitIfGLError("SetLightPosition");
} /*SetLightPosition*/

void SetLightAttenuation(int l, Vector3GLf& at3) {
    if (l < 0 || l >= MAX_NLIGHTS) {
        return;
    }
    light.ls[l].attenuation = at3;
    GLint ofs = l * (lsbofs[8] - lsbofs[3]) + lsbofs[6];
    glBindBuffer(GL_UNIFORM_BUFFER, lsbuf);
    glBufferSubData(GL_UNIFORM_BUFFER, ofs, 3 * sizeof(GLfloat), at3);
    ExitIfGLError("SetLightAttenuation");
} /*SetLightAttenuation*/

void SetLightOnOff(int l, char on) {
    if (l < 0 || l >= MAX_NLIGHTS) {
        return;
    }
    GLuint mask = 1u << l;
    if (on) {
        light.mask |= mask;
        if (l >= light.light_count) {
            light.light_count = l + 1u;
        }
    } else {
        light.mask &= ~mask;
        for (mask = 1u << (light.light_count - 1); mask; mask >>= 1) {
            if (light.mask & mask) {
                break;
            } else {
                light.light_count--;
            }
        }
    }
    glBindBuffer(GL_UNIFORM_BUFFER, lsbuf);
    glBufferSubData(GL_UNIFORM_BUFFER, lsbofs[0], sizeof(GLuint), &light.light_count);
    glBufferSubData(GL_UNIFORM_BUFFER, lsbofs[1], sizeof(GLuint), &light.mask);
    ExitIfGLError("SetLightOnOff");
} /*SetLightOnOff*/

void SetupMaterial(int m, const Vector4GLf& ambr, const Vector4GLf& diffr,
                   const Vector4GLf& specr,
                   GLfloat shn, GLfloat wa, GLfloat we) {
    if (m >= 0 && m < MAX_MATERIALS) {
        matbuf[m] = NewUniformBlockObject(matbsize, matbbp);
        glBindBuffer(GL_UNIFORM_BUFFER, matbuf[m]);
        glBufferSubData(GL_UNIFORM_BUFFER, matbofs[0], 4 * sizeof(GLfloat), ambr);
        glBufferSubData(GL_UNIFORM_BUFFER, matbofs[1], 4 * sizeof(GLfloat), diffr);
        glBufferSubData(GL_UNIFORM_BUFFER, matbofs[2], 4 * sizeof(GLfloat), specr);
        glBufferSubData(GL_UNIFORM_BUFFER, matbofs[3], sizeof(GLfloat), &shn);
        glBufferSubData(GL_UNIFORM_BUFFER, matbofs[4], sizeof(GLfloat), &wa);
        glBufferSubData(GL_UNIFORM_BUFFER, matbofs[5], sizeof(GLfloat), &we);
        ExitIfGLError("SetupMaterial");
    }
} /*SetupMaterial*/

void ChooseMaterial(int m) {
    if (m >= 0 && m < MAX_MATERIALS) {
        glBindBufferBase(GL_UNIFORM_BUFFER, matbbp, matbuf[m]);
        ExitIfGLError("ChooseMaterial");
    }
} /*ChooseMaterial*/

void DeleteMaterial(int m) {
    if (m >= 0 && m < MAX_MATERIALS) {
        glDeleteBuffers(1, &matbuf[m]);
        ExitIfGLError("DeleteMaterial");
    }
} /*DeleteMaterial*/

/* ////////////////////////////////////////////////////////////////////////// */
extern GLuint program_id[];

void ConstructShadowTxtFBO(int l) {
    GLuint fbo, txt;

    if (l < 0 || l >= MAX_NLIGHTS) {
        return;
    }
    glGenTextures(1, &txt);
    glGenFramebuffers(1, &fbo);
    if ((light.ls[l].shadow_txt = txt) &&
        (light.ls[l].shadow_fbo = fbo)) {
        glActiveTexture(GL_TEXTURE2 + l);
        glBindTexture(GL_TEXTURE_2D, txt);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32,
                     SHADOW_MAP_SIZE, SHADOW_MAP_SIZE, 0,
                     GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE,
                        GL_COMPARE_REF_TO_TEXTURE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glBindTexture(GL_TEXTURE_2D, 0);
        glBindFramebuffer(GL_FRAMEBUFFER, fbo);
        glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                             light.ls[l].shadow_txt, 0);
        glDrawBuffer(GL_NONE);
        light.shmask |= 0x01 << l;
        glBindBuffer(GL_UNIFORM_BUFFER, lsbuf);
        glBufferSubData(GL_UNIFORM_BUFFER, lsbofs[2], sizeof(GLuint), &light.shmask);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        ExitIfGLError("ConstructShadowTxtFBO");
    }
} /*ConstructShadowTxtFBO*/

static void SetupShadowViewerTrans(Vector3GLf& org, const Vector3GLf& zv,
                                   Matrix& vmat) {
    Vector3GLf v = zv;
    /* skonstruuj odbicie Householdera */
    GLfloat norm = v.length();
    v[2] += v[2] > 0.0 ? norm : -norm;
    GLfloat gamma = 2.0f / v.length_sq();
    /* odbij kolumny macierzy jednostkowej */
    auto a = Matrix::identity();
    for (int i = 0; i < 2; i++) {
        GLfloat multiplier = v[i] * gamma;
        for (int j = 0; j < 3; j++) {
            a[4 * i + j] -= multiplier * v[j];
        }
    }
    a[8] = zv[0] / norm;
    a[9] = zv[1] / norm;
    a[10] = zv[2] / norm;
    std::copy(org.begin(), org.begin() + 3, &a[12]);
    vmat = M4x4InvertAffineIsometry(a);
} /*SetupShadowViewerTrans*/

void UpdateShadowMatrix(int l) {
    Matrix b = {0.5, 0.0, 0.0, 0.0,
                0.0, 0.5, 0.0, 0.0,
                0.0, 0.0, 0.5, 0.0,
                0.5, 0.5, 0.5, 1.0};
    Matrix lvpm = b * (light.ls[l].shadow_proj * light.ls[l].shadow_view);
    int ofs = l * (lsbofs[8] - lsbofs[3]) + lsbofs[7];
    glBindBuffer(GL_UNIFORM_BUFFER, lsbuf);
    glBufferSubData(GL_UNIFORM_BUFFER, ofs, 16 * sizeof(GLfloat), lvpm);
    ExitIfGLError("UpdateShadowMatrix");
} /*UpdateShadowMatrix*/

void SetupShadowTxtTransformations(int l, Vector3f& sc, float R) {
    Vector3GLf light_position, v;
    if (l < 0 or l >= MAX_NLIGHTS or !light.ls[l].shadow_txt) {
        return;
    }
    auto& lvm = light.ls[l].shadow_view;
    auto& lpm = light.ls[l].shadow_proj;
    if (light.ls[l].position[3] != 0.0) {
        /* zrodlo swiatla w skonczonej odleglosci */
        for (int i = 0; i < 3; i++) {
            light_position[i] = light.ls[l].position[i] / light.ls[l].position[3];
            v[i] = light_position[i] - sc[i];
        }
        SetupShadowViewerTrans(light_position, v, lvm);
        GLfloat d = v.length_sq();
        if (d > 2.0 * R * R) {
            d = std::sqrt(d);
            GLfloat n = d - R;
            GLfloat s = R / d;                  /* sin(alpha) */
            GLfloat t = s / std::sqrt(1.0f - s * s);  /* tg(alpha)*/
            lpm = M4x4ToStandardCubeFrustum(-n * t, n * t, -n * t, n * t, n, d + R);
        } else {
            GLfloat n = 0.4142f * R;
            lpm = M4x4ToStandardCubeFrustum(-n, n, -n, n, n, 2.4143f * R);
        }
    } else {  /* zrodlo swiatla w odleglosci nieskonczonej */
        light_position = sc;

        v = light.ls[l].position.to_vector3();
        SetupShadowViewerTrans(light_position, v, lvm);
        lpm = M4x4ToStandardCubeParallel(-R, R, -R, R, -R, R);
    }
} /*SetupShadowTxtTransformations*/

void BindShadowTxtFBO(int l) {
    if (l < 0 or l >= MAX_NLIGHTS or light.ls[l].shadow_txt == 0) {
        return;
    }
    SetVPMatrix(light.ls[l].shadow_view, light.ls[l].shadow_proj,
                light.ls[l].position);
    glBindFramebuffer(GL_FRAMEBUFFER, light.ls[l].shadow_fbo);
    ExitIfGLError("BindShadowTxtFBO");
} /*BindShadowTxtFBO*/

void DestroyShadowFBO() {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    for (auto& l : light.ls) {
        if (l.shadow_fbo != 0)
            glDeleteFramebuffers(1, &l.shadow_fbo);
        if (l.shadow_txt != 0) {
            glDeleteTextures(1, &l.shadow_txt);
        }
    }
    ExitIfGLError("DestroyShadowFBO");
} /*DestroyShadowFBO*/

