#ifndef LIGHTS_H
#define LIGHTS_H

#include "app2h.h"

#define SHADOW_MAP_SIZE 1024

#define MAX_NLIGHTS   8
#define MAX_MATERIALS 8

typedef struct LSPar {
    Vector4GLf ambient;
    Vector4GLf diffuse;
    Vector4GLf position;
    Vector3GLf attenuation;
    GLuint shadow_fbo, shadow_txt;
    Matrix shadow_view, shadow_proj;
} LSPar;

typedef struct LightBl {
    GLuint light_count, mask, shmask;
    LSPar ls[MAX_NLIGHTS];
} LightBl;

extern GLuint lsbi, lsbuf, lsbbp;

extern GLint lsbsize, lsbofs[9];

extern LightBl light;

extern GLuint matbi, matbuf[MAX_MATERIALS], matbbp;

extern GLint matbsize, matbofs[6];


void SetLightAmbient(int l, Vector4GLf& amb);
void SetLightDiffuse(int l, Vector4GLf& dif);
void SetLightPosition(int l, Vector4GLf& pos);
void SetLightAttenuation(int l, Vector3GLf& at3);
void SetLightOnOff(int l, char on);

void SetupMaterial(int m, const Vector4GLf& ambr, const Vector4GLf& diffr,
                   const Vector4GLf& specr,
                   GLfloat shn, GLfloat wa, GLfloat we);
void ChooseMaterial(int m);
void DeleteMaterial(int m);

void ConstructShadowTxtFBO(int l);
void UpdateShadowMatrix(int l);
void SetupShadowTxtTransformations(int l, Vector3f& sc, float R);
void BindShadowTxtFBO(int l);
void DestroyShadowFBO();

#endif // LIGHTS_H