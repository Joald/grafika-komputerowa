
#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <cmath>

#include "myglheader.h"
#include <GL/glu.h>

#include "../utilities/utilities.h"
#include "linkage.h"

/* ///////////////////////////////////////////////////////////////////////// */
kl_linkage* kl_NewLinkage(int maxo, int maxl, int maxr,
                          int maxj, int maxp, void* usrdata) {
    auto* lkg = new kl_linkage();
    lkg->max_geometric_objects = maxo;
    lkg->max_links = maxl;
    lkg->max_object_refs = maxr;
    lkg->max_half_joints = 2 * maxj;
    lkg->max_articulation_parameters = maxp;
    lkg->objects = new kl_object[maxo];
    lkg->links = new kl_link[maxl];
    lkg->object_refs = new kl_object_ref[maxr];
    lkg->half_joints = new kl_half_joint[2 * maxj];
    lkg->articulation_parameters = new GLfloat[2 * maxp];
    lkg->prevartp = &lkg->articulation_parameters[maxp];
    memset(lkg->articulation_parameters, 0, maxp * sizeof(GLfloat));
    memset(lkg->prevartp, 0x7f, maxp * sizeof(GLfloat));
    lkg->current_root = 0;
    lkg->current_root_tr = M4x4Identity();
    lkg->usrdata = usrdata;

    return lkg;
} /*kl_NewLinkage*/

void kl_DestroyLinkage(kl_linkage* linkage) {
    if (linkage->objects) {
        for (int i = 0; i < linkage->geometric_object_count; i++) {
            if (linkage->objects[i].destroy) {
                linkage->objects[i].destroy(&linkage->objects[i]);
            }
        }
        delete linkage->objects;
    }
    if (linkage->object_refs) {
        for (int i = 0; i < linkage->object_refcount; i++) {
            delete linkage->object_refs[i].vn;
        }
        delete linkage->object_refs;
    }
    delete linkage->links;
    delete linkage->half_joints;
    delete linkage->articulation_parameters;
    delete linkage;
} /*kl_DestroyLinkage*/

/* ///////////////////////////////////////////////////////////////////////// */
void kl_DefaultTransform(kl_object* obj, int refn, Matrix& tr,
                         int nv, int* vn) {
    int k;
    auto& vert = obj->vert;
    auto& tvert = obj->tvert;
    if (!vert or !tvert) {
        return;
    }
    if (vn) {
        switch (obj->vertex_coordinate_count) {
            case 3:
                for (int i = 0; i < nv; i++) {
                    k = 3 * vn[i];
                    tvert[k] = M4x4MultMP3(tr, vert[k].to_vector3());
                }
                break;
            case 4:
                for (int i = 0; i < nv; i++) {
                    k = 4 * vn[i];
                    tvert[k] = tr * vert[k].to_vector3();
                }
                break;
            default:
                return;
        }
    } else {
        switch (obj->vertex_coordinate_count) {
            case 3:
                for (int i = k = 0; i < nv; i++, k += 3) {
                    tvert[k] = M4x4MultMP3(tr, vert[k].to_vector3());
                }
                break;
            case 4:
                for (int i = k = 0; i < nv; i++, k += 4) {
                    tvert[k] = tr * vert[k];
                }
                break;
            default:
                return;
        }
    }
} /*kl_DefaultTransform*/

void kl_obj_stub(kl_object* obj) { UNUSED(obj); } /*kl_obj_stub*/

int kl_NewObject(kl_linkage* linkage, int nvc, int nvert,
                 const Matrix& etrans, void* usrdata,
                 kl_obj_init init,
                 kl_obj_transform transform,
                 kl_obj_postprocess postprocess,
                 kl_obj_redraw redraw,
                 kl_obj_destroy destroy) {
    if (linkage->geometric_object_count >= linkage->max_geometric_objects) {
        return -1;
    }
    int on = linkage->geometric_object_count;
    kl_object* obj = &linkage->objects[on];
    if (etrans) {
        memcpy(obj->Etr, etrans, 16 * sizeof(GLfloat));
    } else {
        obj->Etr = Matrix::identity();
    }
    obj->transform = transform != nullptr ? transform : kl_DefaultTransform;
    obj->postprocess = postprocess != nullptr ? postprocess : kl_obj_stub;
    obj->redraw = redraw != nullptr ? redraw : kl_obj_stub;
    obj->destroy = destroy != nullptr ? destroy : kl_obj_stub;
    obj->vertex_coordinate_count = nvc;
    obj->vertex_count = nvert;
    obj->vert = obj->tvert = nullptr;
    obj->usrdata = usrdata;
    if (!init || init(obj, usrdata)) {
        linkage->geometric_object_count++;
        return on;
    }

    return -1;
} /*kl_NewObject*/

int kl_NewLink(kl_linkage* linkage) {
    if (linkage->link_count >= linkage->max_links) {
        return -1;
    }
    int lkn = linkage->link_count++;
    kl_link* lk = &linkage->links[lkn];
    memset(lk, 0, sizeof(kl_link));
    lk->first_obj_ref = lk->first_half_joint = -1;
    lk->tag = 0;
    return lkn;
} /*kl_NewLink*/

int kl_NewObjRef(kl_linkage* linkage, int lkn, int on, int nv, int* vn) {
    if (linkage->object_refcount < linkage->max_object_refs) {
        int orn = linkage->object_refcount++;
        auto* oref = &linkage->object_refs[orn];
        memset(oref, 0, sizeof(kl_object_ref));
        oref->object_num = on;
        oref->vertex_count = nv;
        if (vn) {
            oref->vn = new int[nv];
            memcpy(oref->vn, vn, nv * sizeof(int));
        } else {
            oref->vn = nullptr;
        }
        oref->next_ref = linkage->links[lkn].first_obj_ref;
        linkage->links[lkn].first_obj_ref = orn;
        return orn;
    }
    return -1;
} /*kl_NewObjRef*/

int kl_NewJoint(kl_linkage* linkage, int l0, int l1, int art, int pnum) {
    int jn0, jn1;
    kl_half_joint* hj0, * hj1;

    if (linkage->half_joint_count < linkage->max_half_joints - 1 &&
        l0 < linkage->link_count && l1 < linkage->link_count) {
        jn0 = linkage->half_joint_count++;
        jn1 = linkage->half_joint_count++;
        hj0 = &linkage->half_joints[jn0];
        hj1 = &linkage->half_joints[jn1];
        memset(hj0, 0, 2 * sizeof(kl_half_joint));
        hj0->other_half = jn1;
        hj1->other_half = jn0;
        hj0->link_begin = hj1->link_end = l0;
        hj0->link_end = hj1->link_begin = l1;
        hj0->articulation_method = art;
        hj0->articulation_parameter = pnum;
        hj1->articulation_method = art == KL_ART_NONE ? KL_ART_NONE : KL_ART_OTHERHALF;
        hj1->articulation_parameter = -1;
        hj0->next = linkage->links[l0].first_half_joint;
        linkage->links[l0].first_half_joint = jn0;
        hj1->next = linkage->links[l1].first_half_joint;
        linkage->links[l1].first_half_joint = jn1;
        hj0->front_location_transform = Matrix::identity();
        hj0->articulation_transform = Matrix::identity();
        hj0->back_location_transform = Matrix::identity();
        hj1->front_location_transform = Matrix::identity();
        hj1->articulation_transform = Matrix::identity();
        hj1->back_location_transform = Matrix::identity();
        return jn0;
    }
    return -1;
} /*kl_NewJoint*/

/* ///////////////////////////////////////////////////////////////////////// */
void kl_SetJointFrontLocationTransform(kl_linkage* linkage, int jn, Matrix& tr, char back) {
    if (jn < 0 || jn >= linkage->half_joint_count) {
        return;
    }
    kl_half_joint* hj = &linkage->half_joints[jn];
    kl_half_joint* hj1 = &linkage->half_joints[hj->other_half];
    hj->front_location_transform = tr;
    auto tr_inv = tr.invert();
    if (tr_inv.has_value()) hj1->back_location_transform = tr_inv.value();
    if (back) {
        hj->back_location_transform = hj1->back_location_transform;
        hj1->front_location_transform = hj->front_location_transform;
    }
} /*kl_SetJointFtr*/

void kl_SetJointBackLocationTransform(kl_linkage* linkage, int jn, Matrix& tr, char front) {
    if (jn < 0 || jn >= linkage->half_joint_count) {
        return;
    }
    kl_half_joint* hj = &linkage->half_joints[jn];
    kl_half_joint* hj1 = &linkage->half_joints[hj->other_half];
    hj->back_location_transform = tr;
    hj1->front_location_transform = tr;
    if (front) {
        hj->front_location_transform = hj1->front_location_transform;
        hj1->back_location_transform = hj->back_location_transform;
    }
} /*kl_SetJointBtr*/

/* ///////////////////////////////////////////////////////////////////////// */
void kl_SetArtParam(kl_linkage* linkage, int pno, int nump, GLfloat* par) {
    if (nump > 0 && pno >= 0 && pno + nump <= linkage->max_articulation_parameters) {
        memcpy(&linkage->articulation_parameters[pno], par, nump * sizeof(GLfloat));
        if (pno + nump > linkage->articulation_parameter_count) {
            linkage->articulation_parameter_count = pno + nump;
        }
    }
} /*kl_SetArtParam*/

static char _kl_changed(GLfloat* a, GLfloat* b, int i, int n) {
    return memcmp(&a[i], &b[i], n * sizeof(GLfloat)) != 0;
} /*_kl_changed*/

static void _kl_UpdateArtTr(kl_linkage* linkage, int jn) {
    GLfloat* artp = linkage->articulation_parameters;
    GLfloat* prevartp = linkage->prevartp;
    kl_half_joint* hj = &linkage->half_joints[jn];
    if (hj->articulation_method == KL_ART_OTHERHALF) {
        return;
    }
    kl_half_joint* hj1 = &linkage->half_joints[hj->other_half];
    int pnum = hj->articulation_parameter;
    switch (hj->articulation_method) {
        case KL_ART_NONE:
            break;

        case KL_ART_TRANS_X:
            if (_kl_changed(artp, prevartp, pnum, 1)) {
                hj->articulation_transform = M4x4Translate({artp[pnum], 0.0, 0.0});
                hj1->articulation_transform = M4x4Translate({-artp[pnum], 0.0, 0.0});
            }
            break;

        case KL_ART_TRANS_Y:
            if (_kl_changed(artp, prevartp, pnum, 1)) {
                hj->articulation_transform = M4x4Translate({0.0, artp[pnum], 0.0});
                hj1->articulation_transform = M4x4Translate({0.0, -artp[pnum], 0.0});
            }
            break;

        case KL_ART_TRANS_Z:
            if (_kl_changed(artp, prevartp, pnum, 1)) {
                hj->articulation_transform = M4x4Translate({0.0, 0.0, artp[pnum]});
                hj1->articulation_transform = M4x4Translate({0.0, 0.0, -artp[pnum]});
            }
            break;

        case KL_ART_TRANS_XYZ:
            if (_kl_changed(artp, prevartp, pnum, 3)) {
                M4x4Translate(artp[pnum + 2]);
                M4x4Translate(-artp[pnum + 2]);
            }
            break;

        case KL_ART_SCALE_X:
            if (_kl_changed(artp, prevartp, pnum, 1)) {
                M4x4Scale(Vector3f());
                M4x4Scale(Vector3f());
            }
            break;

        case KL_ART_SCALE_Y:
            if (_kl_changed(artp, prevartp, pnum, 1)) {
                M4x4Scale(Vector3f());
                M4x4Scale(Vector3f());
            }
            break;

        case KL_ART_SCALE_Z:
            if (_kl_changed(artp, prevartp, pnum, 1)) {
                M4x4Scale(Vector3f());
                M4x4Scale(Vector3f());
            }
            break;

        case KL_ART_SCALE_XYZ:
            if (_kl_changed(artp, prevartp, pnum, 3)) {
                M4x4Scale(Vector3f());
                M4x4Scale(Vector3f());
            }
            break;

        case KL_ART_ROT_X:
            if (_kl_changed(artp, prevartp, pnum, 1)) {
                M4x4RotationX(artp[pnum]);
                M4x4RotationX(-artp[pnum]);
            }
            break;

        case KL_ART_ROT_Y:
            if (_kl_changed(artp, prevartp, pnum, 1)) {
                M4x4RotationY(artp[pnum]);
                M4x4RotationY(-artp[pnum]);
            }
            break;

        case KL_ART_ROT_Z:
            if (_kl_changed(artp, prevartp, pnum, 1)) {
                hj->articulation_transform = M4x4Rotation(Axis::Z, artp[pnum]);
                hj1->articulation_transform = M4x4Rotation(Axis::Z - artp[pnum]);
            }
            break;

        case KL_ART_ROT_V:
            if (_kl_changed(artp, prevartp, pnum, 4)) {
                M4x4CustomRotation(artp[pnum + 2],
                                   artp[pnum + 3]);
                M4x4Transposef(hj1->articulation_transform, hj->articulation_transform);
            }
            break;

        default:
            hj->articulation_transform = Matrix::identity();
            hj1->articulation_transform = Matrix::identity();
            break;
    }
} /*_kl_UpdateArtTr*/

static void _kl_rArticulate(kl_linkage* linkage, int lkn, GLfloat* tr) {
    kl_link* lk;
    GLfloat t0[16], t1[16];
    int r, on, j, l1;
    kl_object* obj;
    kl_object_ref* oref;
    kl_half_joint* hj;

    obj = linkage->objects;
    oref = linkage->object_refs;
    hj = linkage->half_joints;
    lk = &linkage->links[lkn];
    lk->tag = 1;
    /* pass through the list of object references and transform */
    /* the object vertices */
    for (r = lk->first_obj_ref; r >= 0; r = oref[r].next_ref) {
        on = oref[r].object_num;
        M4x4Multiplication(t1, tr, obj[on].Etr);
        obj[on].transform(&obj[on], r, t1, oref[r].vertex_count, oref[r].vn);
    }
    /* now traverse the list of halfjoints originating at */
    /* the current link to (DFS) search the linkage graph */
    for (j = lk->first_half_joint; j >= 0; j = hj[j].next) {
        l1 = hj[j].link_end;
        if (!linkage->links[l1].tag) {
            M4x4Multiplication(t0, tr, hj[j].front_location_transform);
            M4x4Multiplication(t1, t0, hj[j].articulation_transform);
            M4x4Multiplication(t0, t1, hj[j].back_location_transform);
            _kl_rArticulate(linkage, l1, t0);
        }
    }
} /*_kl_rArticulate*/

void kl_Articulate(kl_linkage* linkage) {
    for (int i = 0; i < linkage->link_count; i++) {
        linkage->links[i].tag = 0;
    }
    for (int i = 0; i < linkage->half_joint_count; i++) {
        _kl_UpdateArtTr(linkage, i);
    }
    _kl_rArticulate(linkage, linkage->current_root, linkage->current_root_tr);
    for (int i = 0; i < linkage->geometric_object_count; i++) {
        linkage->objects[i].postprocess(&linkage->objects[i]);
    }
    memcpy(linkage->prevartp, linkage->articulation_parameters, linkage->max_articulation_parameters * sizeof(GLfloat));
} /*kl_Articulate*/

void kl_Redraw(kl_linkage* linkage) {
    for (int i = 0; i < linkage->geometric_object_count; i++) {
        linkage->objects[i].redraw(&linkage->objects[i]);
    }
} /*kl_Redraw*/

