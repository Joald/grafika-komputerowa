#ifndef LINKAGE_H
#define LINKAGE_H

#include "../utilities/utilities.h"

/* possible articulation methods */
enum ArticulationMethod {
    KL_ART_OTHERHALF = -1,
    KL_ART_NONE = 0,
    KL_ART_TRANS_X = 1,
    KL_ART_TRANS_Y = 2,
    KL_ART_TRANS_Z = 3,
    KL_ART_TRANS_XYZ = 4,
    KL_ART_SCALE_X = 5,
    KL_ART_SCALE_Y = 6,
    KL_ART_SCALE_Z = 7,
    KL_ART_SCALE_XYZ = 8,
    KL_ART_ROT_X = 9,
    KL_ART_ROT_Y = 10,
    KL_ART_ROT_Z = 11,
    KL_ART_ROT_V = 12,
};

typedef struct kl_object {
    int vertex_coordinate_count; /* 3 (Carthesian) or 4 (uniform) */
    int vertex_count; /* number of vertices */
    Vector4GLf* vert;
    Vector4GLf* tvert;         /* pointers to vertices */
    Matrix Etr;               /* extra object transformation */
    void* usrdata;              /* pointer to object specific data */
    /* object specific procedures */
    void (* transform)(struct kl_object*, int, Matrix&, int, int*);
    void (* postprocess)(struct kl_object*);
    void (* redraw)(struct kl_object*);        /* drawing */
    void (* destroy)(struct kl_object*);       /* deallocation */
} kl_object;

typedef char (* kl_obj_init)(kl_object*, void*);

typedef void (* kl_obj_transform)(kl_object*, int, Matrix&, int, int*);

typedef void (* kl_obj_postprocess)(kl_object*);

typedef void (* kl_obj_redraw)(kl_object*);

typedef void (* kl_obj_destroy)(kl_object*);

struct kl_object_ref {
    int object_num;     /* object number */
    int next_ref;  /* next reference */
    int vertex_count;     /* number of associated vertices */
    int* vn;    /* vertex numbers */
};

struct kl_link {
    int first_obj_ref;      /* first object reference */
    int first_half_joint;       /* first half-joint */
    char tag;
};

struct kl_half_joint {
    int link_begin, link_end;     /* numbers of links - beginning and end */
    int other_half;  /* number of the other half */
    int next;     /* next halfjoint */
    int articulation_parameter;       /* number of articulation parameter */
    ArticulationMethod articulation_method;        /* type of articulation */
    Matrix front_location_transform;    /* front location transformation */
    Matrix articulation_transform;    /* articulation transformation */
    Matrix back_location_transform;    /* back location transformation */
};

struct kl_linkage {
    int max_geometric_objects, geometric_object_count;
    int max_object_refs, object_refcount;
    int max_links, link_count;
    int max_half_joints, half_joint_count;
    int max_articulation_parameters, articulation_parameter_count;
    std::vector<kl_object> objects;
    std::vector<kl_object_ref> object_refs;
    std::vector<kl_link> links;
    std::vector<kl_half_joint> half_joints;
    std::vector<GLfloat> articulation_parameters; // 0-4 parameters
    GLfloat* prevartp;
    int current_root;
    Matrix current_root_tr;
    void* usrdata;
};

kl_linkage* kl_NewLinkage(int maxo, int maxl, int maxr,
                          int maxj, int maxp, void* usrdata);
void kl_DestroyLinkage(kl_linkage* linkage);

int kl_NewObject(kl_linkage* linkage, int nvc, int nvert,
                 const Matrix& etrans, void* usrdata,
                 kl_obj_init init,
                 kl_obj_transform transform,
                 kl_obj_postprocess postprocess,
                 kl_obj_redraw redraw,
                 kl_obj_destroy destroy);
int kl_NewLink(kl_linkage* linkage);
int kl_NewObjRef(kl_linkage* linkage, int lkn, int on, int nv, int* vn);
int kl_NewJoint(kl_linkage* linkage, int l0, int l1, int art, int pnum);
void kl_SetJointFrontLocationTransform(kl_linkage* linkage, int jn, Matrix& tr, char back);
void kl_SetJointBackLocationTransform(kl_linkage* linkage, int jn, Matrix& tr, char front);

void kl_SetArtParam(kl_linkage* linkage, int pno, int nump, GLfloat* par);
void kl_Articulate(kl_linkage* linkage);
void kl_Redraw(kl_linkage* linkage);

void kl_DefaultTransform(kl_object* obj, int refn, Matrix& tr,
                         int nv, int* vn);

/* stub procedures */
void kl_obj_stub(kl_object* obj);

#endif // LINKAGE_H