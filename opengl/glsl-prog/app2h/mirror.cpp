
#include <cstdlib>
#include <unistd.h>
#include <cstring>
#include <cstdio>
#include <sys/times.h>
#include <cmath>
#include "myglheader.h"
#include <GLFW/glfw3.h>

#include "../utilities/utilities.h"
#include "linkage.h"
#include "texture.h"
#include "bezpatches.h"
#include "mirror.h"
#include "app2h.h"

GLuint mirror_fbo, mirror_txt[2];

GLuint mirror_vao, mirror_vbo;

static const Vector3GLf mvertpos[4] =
  {{1.5, -1.2f, -1.0f},
   {1.5, 1.2,   -1.0f},
   {1.5, 1.2,   1.0},
   {1.5, -1.2f, 1.0}};

void ConstructMirrorFBO() {
    glGenTextures(2, mirror_txt);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, mirror_txt[0]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, MIRRORTXT_W, MIRRORTXT_H,
                 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);
    glBindTexture(GL_TEXTURE_2D, mirror_txt[1]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, MIRRORTXT_W, MIRRORTXT_H,
                 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);

    glGenFramebuffers(1, &mirror_fbo);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, mirror_fbo);
    glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                         mirror_txt[0], 0);
    glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                         mirror_txt[1], 0);
    if (glCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        ExitOnError("Framebuffer incomplete");
    }
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glBindTexture(GL_TEXTURE_2D, 0);
    ExitIfGLError("ConstructMirrorFBO");
} /*ConstructMirrorFBO*/

void ConstructMirrorVAO() {
    glGenVertexArrays(1, &mirror_vao);
    glBindVertexArray(mirror_vao);
    glGenBuffers(1, &mirror_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, mirror_vbo);
    glBufferData(GL_ARRAY_BUFFER,
                 4 * 3 * sizeof(GLfloat), mvertpos, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
                          3 * sizeof(GLfloat), (GLvoid*) nullptr);
    ExitIfGLError("ConstructMirrorVAO");
} /*ConstructMirrorVAO*/

void SetupMirrorVPMatrices(Vector4GLf& eyepos, Vector4GLf& reyepos,
                           Matrix& mvm, Matrix& mpm) {
    GLfloat s, b[3];
    Vector3GLf a;
    Vector4GLf v1, v2, nv, p;
    for (int i = 0; i < 3; i++) {
        v1[i] = mvertpos[1][i] - mvertpos[0][i];
        v2[i] = mvertpos[3][i] - mvertpos[0][i];
        a[i] = mvertpos[0][i] - eyepos[i];
    }
    nv = v2.to_vector3() * v1.to_vector3();
    s = nv.length();
    if (nv.to_vector3().dot(a) < 0.0) {
        s = -s;
    }
    nv[0] /= s;
    nv[1] /= s;
    nv[2] /= s;
    reyepos = eyepos.to_vector3().reflect(mvertpos[0], nv.to_vector3());
    p = reyepos.to_vector3();
    Matrix mfm = {v1, v2, nv, p};
    mfm[3] = mfm[7] = mfm[11] = 0.0;
    mfm[15] = 1.0;
    mvm = mfm.invert().value();
    M4x4MultMP3(mvm, mvertpos[0]);
    M4x4MultMP3(mvm, mvertpos[2]);
    mpm = M4x4FromStandardCubeFrustum(a[0], b[0], a[1], b[1], -a[2], 20.0);
} /*SetupMirrorVPMatrices*/

void DrawMirror(char final, GLuint program) {
    glUseProgram(program);
    glBindVertexArray(mirror_vao);
    if (final) {
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, mirror_txt[0]);
    }
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
} /*DrawMirror*/

void DestroyMirrorFBO() {
    glDeleteFramebuffers(1, &mirror_fbo);
    glDeleteTextures(2, mirror_txt);
    ExitIfGLError("DestroyMirrorFBO");
} /*DestroyMirrorFBO*/

void DestroyMirrorVAO() {
    glDeleteBuffers(1, &mirror_vbo);
    glDeleteVertexArrays(1, &mirror_vao);
    ExitIfGLError("DestroyMirrorVAO");
} /*DestroyMirrorVAO*/

