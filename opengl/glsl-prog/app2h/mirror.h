#ifndef MIRROR_H
#define MIRROR_H


#define MIRRORTXT_W 1024
#define MIRRORTXT_H 1024

extern GLuint mirror_fbo, mirror_txt[2];

extern GLuint mirror_vao, mirror_vbo;

void ConstructMirrorFBO();
void ConstructMirrorVAO();
void SetupMirrorVPMatrices(Vector4GLf& eyepos, Vector4GLf& reyepos,
                           Matrix& mvm, Matrix& mpm);
void DrawMirror(char final, GLuint program);
void DestroyMirrorFBO();
void DestroyMirrorVAO();

#endif // MIRROR_H