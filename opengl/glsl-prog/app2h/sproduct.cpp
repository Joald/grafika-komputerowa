#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include "myglheader.h"

#include "../utilities/utilities.h"
#include "bezpatches.h"
#include "sproduct.h"

const float SQRT3 = 1.7320508f;

Vector3GLf UnitCircle[7] =
  {{0.5,   0.5f * SQRT3,  1.0},
   {-0.5f, 0.5f * SQRT3,  0.5},
   {-1.0f, 0.0,           1.0},
   {-0.5f, -0.5f * SQRT3, 0.5},
   {0.5,   -0.5f * SQRT3, 1.0},
   {1.0,   0.0,           0.5},
   {0.5,   0.5f * SQRT3,  1.0}};

BezierPatchObjf* EnterRSphericalProduct(int eqdeg, int eqarcs, int eqstride,
                                        Vector3GLf eqcp[],
                                        int mdeg, int marcs, int mstride,
                                        Vector3GLf mcp[],
                                        GLfloat* colour) {
    int nw = (eqarcs - 1) * eqstride + eqdeg + 1;
    int nk = (marcs - 1) * mstride + mdeg + 1;
    GLfloat cp[nw * nk * 4];
    int k = 0;
    for (int i = 0; i < nk; i++) {
        for (int j = 0; j < nw; j++) {
            cp[k + 0] = eqcp[i][0] * mcp[j][0];
            cp[k + 1] = eqcp[i][1] * mcp[j][0];
            cp[k + 2] = eqcp[i][2] * mcp[j][1];
            cp[k + 3] = eqcp[i][2] * mcp[j][2];
            k += 4;
        }
    }
    BezierPatchObjf* spr = EnterBezierPatches(
      eqdeg, mdeg, 4, eqarcs, marcs, nw * nk, cp,
      nw * eqstride * 4, mstride * 4, 4 * nw, 4, colour
    );
    return spr;
} /*EnterRSphericalProduct*/

BezierPatchObjf* EnterTorus(float R, float r, Vector4f& colour) {
    Vector3GLf circ[7];
    int i;

    for (i = 0; i < 7; i++) {
        circ[i][0] = r * UnitCircle[i][0] + R * UnitCircle[i][2];
        circ[i][1] = r * UnitCircle[i][1];
        circ[i][2] = UnitCircle[i][2];
    }
    return EnterRSphericalProduct(2, 3, 2, UnitCircle, 2, 3, 2, circ, colour);
} /*EnterTorus*/

