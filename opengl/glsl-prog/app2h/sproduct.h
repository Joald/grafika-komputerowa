#ifndef SPRODUCT_H
#define SPRODUCT_H

#include "../utilities/utilities.h"
#include "bezpatches.h"

BezierPatchObjf* EnterRSphericalProduct(int eqdeg, int eqarcs, int eqstride,
                                        GLfloat eqcp[][3],
                                        int mdeg, int marcs, int mstride,
                                        GLfloat mcp[][3],
                                        GLfloat* colour);

BezierPatchObjf* EnterTorus(float R, float r, Vector4f& colour);

#endif // SPRODUCT_H