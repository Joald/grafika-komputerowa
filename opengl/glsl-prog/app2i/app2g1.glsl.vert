#version 420 core

#define MAX_DEG 10

layout(location=0) in vec4 in_Position;

uniform CPoints {
    float cp[4*(MAX_DEG+1)*(MAX_DEG+1)];
  } cp;

uniform CPIndices {
    int cpi[(MAX_DEG+1)*(MAX_DEG+1)];
  } cpi;

uniform BezPatch {
    int  dim, udeg, vdeg;
    int  stride_u, stride_v, stride_p, stride_q, nq;
    bool use_ind;
    vec4 Colour;
    int  TessLevel;
    bool BezNormals;
  } bezp;

uniform TransBlock {
    mat4 mm, mmti, vm, pm, vpm;
    vec4 eyepos;
  } trb;

void main ( void )
{
  int  n, m, nn, nlines, np, nl, a, b, i;
  vec4 p;

  n = bezp.udeg;  m = bezp.vdeg;
  nn = (n+1)*(m+1);
  nlines = 2*m*n+m+n;           /* liczba odcinkow siatki */
  np = gl_InstanceID / nlines;  /* numer siatki */
  nl = gl_InstanceID % nlines;  /* numer odcinka w siatce */
  if ( nl < (m+1)*n ) {  /* odcinek wiersza */
    if ( bezp.use_ind )
      i = cpi.cpi[nn*np +
                  ((gl_VertexID == 0) ? nl : nl+m+1)] * bezp.dim;
    else {
      a = nl / n;  /* numer wiersza */
      b = nl % n;  /* numer odcinka w wierszu */
      if ( gl_VertexID != 0 ) b ++;
      i = (np / bezp.nq)*bezp.stride_p + (np % bezp.nq)*bezp.stride_q +
          a*bezp.stride_v + b*bezp.stride_u;
    }
  }
  else {                 /* odcinek kolumny */
    nl -= (m+1)*n;
    a = nl / m;  /* numer kolumny */
    b = nl % m;  /* numer odcinka w kolumnie */
    if ( gl_VertexID != 0 ) b ++;
    if ( bezp.use_ind ) {
      i = cpi.cpi[nn*np + a*(m+1) + b] * bezp.dim;
    }
    else {
      i = (np / bezp.nq)*bezp.stride_p + (np % bezp.nq)*bezp.stride_q +
          a*bezp.stride_u + b*bezp.stride_v;
    }
  }
  switch ( bezp.dim ) {
case 2: p = vec4 ( cp.cp[i], cp.cp[i+1], 0.0, 1.0 );  break;
case 3: p = vec4 ( cp.cp[i], cp.cp[i+1], cp.cp[i+2], 1.0 );  break;
case 4: p = vec4 ( cp.cp[i], cp.cp[i+1], cp.cp[i+2], cp.cp[i+3] );  break;
default: p = vec4 ( 0.0 );  break;
  }
  gl_Position = trb.vpm * (trb.mm * p);
} /*main*/
