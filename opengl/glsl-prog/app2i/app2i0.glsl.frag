#version 430 core

layout (location = 0) out vec4 colour;

in float age;

uniform vec4 PartColour = vec4(0.85,0.85,0.85,0.5);

void main ( void )
{
  float t;

  if ( age < 0.0 )
    discard;
  else {
    t = 1.0-age;
    colour = vec4 ( PartColour.xyz, PartColour.w*t*t*t*age );
  }
} /*main*/

