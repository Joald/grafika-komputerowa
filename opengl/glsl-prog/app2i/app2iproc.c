#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <sys/times.h> 
#include <math.h>
#include "myglheader.h"
#include <GLFW/glfw3.h>

#include "../utilities/utilities.h"
#include "linkage.h"
#include "texture.h"
#include "lights.h"
#include "bezpatches.h"
#include "teapot.h"
#include "sproduct.h"
#include "mirror.h"
#include "particles.h"
#include "app2i.h"

GLuint shader_id[13];
GLuint program_id[6], progid0[3], progid1[3];

GLuint trbi, trbuf, trbbp;       /* access to transformations */
GLint  trbsize, trbofs[6];
GLint  LightProcLoc, LightProcInd,
       LambertProcInd, BlinnPhongProcInd;
GLuint ucs_loc, uns_loc, dim_loc, ncp_loc, trnum_loc;
GLuint ccpbi, ctrbi, ctrbi, ctrbbp, ctribi, ctribbp;          

kl_linkage      *mylinkage;
BezierPatchObjf *myteapots[2], *mytoruses[2];
GLuint          lktrbuf[2];

GLuint mytexture;

float   model_rot_axis[3] = {0.0,0.0,1.0},
        teapot_rot_angle0 = 0.0, teapot_rot_angle = 0.0;
GLfloat ident_matrix[16];
TransBl trans;

clock_t app_clock0;
float   clocks_per_sec,
        app_time, app_time0, teapot_time0, part_time0;
char    cnet = false, skeleton = false, particles = false;

void LoadMyShaders ( void )
{
  static const char *filename[] =
    { "app2g0.glsl.vert", "app2g0.glsl.tesc", "app2g0.glsl.tese",
      "app2g0.glsl.geom", "app2g0.glsl.frag",
      "app2g1.glsl.vert", "app2g1.glsl.frag",
      "app2g2.glsl.vert", "app2g2.glsl.frag",
      "app2g3.glsl.tese", "app2g3.glsl.frag",
      "app2g4.glsl.vert", "app2h.glsl.comp" };
  static const GLuint shtype[] =
    { GL_VERTEX_SHADER, GL_TESS_CONTROL_SHADER, GL_TESS_EVALUATION_SHADER,
      GL_GEOMETRY_SHADER, GL_FRAGMENT_SHADER,
      GL_VERTEX_SHADER, GL_FRAGMENT_SHADER,
      GL_VERTEX_SHADER, GL_FRAGMENT_SHADER,
      GL_TESS_EVALUATION_SHADER, GL_FRAGMENT_SHADER,
      GL_VERTEX_SHADER, GL_COMPUTE_SHADER };
  static const GLchar *UTBNames[] =
    { "TransBlock", "TransBlock.mm", "TransBlock.mmti", "TransBlock.vm",
      "TransBlock.pm", "TransBlock.vpm", "TransBlock.eyepos" };
  static const GLchar *ULSNames[] =
    { "LSBlock", "LSBlock.nls", "LSBlock.mask", "LSBlock.shmask",
      "LSBlock.ls[0].ambient", "LSBlock.ls[0].direct", "LSBlock.ls[0].position",
      "LSBlock.ls[0].attenuation", "LSBlock.ls[0].shadow_vpm",
      "LSBlock.ls[1].ambient" };
  static const GLchar *UCPNames[] =
    { "CPoints", "CPoints.cp" };
  static const GLchar *UCPINames[] =
    { "CPIndices", "CPIndices.cpi" };
  static const GLchar *UBezPatchNames[] =
    { "BezPatch", "BezPatch.dim", "BezPatch.udeg", "BezPatch.vdeg",
      "BezPatch.stride_u", "BezPatch.stride_v",
      "BezPatch.stride_p", "BezPatch.stride_q", "BezPatch.nq",
      "BezPatch.use_ind", "BezPatch.Colour",
      "BezPatch.TessLevel", "BezPatch.BezNormals" };
  static const GLchar *UMatNames[] =
    { "MatBlock", "MatBlock.mat.ambref", "MatBlock.mat.dirref",
      "MatBlock.mat.specref", "MatBlock.mat.shininess",
      "MatBlock.mat.wa", "MatBlock.mat.we" };
  static const GLchar *UTexCoordNames[] =
    { "BezPatchTexCoord", "BezPatchTexCoord.txc" };
  static const GLchar LightProcName[] = "Lighting";
  static const GLchar LambertProcName[] = "LambertLighting";
  static const GLchar BlinnPhongProcName[] = "BlinnPhongLighting";
  static const GLchar ColourSourceName[] = "ColourSource";
  static const GLchar NormalSourceName[] = "NormalSource";
  static const GLchar *UCompTrNames[] =
    { "Tr", "Tr.tr" };
  static const GLchar *UCompTrIndNames[] =
    { "TrInd", "TrInd.ind" };
  static const GLchar DimName[] = "dim";
  static const GLchar NcpName[] = "ncp";
  static const GLchar TrNumName[] = "trnum";

  GLint  i;
  GLuint shid[5];

  for ( i = 0; i < 13; i++ )
    shader_id[i] = CompileShaderFiles ( shtype[i], 1, &filename[i] );
  program_id[0] = LinkShaderProgram ( 5, shader_id );
  program_id[1] = LinkShaderProgram ( 2, &shader_id[5] );
  program_id[2] = LinkShaderProgram ( 2, &shader_id[7] );
  shid[0] = shader_id[0];  shid[1] = shader_id[1];
  shid[2] = shader_id[9];  shid[3] = shader_id[10];
  program_id[3] = LinkShaderProgram ( 4, shid );
  shid[0] = shader_id[11];  shid[1] = shader_id[10];
  program_id[4] = LinkShaderProgram ( 2, shid );
  progid0[0] = program_id[3];  progid0[1] = program_id[1];  progid0[2] = program_id[4];
  progid1[0] = program_id[0];  progid1[1] = program_id[1];  progid1[2] = program_id[2];
  program_id[5] = LinkShaderProgram ( 1, &shader_id[12] );
  GetAccessToUniformBlock ( program_id[0], 6, &UTBNames[0],
                            &trbi, &trbsize, trbofs, &trbbp );
  GetAccessToUniformBlock ( program_id[0], 9, &ULSNames[0],
                            &lsbi, &lsbsize, lsbofs, &lsbbp );
  GetAccessToUniformBlock ( program_id[0], 1, &UCPNames[0],
                            &cpbi, &i, cpbofs, &cpbbp );
  GetAccessToUniformBlock ( program_id[0], 1, &UCPINames[0],
                            &cpibi, &i, cpibofs, &cpibbp );
  GetAccessToUniformBlock ( program_id[0], 12, &UBezPatchNames[0],
                            &bezpbi, &bezpbsize, bezpbofs, &bezpbbp );
  GetAccessToUniformBlock ( program_id[0], 1, &UTexCoordNames[0],
                            &txcbi, &i, txcofs, &txcbp );
  GetAccessToUniformBlock ( program_id[0], 6, &UMatNames[0],
                            &matbi, &matbsize, matbofs, &matbbp );
  ucs_loc = glGetUniformLocation ( program_id[0], ColourSourceName );
  uns_loc = glGetUniformLocation ( program_id[0], NormalSourceName );
  LightProcLoc = glGetSubroutineUniformLocation ( program_id[0],
                        GL_FRAGMENT_SHADER, LightProcName );
  LambertProcInd = glGetSubroutineIndex ( program_id[0], GL_FRAGMENT_SHADER,
                                          LambertProcName );
  BlinnPhongProcInd = glGetSubroutineIndex ( program_id[0], GL_FRAGMENT_SHADER,
                                             BlinnPhongProcName );
  LightProcInd = LambertProcInd;
  GetAccessToUniformBlock ( program_id[5], 1, &UCompTrNames[0],
                            &ctrbi, &i, &i, &ctrbbp );
  GetAccessToUniformBlock ( program_id[5], 1, &UCompTrIndNames[0],
                            &ctribi, &i, &i, &ctribbp );
  dim_loc = glGetUniformLocation ( program_id[5], DimName );
  ncp_loc = glGetUniformLocation ( program_id[5], NcpName );
  trnum_loc = glGetUniformLocation ( program_id[5], TrNumName );

  trbuf = NewUniformBlockObject ( trbsize, trbbp );
  lsbuf = NewUniformBlockObject ( lsbsize, lsbbp );
  matbuf[1] = NewUniformBlockObject ( matbsize, matbbp );
  matbuf[0] = NewUniformBlockObject ( matbsize, matbbp );

  AttachUniformBlockToBP ( program_id[1], UTBNames[0], trbbp );
  AttachUniformBlockToBP ( program_id[1], UCPNames[0], cpbbp );
  AttachUniformBlockToBP ( program_id[1], UCPINames[0], cpibbp );
  AttachUniformBlockToBP ( program_id[1], UBezPatchNames[0], bezpbbp );
  AttachUniformBlockToBP ( program_id[2], UTBNames[0], trbbp );
  AttachUniformBlockToBP ( program_id[3], UTBNames[0], trbbp );
  AttachUniformBlockToBP ( program_id[3], UCPNames[0], cpbbp );
  AttachUniformBlockToBP ( program_id[3], UCPINames[0], cpibbp );
  AttachUniformBlockToBP ( program_id[3], UBezPatchNames[0], bezpbbp );
  AttachUniformBlockToBP ( program_id[4], UTBNames[0], trbbp );
  AttachUniformBlockToBP ( program_id[5], UCPNames[0], cpbbp );
  ExitIfGLError ( "LoadMyShaders" );
} /*LoadMyShaders*/

void SetVPMatrix ( GLfloat vm[16], GLfloat pm[16], GLfloat ep[4] )
{
  GLfloat vpm[16];

  M4x4Multf ( vpm, pm, vm );
  glBindBuffer ( GL_UNIFORM_BUFFER, trbuf );
  glBufferSubData ( GL_UNIFORM_BUFFER, trbofs[2], 16*sizeof(GLfloat), vm );
  glBufferSubData ( GL_UNIFORM_BUFFER, trbofs[3], 16*sizeof(GLfloat), pm );
  glBufferSubData ( GL_UNIFORM_BUFFER, trbofs[4], 16*sizeof(GLfloat), vpm );
  glBufferSubData ( GL_UNIFORM_BUFFER, trbofs[5], 4*sizeof(GLfloat), ep );
  ExitIfGLError ( "SetVPMatrix" );
} /*SetVPMatrix*/

void SetModelMatrix ( GLfloat mm[16], GLfloat mmti[16] )
{
  memcpy ( trans.mm, mm, 16*sizeof(GLfloat) );
  memcpy ( trans.mmti, mmti, 16*sizeof(GLfloat) );
  glBindBuffer ( GL_UNIFORM_BUFFER, trbuf );
  glBufferSubData ( GL_UNIFORM_BUFFER, trbofs[0], 16*sizeof(GLfloat), trans.mm );
  glBufferSubData ( GL_UNIFORM_BUFFER, trbofs[1], 16*sizeof(GLfloat), trans.mmti );
  ExitIfGLError ( "SetModelMatrix" );
} /*SetModelMatrix*/

void InitViewMatrix ( void )
{
  memcpy ( trans.eyepos, viewer_pos0, 4*sizeof(GLfloat) );
  M4x4Translatef ( trans.wvm, -viewer_pos0[0], -viewer_pos0[1], -viewer_pos0[2] );
  SetupMirrorVPMatrices ( trans.eyepos, trans.reyepos, trans.mvm, trans.mpm );
} /*InitViewMatrix*/

void RotateViewer ( double delta_xi, double delta_eta )
{
  float   vi[3], lgt, angi, vk[3], angk;
  GLfloat tm[16], rm[16];

  if ( delta_xi == 0 && delta_eta == 0 )
    return;  /* natychmiast uciekamy - nie chcemy dzielic przez 0 */
  vi[0] = (float)delta_eta*(right-left)/(float)win_height;
  vi[1] = (float)delta_xi*(top-bottom)/(float)win_width;
  vi[2] = 0.0;
  lgt = sqrt ( V3DotProductf ( vi, vi ) );
  vi[0] /= lgt;
  vi[1] /= lgt;
  angi = -0.052359878;  /* -3 stopnie */
  V3CompRotationsf ( vk, &angk, viewer_rvec, viewer_rangle, vi, angi );
  memcpy ( viewer_rvec, vk, 3*sizeof(float) );
  viewer_rangle = angk;
  M4x4Translatef ( tm, -viewer_pos0[0], -viewer_pos0[1], -viewer_pos0[2] );
  M4x4RotateVf ( rm, viewer_rvec[0], viewer_rvec[1], viewer_rvec[2],
                 -viewer_rangle );
  M4x4Multf ( trans.wvm, tm, rm );
  M4x4Transposef ( tm, rm );
  M4x4MultMVf ( trans.eyepos, tm, viewer_pos0 );
  SetupMirrorVPMatrices ( trans.eyepos, trans.reyepos, trans.mvm, trans.mpm );
} /*RotateViewer*/

void InitMyObject ( void )
{
  struct tms clk;

  memset ( &trans, 0, sizeof(TransBl) );
  memset ( &light, 0, sizeof(LightBl) );
  M4x4Identf ( ident_matrix );
  SetModelMatrix ( ident_matrix, ident_matrix );
  InitViewMatrix ();
  ConstructBezierPatchDomain ();
  mylinkage = ConstructMyLinkage ();
  ConstructMirrorFBO ();
  ConstructMirrorVAO ();
  InitLights ();
  LoadMyTextures ();
  LoadParticleShaders ();
  InitParticleSystem ();
  clocks_per_sec = (float)sysconf(_SC_CLK_TCK);
  app_clock0 = times ( &clk );
  app_time0 = teapot_time0 = part_time0 = 0.0;
  ArticulateMyLinkage ( app_time0 );
} /*InitMyObject*/

void LoadMyTextures ( void )
{
  mytexture = CreateMyTexture ( 1024 );
  LoadMyTextureImage ( mytexture, 1024, 1024,   0,   0, "jaszczur.tif" );
  LoadMyTextureImage ( mytexture, 1024, 1024, 512,   0, "salamandra.tif" );
  LoadMyTextureImage ( mytexture, 1024, 1024,   0, 512, "lis.tif" );
  LoadMyTextureImage ( mytexture, 1024, 1024, 512, 512, "kwiatki.tif" );
  SetupMyTextureMipmaps ( mytexture );
} /*LoadMyTextures*/

void InitLights ( void )
{
  GLfloat amb0[4] = { 0.2, 0.2, 0.3, 1.0 };
  GLfloat dif0[4] = { 0.8, 0.8, 0.8, 1.0 };
  GLfloat pos0[4] = { -0.2, 1.0, 1.0, 0.0 };
  GLfloat atn0[3] = { 1.0, 0.0, 0.0 };
  GLfloat csc[3]  = { 0.0, 0.0, 0.0 };

  memset ( &light, 0, sizeof(LightBl) );
  SetLightAmbient ( 0, amb0 );
  SetLightDiffuse ( 0, dif0 );
  SetLightPosition ( 0, pos0 );
  SetLightAttenuation ( 0, atn0 );
  SetLightOnOff ( 0, 1 );
  ConstructShadowTxtFBO ( 0 );
  SetupShadowTxtTransformations ( 0, csc, 2.2 );
  UpdateShadowMatrix ( 0 );
} /*InitLights*/

void DrawScene ( char final, GLuint programs[3] )
{
  glColorMask ( final, final, final, final );
  DrawMyTeapot ( final, programs[0] );
  if ( cnet )
    DrawMyTeapotCNet ( programs[1] );
  DrawMyTorus ( final, programs[0] );
  if ( cnet )
    DrawMyTorusCNet ( programs[1] );
  glColorMask ( true, true, true, true );
} /*DrawScene*/

void DrawSceneToMirror ( void )
{
  glBindFramebuffer ( GL_DRAW_FRAMEBUFFER, mirror_fbo );
  glViewport ( 0, 0, MIRRORTXT_W, MIRRORTXT_H );
  SetVPMatrix ( trans.mvm, trans.mpm, trans.reyepos );
  glClearColor ( 0.95, 0.95, 0.95, 1.0 );
  glClear ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
  DrawScene ( true, progid1 );
  if ( particles )
    DrawParticles ( false );
  glFlush ();
} /*DrawSceneToMirror*/

void DrawSceneToShadows ( void )
{
  int    l;
  GLuint mask;

  glViewport ( 0, 0, SHADOW_MAP_SIZE, SHADOW_MAP_SIZE );
  glEnable ( GL_POLYGON_OFFSET_FILL );
  glPolygonOffset ( 2.0, 4.0 );
  for ( l = 0, mask = 0x00000001;  l < light.nls;  l++, mask <<= 1 )
    if ( light.shmask & mask ) {
      BindShadowTxtFBO ( l );
      glClearColor ( 1.0, 0.0, 0.0, 1.0 );
      glClear ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
      DrawScene ( false, progid0 );
      glPolygonMode ( GL_FRONT_AND_BACK, GL_FILL );
      DrawMirror ( false, progid0[2] );
      if ( particles )
        DrawParticles ( true );
    }
  glBindFramebuffer ( GL_FRAMEBUFFER, 0 );
  glDisable ( GL_POLYGON_OFFSET_FILL );
  for ( l = 0, mask = 0x00000001;  l < light.nls;  l++, mask <<= 1 )
    if ( light.shmask & mask ) {
      glActiveTexture ( GL_TEXTURE2+l );
      glBindTexture ( GL_TEXTURE_2D, light.ls[l].shadow_txt[0] );
      glActiveTexture ( GL_TEXTURE2+MAX_NLIGHTS+l );
      glBindTexture ( GL_TEXTURE_2D, light.ls[l].shadow_txt[1] );
    }
  ExitIfGLError ( "DrawSceneToShadows" );
} /*DrawSceneToShadows*/

void DrawSceneToWindow ( void )
{
  glEnable ( GL_DEPTH_TEST );
  DrawSceneToShadows ();
  DrawSceneToMirror ();
  glBindFramebuffer ( GL_DRAW_FRAMEBUFFER, 0 );
  glViewport ( 0, 0, win_width, win_height );
  SetVPMatrix ( trans.wvm, trans.wpm, trans.eyepos );
  glClearColor ( 1.0, 1.0, 1.0, 1.0 );
  glClear ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
  glPolygonMode ( GL_FRONT_AND_BACK, GL_FILL );
  DrawMirror ( true, progid1[2] );
  DrawScene ( true, progid1 );
  if ( particles )
    DrawParticles ( false );
} /*DrawSceneToWindow*/

