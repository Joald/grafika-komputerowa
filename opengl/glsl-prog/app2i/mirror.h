
#define MIRRORTXT_W 1024
#define MIRRORTXT_H 1024

extern GLuint  mirror_fbo, mirror_txt[2];
extern GLuint  mirror_vao, mirror_vbo;

void ConstructMirrorFBO ( void );
void ConstructMirrorVAO ( void );
void SetupMirrorVPMatrices ( GLfloat eyepos[4], GLfloat reyepos[4],
                             GLfloat mvm[16], GLfloat mpm[16] );
void DrawMirror ( char final, GLuint program );
void DestroyMirrorFBO ( void );
void DestroyMirrorVAO ( void );

