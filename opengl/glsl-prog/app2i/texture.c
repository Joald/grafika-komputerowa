#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <tiffio.h>
#include "myglheader.h"

#include "../utilities/utilities.h"
#include "texture.h"

GLuint CreateMyTexture ( int wh )
{
  GLuint tex;
  int    w, l;

  glGenTextures ( 1, &tex );
  glBindTexture ( GL_TEXTURE_2D, tex );
  for ( w = wh, l = 0;  !(w & 0x01);  l++ )
    w >>= 1;
  glTextureStorage2D ( tex, l, GL_RGBA8, wh, wh );
  ExitIfGLError ( "CreateMyTexture" );
  return tex;
} /*CreateMyTexture*/

GLubyte *ReadTiffImage ( const char *fn, int *width, int *height )
{
  TIFF    *tif;
  int     w, h, npix;
  GLubyte *image;

  image = NULL;
  if ( fn[0] ) {
    printf ( "%s\n", fn );
    tif = TIFFOpen ( fn, "r" );
    if ( tif ) {
      TIFFGetField ( tif, TIFFTAG_IMAGEWIDTH, &w );
      TIFFGetField ( tif, TIFFTAG_IMAGELENGTH, &h );
      npix = w*h;
      image = malloc ( 4*npix );
      if ( !image )
        goto failure;
      memset ( image, 0, npix*sizeof(uint32) );
      if ( !TIFFReadRGBAImage ( tif, w, h, (uint32*)image, 0 ) )
        goto failure;
      TIFFClose ( tif );
      *width = w;
      *height = h;
      return image;
    }
    else
      return NULL;

failure:
    if ( image )
      free ( image );
    TIFFClose ( tif );
    return NULL;
  }
  else
    return NULL;
} /*ReadTiffImage*/

char LoadMyTextureImage ( GLuint tex, int txwidth, int txheight,
                          int x, int y, const char *filename )
{
  int     w, h;
  GLubyte *image;

  if ( !(image = ReadTiffImage ( filename, &w, &h )) )
    return 0;
  if ( x+w <= txwidth && y+w <= txheight ) {
    glTextureSubImage2D ( tex, 0, x, y, w, h, GL_RGBA, GL_UNSIGNED_BYTE, image );
    free ( image );
    ExitIfGLError ( "LoadMyTextureImage" );
    return 1;
  }
  else {
    free ( image );
    return 0;
  }
} /*LoadMyTextureImage*/

char SetupMyTextureMipmaps ( GLuint tex )
{
  glGenerateTextureMipmap ( tex );
  glTextureParameteri ( tex, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
  glTextureParameteri ( tex, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR );
  ExitIfGLError ( "SetupMyTextureMipmaps" );
  return 1;
} /*SetupMyTextureMipmaps*/

void SaveTexture ( const char *fn, GLuint tex, int w, int h )
{
  TIFF          *tif;
  unsigned char *image, *a, *b, *c;
  int           n, i;

  image = malloc ( 3*w*(h+1)*sizeof(char) );
  if ( !image )
    return;
  if ( (tif = TIFFOpen ( fn, "w" )) ) {
    n = w*h;
    glGetTextureImage ( tex, 0, GL_RGB, GL_UNSIGNED_BYTE, 3*n*sizeof(char), image );
    for ( i = 0, a = image, b = &image[3*w*(h-1)], c = &image[3*n];
          i+i < h;
          i++, a += 3*w, b -= 3*w ) {
      memcpy ( c, a, 3*w*sizeof(char) );
      memcpy ( a, b, 3*w*sizeof(char) );
      memcpy ( b, c, 3*w*sizeof(char) );
    }
    TIFFSetField ( tif, TIFFTAG_IMAGEWIDTH, w );
    TIFFSetField ( tif, TIFFTAG_IMAGELENGTH, h );
    TIFFSetField ( tif, TIFFTAG_BITSPERSAMPLE, 8 );
    TIFFSetField ( tif, TIFFTAG_SAMPLESPERPIXEL, 3 );
    TIFFSetField ( tif, TIFFTAG_ROWSPERSTRIP, h );
    TIFFSetField ( tif, TIFFTAG_COMPRESSION, COMPRESSION_LZW );
    TIFFSetField ( tif, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB );
    TIFFSetField ( tif, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG );
    TIFFSetField ( tif, TIFFTAG_XRESOLUTION, 300.0 );
    TIFFSetField ( tif, TIFFTAG_YRESOLUTION, 300.0 );
    TIFFSetField ( tif, TIFFTAG_RESOLUTIONUNIT, RESUNIT_INCH );
    TIFFWriteEncodedStrip ( tif, 0, image, 3*n );
    TIFFClose ( tif );
    printf ( "%s\n", fn );
  }
  free ( image );
} /*SaveTexture*/

