#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include "myglheader.h"

#include "../utilities/utilities.h"
#include "accumbuf.h"

#define ACCBUF_NONE     0
#define ACCBUF_CLEAR    1
#define ACCBUF_ADD      2
#define ACCBUF_MULT     3
#define ACCBUF_MULT_ADD 4

#define ACCBUF_LOC_SIZE_X  1024
#define GWX (acb->width+ACCBUF_LOC_SIZE_X-1)/ACCBUF_LOC_SIZE_X

static GLuint accshid, accprid;
static GLuint action_loc, fct_loc;

char LoadAccumBufShaders ( void )
{
  static const char *filename[] = { "accbuf0.glsl.comp" };
  static const char *uvname[] = { "action", "fct" };

  accshid = CompileShaderFiles ( GL_COMPUTE_SHADER, 1, &filename[0] );
  accprid = LinkShaderProgram ( 1, &accshid );
  action_loc = glGetUniformLocation ( accprid, uvname[0] );
  fct_loc = glGetUniformLocation ( accprid, uvname[1] );
  ExitIfGLError ( "LoadAccumBufShaders" );
  return true;
} /*LoadAccumBufShaders*/

void DeleteAccumBufShaders ( void )
{
  glUseProgram ( 0 );
  glDeleteShader ( accshid );
  glDeleteProgram ( accprid );
  ExitIfGLError ( "DeleteAccumBufShaders" );
} /*DeleteAccumBufShaders*/

static char AllocAccTextures ( AccumBuf *acb, int w, int h )
{
  glGenTextures ( 3, acb->txt );
  glBindTexture ( GL_TEXTURE_2D, acb->txt[0] );
  glTexStorage2D ( GL_TEXTURE_2D, 1, GL_RGBA32F, w, h );
  glBindFramebuffer ( GL_FRAMEBUFFER, acb->fbo[0] );
  glFramebufferTexture ( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, acb->txt[0], 0 );
  if ( glCheckFramebufferStatus ( GL_FRAMEBUFFER ) != GL_FRAMEBUFFER_COMPLETE )
    ExitOnError ( "AllocAccTextures 0" );
  glBindTexture ( GL_TEXTURE_2D, acb->txt[1] );
  glTexStorage2D ( GL_TEXTURE_2D, 1, GL_RGBA32F, w, h );
  glBindTexture ( GL_TEXTURE_2D, acb->txt[2] );
  glTexStorage2D ( GL_TEXTURE_2D, 1, GL_DEPTH_COMPONENT32F, w, h );
  glBindFramebuffer ( GL_FRAMEBUFFER, acb->fbo[1] );
  glFramebufferTexture ( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, acb->txt[1], 0 );
  glFramebufferTexture ( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, acb->txt[2], 0 );
  if ( glCheckFramebufferStatus ( GL_FRAMEBUFFER ) != GL_FRAMEBUFFER_COMPLETE )
    ExitOnError ( "AllocAccTextures 1" );
  glBindFramebuffer ( GL_FRAMEBUFFER, 0 );
  glBindTexture ( GL_TEXTURE_2D, 0 );
  acb->width = w;  acb->height = h;
  ExitIfGLError ( "AllocAccTextures" );
  return true;
} /*AllocAccTextures*/

AccumBuf *NewAccumBuf ( int w, int h )
{
  AccumBuf *acb;

  if ( (acb = malloc ( sizeof(AccumBuf) )) ) {
    glGenFramebuffers ( 2, acb->fbo );
    if ( AllocAccTextures ( acb, w, h ) )
      return acb;
    else {
      glDeleteFramebuffers ( 2, acb->fbo );
      free ( acb );
    }
  }
  return NULL;
} /*NewAccumBuf*/

char SetAccumBufSize ( AccumBuf *acb, int w, int h )
{
  if ( w != acb->width || h != acb->height ) {
    glDeleteTextures ( 3, acb->txt );
    if ( !AllocAccTextures ( acb, w, h ) ) {
      glDeleteFramebuffers ( 2, acb->fbo );
      free ( acb );
      return false;
    }
  }
  return true;
} /*SetAccumBufSize*/

void DestroyAccumBuf ( AccumBuf *acb )
{
  glDeleteFramebuffers ( 2, acb->fbo );
  glDeleteTextures ( 3, acb->txt );
  free ( acb );
} /*DestroyAccumBuf*/

void AccumBufClear ( AccumBuf *acb )
{
  glUseProgram ( accprid );
  glBindImageTexture ( 0, acb->txt[0], 0, GL_FALSE, 0,
                       GL_WRITE_ONLY, GL_RGBA32F );
  glUniform1i ( action_loc, ACCBUF_CLEAR );
  glDispatchCompute ( GWX, acb->height, 1 );
  glMemoryBarrier ( GL_SHADER_IMAGE_ACCESS_BARRIER_BIT );
  ExitIfGLError ( "AccumBufClear" );
} /*AccumBufClear*/

void AccumBufAdd ( AccumBuf *acb )
{
  glUseProgram ( accprid );
  glBindImageTexture ( 0, acb->txt[0], 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA32F );
  glBindImageTexture ( 1, acb->txt[1], 0, GL_FALSE, 0, GL_READ_ONLY, GL_RGBA32F );
  glUniform1i ( action_loc, ACCBUF_ADD );
  glDispatchCompute ( GWX, acb->height, 1 );
  glMemoryBarrier ( GL_SHADER_IMAGE_ACCESS_BARRIER_BIT );
  ExitIfGLError ( "AccumBufAdd" );
} /*AccumBufAdd*/

void AccumBufMult ( AccumBuf *acb, GLfloat a )
{
  glUseProgram ( accprid );
  glBindImageTexture ( 0, acb->txt[0], 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA32F );
  glUniform1i ( action_loc, ACCBUF_MULT );
  glUniform1f ( fct_loc, a );
  glDispatchCompute ( GWX, acb->height, 1 );
  glMemoryBarrier ( GL_SHADER_IMAGE_ACCESS_BARRIER_BIT );
  ExitIfGLError ( "AccumBufMult" );
} /*AccumBufMult*/

void AccumBufMultAdd ( AccumBuf *acb, GLfloat a )
{
  glUseProgram ( accprid );
  glBindImageTexture ( 0, acb->txt[0], 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA32F );
  glBindImageTexture ( 1, acb->txt[1], 0, GL_FALSE, 0, GL_READ_ONLY, GL_RGBA32F );
  glUniform1i ( action_loc, ACCBUF_MULT_ADD );
  glUniform1f ( fct_loc, a );
  glDispatchCompute ( GWX, acb->height, 1 );
  glMemoryBarrier ( GL_SHADER_IMAGE_ACCESS_BARRIER_BIT );
  ExitIfGLError ( "AccumBufMultAdd" );
} /*AccumBufMultAdd*/

