
typedef struct AccumBuf {
          GLuint fbo[2];
          GLuint txt[3];
          int    width, height;
        } AccumBuf;

char LoadAccumBufShaders ( void );
void DeleteAccumBufShaders ( void );

AccumBuf *NewAccumBuf ( int w, int h );
char SetAccumBufSize ( AccumBuf *acb, int w, int h );
void DestroyAccumBuf ( AccumBuf *acb );

void AccumBufClear ( AccumBuf *acb );
void AccumBufAdd ( AccumBuf *acb );
void AccumBufMult ( AccumBuf *acb, GLfloat a );
void AccumBufMultAdd ( AccumBuf *acb, GLfloat a );

