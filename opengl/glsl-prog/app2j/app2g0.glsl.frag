#version 450 core

#define MAX_NLIGHTS 8

layout(early_fragment_tests) in;

in NVertex {
    float instance;
    vec4  Colour;
    vec3  Position;
    vec3  pu, pv, Normal;
    vec2  PatchCoord, TexCoord;
    vec4  ShadowPos[MAX_NLIGHTS];
  } In;

out vec4 out_Colour;

uniform TransBlock {
    mat4 mm, mmti, vm, pm, vpm;
    vec4 eyepos;
  } trb;

struct LSPar {
    vec4 ambient;
    vec4 direct;
    vec4 position;
    vec3 attenuation;
    mat4 shadow_vpm;
  };

uniform LSBlock {
    uint  nls;              /* liczba zrodel swiatla */
    uint  mask;             /* maska wlaczonych zrodel */
    uint  shmask;           /* maska tekstur cienia */
    LSPar ls[MAX_NLIGHTS];  /* poszczegolne zrodla swiatla */
  } light;

struct Material {
    vec4  ambref;     /* odbijanie swiatla rozproszonego */
    vec4  dirref;     /* odbijanie swiatla kierunkowego */
    vec4  specref;    /* odbijanie zwierciadlane swiatla */
    float shininess, wa, we;  /* polysk */
  };

uniform MatBlock {
    Material mat;
  } mat;

layout (binding = 0) uniform sampler2D tex;
layout (binding = 2) uniform sampler2DShadow shtex[MAX_NLIGHTS];
layout (binding = 10) uniform sampler2D parttex[MAX_NLIGHTS];   /* 10 = 2+MAX_NLIGHTS */

uniform int ColourSource, NormalSource;

subroutine void LightingProc ( void );

subroutine uniform LightingProc Lighting;

Material mm;
vec3     normal, nv;

void GetColours ( vec2 tc )
{
  switch ( ColourSource ) {
default:
    mm.ambref = mm.dirref = In.Colour;
    mm.specref = vec4 ( 0.25 );
    mm.shininess = 20.0;  mm.wa = mm.we = 1.0;
    break;
case 1:
    mm = mat.mat;
    break;
case 2:
    mm = mat.mat;
    if ( tc.x >= 0.0 && tc.x <= 1.0 && tc.y >= 0.0 && tc.y <= 1.0 )
      mm.ambref = mm.dirref = texture ( tex, tc );
    break;
  }
} /*GetColours*/

#define SQRT3 1.7320508
#define C     (-0.003)

const vec2 dtxm[3] = { vec2(10.0,0.0), vec2(0.0,10.0), vec2(0.0,0.0) };

vec3 NormalTex ( float dx, float dy )
{
  float r;
  vec2  Dd, Ddq;
  vec3  dpu, dpv;

  r = sqrt ( dx*dx + dy*dy );
  if ( r >= 0.5 || r == 0.0 )
    return nv;
  Dd = C*(48.0*r-24.0)*vec2 ( dx, dy );
  Ddq = Dd.x*dtxm[0] + Dd.y*dtxm[1];
  dpu = In.pu + Ddq.x*nv;
  dpv = In.pv + Ddq.y*nv;
  return normalize ( cross ( dpu, dpv ) );
} /*NormalTex*/

vec3 NormalTexture1 ( vec2 pc )
{
  pc = fract ( dtxm[0]*pc.x + dtxm[1]*pc.y + dtxm[2] );
  return NormalTex ( pc.x-0.5, pc.y-0.5 );
} /*NormalTexture1*/

vec3 NormalTexture2 ( vec2 pc )
{
  int  c;
  float dx, dy;

  pc = dtxm[0]*pc.x + dtxm[1]*pc.y + dtxm[2];
  pc = fract ( vec2 ( pc.x/SQRT3, pc.y ) );
  switch ( (pc.x > 0.5 ? 1 : 0) + (pc.y > 0.5 ? 2 : 0) ) {
case 0: c = 3.0*pc.x-pc.y > 0.5 ? 1 : 0;  break;
case 1: c = 3.0*pc.x+pc.y > 2.5 ? 3 : 1;  break;
case 2: c = 3.0*pc.x+pc.y > 1.5 ? 2 : 0;  break;
case 3: c = 3.0*pc.x-pc.y > 1.5 ? 3 : 2;  break;
  }
  switch ( c ) {
case 0: dx = pc.x;      dy = pc.y-0.5;  break;  /* a */
case 1: dx = pc.x-0.5;  dy = pc.y;      break;  /* c */
case 2: dx = pc.x-0.5;  dy = pc.y-1.0;  break;  /* b */
case 3: dx = pc.x-1.0;  dy = pc.y-0.5;  break;  /* d */
  }
  return NormalTex ( dx*SQRT3, dy );
} /*NormalTexture2*/

vec3 GetNormalVector ( vec2 pc )
{
  switch ( NormalSource ) {
default: return nv;
 case 1: return NormalTexture1 ( pc );
 case 2: return NormalTexture2 ( pc );
  }
} /*GetNormalVector*/

vec3 posDifference ( vec4 p, vec3 pos, out float dist )
{
  vec3 v;

  if ( p.w != 0.0 ) {
    v = p.xyz/p.w-pos.xyz;
    dist = sqrt ( dot ( v, v ) );
  }
  else
    v = p.xyz;
  return normalize ( v );
} /*posDifference*/

float attFactor ( vec3 att, float dist )
{
  return 1.0/(((att.z*dist)+att.y)*dist+att.x);
} /*attFactor*/

float IsEnlighted ( uint l )
{
  float s;

  s = textureProj ( shtex[l], In.ShadowPos[l] );
  if ( s > 0.0 )
    return s*textureProj ( parttex[l], In.ShadowPos[l] ).r;
  else
    return 0.0;
} /*IsEnlighted*/

subroutine (LightingProc) void LambertLighting ( void )
{
  vec3  lv, vv;
  float d, e, s, dist;
  uint  i, mask;

  vv = posDifference ( trb.eyepos, In.Position, dist );
  e = dot ( vv, nv );
  out_Colour = vec4(0.0);
  for ( i = 0, mask = 0x00000001;  i < light.nls;  i++, mask <<= 1 )
    if ( (light.mask & mask) != 0 ) {
      out_Colour += light.ls[i].ambient * mm.ambref;
      s = ((light.shmask & mask) != 0) ? IsEnlighted ( i ) : 1.0;
      if ( s > 0.0 ) {
        lv = posDifference ( light.ls[i].position, In.Position, dist );
        d = dot ( lv, normal );
        if ( e > 0.0 ) {
          if ( dot ( lv, nv ) > 0.0 ) {
            if ( light.ls[i].position.w != 0.0 )
              d *= attFactor ( light.ls[i].attenuation, dist );
            out_Colour += (s*d * light.ls[i].direct) * mm.dirref;
          }
        }
        else {
          if ( dot ( lv, nv ) < 0.0 ) {
            if ( light.ls[i].position.w != 0.0 )
              d *= attFactor ( light.ls[i].attenuation, dist );
            out_Colour -= (s*d * light.ls[i].direct) * mm.dirref;
          }
        }
      }
    }
  out_Colour = vec4 ( clamp ( out_Colour.rgb, 0.0, 1.0 ), 1.0 );
} /*LambertLighting*/

float wFactor ( float lvn, float wa, float we )
{
  return 1.0+(wa-1.0)*pow ( 1.0-lvn, we );
} /*wFactor*/

subroutine (LightingProc) void BlinnPhongLighting ( void )
{
  vec3  lv, vv, hv;
  float a, d, e, f, s, dist;
  uint  i, mask;

  vv = posDifference ( trb.eyepos, In.Position, dist );
  e = dot ( vv, nv );
  out_Colour = vec4(0.0);
  for ( i = 0, mask = 0x00000001;  i < light.nls;  i++, mask <<= 1 )
    if ( (light.mask & mask) != 0 ) {
      out_Colour += light.ls[i].ambient * mm.ambref;
      if ( (light.shmask & mask) == 0 )
        continue;
      if ( (s = IsEnlighted ( i )) > 0.0 ) {
        lv = posDifference ( light.ls[i].position, In.Position, dist );
        d = dot ( lv, normal );
        if ( e > 0.0 ) {
          if ( dot ( lv, nv ) > 0.0 ) {
            a = light.ls[i].position.w != 0.0 ?
                  s*attFactor ( light.ls[i].attenuation, dist ) : s;
            out_Colour += (a*d*light.ls[i].direct) * mm.dirref;
            hv = normalize ( lv+vv );
            f = pow ( dot ( hv, normal ), mm.shininess );
            out_Colour += (a*f*wFactor(d,mm.wa,mm.we))*mm.specref;
          }
        }
        else {
          if ( dot ( lv, nv ) < 0.0 ) {
            a = light.ls[i].position.w != 0.0 ?
                  s*attFactor ( light.ls[i].attenuation, dist ) : s;
            out_Colour -= (a*d*light.ls[i].direct) * mm.dirref;
            hv = normalize ( lv+vv );
            f = pow ( -dot ( hv, normal ), mm.shininess );
            out_Colour += (a*f*wFactor(-d,mm.wa,mm.we))*mm.specref;
          }
        }
      }
    }
  out_Colour = vec4 ( clamp ( out_Colour.rgb, 0.0, 1.0 ), 1.0 );
} /*BlinnPhongLighting*/

void main ( void )
{
  nv = normalize ( In.Normal );
  normal = GetNormalVector ( In.PatchCoord );
  GetColours ( In.TexCoord );
  Lighting ();
} /*main*/
