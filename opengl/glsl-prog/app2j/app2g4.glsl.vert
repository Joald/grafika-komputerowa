#version 450 core

layout(location=0) in vec4 in_Position;

uniform TransBlock {
    mat4 mm, mmti, vm, pm, vpm;
    vec4 eyepos;
  } trb;

void main ( void )
{
  gl_Position = trb.vpm * (trb.mm * in_Position);
} /*main*/
