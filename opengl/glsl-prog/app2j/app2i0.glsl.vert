#version 430 core

layout (location = 0) in vec4 vert;

out float age;

uniform TransBlock {
  mat4 mm, mmti, vm, pm, vpm;
  vec4 eyepos;
} trb;
        
void main ( void )
{
  age = vert.w;
  gl_Position = trb.vpm * vec4 ( vert.xyz, 1.0 );
} /*main*/

