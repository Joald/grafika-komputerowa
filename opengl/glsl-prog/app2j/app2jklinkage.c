#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <sys/times.h> 
#include <math.h>
#include "myglheader.h"
#include <GLFW/glfw3.h>

#include "../utilities/utilities.h"
#include "accumbuf.h"
#include "linkage.h"
#include "texture.h"
#include "lights.h"
#include "bezpatches.h"
#include "teapot.h"
#include "sproduct.h"
#include "particles.h"
#include "mirror.h"
#include "app2j.h"

void ConstructMyTeapot ( BezierPatchObjf *teapots[2] )
{
  const GLfloat MyColour[4] = { 1.0, 1.0, 1.0, 1.0 };
  const GLfloat ambr[4]  = { 0.75, 0.65, 0.3, 1.0 };
  const GLfloat diffr[4] = { 0.75, 0.65, 0.3, 1.0 };
  const GLfloat specr[4] = { 0.7, 0.7, 0.6, 1.0 };
  const GLfloat shn = 60.0, wa = 5.0, we = 5.0;
  const GLfloat txc[32][4] =
    {{-1.0,0.0,-1.0,0.0},{-1.0,0.0,-1.0,0.0},{-1.0,0.0,-1.0,0.0},{-1.0,0.-1,0.0,0.0},
     {0.5,0.5,0.0,0.0},{0.5,1.0,0.0,0.5},{1.0,0.5,0.5,0.0},{1.0,1.0,0.5,0.5},
     {-1.0,0.0,-1.0,0.0},{-1.0,0.0,-1.0,0.0},{-1.0,0.0,-1.0,0.0},{-1.0,0.0,-1.0,0.0},
     {-1.0,0.0,-1.0,0.0},{-1.0,0.0,-1.0,0.0},{-1.0,0.0,-1.0,0.0},{-1.0,0.0,-1.0,0.0},
     {-1.0,0.0,-1.0,0.0},{-1.0,0.0,-1.0,0.0},{-1.0,0.0,-1.0,0.0},{-1.0,0.0,-1.0,0.0},
     {-1.0,0.0,-1.0,0.0},{-1.0,0.0,-1.0,0.0},{-1.0,0.0,-1.0,0.0},{-1.0,0.0,-1.0,0.0},
     {-1.0,0.0,-1.0,0.0},{-1.0,0.0,-1.0,0.0},{-1.0,0.0,-1.0,0.0},{-1.0,0.0,-1.0,0.0},
     {-1.0,0.0,-1.0,0.0},{-1.0,0.0,-1.0,0.0},{-1.0,0.0,-1.0,0.0},{-1.0,0.0,-1.0,0.0}};

  teapots[0] = ConstructTheTeapot ( MyColour );
  teapots[1] = malloc ( sizeof(BezierPatchObjf) );
  if ( teapots[0] && teapots[1] ) {
    SetupMaterial ( 0, ambr, diffr, specr, shn, wa, we );
    teapots[0]->buf[3] = NewUniformBlockObject ( 32*4*sizeof(GLfloat), txcbp );
    glBufferSubData ( GL_UNIFORM_BUFFER, txcofs[0], 32*4*sizeof(GLfloat), txc );
    SetBezierPatchOptions ( teapots[0], BezNormals, TessLevel );
    memcpy ( teapots[1], teapots[0], sizeof(BezierPatchObjf) );
    glGenBuffers ( 1, &teapots[1]->buf[1] );
    glBindBuffer ( GL_UNIFORM_BUFFER, teapots[1]->buf[1] );
    glBufferData ( GL_UNIFORM_BUFFER, cpbofs[1]+306*3*sizeof(GLfloat),
                   NULL, GL_DYNAMIC_DRAW );
    glBufferSubData ( GL_UNIFORM_BUFFER, cpbofs[0],
                      sizeof(GLint), &teapots[1]->dim );
    ExitIfGLError ( "ConstructMyTeapot" );
  }
  else
    ExitOnError ( "ConstructMyTeapot" );
} /*ConstructMyTeapot*/

void DrawMyTeapot ( char final, GLuint program )
{
  if ( skeleton ) {
    glLineWidth ( 2.0 );
    glPolygonMode ( GL_FRONT_AND_BACK, GL_LINE );
  }
  else
    glPolygonMode ( GL_FRONT_AND_BACK, GL_FILL );
  glUseProgram ( program );
  if ( final ) {
    glUniformSubroutinesuiv ( GL_FRAGMENT_SHADER, 1, (GLuint*)&LightProcInd );
    glUniform1i ( ucs_loc, colour_source );
    glUniform1i ( uns_loc, normal_source );
    ChooseMaterial ( 0 );
    if ( colour_source == 2 ) {
      glBindBufferBase ( GL_UNIFORM_BUFFER, txcbp, myteapots[0]->buf[3] );
      glActiveTexture ( GL_TEXTURE0 );
      glBindTexture ( GL_TEXTURE_2D, mytexture );
      DrawBezierPatches ( myteapots[1] );
      glBindTexture ( GL_TEXTURE_2D, 0 );
      return;
    }
  }
  DrawBezierPatches ( myteapots[1] );
} /*DrawMyTeapot*/

void DrawMyTeapotCNet ( GLuint program )
{
  glUseProgram ( program );
  glLineWidth ( 1.0 );
  DrawBezierNets ( myteapots[1] );
} /*DrawMyTeapotCNet*/

void ConstructMyTorus ( BezierPatchObjf *toruses[2] )
{
  GLfloat MyColour[4] = { 0.2, 0.3, 1.0, 1.0 };
  const GLfloat ambr[4]  = { 0.0, 1.0, 1.0, 1.0 };
  const GLfloat diffr[4] = { 0.0, 0.4, 1.0, 1.0 };
  const GLfloat specr[4] = { 0.7, 0.7, 0.7, 1.0 };
  const GLfloat shn = 20.0, wa = 2.0, we = 5.0;

  toruses[0] = EnterTorus ( 1.0, 0.5, MyColour );
  toruses[1] = malloc ( sizeof(BezierPatchObjf) );
  if ( toruses[0] && toruses[1] ) {
    SetupMaterial ( 1, ambr, diffr, specr, shn, wa, we );
    SetBezierPatchOptions ( toruses[0], BezNormals, TessLevel );
    memcpy ( toruses[1], toruses[0], sizeof(BezierPatchObjf) );
    glGenBuffers ( 1, &toruses[1]->buf[1] );
    glBindBuffer ( GL_UNIFORM_BUFFER, toruses[1]->buf[1] );
    glBufferData ( GL_UNIFORM_BUFFER, cpbofs[1]+49*4*sizeof(GLfloat),
                   NULL, GL_DYNAMIC_DRAW );
    glBufferSubData ( GL_UNIFORM_BUFFER, cpbofs[0],
                      sizeof(GLint), &toruses[1]->dim );
    ExitIfGLError ( "ConstructMyTorus" );
  }
  else
    ExitOnError ( "ConstructMyTorus" );
} /*ConstructMyTorus*/

void DrawMyTorus ( char final, GLuint program )
{
  if ( skeleton ) {
    glLineWidth ( 1.0 );
    glPolygonMode ( GL_FRONT_AND_BACK, GL_LINE );
  }
  else
    glPolygonMode ( GL_FRONT_AND_BACK, GL_FILL );
  glUseProgram ( program );
  if ( final ) {
    glUniformSubroutinesuiv ( GL_FRAGMENT_SHADER, 1, (GLuint*)&LightProcInd );
    glUniform1i ( ucs_loc, colour_source > 0 ? 1 : 0 );
    glUniform1i ( uns_loc, normal_source );
    ChooseMaterial ( 1 );
  }
  DrawBezierPatches ( mytoruses[1] );
} /*DrawMyTorus*/

void DrawMyTorusCNet ( GLuint program )
{
  glUseProgram ( program );
  glLineWidth ( 1.0 );
  DrawBezierNets ( mytoruses[1] );
} /*DrawMyTorusCNet*/

static int r0[198] =
  {  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15,
    16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31,
    32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47,
    48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63,
    64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79,
    80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95,
    96, 97, 98, 99,100,101,102,103,104,105,106,107,108,109,110,111,
   112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,
   128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,
   144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,
   160,161,162,163,164,165,166,167,168,177,178,179,180,269,274,275,
   276,277,278,279,280,281,285,286,287,288,289,290,294,295,296,297,
   298,299,302,303,304,305};
static int r1[30] =
  {169,170,171,172,173,174,175,176,181,182,183,184,185,186,187,
   188,189,190,191,192,193,194,195,196,197,198,199,200,201,202};
static int r2[62] =
  {203,206,207,208,209,210,211,212,213,214,216,217,218,219,220,221,
   223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,
   239,240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,
   255,256,257,258,259,260,261,262,263,264,265,266,267,268};

static void LKPostprocessBP ( kl_object *obj )
{
  BezierPatchObjf **bp;
  GLint           ntr;

  glUseProgram ( program_id[5] );
  bp = (BezierPatchObjf**)obj->usrdata;
  glBindBufferBase ( GL_UNIFORM_BUFFER, ctrbbp, lktrbuf[0] );
  glBindBufferBase ( GL_UNIFORM_BUFFER, cpbbp, bp[0]->buf[1] );
  glBindBufferBase ( GL_SHADER_STORAGE_BUFFER, 0, bp[1]->buf[1] );
  if ( bp[0] == myteapots[0] ) {
    glBindBufferBase ( GL_UNIFORM_BUFFER, ctribbp, lktrbuf[1] );
    ntr = -1;
  }
  else if ( bp[0] == mytoruses[0] )
    ntr = 3;
  else
    ExitOnError ( "LKPostprocessBP" );
  glUniform1i ( dim_loc, bp[0]->dim );
  glUniform1i ( trnum_loc, ntr );
  glUniform1i ( ncp_loc, (GLint)obj->nvert );
  glDispatchCompute ( (obj->nvert+127)/128, 1, 1 );
  glMemoryBarrier ( GL_UNIFORM_BARRIER_BIT );
  ExitIfGLError ( "LKPostprocessBP" );
} /*LKPostprocessBP*/

static void LKTransformBP ( kl_object *obj, int refn, GLfloat *tr,
                            int nv, int *vn )
{
  glBindBuffer ( GL_UNIFORM_BUFFER, lktrbuf[0] );
  glBufferSubData ( GL_UNIFORM_BUFFER, refn*16*sizeof(GLfloat),
                    16*sizeof(GLfloat), tr );
  ExitIfGLError ( "LKTransformBP" );
} /*LKTransformBP*/

static void PrepareKLBuffers ( kl_linkage *lkg )
{
  GLuint *cpi;
  int    r, i, nv;

  nv = lkg->obj[0].nvert;
  if ( (cpi = malloc ( nv*sizeof(GLuint) )) ) {
    memset ( cpi, 0, nv*sizeof(GLuint) );
    for ( r = 0; r < lkg->norefs; r++ )
      if ( lkg->oref[r].on == 0 ) {
        for ( i = 0; i < lkg->oref[r].nv; i++ )
          cpi[lkg->oref[r].vn[i]] = r;
      }
    glGenBuffers ( 2, lktrbuf );
    glBindBuffer ( GL_UNIFORM_BUFFER, lktrbuf[0] );
    glBufferData ( GL_UNIFORM_BUFFER, lkg->norefs*16*sizeof(GLfloat),
                   NULL, GL_DYNAMIC_DRAW );
    glBindBuffer ( GL_UNIFORM_BUFFER, lktrbuf[1] );
    glBufferData ( GL_UNIFORM_BUFFER, nv*sizeof(GLuint), cpi, GL_STATIC_DRAW );
    ExitIfGLError ( "PrepareKLBuffers" );
    free ( cpi );
  }
  else
    ExitOnError ( "PrepareKLBuffers" );
} /*PrepareKLBuffers*/

kl_linkage *ConstructMyLinkage ( void )
{
#define SCF (1.0/3.0)
  kl_linkage *lkg;
  int        obj[3], l[10], j[9];
  int        i;
  GLfloat    tra[16], trb[16], trc[16];

  ConstructMyTeapot ( myteapots );
  ConstructMyTorus ( mytoruses );
  if ( (lkg = kl_NewLinkage ( 3, 10, 5, 9, 9, NULL )) ) {
    M4x4Scalef ( tra, SCF, SCF, SCF*4.0/3.0 );
    obj[0] = kl_NewObject ( lkg, 3, 306, tra, (void*)myteapots,
                  NULL, LKTransformBP, LKPostprocessBP, NULL, NULL );
    obj[2] = kl_NewObject ( lkg, 3, 0, tra, NULL, NULL,
                            LKTransformParticles, NULL, NULL, NULL );
    M4x4Scalef ( tra, 0.1, 0.1, 0.1 );
    M4x4RotateXf ( trb, 0.5*PI );
    M4x4Multf ( trc, trb, tra );
    obj[1] = kl_NewObject ( lkg, 4, 49, trc, (void*)mytoruses,
                  NULL, LKTransformBP, LKPostprocessBP, NULL, NULL );
    for ( i = 0; i < 10; i++ )
      l[i] = kl_NewLink ( lkg );
    kl_NewObjRef ( lkg, l[2], obj[0], 198, r0 );
    kl_NewObjRef ( lkg, l[4], obj[0], 30, r1 );
    kl_NewObjRef ( lkg, l[6], obj[0], 62, r2 );
    kl_NewObjRef ( lkg, l[9], obj[1], 49, NULL );
    kl_NewObjRef ( lkg, l[4], obj[2], 0, NULL );
    j[0] = kl_NewJoint ( lkg, l[0], l[1], KL_ART_ROT_Z, 0 );
    j[1] = kl_NewJoint ( lkg, l[1], l[2], KL_ART_ROT_Y, 1 );
    j[2] = kl_NewJoint ( lkg, l[2], l[3], KL_ART_ROT_Y, 2 );
    j[3] = kl_NewJoint ( lkg, l[3], l[4], KL_ART_ROT_Y, 3 );
    j[4] = kl_NewJoint ( lkg, l[2], l[5], KL_ART_ROT_Y, 4 );
    j[5] = kl_NewJoint ( lkg, l[5], l[6], KL_ART_ROT_Y, 5 );
    j[6] = kl_NewJoint ( lkg, l[2], l[7], KL_ART_ROT_Y, 6 );
    j[7] = kl_NewJoint ( lkg, l[7], l[8], KL_ART_ROT_Y, 7 );
    j[8] = kl_NewJoint ( lkg, l[8], l[9], KL_ART_ROT_Z, 8 );
    M4x4Translatef ( lkg->current_root_tr, 0.0, 0.0, -0.6 );
    M4x4Translatef ( tra, -0.43, 0.0, 0.92 );
    kl_SetJointFtr ( lkg, j[1], tra, 1 );
    M4x4Translatef ( tra, 0.78, 0.0, 0.59 );
    kl_SetJointFtr ( lkg, j[2], tra, 1 );
    M4x4Translatef ( tra, 0.78, 0.0, 0.91 );
    kl_SetJointFtr ( lkg, j[3], tra, 1 );
    M4x4Translatef ( tra, 0.6, 0.0, 1.0 );
    kl_SetJointFtr ( lkg, j[4], tra, 1 );
    M4x4Translatef ( tra, -0.6, 0.0, 1.0 );
    kl_SetJointFtr ( lkg, j[5], tra, 1 );
    M4x4Translatef ( tra, 0.52, 0.0, 0.97 );
    kl_SetJointFtr ( lkg, j[6], tra, 0 );
    M4x4Translatef ( tra, 0.52, 0.0, 0.0 );
    kl_SetJointFtr ( lkg, j[7], tra, 0 );
    PrepareKLBuffers ( lkg );
  }
  else
    ExitOnError ( "ConstructMyLinkage" );
  return lkg;
#undef SCF
} /*ConstructMyLinkage*/

GLfloat TeapotRotAngle2 ( float time )
{
  float s;

  s = sin ( 2.0*PI*time/8.0 );
  return s > 0.5 ? 3.0*(s-0.5) : 0.0;
} /*TeapotRotAngle2*/

GLfloat SpoutAngle ( float time )
{
  float s;

  s = sin ( 2.0*PI*time/8.0 );
  return s > 0.25 ? 2.0*(s-0.25) : 0.0;
} /*SpoutAngle*/

GLfloat LidAngle1 ( float time )
{
  float s;

  s = sin ( 2.0*PI*time/8.0 );
  if ( s <= 0.8 ) return 0.0;
  s = 6.0*(s-0.8);
  return s < 0.4 ? s : 0.4;
} /*LidAngle1*/

GLfloat LidAngle2 ( float time )
{
  float s;

  s = sin ( 2.0*PI*time/8.0 );
  return s < 0.0 ? s : 0.0;
} /*LidAngle2*/

GLfloat TorusRotAngle1 ( float time )
{
  float s;

  if ( particles )
    return 4.6*0.4;
  else {
    s = sin ( 2.0*PI*time/8.0 );
    return s < -0.2 ? 4.6*(0.2-s) : 4.6*0.4;
  }
} /*TorusRotAngle1*/

void ArticulateMyLinkage ( float time )
{
  GLfloat par[9];

  teapot_rot_angle = teapot_rot_angle0 + 0.78539816*(time-teapot_time0);
  par[0] = animate ? teapot_rot_angle : teapot_rot_angle0;
  par[1] = TeapotRotAngle2 ( time );
  par[2] = SpoutAngle ( time );
  par[3] = -0.5*par[2];
  par[4] = LidAngle1 ( time );
  par[5] = LidAngle2 ( time );
  par[7] = -(par[6] = TorusRotAngle1 ( time ));
  par[8] = -2.0*PI*time;
  kl_SetArtParam ( mylinkage, 0, 9, par );
  kl_Articulate ( mylinkage );
} /*ArticulateMyLinkage*/

