
#define SHADOW_MAP_SIZE 1024

#define MAX_NLIGHTS   8
#define MAX_MATERIALS 8

typedef struct LSPar {
          GLfloat ambient[4];
          GLfloat diffuse[4];
          GLfloat position[4];
          GLfloat attenuation[3];
          GLuint  shadow_fbo, shadow_txt[2];
          GLfloat shadow_view[16], shadow_proj[16];
        } LSPar;

typedef struct LightBl {
          GLuint nls, mask, shmask;
          LSPar  ls[MAX_NLIGHTS];
        } LightBl;

extern GLuint  lsbi, lsbuf, lsbbp;
extern GLint   lsbsize, lsbofs[9];
extern LightBl light;

extern GLuint matbi, matbuf[MAX_MATERIALS], matbbp;
extern GLint  matbsize, matbofs[6];


void SetVPMatrix ( GLfloat vm[16], GLfloat pm[16], GLfloat ep[4] ); /* app2g.h */

void SetLightAmbient ( int l, GLfloat amb[4] );
void SetLightDiffuse ( int l, GLfloat dif[4] );
void SetLightPosition ( int l, GLfloat pos[4] );
void SetLightAttenuation ( int l, GLfloat at3[3] );
void SetLightOnOff ( int l, char on );

void SetupMaterial ( int m, const GLfloat ambr[4], const GLfloat diffr[4],
                     const GLfloat specr[4],
                     GLfloat shn, GLfloat wa, GLfloat we );
void ChooseMaterial ( int m );
void DeleteMaterial ( int m );

void ConstructShadowTxtFBO ( int l );
void UpdateShadowMatrix ( int l );
void SetupShadowTxtTransformations ( int l, float sc[3], float R );
void BindShadowTxtFBO ( int l );
void DestroyShadowFBO ( void );

