#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <math.h>
#include <sys/times.h>

#include "myglheader.h"
#include <GLFW/glfw3.h>

#include "../utilities/utilities.h"
#include "accumbuf.h"
#include "linkage.h"
#include "bezpatches.h"
#include "particles.h"
#include "lights.h"
#include "app2j.h"

GLuint  part_shader_id[3]; 
GLuint  part_program_id[2];
GLint   psloc[14], pcloc;

PartSystem ps;

GLuint veli, posi;
GLuint rvpbp, posbp, velbp;
GLint  velsize, possize;

void LoadParticleShaders ( void )
{
  static const char *filename[] =
    { "app2i0.glsl.comp", "app2i0.glsl.vert", "app2i0.glsl.frag" };
  static const GLchar *SBNames[] =
    { "RandomVecPool", "PartPos", "PartVel" };
  static const GLchar *UPNames[] =
    { "ps.tablength", "ps.iter", "ps.dt", "ps.Tpart", "ps.vel", "ps.TC",
      "ps.acc", "ps.divv", "ps.divp", "ps.pos0", "ps.pos1",
      "ps.vel0", "ps.vel1", "ps.conv" };
  static const GLchar PCName[] = "PartColour";
  GLint i, size;

  part_shader_id[0] = CompileShaderFiles ( GL_COMPUTE_SHADER, 1, &filename[0] );
  part_program_id[0] = LinkShaderProgram ( 1, &part_shader_id[0] );
  part_shader_id[1] = CompileShaderFiles ( GL_VERTEX_SHADER, 1, &filename[1] );
  part_shader_id[2] = CompileShaderFiles ( GL_FRAGMENT_SHADER, 1, &filename[2]);
  part_program_id[1] = LinkShaderProgram ( 2, &part_shader_id[1] );
  for ( i = 0; i < 14; i++ )
    psloc[i] = glGetUniformLocation ( part_program_id[0], UPNames[i] );
  GetAccessToStorageBlock ( part_program_id[0], 0, &SBNames[0],
                            &posi, &size, NULL, &rvpbp );
  GetAccessToStorageBlock ( part_program_id[0], 0, &SBNames[1],
                            &posi, &size, NULL, &posbp );
  GetAccessToStorageBlock ( part_program_id[0], 0, &SBNames[2],
                            &veli, &size, NULL, &velbp );
  pcloc = glGetUniformLocation ( part_program_id[1], PCName );
  AttachUniformBlockToBP ( part_program_id[1], "TransBlock", trbbp );
  ExitIfGLError ( "LoadParticleShaders" );
} /*LoadParticleShaders*/

GLfloat RandomFloat ( void )
{
  float res;
  unsigned int tmp;
  static unsigned int seed = 0xFFFF0C59;

  seed *= 16807;
  tmp = seed ^ (seed >> 4) ^ (seed << 15);
  *((unsigned int *) &res) = (tmp >> 9) | 0x3F800000;
  return res - 1.0;
} /*RandomFloat*/

static int cnt = 0;

void RandomVector ( GLfloat rv[4] )
{
  int i;

  for ( ; ; cnt++ ) {
    for ( i = 0; i < 3; i++ )
      rv[i] = 2.0*RandomFloat() - 1.0;
    if ( V3DotProductf ( rv, rv ) <= 1.0 ) {
      rv[3] = 0.0;
      return;
    }
  }
} /*RandomVector*/

void InitParticleSystem ( void )
{
  GLfloat *mbuf;
  int     i;

  glGenVertexArrays ( 1, &ps.vao );
  glBindVertexArray ( ps.vao );
  glGenBuffers ( 3, ps.sbo );
  if ( !(mbuf = (GLfloat*)malloc ( PARTICLE_COUNT*4*sizeof(GLfloat) )) )
    ExitOnError ( "InitParticles" );
  glBindBuffer ( GL_ARRAY_BUFFER, ps.sbo[0] );
  for ( i = 0; i < PARTICLE_COUNT; i++ ) {
    RandomVector ( &mbuf[4*i] );
    mbuf[4*i+3] = (float)i/(float)PARTICLE_COUNT - 1.0;
  }
  glBufferData ( GL_ARRAY_BUFFER, PARTICLE_COUNT*4*sizeof(GLfloat),
                 mbuf, GL_STATIC_DRAW );
  glBindBuffer ( GL_ARRAY_BUFFER, ps.sbo[1] );
  glBufferData ( GL_ARRAY_BUFFER, PARTICLE_COUNT*4*sizeof(GLfloat),
                 mbuf, GL_DYNAMIC_COPY );
  glVertexAttribPointer ( 0, 4, GL_FLOAT, GL_FALSE, 0, NULL );
  glEnableVertexAttribArray ( 0 );
  glBindBuffer ( GL_ARRAY_BUFFER, ps.sbo[2] );
  glBufferData ( GL_ARRAY_BUFFER, PARTICLE_COUNT*4*sizeof(GLfloat),
                 NULL, GL_DYNAMIC_COPY );
  free ( mbuf );
  glUseProgram ( part_program_id[0] );
  glUniform1i ( psloc[0], ps.tablength = PARTICLE_COUNT );
  glUniform1i ( psloc[1], ps.iter = 0 );
  glUniform1f ( psloc[2], ps.dt = 0.0 );
  glUniform1f ( psloc[3], ps.Tpart = 1.5 );
  glUniform1f ( psloc[5], ps.TC = 0.4 );
  glUniform1f ( psloc[6], ps.acc = 0.6 );
  glUniform1f ( psloc[7], ps.divv = 0.2 );
  glUniform1f ( psloc[8], ps.divp = 0.04 );
  ps.conv[0] = ps.conv[1] = 0.0;  ps.conv[2] = 0.5;
  glUniform3fv ( psloc[13], 1, ps.conv );
  ExitIfGLError ( "InitPartSystem" );
} /*InitPartSystem*/

void LKTransformParticles ( kl_object *obj, int refn, GLfloat *tr,
                            int nv, int *vn )
{
  const GLfloat p0[3] = {3.083333,0.0,2.4875},
                v0[3] = {0.8,0.0,0.6};

  memcpy ( ps.pos0, ps.pos1, 3*sizeof(GLfloat) );
  M4x4MultMP3f ( ps.pos1, tr, p0 );
  memcpy ( ps.vel0, ps.vel1, 3*sizeof(GLfloat) );
  M4x4MultMV3f ( ps.vel1, tr, v0 );
  V3Normalisef ( ps.vel1 );
} /*LKTransformParticles*/

void MoveParticles ( float time )
{
  GLfloat deltat;

  if ( (deltat = time-ps.last_time) < 0.01 )
    return;
  ps.last_time = time;
  if ( deltat > 1.0 )
    deltat = 1.0;
  glUseProgram ( part_program_id[0] );
  glBindBufferBase ( GL_SHADER_STORAGE_BUFFER, rvpbp, ps.sbo[0] );
  glBindBufferBase ( GL_SHADER_STORAGE_BUFFER, posbp, ps.sbo[1] );
  glBindBufferBase ( GL_SHADER_STORAGE_BUFFER, velbp, ps.sbo[2] );
  time -= ps.time0;
  glUniform1i ( psloc[1], ps.iter = (ps.iter+1) % PARTICLE_COUNT );
  glUniform1f ( psloc[2], ps.dt = deltat );
  glUniform1f ( psloc[4], ps.vel = 1.0+0.1*sin ( 18.0*PI*time*(1.0+sin(time)) ) );
  glUniform3fv ( psloc[9], 1, ps.pos0 );
  glUniform3fv ( psloc[10], 1, ps.pos1 );
  glUniform3fv ( psloc[11], 1, ps.vel0 );
  glUniform3fv ( psloc[12], 1, ps.vel1 );
  glDispatchCompute ( PARTICLE_GROUP_COUNT, 1, 1 );
  glMemoryBarrier ( GL_SHADER_IMAGE_ACCESS_BARRIER_BIT );
  ExitIfGLError ( "MoveParticles" );
} /*MoveParticles*/

void DrawParticles ( char shadow )
{
  GLfloat c[4];

  glDepthMask ( false );
  glUseProgram ( part_program_id[1] );
  if ( shadow ) {
    c[0] = c[1] = c[2] = 0.7;
    c[3] = (float)(SHADOW_MAP_SIZE*SHADOW_MAP_SIZE);
  }
  else {
    c[0] = c[1] = c[2] = 0.85;
    c[3] = (float)(camera.win_height*camera.win_height);
  }
  c[3] = exp ( -0.001*c[3]/(float)PARTICLE_COUNT );
  glUniform4fv ( pcloc, 1, c );
  glBindVertexArray ( ps.vao );
  glEnable ( GL_DEPTH_CLAMP );
  glEnable ( GL_BLEND );
  glBlendFunc ( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
  glPointSize ( 1.0 );
  glDrawArrays ( GL_POINTS, 0, PARTICLE_COUNT );
  glDisable ( GL_BLEND );
  glDisable ( GL_DEPTH_CLAMP );
  glDepthMask ( true );
  ExitIfGLError ( "DrawParticles" );
} /*DrawParticles*/

void ResetParticles ( float time )
{
  ps.time0 = ps.last_time = time;
  glBindBuffer ( GL_COPY_READ_BUFFER, ps.sbo[0] );
  glBindBuffer ( GL_COPY_WRITE_BUFFER, ps.sbo[1] );
  glCopyBufferSubData ( GL_COPY_READ_BUFFER, GL_COPY_WRITE_BUFFER,
                        0, 0, PARTICLE_COUNT*4*sizeof(GLfloat) );
  glUseProgram ( part_program_id[0] );
  glUniform1i ( psloc[1], (ps.iter = 0) );
  ExitIfGLError ( "ResetParticles" );
} /*ResetParticles*/

void DestroyParticleSystem ( void )
{
  glDeleteVertexArrays ( 1, &ps.vao );
  glDeleteBuffers ( 3, ps.sbo );
} /*DestroyParticleSystem*/

void DeleteParticleShaders ( void )
{
  int i;

  glUseProgram ( 0 );
  for ( i = 0; i < 3; i++ )
    glDeleteShader ( part_shader_id[i] );
  for ( i = 0; i < 2; i++ )
    glDeleteProgram ( part_program_id[i] );
} /*DeleteParticleShaders*/

