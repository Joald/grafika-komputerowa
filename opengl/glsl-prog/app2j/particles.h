
#define PARTICLE_GROUP_SIZE  1024
#define PARTICLE_GROUP_COUNT 1024
#define PARTICLE_COUNT       (PARTICLE_GROUP_SIZE*PARTICLE_GROUP_COUNT)

typedef struct {
          GLuint  vao, sbo[3];
          float   time0, last_time;
          GLint   tablength, iter;
          GLfloat dt, Tpart, vel, TC, acc, divv, divp;
          GLfloat pos0[3], pos1[3], vel0[3], vel1[3], conv[3];
        } PartSystem;

extern GLuint     part_shader_id[3];
extern GLuint     part_program_id[2];
extern PartSystem ps;

GLfloat RandomFloat ( void );
void RandomVector ( GLfloat rv[4] );

void LoadParticleShaders ( void );
void InitParticleSystem ( void );

void LKTransformParticles ( kl_object *obj, int refn, GLfloat *tr,
                            int nv, int *vn );
void MoveParticles ( float time );
void DrawParticles ( char final );
void ResetParticles ( float time );

void DestroyParticleSystem ( void );
void DeleteParticleShaders ( void );

