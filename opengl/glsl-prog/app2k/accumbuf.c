#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include "myglheader.h"

#include "../utilities/utilities.h"
#include "accumbuf.h"

#define ACCBUF_NONE     0
#define ACCBUF_AVERAGE  5

#define ACCBUF_LOC_SIZE_X  1024
#define GWX (acb->width+ACCBUF_LOC_SIZE_X-1)/ACCBUF_LOC_SIZE_X

static GLuint accshid[2], accprid[2];

char LoadAccumBufShaders ( void )
{
  static const char *filename[] = { "accbuf0.glsl.comp", "accbuf1.glsl.comp" };

  accshid[0] = CompileShaderFiles ( GL_COMPUTE_SHADER, 1, &filename[0] );
  accprid[0] = LinkShaderProgram ( 1, &accshid[0] );
  accshid[1] = CompileShaderFiles ( GL_COMPUTE_SHADER, 1, &filename[1] );
  accprid[1] = LinkShaderProgram ( 1, &accshid[1] );
  ExitIfGLError ( "LoadAccumBufShaders" );
  return true;
} /*LoadAccumBufShaders*/

void DeleteAccumBufShaders ( void )
{
  glUseProgram ( 0 );
  glDeleteShader ( accshid[0] );
  glDeleteProgram ( accprid[0] );
  glDeleteShader ( accshid[1] );
  glDeleteProgram ( accprid[1] );
  ExitIfGLError ( "DeleteAccumBufShaders" );
} /*DeleteAccumBufShaders*/

static char AllocAccTextures ( AccumBuf *acb, int w, int h )
{
  glGenTextures ( 3, acb->txt );
  glBindTexture ( GL_TEXTURE_2D, acb->txt[0] );
  glTexStorage2D ( GL_TEXTURE_2D, 1, GL_RGBA32F, w, h );
  glBindTexture ( GL_TEXTURE_2D, 0 );
  glBindFramebuffer ( GL_FRAMEBUFFER, acb->fbo[0] );
  glFramebufferTexture ( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, acb->txt[0], 0 );
  if ( glCheckFramebufferStatus ( GL_FRAMEBUFFER ) != GL_FRAMEBUFFER_COMPLETE )
    ExitOnError ( "AllocAccTextures 0" );
  glBindTexture ( GL_TEXTURE_2D_ARRAY, acb->txt[1] );
  glTexStorage3D ( GL_TEXTURE_2D_ARRAY, 1, GL_RGBA32F, w, h, NLAYERS );
  glBindTexture ( GL_TEXTURE_2D_ARRAY, acb->txt[2] );
  glTexStorage3D ( GL_TEXTURE_2D_ARRAY, 1, GL_DEPTH_COMPONENT32F, w, h, NLAYERS );
  glBindTexture ( GL_TEXTURE_2D_ARRAY, 0 );
  glBindFramebuffer ( GL_FRAMEBUFFER, acb->fbo[1] );
  glFramebufferTexture ( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, acb->txt[1], 0 );
  glFramebufferTexture ( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, acb->txt[2], 0 );
  if ( glCheckFramebufferStatus ( GL_FRAMEBUFFER ) != GL_FRAMEBUFFER_COMPLETE )
    ExitOnError ( "AllocAccTextures 1" );
  glBindFramebuffer ( GL_FRAMEBUFFER, 0 );
  acb->width = w;  acb->height = h;
  ExitIfGLError ( "AllocAccTextures" );
  return true;
} /*AllocAccTextures*/

AccumBuf *NewAccumBuf ( int w, int h )
{
  AccumBuf *acb;

  if ( (acb = malloc ( sizeof(AccumBuf) )) ) {
    glGenFramebuffers ( 2, acb->fbo );
    if ( AllocAccTextures ( acb, w, h ) )
      return acb;
    else {
      glDeleteFramebuffers ( 2, acb->fbo );
      free ( acb );
    }
  }
  return NULL;
} /*NewAccumBuf*/

char SetAccumBufSize ( AccumBuf *acb, int w, int h )
{
  if ( w != acb->width || h != acb->height ) {
    glDeleteTextures ( 3, acb->txt );
    if ( !AllocAccTextures ( acb, w, h ) ) {
      glDeleteFramebuffers ( 2, acb->fbo );
      free ( acb );
      return false;
    }
  }
  return true;
} /*SetAccumBufSize*/

void DestroyAccumBuf ( AccumBuf *acb )
{
  glDeleteFramebuffers ( 2, acb->fbo );
  glDeleteTextures ( 3, acb->txt );
  free ( acb );
} /*DestroyAccumBuf*/

void AccumBufAverage ( AccumBuf *acb, char stereo )
{
  if ( stereo )
    glUseProgram ( accprid[1] );
  else
    glUseProgram ( accprid[0] );
  glBindImageTexture ( 0, acb->txt[0], 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA32F );
  glBindImageTexture ( 1, acb->txt[1], 0, GL_FALSE, 0, GL_READ_ONLY, GL_RGBA32F );
  glDispatchCompute ( GWX, acb->height, 1 );
  glMemoryBarrier ( GL_SHADER_IMAGE_ACCESS_BARRIER_BIT );
  ExitIfGLError ( "AccumBufAverage" );
} /*AccumBufAverage*/

