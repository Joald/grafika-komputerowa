
#define NLAYERS 16

typedef struct AccumBuf {
          GLuint fbo[2];
          GLuint txt[3];
          int    width, height;
        } AccumBuf;

char LoadAccumBufShaders ( void );
void DeleteAccumBufShaders ( void );

AccumBuf *NewAccumBuf ( int w, int h );
char SetAccumBufSize ( AccumBuf *acb, int w, int h );
void DestroyAccumBuf ( AccumBuf *acb );

void AccumBufAverage ( AccumBuf *acb, char stere0 );

