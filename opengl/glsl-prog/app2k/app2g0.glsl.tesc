#version 450 core

layout (vertices=4) out;

in VertInstance {
    int instance;
  } In[];

out TCInstance {
    int instance;
  } Out[];

uniform BezPatch {
    int  dim, udeg, vdeg;
    int  stride_u, stride_v, stride_p, stride_q, nq;
    bool use_ind;
    vec4 Colour;
    int  TessLevel;
    bool BezNormals;
  } bezp;

void main ( void )
{
  if ( gl_InvocationID == 0 ) {
    gl_TessLevelOuter[0] = gl_TessLevelOuter[1] =
    gl_TessLevelOuter[2] = gl_TessLevelOuter[3] = bezp.TessLevel;
    gl_TessLevelInner[0] = gl_TessLevelInner[1] = bezp.TessLevel;
    Out[gl_InvocationID].instance = In[gl_InvocationID].instance;
  }
  gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;
} /*main*/
