#version 450 core

#define NLAYERS 16

layout (invocations=NLAYERS,lines) in;
layout (max_vertices=2,line_strip) out;

uniform TransBlock {
    mat4 mm, mmti;
    mat4 vpm[NLAYERS];
    vec4 eyepos[NLAYERS];
  } trb;

void main ( void )
{
  int i;

  for ( i = 0; i < 2; i++ ) {
    gl_Layer = gl_InvocationID;
    gl_Position = trb.vpm[gl_InvocationID] * gl_in[i].gl_Position;
    EmitVertex ();
  }
  EndPrimitive ();
} /*main*/

