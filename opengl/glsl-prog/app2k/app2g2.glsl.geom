#version 450 core

#define NLAYERS 16

layout(invocations=NLAYERS,triangles) in;
layout(triangle_strip,max_vertices=3) out;

in GVertex {
    vec2 TexCoord;
  } In[];

out FVertex {
    vec3 TexCoord;
  } Out;

uniform TransBlock {
    mat4 mm, mmti;
    mat4 vpm[NLAYERS];
    vec4 eyepos[NLAYERS];
  } trb;

void main ( void )
{
  int i;

  for ( i = 0; i < 3; i++ ) {
    gl_Layer = gl_InvocationID;
    Out.TexCoord = vec3 ( In[i].TexCoord, float(gl_Layer) );
    gl_Position = trb.vpm[gl_InvocationID] * gl_in[i].gl_Position;
    EmitVertex ();
  }
  EndPrimitive ();
} /*main*/

