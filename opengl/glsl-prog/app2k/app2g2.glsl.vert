#version 450 core

#define NLAYERS 16

layout(location=0) in vec4 in_Position;
layout(location=1) in vec4 in_Normal;

uniform TransBlock {
    mat4 mm, mmti;
    mat4 vpm[NLAYERS];
    vec4 eyepos[NLAYERS];
  } trb;

out GVertex {
    vec2 TexCoord;
  } Out;

void main ( void )
{
  switch ( gl_VertexID ) {
default: Out.TexCoord = vec2 ( 0.0, 0.0 );  break;
 case 1: Out.TexCoord = vec2 ( 1.0, 0.0 );  break;
 case 2: Out.TexCoord = vec2 ( 1.0, 1.0 );  break;
 case 3: Out.TexCoord = vec2 ( 0.0, 1.0 );  break;
  }
  gl_Position = trb.mm * in_Position;
} /*main*/
