#version 450 core

#define NLAYERS 16
#define MAX_DEG 10

layout(quads,equal_spacing,ccw) in;

in TCInstance {
    int instance;
  } In[];

uniform CPoints {
    float cp[1];
  } cp;

uniform CPIndices {
    int cpi[1];
  } cpi;

uniform BezPatch {
    int  dim, udeg, vdeg;
    int  stride_u, stride_v, stride_p, stride_q, nq;
    bool use_ind;
    vec4 Colour;
    int  TessLevel;
    bool BezNormals;
  } bezp;

uniform TransBlock {
    mat4 mm, mmti;
    mat4 vpm[NLAYERS];
    vec4 eyepos[NLAYERS];
  } trb;

int inst;

void BCHorner2f ( int n, vec2 bcp[MAX_DEG+1], float t, out vec2 p )
{
  int   i, b;
  float s, d;
  vec2  q;

  s = 1.0-t;  d = t;  b = n;
  q = bcp[0];
  for ( i = 1; i <= n; i++ ) {
    q = s*q + (b*d)*bcp[i];
    d *= t;  b = (b*(n-i))/(i+1);
  }
  p = q;
} /*BCHorner2f*/

void BPHorner2f ( float u, float v, out vec4 pos )
{
  vec2 p[MAX_DEG+1], q[MAX_DEG+1], r;
  int  i, j, k, l, i0;
  vec4 Pos, Normal;

  if ( bezp.use_ind )
    i0 = inst*bezp.stride_p;
  else {
    i = inst / bezp.nq;
    j = inst % bezp.nq;
    i0 = i*bezp.stride_p + j*bezp.stride_q;
  }
  for ( i = k = 0;  i <= bezp.udeg;  i++ ) {
    if ( bezp.use_ind ) {
      for ( j = 0;  j <= bezp.vdeg;  j++, k++ ) {
        l = 2*cpi.cpi[i0+k];
        p[j] = vec2 ( cp.cp[l], cp.cp[l+1] );
      }
    }
    else {
      for ( j = 0, l = i0+i*bezp.stride_u;  j <= bezp.vdeg;  j++, l += bezp.stride_v )
        p[j] = vec2 ( cp.cp[l], cp.cp[l+1] );
      k += bezp.stride_u;
    }
    BCHorner2f ( bezp.vdeg, p, v, q[i] );
  }
  BCHorner2f ( bezp.udeg, q, u, r );
  pos = vec4 ( r, 0.0, 1.0 );
} /*BPHorner2f*/

void BCHorner3f ( int n, vec3 bcp[MAX_DEG+1], float t, out vec3 p )
{
  int   i, b;
  float s, d;
  vec3  q;

  s = 1.0-t;  d = t;  b = n;
  q = bcp[0];
  for ( i = 1; i <= n; i++ ) {
    q = s*q + (b*d)*bcp[i];
    d *= t;  b = (b*(n-i))/(i+1);
  }
  p = q;
} /*BCHorner3f*/

void BPHorner3f ( float u, float v, out vec4 pos )
{
  vec3 p[MAX_DEG+1], q[MAX_DEG+1], r;
  int  i, j, k, l, i0;

  if ( bezp.use_ind )
    i0 = inst*bezp.stride_p;
  else {
    i = inst / bezp.nq;
    j = inst % bezp.nq;
    i0 = i*bezp.stride_p + j*bezp.stride_q;
  }
  for ( i = k = 0;  i <= bezp.udeg;  i++ ) {
    if ( bezp.use_ind ) {
      for ( j = 0;  j <= bezp.vdeg;  j++, k++ ) {
        l = 3*cpi.cpi[i0+k];
        p[j] = vec3 ( cp.cp[l], cp.cp[l+1], cp.cp[l+2] );
      }
    }
    else {
      for ( j = 0, l = i0+i*bezp.stride_u;  j <= bezp.vdeg;  j++, l += bezp.stride_v )
        p[j] = vec3 ( cp.cp[l], cp.cp[l+1], cp.cp[l+2] );
      k += bezp.stride_u;
    }
    BCHorner3f ( bezp.vdeg, p, v, q[i] );
  }
  BCHorner3f ( bezp.udeg, q, u, r );
  pos = vec4 ( r, 1.0 );
} /*BPHorner3f*/

void BCHorner4f ( int n, vec4 bcp[MAX_DEG+1], float t, out vec4 p )
{
  int   i, b;
  float s, d;
  vec4  q;

  s = 1.0-t;  d = t;  b = n;
  q = bcp[0];
  for ( i = 1; i <= n; i++ ) {
    q = s*q + (b*d)*bcp[i];
    d *= t;  b = (b*(n-i))/(i+1);
  }
  p = q;
} /*BCHorner4f*/

void BPHorner4f ( float u, float v, out vec4 pos )
{
  vec4  p[MAX_DEG+1], q[MAX_DEG+1];
  int   i, j, k, l, i0;

  if ( bezp.use_ind )
    i0 = inst*bezp.stride_p;
  else {
    i = inst / bezp.nq;
    j = inst % bezp.nq;
    i0 = i*bezp.stride_p + j*bezp.stride_q;
  }
  for ( i = k = 0;  i <= bezp.udeg;  i++ ) {
    if ( bezp.use_ind ) {
      for ( j = 0;  j <= bezp.vdeg;  j++, k++ ) {
        l = 4*cpi.cpi[i0+k];
        p[j] = vec4 ( cp.cp[l], cp.cp[l+1], cp.cp[l+2], cp.cp[l+3] );
      }
    }
    else {
      for ( j = 0, l = i0+i*bezp.stride_u;  j <= bezp.vdeg;  j++, l += bezp.stride_v )
        p[j] = vec4 ( cp.cp[l], cp.cp[l+1], cp.cp[l+2], cp.cp[l+3] );
    }
    BCHorner4f ( bezp.vdeg, p, v, q[i] );
  }
  BCHorner4f ( bezp.udeg, q, u, pos );
  pos = vec4 ( pos.xyz/pos.w, 1.0 );
} /*BPHorner4f*/

void main ( void )
{
  vec4 pos;

  inst = In[0].instance;
  pos = vec4 ( 0.0 );
  switch ( bezp.dim ) {
case 2: BPHorner2f ( gl_TessCoord.x, gl_TessCoord.y, pos );  break;
case 3: BPHorner3f ( gl_TessCoord.x, gl_TessCoord.y, pos );  break;
case 4: BPHorner4f ( gl_TessCoord.x, gl_TessCoord.y, pos );  break;
  }
  gl_Position = trb.vpm[0] * (trb.mm * pos);
} /*main*/
