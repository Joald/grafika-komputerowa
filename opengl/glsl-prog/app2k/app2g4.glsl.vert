#version 450 core

#define NLAYERS 16

layout(location=0) in vec4 in_Position;

uniform TransBlock {
    mat4 mm, mmti;
    mat4 vpm[NLAYERS];
    vec4 eyepos[NLAYERS];
  } trb;

void main ( void )
{
  gl_Position = trb.vpm[0] * (trb.mm * in_Position);
} /*main*/
