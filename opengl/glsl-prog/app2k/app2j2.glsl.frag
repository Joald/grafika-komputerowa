#version 450 core

in GVertex {
    vec3 TexCoord;
  } In;

out vec4 out_Colour;

layout (binding = 1) uniform sampler2DArray tex;

void main ( void )
{
  if ( gl_FrontFacing )
    out_Colour = vec4(0.3);
  else
    out_Colour = texture ( tex, In.TexCoord );
} /*main*/

