#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/times.h>
#include "myglheader.h"
#include <GLFW/glfw3.h>

#include "../utilities/utilities.h"
#include "accumbuf.h"
#include "linkage.h"
#include "lights.h"
#include "bezpatches.h"
#include "mirror.h"
#include "particles.h"
#include "app2k.h"

GLFWwindow   *mywindow;
double  last_xi, last_eta;

int     app_state = STATE_NOTHING;
float   viewer_rvec[3] = {1.0,0.0,0.0};
float   viewer_rangle = 0.0;
float   viewer_pos0[4] = {0.0,0.0,10.0,1.0};

GLint   BezNormals = GL_TRUE;
GLint   TessLevel = 10;
char    redraw;
char    animate = false, hold = false,
        colour_source = 0, normal_source = 0;

void myGLFWErrorHandler ( int error, const char *description )
{
  fprintf ( stderr, "GLFW error: %s\n", description );
  exit ( 1 );
} /*myGLFWErrorHandler*/

void ReshapeFunc ( GLFWwindow *win, int width, int height )
{
  SetAccumBufSize ( acb, camera.win_width = width, camera.win_height = height );
  if ( particles )
    ResetParticles ( app_time );
} /*ReshapeFunc*/

void Redraw ( GLFWwindow *win )
{
  DrawSceneToWindow ();
  glUseProgram ( 0 );
  glFlush ();
  glfwSwapBuffers ( win );
  ExitIfGLError ( "Redraw" );
  redraw = false;
} /*Redraw*/

void DisplayFunc ( GLFWwindow *win )
{
  redraw = true;
} /*DisplayFunc*/

void ToggleAnimation ( void )
{
  if ( (animate = !animate) ) {
    teapot_time0 = app_time;
    teapot_rot_angle = teapot_rot_angle0;
  }
  else
    teapot_rot_angle0 = teapot_rot_angle;
} /*ToggleAnimation*/

void KeyFunc ( GLFWwindow *win, int key, int scancode, int action, int mode )
{
  if ( action == GLFW_PRESS || action == GLFW_REPEAT ) {
    switch ( key ) {
  case GLFW_KEY_ESCAPE:
      glfwSetWindowShouldClose ( mywindow, 1 );
      break;
  case GLFW_KEY_RIGHT:
      ZoomIn ();
      break;
  case GLFW_KEY_LEFT:
      ZoomOut ();
      break;
  case GLFW_KEY_DOWN:
      FocusDiaphragmShut ();
      break;
  case GLFW_KEY_UP:
      FocusDiaphragmOpen ();
      break;
  case GLFW_KEY_F1:
      stereo = !stereo;
      break;
  default:
      break;
    }
  }
} /*KeyFunc*/

void CharFunc ( GLFWwindow *win, unsigned int charcode )
{
  switch ( charcode ) {
case '+':
    if ( TessLevel < MAX_TESS_LEVEL ) {
      SetBezierPatchOptions ( myteapots[0], BezNormals, ++TessLevel );
      SetBezierPatchOptions ( mytoruses[0], BezNormals, ++TessLevel );
      break;
    }
    else return;
case '-':
    if ( TessLevel > MIN_TESS_LEVEL ) {
      SetBezierPatchOptions ( myteapots[0], BezNormals, --TessLevel );
      SetBezierPatchOptions ( mytoruses[0], BezNormals, --TessLevel );
      break;
    }
    else return;
case ' ':
    ToggleAnimation ();
    return;
case 'B':  case 'b':
    LightProcInd = (LightProcInd == LambertProcInd) ?
                   BlinnPhongProcInd : LambertProcInd;
    break;
case 'C':  case 'c':
    cnet = !cnet;
    break;
case 'D':  case 'd':
    normal_source = (normal_source+1) % 3;
    break;
case 'L':  case 'l':
    skeleton = true;
    break;
case 'N':  case 'n':
    BezNormals = BezNormals == 0;
    SetBezierPatchOptions ( myteapots[0], BezNormals, TessLevel );
    SetBezierPatchOptions ( mytoruses[0], BezNormals, TessLevel );
    break;
case 'S':  case 's':
    skeleton = false;
    break;
case 'P':  case 'p':
    if ( (particles = !particles) )
      ResetParticles ( app_time );
    break;
case 'R':  case 'r':
    if ( particles )
      ResetParticles ( app_time );
    break;
case '0':
    colour_source = 0;
    break;
case '1':
    colour_source = 1;
    break;
case '2':
    colour_source = 2;
    break;
case '>':
    FocusFar ();
    break;
case '<':
    FocusNear ();
    break;
case 'X':  case 'x':
    if ( (hold = !hold) ) {
      SetIdleFunc ( NULL );
      return;
    }
    else
      SetIdleFunc ( IdleFunc );
    break;
case 'T':  case 't':
    dumptxt = true;
    break;
default:    /* ignorujemy wszystkie inne klawisze */
    return;
  }
  redraw = true;
} /*CharFunc*/

void MouseFunc ( GLFWwindow *win, int button, int action, int mods )
{
  switch ( app_state ) {
case STATE_NOTHING:
    if ( button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS ) {
      glfwGetCursorPos ( win, &last_xi, &last_eta );
      app_state = STATE_TURNING;
    }
    break;
case STATE_TURNING:
    if ( button == GLFW_MOUSE_BUTTON_LEFT && action != GLFW_PRESS )
      app_state = STATE_NOTHING;
    break;
default:
    break;
  }
} /*MouseFunc*/

void MotionFunc ( GLFWwindow *win, double x, double y )
{
  switch ( app_state ) {
case STATE_TURNING:
    if ( x != last_xi || y != last_eta ) {
      RotateViewer ( x-last_xi, y-last_eta );
      last_xi = x,  last_eta = y;
      redraw = true;
    }
    break;
default:
    break;
  }
} /*MotionFunc*/

void IdleFunc ( void )
{
  struct tms clk;

  app_time1 = app_time;
  app_time = (float)(times ( &clk )-app_clock0)/clocks_per_sec;
  ArticulateMyLinkage ( app_time );
  if ( particles )
    MoveParticles ( app_time );
  redraw = true;
} /*IdleFunc*/

void Initialise ( int argc, char **argv )
{
  glfwSetErrorCallback ( myGLFWErrorHandler );
  if ( !glfwInit () )
    ExitOnError ( "glfwInit failed" );
  if ( !(mywindow = glfwCreateWindow ( camera.win_width = 480, camera.win_height = 360,
                                       "aplikacja 2K", NULL, NULL )) ) {
    glfwTerminate ();
    ExitOnError ( "glfwCreateWindow failed" );
  }
  glfwMakeContextCurrent ( mywindow );
  GetGLProcAddresses ();
#ifdef USE_GL3W
  if ( !gl3wIsSupported ( 4, 5 ) )
    ExitOnError ( "Initialise: OpenGL version 4.2 not supported\n" );
#endif
  PrintGLVersion ();
  glfwSetWindowSizeCallback ( mywindow, ReshapeFunc );
  glfwSetWindowRefreshCallback ( mywindow, DisplayFunc );
  glfwSetKeyCallback ( mywindow, KeyFunc );
  glfwSetCharCallback ( mywindow, CharFunc );
  glfwSetMouseButtonCallback ( mywindow, MouseFunc );
  glfwSetCursorPosCallback ( mywindow, MotionFunc );
  LoadMyShaders ();
  LoadAccumBufShaders ();
  acb = NewAccumBuf ( camera.win_width, camera.win_height );
  InitMyObject ();
  ReshapeFunc ( mywindow, camera.win_width, camera.win_height );
  redraw = true;
} /*Initialise*/

void Cleanup ( void )
{
  int i;

  glUseProgram ( 0 );
  for ( i = 0; i < 15; i++ )
    glDeleteShader ( shader_id[i] );
  for ( i = 0; i < 6; i++ )
    glDeleteProgram ( program_id[i] );
  glDeleteBuffers ( 1, &trbuf );
  glDeleteBuffers ( 1, &lsbuf );
  DeleteBezierPatchDomain ();
  DeleteBezierPatches ( myteapots[0] );
  glDeleteBuffers ( 1, &myteapots[1]->buf[1] );
  free ( myteapots[1] );
  DeleteBezierPatches ( mytoruses[0] );
  glDeleteBuffers ( 1, &mytoruses[1]->buf[1] );
  free ( mytoruses[1] );
  glDeleteBuffers ( 2, lktrbuf );
  kl_DestroyLinkage ( mylinkage );
  for ( i = 0; i < 2; i++ )
    DeleteMaterial ( i );
  glDeleteTextures ( 1, &mytexture );
  DestroyMirrorVAO ();
  DestroyMirrorFBO ();
  DestroyShadowFBO ();
  DestroyAccumBuf ( acb );
  DeleteAccumBufShaders ();
  DeleteParticleShaders ();
  DestroyParticleSystem ();
  glfwDestroyWindow ( mywindow );
  glfwTerminate ();
} /*Cleanup*/

static void (*idlefunc)(void) = NULL;

void SetIdleFunc ( void(*IdleFunc)(void) )
{
  idlefunc = IdleFunc;
} /*SetIdleFunc*/

void MessageLoop ( void )
{
  do {
    if ( idlefunc ) {
      idlefunc ();
      glfwPollEvents ();
    }
    else
      glfwWaitEvents ();
    if ( redraw )
      Redraw ( mywindow );
  } while ( !glfwWindowShouldClose ( mywindow ) );
} /*MessageLoop*/

int main ( int argc, char **argv )
{
  Initialise ( argc, argv );
  SetIdleFunc ( IdleFunc );
  MessageLoop ();
  Cleanup ();
  exit ( 0 );
} /*main*/

