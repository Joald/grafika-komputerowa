
#define STATE_NOTHING   0
#define STATE_TURNING   1

#define MIN_TESS_LEVEL  3
#define MAX_TESS_LEVEL 32

#define MIN_FOV_DIST      400.0       /* mm */
#define MAX_FOV_DIST     1200.0       /* mm */
#define MIN_FRAME_HEIGHT    0.1
#define MAX_FRAME_HEIGHT   20.0
#define MIN_DIAPHRAGM       2.0
#define MAX_DIAPHRAGM      22.0

#define SCREEN_DPI        118.0       /* dots per inch */
#define SCREEN_DPMM (SCREEN_DPI/25.4) /* dots per mm */
#define SCREEN_DIST       800.0       /* mm */
#define EYE_DIST           67.0       /* mm */

typedef struct TransBl {
          GLfloat mm[16], mmti[16],    /* model matrix */
                  wvm0[16],
                  wvpm1[NLAYERS][16],  /* window view and projection matrices */
                  mvpm[NLAYERS][16];   /* mirror view and projection matrices */
          GLfloat eyepos0[4],
                  eyepos1[NLAYERS][4], reyepos[NLAYERS][4];
        } TransBl;

typedef struct Camera {
          int   win_width, win_height;
          float frameheight, unitmm;
          float F, N, dist;
          float left, right, bottom, top, near, far, rl, tb;
          float eyeshift;
        } Camera;

extern GLuint shader_id[15];
extern GLuint program_id[6], progid0[3], progid1[3];
extern GLuint trbi, trbuf, trbbp;       /* access to transformations */
extern GLint  trbsize, trbofs[4];
extern GLint  LightProcLoc, LightProcInd,
              LambertProcInd, BlinnPhongProcInd;
extern GLuint ucs_loc, uns_loc, dim_loc, ncp_loc, trnum_loc;
extern GLuint ccpbi, ctrbi, ctrbi, ctrbbp, ctribi, ctribbp;

extern kl_linkage      *mylinkage;
extern BezierPatchObjf *myteapots[2], *mytoruses[2];
extern GLuint          lktrbuf[2];

extern GLuint mytexture;

extern GLuint mirror_vao, mirror_vbo;

extern float   model_rot_axis[3], teapot_rot_angle0, teapot_rot_angle,
               torus_rot_angle0, torus_rot_angle;
extern GLfloat ident_matrix[16];

extern TransBl trans;
extern Camera  camera;

extern GLFWwindow  *mywindow;
extern double      last_xi, last_eta;
extern int         app_state;
extern float       viewer_rvec[3];
extern float       viewer_rangle;
extern float       viewer_pos0[4];
extern GLint       BezNormals;
extern GLint       TessLevel;
extern char        animate, hold, colour_source, normal_source, stereo;
extern clock_t     app_clock0;
extern float       clocks_per_sec,
                   app_time0, app_time1, app_time, teapot_time0;
extern char        cnet, skeleton, particles;

extern AccumBuf    *acb;

void SetVPMatrices ( int k, GLfloat vpm[][16], GLfloat ep[][4] );
void SetModelMatrix ( GLfloat mm[16], GLfloat mmti[16] );

void InitViewMatrix ( void );
void RotateViewer ( double deltaxi, double deltaeta );

void LoadMyTextures ( void );
void InitLights ( void );

void ConstructMyTeapot ( BezierPatchObjf *teapots[2] );
void DrawMyTeapot ( char final, GLuint program );
void DrawMyTeapotCNet ( GLuint program );

void ConstructMyTorus ( BezierPatchObjf *toruses[2] );
void DrawMyTorus ( char final, GLuint program );
void DrawMyTorusCNet ( GLuint program );

kl_linkage *ConstructMyLinkage ( void );
GLfloat TeapotRotAngle1 ( float time );
GLfloat TeapotRotAngle2 ( float time );
GLfloat SpoutAngle ( float time );
GLfloat LidAngle ( float time );
GLfloat TorusRotAngle1 ( float time );
GLfloat TorusRotAngle2 ( float time );
void ArticulateMyLinkage ( float time );

void ConstructMirror ( void );

void DrawSceneToMirror ( void );
void DrawSceneToShadows ( void );
void DrawSceneToWindow ( void );

void FocusFar ( void );
void FocusNear ( void );
void ZoomIn ( void );
void ZoomOut ( void );
void FocusDiaphragmShut ( void );
void FocusDiaphragmOpen ( void );

void LoadMyShaders ( void );
void InitMyObject ( void );


void myGLFWErrorHandler ( int error, const char *description );
void ReshapeFunc ( GLFWwindow *win, int width, int height );
void DisplayFunc ( GLFWwindow *win );
void MouseFunc ( GLFWwindow *win, int button, int action, int mods );
void MotionFunc ( GLFWwindow *win, double x, double y );
void IdleFunc ( void );
void CharFunc ( GLFWwindow *win, unsigned int charcode );

void Initialise ( int argc, char **argv );
void Cleanup ( void );
void SetIdleFunc ( void(*IdleFunc)(void) );
void MessageLoop ( void );

