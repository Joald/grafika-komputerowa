#version 450 core

#define NLAYERS 16

layout (invocations=NLAYERS,points) in;
layout (points,max_vertices=1) out;

in  float Age[];
out float age;

uniform TransBlock {
    mat4 mm, mmti;
    mat4 vpm[NLAYERS];
    vec4 eyepos[NLAYERS];
  } trb;

void main ( void )
{
  if ( (age = Age[0]) >= 0.0 ) {
    gl_Layer = gl_InvocationID;
    gl_Position = trb.vpm[gl_InvocationID] * gl_in[0].gl_Position;
    EmitVertex ();
    EndPrimitive ();
  }
} /*main*/

