#version 450 core

layout (location = 0) in vec4 vert;

out float Age;

void main ( void )
{
  Age = vert.w;
  gl_Position = vec4 ( vert.xyz, 1.0 );
} /*main*/

