
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <sys/times.h> 
#include <math.h>
#include "myglheader.h"
#include <GLFW/glfw3.h>

#include "../utilities/utilities.h"
#include "accumbuf.h"
#include "linkage.h"
#include "texture.h"
#include "bezpatches.h"
#include "mirror.h"
#include "app2k.h"

GLuint  mirror_fbo, mirror_txt[2];
GLuint  mirror_vao, mirror_vbo;

static const GLfloat mvertpos[4][3] =
  {{1.5,-1.2,-1.0},{1.5,1.2,-1.0},{1.5,1.2,1.0},{1.5,-1.2,1.0}};

void ConstructMirrorFBO ( void )
{
  GLenum status;

  glGenTextures ( 2, mirror_txt );
  glActiveTexture ( GL_TEXTURE1 );
  glBindTexture ( GL_TEXTURE_2D_ARRAY, mirror_txt[0] );
  glTexParameteri ( GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
  glTexParameteri ( GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
  glTexStorage3D ( GL_TEXTURE_2D_ARRAY, 1, GL_RGBA32F,
                   MIRRORTXT_W, MIRRORTXT_H, NLAYERS );
  ExitIfGLError ( "ConstructMirrorFBO 0" );
  glBindTexture ( GL_TEXTURE_2D_ARRAY, mirror_txt[1] );
  glTexStorage3D ( GL_TEXTURE_2D_ARRAY, 1, GL_DEPTH_COMPONENT32F,
                   MIRRORTXT_W, MIRRORTXT_H, NLAYERS );
  ExitIfGLError ( "ConstructMirrorFBO 1" );

  glGenFramebuffers ( 1, &mirror_fbo );
  glBindFramebuffer ( GL_DRAW_FRAMEBUFFER, mirror_fbo );
  glFramebufferTexture ( GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                         mirror_txt[0], 0 );
  glFramebufferTexture ( GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                         mirror_txt[1], 0 );
  if ( (status = glCheckFramebufferStatus ( GL_DRAW_FRAMEBUFFER )) !=
       GL_FRAMEBUFFER_COMPLETE )
    ExitOnError ( "Mirror framebuffer incomplete" );
  glBindFramebuffer ( GL_DRAW_FRAMEBUFFER, 0 );
  glBindTexture ( GL_TEXTURE_2D_ARRAY, 0 );
  ExitIfGLError ( "ConstructMirrorFBO" );
} /*ConstructMirrorFBO*/

void ConstructMirrorVAO ( void )
{
  glGenVertexArrays ( 1, &mirror_vao );
  glBindVertexArray ( mirror_vao );
  glGenBuffers ( 1, &mirror_vbo );
  glBindBuffer ( GL_ARRAY_BUFFER, mirror_vbo );
  glBufferData ( GL_ARRAY_BUFFER,
                 4*3*sizeof(GLfloat), mvertpos, GL_STATIC_DRAW );
  glEnableVertexAttribArray ( 0 );
  glVertexAttribPointer ( 0, 3, GL_FLOAT, GL_FALSE,
                          3*sizeof(GLfloat), (GLvoid*)0 );
  ExitIfGLError ( "ConstructMirrorVAO" );
} /*ConstructMirrorVAO*/

void SetupMirrorVPMatrices ( GLfloat eyepos[4], GLfloat reyepos[4],
                             GLfloat mvpm[16] )
{
  GLfloat mfm[16], mvm[16], mpm[16], *v1, *v2, *nv, *p;
  GLfloat s, a[3], b[3];
  int     i;

  v1 = &mfm[0], v2 = &mfm[4], nv = &mfm[8], p = &mfm[12];
  for ( i = 0; i < 3; i++ ) {
    v1[i] = mvertpos[1][i]-mvertpos[0][i];
    v2[i] = mvertpos[3][i]-mvertpos[0][i];
    a[i] = mvertpos[0][i]-eyepos[i];
  }
  V3CrossProductf ( nv, v2, v1 );
  s = sqrt ( V3DotProductf ( nv, nv ) );
  if ( V3DotProductf ( nv, a ) < 0.0 )
    s = -s;
  nv[0] /= s;  nv[1] /= s;  nv[2] /= s;
  V3ReflectPointf ( reyepos, mvertpos[0], nv, eyepos );
  memcpy ( p, reyepos, 3*sizeof(GLfloat) );
  mfm[3] = mfm[7] = mfm[11] = 0.0;
  mfm[15] = 1.0;
  M4x4Invertf ( mvm, mfm );
  M4x4MultMP3f ( a, mvm, mvertpos[0] );
  M4x4MultMP3f ( b, mvm, mvertpos[2] );
  M4x4Frustumf ( mpm, NULL, a[0], b[0], a[1], b[1], -a[2], camera.far );
  M4x4Multf ( mvpm, mpm, mvm );
} /*SetupMirrorVPMatrices*/

void DrawMirror ( char final, GLuint program )
{
  glUseProgram ( program );
  glBindVertexArray ( mirror_vao );
  if ( final ) {
    glActiveTexture ( GL_TEXTURE1 );
    glBindTexture ( GL_TEXTURE_2D_ARRAY, mirror_txt[0] );
  }
  glDrawArrays ( GL_TRIANGLE_FAN, 0, 4 );
} /*DrawMirror*/

void DestroyMirrorFBO ( void )
{
  glDeleteFramebuffers ( 1, &mirror_fbo );
  glDeleteTextures ( 2, mirror_txt );
  ExitIfGLError ( "DestroyMirrorFBO" );
} /*DestroyMirrorFBO*/

void DestroyMirrorVAO ( void )
{
  glDeleteBuffers ( 1, &mirror_vbo );
  glDeleteVertexArrays ( 1, &mirror_vao );
  ExitIfGLError ( "DestroyMirrorVAO" );
} /*DestroyMirrorVAO*/

