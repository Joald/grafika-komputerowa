
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include <GL/glew.h>

#include "../utilities/utilities.h"
#include "scanner.h"
#include "readsmf.h"

float   vert[4*MAXVERT];
facet   fac[MAXFAC];
int     facetv[MAXFACV];
int     nvert, nfac, nfacv;

/* ////////////////////////////////////////////////////////////////////////// */
#define SMF_FIRST_KEY              100
#define SMF_KEY_BEGIN              100
#define SMF_KEY_BIND               101
#define SMF_KEY_C                  102
#define SMF_KEY_E                  103
#define SMF_KEY_END                104
#define SMF_KEY_F                  105
#define SMF_KEY_FACE               106
#define SMF_KEY_FT                 107
#define SMF_KEY_G                  108
#define SMF_KEY_N                  109
#define SMF_KEY_Q                  110
#define SMF_KEY_R                  111
#define SMF_KEY_ROT                112
#define SMF_KEY_SCALE              113
#define SMF_KEY_SET                114
#define SMF_KEY_T                  115
#define SMF_KEY_T_SCALE            116
#define SMF_KEY_T_TRANS            117
#define SMF_KEY_TEX                118
#define SMF_KEY_TRANS              119
#define SMF_KEY_V                  120
#define SMF_KEY_VERTEX             121
#define SMF_KEY_VERTEX_CORRECTION  122
#define SMF_KEY_VN                 123
#define SMF_KEY_VT                 124
#define SMF_KEY_X                  125
#define SMF_KEY_Y                  126
#define SMF_KEY_Z                  127
#define SMF_NUM_KEYWORDS (SMF_KEY_Z-SMF_FIRST_KEY+1)

const char *smf_keyword[SMF_NUM_KEYWORDS] =
  { "begin",
    "bind",
    "c",
    "e",
    "end",
    "f",
    "face",
    "ft",
    "g",
    "n",
    "q",
    "r",
    "rot",
    "scale",
    "set",
    "t",
    "t_scale",
    "t_trans",
    "tex",
    "trans",
    "v",
    "vertex",
    "vertex_correction",
    "vn",
    "vt",
    "x",
    "y",
    "z" };

typedef struct {
    FILE *f;
  } smf_input_struct;

static smf_input_struct smf_data;
static scanner          smf_scanner;
  
/* ////////////////////////////////////////////////////////////////////////// */
void smf_GetNextSymbol ( void )
{
  int ns;

  ns = GetNextSymbol ( &smf_scanner );
  if ( ns == SYMB_IDENT )
    smf_scanner.nextsymbol = BinSearchNameTable ( SMF_NUM_KEYWORDS,
            SMF_FIRST_KEY, smf_keyword, smf_scanner.nextname );
} /*smf_GetNextSymbol*/

void smf_SkipToLineEnd ( void )
{
  do {
    GetNextChar ( &smf_scanner );
  } while ( smf_scanner.nextchar != '\n' && smf_scanner.nextchar != EOF );
  if ( smf_scanner.nextchar == '\n' ) {
    GetNextChar ( &smf_scanner );
    smf_GetNextSymbol ();
  }
} /*smf_SkipToLineEnd*/

void smf_CloseInputFile ( void )
{
  if ( smf_data.f ) {
    fclose ( smf_data.f );
    smf_data.f = NULL;
  }
  ShutDownScanner ( &smf_scanner );
} /*smf_CloseInputFile*/

static int _smf_ReadInputFile ( void *userdata, int buflength, char *buffer )
{
  smf_input_struct *instr;
  int              inbufcount;

  instr = (smf_input_struct*)userdata;
  inbufcount = fread ( buffer, 1, buflength, instr->f );
  return inbufcount;
} /*_smf_ReadInputFile*/

char smf_OpenInputFile ( const char *filename )
{
  smf_data.f = fopen ( filename, "r+" );
  if ( !smf_data.f )
    return 0;
  if ( InitScanner ( &smf_scanner, 0, NULL, MAX_NAME_LENGTH, NULL,
                         '#', '\n', _smf_ReadInputFile, &smf_data ) ) {
    smf_GetNextSymbol ();
    return 1;
  }
  else {
    smf_CloseInputFile ();
    return 0;
  }
} /*smf_OpenInputFile*/

int smf_GetSign ( void )
{
  int sign;

  sign = 1;
  if ( smf_scanner.nextsymbol == SYMB_MINUS ) {
    smf_GetNextSymbol ();
    sign = -1;
  }
  else if ( smf_scanner.nextsymbol == SYMB_PLUS )
    smf_GetNextSymbol ();
  return sign;
} /*smf_GetSign*/

char ReadSMFFile ( char *fn )
{
  int     vertc;
  int     sign;
  int     i, j, fhe, deg;
  GLfloat tr[16], tt[16], tm[16];
  float   tx[3], *vcp;

  if ( !smf_OpenInputFile ( fn ) )
    return 0;

  vcp = &vert[0];
  M4x4Identf ( tr );
  nvert = nfac = nfacv = 0;
  vertc = 0;
  do {
    switch ( smf_scanner.nextsymbol ) {
case SMF_KEY_BEGIN:
      smf_GetNextSymbol ();
      nvert = nfac = nfacv = 0;
      vertc = 0;
      M4x4Identf ( tr );
      break;

case SYMB_COLON:
      smf_SkipToLineEnd ();
      break;

case SYMB_PERCENT:  /* also a comment */
      smf_SkipToLineEnd ();
      break;

case SMF_KEY_BIND:
case SMF_KEY_C:
case SMF_KEY_E:
case SMF_KEY_FACE:
case SMF_KEY_FT:
case SMF_KEY_G:
case SMF_KEY_N:
case SMF_KEY_R:
case SMF_KEY_TEX:
case SMF_KEY_T_SCALE:
case SMF_KEY_T_TRANS:
case SMF_KEY_VN:
case SMF_KEY_VT:
      smf_SkipToLineEnd ();
      break;

case SMF_KEY_END:
      goto success;
      break;

case SMF_KEY_F:
case SMF_KEY_T:
case SMF_KEY_Q:
      if ( nfac >= MAXFAC ) {
        printf ( "za duzo scian\n" );
        goto failure;
      }
      fac[nfac].degree = 0;
      fac[nfac].first = nfacv;
      smf_GetNextSymbol ();
      for (;;) {
        if ( nfacv >= MAXFACV ) {
          printf ( "za duzo krawedzi\n" );
          goto failure;
        }
        if ( smf_scanner.nextsymbol == SYMB_INTEGER ) {
          facetv[nfacv] = smf_scanner.nextinteger + vertc - 1;
          fac[nfac].degree ++;
          nfacv ++;
          smf_GetNextSymbol ();
        }
        else break;
      }
      if ( fac[nfac].degree < 3 )
        goto skip_facet;
        /* sprawdzenie: zdarzaja sie bledne dane, sciana z powtarzajacym sie */
        /* wierzcholkiem - taka odrzucam */
      deg = fac[nfac].degree;
      fhe = fac[nfac].first;
      for ( i = 1; i < deg; i++ )
        for ( j = 0; j < i; j++ )
          if ( facetv[fhe+j] == facetv[fhe+i] ) {
            nfacv = fhe;
            goto skip_facet;
          }
      nfac ++;
skip_facet:
      break;

case SMF_KEY_ROT:
      smf_GetNextSymbol ();
      i = smf_scanner.nextsymbol;
      smf_GetNextSymbol ();
      sign = smf_GetSign ();
      if ( smf_scanner.nextsymbol == SYMB_INTEGER ||
           smf_scanner.nextsymbol == SYMB_FLOAT ) {
        tx[0] = smf_scanner.nextfloat*PI/180.0;
        smf_GetNextSymbol ();
      }
      else {
        printf ( "blad czytania obrotu\n" );
        goto failure;
      }
      switch ( i ) {
        break;
    case SMF_KEY_X:
        M4x4RotateXf ( tt, tx[0] );
        goto rotate;
    case SMF_KEY_Y:
        M4x4RotateYf ( tt, tx[0] );
        goto rotate;
    case SMF_KEY_Z:
        M4x4RotateZf ( tt, tx[0] );
rotate:
        M4x4Multf ( tm, tr, tt );
        memcpy ( tr, tm, 16*sizeof(GLfloat) );
        break;
    default:
        printf ( "blad czytania obrotu\n" );
        goto failure;
      }
      break;

case SMF_KEY_SCALE:
      smf_GetNextSymbol ();
      for ( i = 0; i < 3; i++ ) {
        sign = smf_GetSign ();
        if ( smf_scanner.nextsymbol == SYMB_INTEGER ||
             smf_scanner.nextsymbol == SYMB_FLOAT ) {
          tx[i] = sign*smf_scanner.nextfloat;
          smf_GetNextSymbol ();
        }
        else {
          printf ( "blad czytania skalowania\n" );
          goto failure;
        }
      }
      M4x4Scalef ( tt, tx[0], tx[1], tx[2] );
      M4x4Multf ( tm, tr, tt );
      memcpy ( tr, tm, 16*sizeof(GLfloat) );
      break;

case SMF_KEY_TRANS:
      smf_GetNextSymbol ();
      for ( i = 0; i < 3; i++ ) {
        sign = smf_GetSign ();
        if ( smf_scanner.nextsymbol == SYMB_INTEGER ||
             smf_scanner.nextsymbol == SYMB_FLOAT ) {
          tx[i] = sign*smf_scanner.nextfloat;
          smf_GetNextSymbol ();
        }
        else {
          printf ( "blad czytania translacji\n" );
          goto failure;
        }
      }
      M4x4Translatef ( tt, tx[0], tx[1], tx[2] );
      M4x4Multf ( tm, tr, tt );
      memcpy ( tr, tm, 16*sizeof(GLfloat) );
      break;

case SMF_KEY_SET:
      smf_GetNextSymbol ();
      if ( smf_scanner.nextsymbol == SMF_KEY_VERTEX_CORRECTION ) {
        smf_GetNextSymbol ();
        sign = smf_GetSign ();
        if ( smf_scanner.nextsymbol == SYMB_INTEGER ) {
          vertc = sign*smf_scanner.nextinteger;
          smf_GetNextSymbol ();
        }
        else
          goto failure;
      }
      else
        smf_SkipToLineEnd ();
      break;

case SMF_KEY_V:
      if ( nvert >= MAXVERT ) {
        printf ( "za duzo wierzcholkow\n" );
        goto failure;
      }
      smf_GetNextSymbol ();
      for ( i = 0; i < 3; i++ ) {
        sign = smf_GetSign ();
        if ( smf_scanner.nextsymbol == SYMB_INTEGER ||
             smf_scanner.nextsymbol == SYMB_FLOAT ) {
          vcp[4*nvert+i] = sign*smf_scanner.nextfloat;
          smf_GetNextSymbol ();
        }
        else {
          printf ( "blad czytania wierzcholka\n" );
          goto failure;
        }
      }
      vcp[4*nvert+3] = 1.0;
      memcpy ( tt, &vert[nvert], 3*sizeof(GLfloat) );
      M4x4MultMP3f ( &vert[nvert], tr, tt );
      nvert ++;
      break;
case SMF_KEY_VERTEX:
case SMF_KEY_VERTEX_CORRECTION:
case SMF_KEY_X:
case SMF_KEY_Y:
case SMF_KEY_Z:
      smf_SkipToLineEnd ();
      break;
case SYMB_EOF:
      break;
case SYMB_ERROR:
      printf ( "SYMB_ERROR at line %d, column %d\n",
               smf_scanner.linenum+1, smf_scanner.colnum+1 );
      goto failure;
default:
      printf ( "SYMBOL %d (%c) at line %d, column %d\n",
               smf_scanner.nextsymbol, smf_scanner.nextsymbol,
               smf_scanner.linenum+1, smf_scanner.colnum+1 );
      goto failure;
    }
  } while ( smf_scanner.nextsymbol != EOF );

success:
  printf ( "nvert = %d, nfac = %d\n", nvert, nfac );
  return 1;

failure:
  smf_CloseInputFile ();
  return 0;
} /*ReadSMFFile*/

