
#define MAX_NAME_LENGTH    32

#define MAXVERT         50000
#define MAXFAC          50000
#define MAXFACV        150000

typedef struct {
    int degree, first;
  } facet;

extern float   vert[4*MAXVERT];
extern facet   fac[MAXFAC];
extern int     facetv[MAXFACV];
extern int     nvert, nfac, nfacv;

char ReadSMFFile ( char *fn );

