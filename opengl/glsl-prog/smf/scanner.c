
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "scanner.h"

/* ////////////////////////////////////////////////////////////////////////// */
char InitScanner ( scanner *sc,
                   int inbuflength, char *inbuffer,
                   int maxnamelength, char *namebuffer,
                   int bcomment, int ecomment,
                   int (*InputData)(void *usrdata,int buflength,char *buffer),
                   void *userdata )
{
  memset ( sc, 0, sizeof(scanner) );
        /* attach or allocate buffers */
  if ( inbuffer ) {
    sc->inbuffer = inbuffer;
    sc->alloc_inb = 0;
  }
  else {
    if ( inbuflength < 1 )
      inbuflength = SCANNER_BUFLENGTH;
    sc->inbuffer = malloc ( inbuflength );
    sc->alloc_inb = 1;
  }
  sc->inbuflength = inbuflength;
  if ( namebuffer ) {
    sc->nextname = namebuffer;
    sc->alloc_nb = 0;
  }
  else {
    sc->nextname = malloc( maxnamelength+1 );
    sc->alloc_nb = 1;
  }
  if ( !sc->inbuffer || !sc->nextname )
    goto failure;
  sc->namebuflength = maxnamelength+1;
        /* store data */
  sc->bcomment = bcomment;
  sc->ecomment = ecomment;
  sc->InputData = InputData;
  sc->userdata = userdata;
        /* initialise input position */
  sc->inbufpos = sc->inbufcount = sc->namebufcount = 0;
  sc->linenum = sc->colnum = 0;
  sc->nextsymbol = SYMB_NONE;
  GetNextChar ( sc );
  return 1;

failure:
  ShutDownScanner ( sc );
  return 0;
} /*InitScanner*/

void GetNextChar ( scanner *sc )
{
  if ( sc->inbufpos >= sc->inbufcount ) {
    if ( !sc->InputData || !sc->inbuffer )
      goto end_of_input;
    sc->inbufcount = sc->InputData ( sc->userdata, sc->inbuflength, sc->inbuffer );
    if ( sc->inbufcount <= 0 ) {
end_of_input:
      sc->nextchar = EOF;
      return;
    }
    sc->inbufpos = 0;
  }
  sc->nextchar = (int)sc->inbuffer[sc->inbufpos++];
  if ( sc->nextchar == '\n' )
    sc->linenum ++, sc->colnum = 0;
  else
    sc->colnum ++;
} /*GetNextChar*/

int GetNextSymbol ( scanner *sc )
{
#define ADDTONBUF \
{ if ( sc->namebufcount < sc->namebuflength-1 ) \
    sc->nextname[sc->namebufcount++] = (char)sc->nextchar; \
  else \
    return (sc->nextsymbol = SYMB_ERROR); \
  GetNextChar ( sc ); \
}

repeat_scan:
  if ( sc->nextchar == sc->bcomment ) {
    do {
      GetNextChar ( sc );
    } while ( sc->nextchar != sc->ecomment && sc->nextchar != EOF );
    if ( sc->nextchar != EOF ) {
      GetNextChar ( sc );
      goto repeat_scan;
    }
  }
  else if ( isdigit ( sc->nextchar ) ) {
        /* integral part */
    sc->nextsymbol = SYMB_INTEGER;
    sc->namebufcount = 0;
    do {
      ADDTONBUF
    } while ( isdigit ( sc->nextchar ) );
    if ( sc->nextchar == '.' ) {
        /* fractional part */
      ADDTONBUF
      while ( isdigit ( sc->nextchar ) ) {
        ADDTONBUF
      }
      sc->nextsymbol = SYMB_FLOAT;
    }
    if ( sc->nextchar == 'e' || sc->nextchar == 'E' ) {
        /* exponent */
      ADDTONBUF
      if ( sc->nextchar == '-' || sc->nextchar == '+' ) {
        ADDTONBUF
      }
      if ( !isdigit ( sc->nextchar ) )
        return (sc->nextsymbol = SYMB_ERROR);
      do {
        ADDTONBUF
      } while ( isdigit ( sc->nextchar ) );
      sc->nextsymbol = SYMB_FLOAT;
    }
        /* convert the character string to a number */
    sc->nextname[sc->namebufcount] = 0;
    if ( sc->nextsymbol == SYMB_INTEGER ) {
      sscanf ( sc->nextname, "%d", &sc->nextinteger );
      sc->nextfloat = (float)sc->nextinteger;
    }
    else
      sscanf ( sc->nextname, "%lf", &sc->nextfloat );
  }
  else if ( isalpha ( sc->nextchar ) ) {
        /* a keyword */
    sc->namebufcount = 0;
    do {
      ADDTONBUF
    } while ( isalpha ( sc->nextchar ) || isdigit (sc->nextchar) ||
              sc->nextchar == '_' );
    sc->nextname[sc->namebufcount] = 0;
    sc->nextsymbol = SYMB_IDENT;
  }
  else {
    switch ( sc->nextchar ) {
  case ' ':
  case '\n':
  case '\r':
  case '\t':
      GetNextChar ( sc );
      goto repeat_scan;
  case '"':
      GetNextChar ( sc );
      sc->namebufcount = 0;
      do {
        ADDTONBUF
      } while ( sc->nextchar != '"' );
      sc->nextname[sc->namebufcount] = 0;
      GetNextChar ( sc );
      sc->nextsymbol = SYMB_STRING;
      break;
  case EOF:
      ShutDownScanner ( sc );
      sc->nextsymbol = SYMB_EOF;
      break;
  case '+':
      sc->nextsymbol = SYMB_PLUS;      goto get_next_char;
  case '-':
      sc->nextsymbol = SYMB_MINUS;     goto get_next_char;
  case '*':
      sc->nextsymbol = SYMB_STAR;      goto get_next_char;
  case '/':
      sc->nextsymbol = SYMB_SLASH;     goto get_next_char;
  case '(':
      sc->nextsymbol = SYMB_LPAREN;    goto get_next_char;
  case ')':
      sc->nextsymbol = SYMB_RPAREN;    goto get_next_char;
  case '[':
      sc->nextsymbol = SYMB_LBRACKET;  goto get_next_char;
  case ']':
      sc->nextsymbol = SYMB_RBRACKET;  goto get_next_char;
  case '{':
      sc->nextsymbol = SYMB_LBRACE;    goto get_next_char;
  case '}':
      sc->nextsymbol = SYMB_RBRACE;    goto get_next_char;
  case '<':
      sc->nextsymbol = SYMB_LANGLE;    goto get_next_char;
  case '>':
      sc->nextsymbol = SYMB_RANGLE;    goto get_next_char;
  case '=':
      sc->nextsymbol = SYMB_EQUAL;     goto get_next_char;
  case '\'':
      sc->nextsymbol = SYMB_CHAR;      goto get_next_char;
  case ',':
      sc->nextsymbol = SYMB_COMMA;     goto get_next_char;
  case '.':
      sc->nextsymbol = SYMB_DOT;       goto get_next_char;
  case ';':
      sc->nextsymbol = SYMB_SEMICOLON; goto get_next_char;
  case ':':
      sc->nextsymbol = SYMB_COLON;     goto get_next_char;
  case '%':
      sc->nextsymbol = SYMB_PERCENT;   goto get_next_char;
  case '#':
      sc->nextsymbol = SYMB_HASH;      goto get_next_char;
  case '!':
      sc->nextsymbol = SYMB_EXCLAM;    goto get_next_char;
  case '~':
      sc->nextsymbol = SYMB_TILDE;     goto get_next_char;
  case '@':
      sc->nextsymbol = SYMB_AT;        goto get_next_char;
  case '$':
      sc->nextsymbol = SYMB_DOLLAR;    goto get_next_char;
  case '&':
      sc->nextsymbol = SYMB_ET;        goto get_next_char;
  case '\\':
      sc->nextsymbol = SYMB_BACKSLASH; goto get_next_char;
  case '?':
      sc->nextsymbol = SYMB_QUESTION;  goto get_next_char;
  case '|':
      sc->nextsymbol = SYMB_VERTBAR;   goto get_next_char;
  default:
      sc->nextsymbol = SYMB_OTHER;
get_next_char:
      GetNextChar ( sc );
      break;
    }
  }

  return sc->nextsymbol;
#undef ADDTONBUF
} /*GetNextSymbol*/

void ShutDownScanner ( scanner *sc )
{
  if ( sc->alloc_inb ) {
    if ( sc->inbuffer ) free ( sc->inbuffer );
  }
  if ( sc->alloc_nb ) {
    if ( sc->nextname ) free ( sc->nextname );
  }
  memset ( sc, 0, sizeof(scanner) );
  sc->nextsymbol = SYMB_ERROR;
} /*ShutDownScanner*/

int BinSearchNameTable ( int nnames, int firstname, const char **names,
                             const char *name )
{
  int i, j, k, l;

  i = k = 0;  j = nnames;
  do {
    k = (i+j)/2;
    l = strcmp ( name, names[k] );
    if ( l == 0 )
      return firstname + k;
    else if ( l < 0 )
      j = k;
    else
      i = k+1;
  } while ( j-i > 0 );
  return SYMB_ERROR;
} /*BinSearchNameTable*/

