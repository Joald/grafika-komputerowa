
/* symbol identifiers - somewhat random, to be rethought */
#define SYMB_ERROR      -2
#define SYMB_EOF        -1
#define SYMB_NONE        0
#define SYMB_PLUS        1
#define SYMB_MINUS       2
#define SYMB_STAR        3
#define SYMB_SLASH       4
#define SYMB_LPAREN      5
#define SYMB_RPAREN      6
#define SYMB_LBRACKET    7
#define SYMB_RBRACKET    8
#define SYMB_LBRACE      9
#define SYMB_RBRACE     10
#define SYMB_LANGLE     11
#define SYMB_RANGLE     12
#define SYMB_EQUAL      13
#define SYMB_INTEGER    14
#define SYMB_FLOAT      15
#define SYMB_CHAR       16
#define SYMB_STRING     17
#define SYMB_COMMA      18
#define SYMB_DOT        19
#define SYMB_SEMICOLON  20
#define SYMB_COLON      21
#define SYMB_PERCENT    22
#define SYMB_HASH       23
#define SYMB_EXCLAM     24
#define SYMB_TILDE      25
#define SYMB_AT         26
#define SYMB_DOLLAR     27
#define SYMB_ET         28
#define SYMB_BACKSLASH  29
#define SYMB_QUESTION   30
#define SYMB_VERTBAR    31
#define SYMB_OTHER      32
#define SYMB_IDENT      33
#define SYMB_FIRSTOTHER 34

#define SCANNER_BUFLENGTH 1024

typedef struct scanner {
    int    inbuflength, namebuflength;
    char   *inbuffer;
    char   *nextname;
    char   alloc_inb, alloc_nb;
    int    bcomment, ecomment;
    int    inbufpos, inbufcount, namebufcount;
    int    linenum, colnum;
    int    nextchar;
    int    nextsymbol;
    int    nextinteger;
    double nextfloat;
    int    (*InputData)( void *usrdata, int buflength, char *buffer );
    void   *userdata;
  } scanner;

char InitScanner ( scanner *sc,
                   int inbuflength, char *inbuffer,
                   int maxnamelength, char *namebuffer,
                   int bcomment, int ecomment,
                   int (*InputData)(void *userdata,int buflength,char *buffer),
                   void *userdata );
void GetNextChar ( scanner *sc );
int GetNextSymbol ( scanner *sc );
void ShutDownScanner ( scanner *sc );

int BinSearchNameTable ( int nnames, int firstname, const char **names,
                         const char *name );

