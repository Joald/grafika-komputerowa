
extern Display    *xdisplay;
extern int        xscreen;
extern Window     xrootwin, xmywin;
extern Visual     *xvisual;
extern Colormap   xcolormap;
extern GLXContext glxcontext;

void InitGLContext ( int major, int minor );

