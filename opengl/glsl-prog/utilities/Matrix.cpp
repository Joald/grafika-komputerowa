#include "Matrix.h"

std::optional<PALU> Matrix::PALUDecompose() const {

    Matrix lu = *this;
    Vector3i p;
    for (int j = 0; j < 3; j++) {
        GLfloat d = std::abs(lu[IND(j, j)]);
        p[j] = j;
        for (int i = j + 1; i < 4; i++) { // Check if we need to permute rows.
            auto row_value = std::abs(lu[IND(i, j)]);
            if (row_value > d) {
                d = row_value;
                p[j] = i;
            }
        }
        if (are_close(d, 0.0)) { // the whole column was 0, matrix is not invertible
            return {};
        }
        if (p[j] != j) { // permutation is not identity
            int i = p[j];
            for (int k = 0; k < 4; k++) {
                std::swap(lu[IND(i, k)], lu[IND(j, k)]);
            }
        }
        for (int i = j + 1; i < 4; i++) {
            d = lu[IND(i, j)] /= lu[IND(j, j)];
            for (int k = j + 1; k < 4; k++) {
                lu[IND(i, k)] -= d * lu[IND(j, k)];
            }
        }
    }
    if (are_close(lu[15], 0.0)) {
        return {};
    } else {
        return std::make_optional<PALU>(lu, p, transposed);
    }
}

std::optional<Matrix> Matrix::invert() const {
    auto op = PALUDecompose();
    if (!op) {
        return {};
    }
    auto& palu = op.value();
    Matrix res;
    for (int i = 0; i < 4; i++) {
        Vector4f e;
        e[i] = 1.0;
        palu.solve(res.column(i), e);
    }
    return {res};
}
