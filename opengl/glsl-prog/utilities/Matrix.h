#ifndef OPENGL_MATRIX_H
#define OPENGL_MATRIX_H

#include <optional>
#include <vector>
#include <GL/gl.h>
#include <stdexcept>
#include <algorithm>
#include "Vectors.h"
#include "math.h"


struct PALU;

class Matrix {
    static const int N = 16;
    std::vector<GLfloat> data;
    bool transposed = false;

    void real_transpose() {
        if (!transposed) {
            return;
        }
        transposed = false;
        auto& A = *this;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < i; j++) {
                std::swap(A[IND(i, j)], A[IND(j, i)]);
            }
        }
    }

    void real_transpose() const {
        // this does not change the logical state,
        // only the internal representation
        const_cast<Matrix*>(this)->real_transpose();
    }

public:

    Matrix() : data(N) {}

    Matrix(const GLfloat arr[N]) : data() {
        std::copy(arr, arr + N, std::back_inserter(data));
    }

    Matrix(std::initializer_list<GLfloat> list) : data() {
        if (list.size() != N) {
            throw std::invalid_argument("Matrices are 4x4, please specify 16 floats!");
        }
        std::copy(list.begin(), list.end(), std::back_inserter(data));
    }

    Matrix(std::vector<GLfloat> v) : data(std::move(v)) {
        if (data.size() != N) {
            throw std::invalid_argument("Matrices are 4x4, please specify 16 floats!");
        }
    }

    Matrix(std::initializer_list<Vector4GLf> list) : data() {
        if (list.size() != N / 4) {
            throw std::invalid_argument("Matrices are 4x4, please specify 4 vectors!");
        }
        data.reserve(N);
        for (auto& i : list) {
            std::copy(i.begin(), i.end(), std::back_inserter(data));
        }
    }

    Matrix(Matrix&& other) noexcept: data(std::move(other.data)) {}

    Matrix& operator=(Matrix&& other) noexcept {
        data = std::move(other.data);
        return *this;
    }

    Matrix(const Matrix& other): data(other.data) {}

    Matrix& operator=(const Matrix& other)  {
        if (this != &other) {
            transposed = other.transposed;
            data = other.data;
        }
        return *this;
    }

    operator std::vector<GLfloat>&() {
        real_transpose();
        return data;
    }

    operator GLfloat*() {
        real_transpose();
        return data.data();
    }

    operator const GLfloat*() const {
        real_transpose();
        return data.data();
    }

    decltype(data.size()) size() const {
        return data.size();
    }

    GLfloat& operator[](int i) {
        real_transpose();
        return data[i];
    }

    const GLfloat& operator[](int i) const {
        real_transpose();
        return data[i];
    }

    Vector4GLf column(int i) {
        real_transpose();
        Vector4GLf res;
        auto it = data.begin() + i * 4;
        std::copy(it, it + 4, res.begin());
        return res;
    }

    std::optional<PALU> PALUDecompose() const;

    std::optional<Matrix> invert() const;

    Matrix& transpose() {
        transposed = !transposed;
        return *this;
    }

    Matrix operator*(const Matrix& B) {
        if (transposed) {
            real_transpose();
        }
        auto& A = *this;
        auto ab = Matrix();
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                ab[IND(i, j)] = A[IND(i, 0)] * B[IND(0, j)];
                for (int k = 1; k < 4; k++) {
                    ab[IND(i, j)] += A[IND(i, k)] * B[IND(k, j)];
                }
            }
        }
        return ab;
    }

    Vector4GLf operator*(const Vector4GLf& v) {
        auto& A = *this;
        Vector4GLf Av;
        if (transposed) {
            for (int i = 0; i < 4; i++) {
                Av[i] = A[IND(0, i)] * v[0];
                for (int j = 1; j < 4; j++) {
                    Av[i] += A[IND(j, i)] * v[j];
                }
            }
        } else {
            for (int i = 0; i < 4; i++) {
                Av[i] = A[IND(i, 0)] * v[0];
                for (int j = 1; j < 4; j++) {
                    Av[i] += A[IND(i, j)] * v[j];
                }
            }
        }
        return Av;
    }


    /////////////////////////////////////////////////////////////////////////////
    ///                            Static functions                           ///
    /////////////////////////////////////////////////////////////////////////////

    static Matrix identity() {
        return {Vector4GLf::e(1), Vector4GLf::e(2), Vector4GLf::e(3), Vector4GLf::e(4)};
    }

};

struct PALU {
    const Matrix lu;
    const Vector3i p;
    bool transposed;

    PALU(Matrix lu, Vector3i p, bool transposed = false) :
      lu(std::move(lu)), p(std::move(p)), transposed(transposed) {}

    PALU& transpose() {
        transposed = !transposed;
        return *this;
    }


    /**
     * @brief Given a PA=LU decomposition of a 4x4 matrix and a vector b, solve
     *  the linear equation system Ax=b
     * @param solution: RandomAccessIterator pointing to the beginning
     *  of a size 4 column vector of GLfloats to solve.
     * @param b - a vector of GLfloats of size 4 - the right side of the Ax=b equation
     */
    template<class RandomAccessIterator>
    void solve(RandomAccessIterator solution, Vector4GLf b) {
        if (transposed) {
            for (int i = 0; i < 4; ++i) {
                solution[i] = b[i];
            }
            for (int i = 0; i <= 3; i++) {
                for (int j = 0; j < i; j++) {
                    solution[i] -= lu[IND(j, i)] * solution[j];
                }
                solution[i] /= lu[IND(i, i)];
            }
            for (int i = 2; i >= 0; i--) {
                for (int j = i + 1; j < 4; j++) {
                    solution[i] -= lu[IND(j, i)] * solution[j];
                }
            }
            for (int i = 2; i >= 0; i--) {
                if (p[i] != i) {
                    std::swap(solution[i], solution[p[i]]);
                }
            }
        } else { // not transposed
            for (int i = 0; i < 4; ++i) {
                solution[i] = b[i];
            }
            for (int i = 0; i < 3; i++) {
                if (p[i] != i) {
                    std::swap(solution[i], solution[p[i]]);
                }
            }
            for (int i = 1; i < 4; i++) {
                for (int j = 0; j < i; j++) {
                    solution[i] -= lu[IND(i, j)] * solution[j];
                }
            }
            for (int i = 3; i >= 0; i--) {
                for (int j = i + 1; j < 4; j++) {
                    solution[i] -= lu[IND(i, j)] * solution[j];
                }
                solution[i] /= lu[IND(i, i)];
            }
        }
    }
};


#endif //OPENGL_MATRIX_H
