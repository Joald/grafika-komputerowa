#ifndef OPENGL_MATH_H
#define OPENGL_MATH_H

#include <type_traits>
#include <cmath>

const float PI = 3.1415926535897932384f;

#define ROUNDUP(a, b) ((((a)+(b)-1)/(b))*(b))
#define IND(i, j) ((i)+4*(j))

template<class F1, class F2>
bool are_close(const F1& f1, const F2& f2) {
    static_assert(std::is_floating_point_v<F1>);
    static_assert(std::is_floating_point_v<F2>);
    const double epsilon = 0.0001;
    return (f1 - f2) < epsilon;
}

#endif //OPENGL_MATH_H
