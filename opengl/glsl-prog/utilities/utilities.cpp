#include <sstream>
#include "utilities.h"

#ifdef ENABLE_SPIRV
PFNGLSPECIALIZESHADERARB glSpecializeShaderARB = NULL;
#endif

/* ////////////////////////////////////////////////////////////////////////// */

void PrintGLVersion() {
    std::cout << "OpenGL " << glGetString(GL_VERSION) << ", GLSL " << glGetString(GL_SHADING_LANGUAGE_VERSION) << "\n";
} /*PrintGLVersion*/

__attribute__((noreturn))
void ExitOnError(const std::string& msg) {
    std::cerr << "Error: " << msg << std::endl;
    exit(1);
} /*ExitOnError*/

void ExitIfGLError(const std::string& msg) {
    GLenum err = glGetError();
    if (err != GL_NO_ERROR) {
        std::cerr << "Error " << err << ": " << gluErrorString(err) << " (" << msg.c_str() << ")\n";
        exit(1);
    }
} /*ExitIfGLError*/

void GetGLProcAddresses() {
#ifdef USE_GL3W
    if ( gl3wInit () )
      ExitOnError ( "GetGLProcAddresses (gl3w)\n" );
#else
    auto err = glewInit();

    if (err != GLEW_OK) {
        std::cerr << "Error: " << glewGetErrorString(err) << "\n";
        exit(1);
    }
#endif
} /*GetGLProcAddresses*/

/* ////////////////////////////////////////////////////////////////////////// */

GLuint LinkShaderProgram(int shader_count, const GLuint* shaders) {
    GLuint program_id = glCreateProgram();
    ExitIfGLError("LinkShaderProgram - cannot create program");

    for (int i = 0; i < shader_count; i++) {
        glAttachShader(program_id, shaders[i]);
        ExitIfGLError("LinkShaderProgram - cannot attach shader no. " + std::to_string(i) + " with id " +
                      std::to_string(shaders[i]));
    }

    glLinkProgram(program_id);
    ExitIfGLError("LinkShaderProgram - cannot link program");

    GLint log_size;
    glGetProgramiv(program_id, GL_INFO_LOG_LENGTH, &log_size);
    if (!log_size) {
        ExitIfGLError("LinkShaderProgram - log is empty");
        return program_id;
    }
    GLchar log[log_size + 1];
    if (log_size > 1) {
        glGetProgramInfoLog(program_id, log_size, &log_size, log);
        std::cerr << log << std::endl;
    }

    ExitIfGLError("LinkShaderProgram");
    return program_id;
} /*LinkShaderProgram*/

/* ////////////////////////////////////////////////////////////////////////// */
#ifdef ENABLE_SPIRV
GLuint CreateSPIRVShader ( GLenum shader_type, int size, const GLuint *spirv )
{
  GLuint shader_id;
  GLint  status;

  if ( (shader_id = glCreateShader ( shader_type )) != 0 ) {
    glShaderBinary ( 1, &shader_id, GL_SHADER_BINARY_FORMAT_SPIR_V_ARB,
                     spirv, size );
    glSpecializeShaderARB ( shader_id, "main", 0, NULL, NULL );
    glGetShaderiv ( shader_id, GL_COMPILE_STATUS, &status );
    if ( !status ) {
      glDeleteShader ( shader_id );
      shader_id = 0;
    }
  }
  ExitIfGLError ( "CompileSPIRVString" );
  return shader_id;
} /*CreateSPIRVShader*/

GLuint LoadSPIRVFile ( GLenum shader_type, const char *filename )
{
  GLuint shader_id = 0;
  FILE   *f;
  int    size;
  GLuint *spirv;

  if ( !(f = fopen ( filename, "rb" )) )
    return 0;
  fseek ( f, 0, SEEK_END );
  size = ftell ( f );
  rewind ( f );
  if ( !(spirv = malloc ( size )) )
    goto way_out;
  if ( fread ( spirv, sizeof(char), size, f ) != size )
    goto way_out;
  shader_id = CreateSPIRVShader ( shader_type, size, spirv );
way_out:
  fclose ( f );
  if ( spirv ) free ( spirv );
  return shader_id;
} /*LoadSPIRVFile*/
#endif

/* ////////////////////////////////////////////////////////////////////////// */
GLuint NewUniformBindingPoint() {
    static GLint max_uniform_binding_point = 0;
    static GLuint next_uniform_binding_point = 0;
    if (!max_uniform_binding_point) {
        glGetIntegerv(GL_MAX_UNIFORM_BUFFER_BINDINGS, &max_uniform_binding_point);
    }
    if (next_uniform_binding_point < max_uniform_binding_point) {
        return next_uniform_binding_point++;
    } else {
        ExitOnError("NewUniformBindingPoint");
    }
    return 0;
} /*NewUniformBindingPoint*/

void GetAccessToUniformBlock(GLuint prog, int n, const GLchar** names, GLuint* ind, GLint* size, GLint* ofs,
                             GLuint* binding_point) {
    const int MAX_UNIFORM_FIELDS = 32;
    GLuint ufi[MAX_UNIFORM_FIELDS];

    *ind = glGetUniformBlockIndex(prog, names[0]);
    glGetActiveUniformBlockiv(prog, *ind, GL_UNIFORM_BLOCK_DATA_SIZE, size);
    if (n > 0) {
        glGetUniformIndices(prog, n, &names[1], ufi);
        glGetActiveUniformsiv(prog, n, ufi, GL_UNIFORM_OFFSET, ofs);
    }
    *binding_point = NewUniformBindingPoint();
    glUniformBlockBinding(prog, *ind, *binding_point);
    ExitIfGLError("GetAccessToUniformBlock");
} /*GetAccessToUniformBlock*/

GLuint NewUniformBlockObject(GLint size, GLuint binding_point) {
    GLuint buffer;
    glGenBuffers(1, &buffer);
    glBindBufferBase(GL_UNIFORM_BUFFER, binding_point, buffer);
    glBufferData(GL_UNIFORM_BUFFER, size, nullptr, GL_DYNAMIC_DRAW);
    ExitIfGLError("NewUniformBlockObject");
    return buffer;
} /*NewUniformBlockObject*/

void AttachUniformBlockToBindingPoint(GLuint program_id, const GLchar* name, GLuint binding_point) {
    GLuint ind = glGetUniformBlockIndex(program_id, name);
    glUniformBlockBinding(program_id, ind, binding_point);
    ExitIfGLError("AttachUniformBlockToBindingPoint");
} /*AttachUniformBlockToBindingPoint*/

void GetAccessToStorageBlock(GLuint prog, int n, const GLchar** names, GLuint* ind, GLint* size, GLint* ofs,
                             GLuint* bpoint) {
    int i;
    GLuint prop;

    ind[0] = glGetProgramResourceIndex(prog, GL_SHADER_STORAGE_BLOCK, names[0]);
    prop = GL_BUFFER_DATA_SIZE;
    glGetProgramResourceiv(prog, GL_SHADER_STORAGE_BLOCK, ind[0], 1, &prop, 1, nullptr, size);
    prop = GL_BUFFER_BINDING;
    glGetProgramResourceiv(prog, GL_SHADER_STORAGE_BLOCK, ind[0], 1, &prop, 1, nullptr, (GLint*) bpoint);
    if (n > 0) {
        prop = GL_OFFSET;
        for (i = 1; i <= n; i++) {
            ind[i] = glGetProgramResourceIndex(prog, GL_BUFFER_VARIABLE, names[i]);
            glGetProgramResourceiv(prog, GL_BUFFER_VARIABLE, ind[i], 1, &prop, 1, nullptr, &ofs[i - 1]);
        }
    }
    ExitIfGLError("GetAccessToStorageBlock");
} /*GetAccessToStorageBlock*/

/* ////////////////////////////////////////////////////////////////////////// */

/**
 * @brief Returns a 4x4 identity matrix
 * @return the matrix
 */
Matrix M4x4Identity() {
    Matrix a;
    a[0] = a[5] = a[10] = a[15] = 1.0;
    return a;
} /*M4x4Identity*/

/**
 * @brief Returns a matrix that translates a vector
 * @param translation_vector - the vector to translate by
 * @return the matrix
 */
Matrix M4x4Translate(Vector3f translation_vector) {
    auto a = M4x4Identity();
    a[12] = translation_vector.x;
    a[13] = translation_vector.y;
    a[14] = translation_vector.z;
    return a;
} /*M4x4Translate*/

/**
 * @brief Returns a matrix that scales a vector
 * @param scale_vector - the vector to scale by
 * @return the matrix
 */
Matrix M4x4Scale(Vector3f scale_vector) {
    auto a = M4x4Identity();
    a[0] = scale_vector.x;
    a[5] = scale_vector.y;
    a[10] = scale_vector.z;
    return a;
} /*M4x4Scale*/

/**
 * @brief Returns a matrix that rotates a vector around the given axis
 * @param phi - angly to rotate by
 * @param axis - the axis
 * @return the matrix
 */
Matrix M4x4Rotation(Axis axis, float phi) {
    auto a = M4x4Identity();
    switch (axis) {
        case Axis::X:a[5] = a[10] = std::cos(phi);
            a[9] = -(a[6] = std::sin(phi));
            break;
        case Axis::Y:a[0] = a[10] = std::cos(phi);
            a[2] = -(a[8] = std::sin(phi));
            break;
        case Axis::Z:a[0] = a[5] = std::cos(phi);
            a[4] = -(a[1] = std::sin(phi));
            break;
    }
    return a;
} /*M4x4Rotation*/


Matrix M4x4CustomRotation(Vector3f v, float phi) {
    auto a = M4x4Identity();
    v.normalize();
    float s = std::sin(phi);
    float c = std::cos(phi);
    float c1 = 1.0f - c;
    a[0] = v.x * v.x * c1 + c;
    a[1] = a[4] = v.x * v.x * c1;
    a[5] = v.x * v.x * c1 + c;
    a[2] = a[8] = v.x * v.z * c1;
    a[6] = a[9] = v.x * v.z * c1;
    a[10] = v.z * v.z * c1 + c;
    a[6] += s * v.x;
    a[2] -= s * v.x;
    a[1] += s * v.z;
    a[9] -= s * v.x;
    a[8] += s * v.x;
    a[4] -= s * v.z;
    return a;
} /*M4x4CustomRotation*/

/**
 * @brief
 * @param ai
 * @param a
 */
Matrix M4x4InvertAffineIsometry(const Matrix& a) {
    Matrix ai;
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            ai[IND(i, j)] = a[IND(j, i)];
        }
    }
    for (int i = 0; i < 3; i++) {
        ai[IND(i, 3)] = -(ai[IND(i, 0)] * a[IND(0, 3)] + ai[IND(i, 1)] * a[IND(1, 3)] + ai[IND(i, 2)] * a[IND(2, 3)]);
    }
    ai[3] = ai[7] = ai[11] = 0.0;
    ai[15] = 1.0;
    return ai;
} /*M4x4InvertAffineIsometry*/


/* ////////////////////////////////////////////////////////////////////////// */
Matrix M4x4ToStandardCubeFrustum(
  // Formerly M4x4Frustumf
  float left, float right,
  float bottom, float top,
  float near, float far) {
    Matrix a;
    float rl = right - left;
    float tb = top - bottom;
    float nf = near - far;
    float nn = near + near;
    a[0] = nn / rl;
    a[8] = (right + left) / rl;
    a[5] = nn / tb;
    a[9] = (top + bottom) / tb;
    a[10] = (far + near) / nf;
    a[14] = far * nn / nf;
    a[11] = -1.0f;
    return a;
}

Matrix M4x4FromStandardCubeFrustum(
  float left, float right,
  float bottom, float top,
  float near, float far) {
    Matrix ai;
    float rl = right - left;
    float tb = top - bottom;
    float nf = near - far;
    float nn = near + near;
    ai[0] = rl / nn;
    ai[12] = (right + left) / nn;
    ai[5] = tb / nn;
    ai[13] = (top + bottom) / nn;
    ai[14] = -1.0f;
    ai[11] = nf / (far * nn);
    ai[15] = (far + near) / (far * nn);
    return ai;
} /*M4x4ToStandardCubeFrustum*/

Matrix M4x4ToStandardCubeParallel(float left, float right, float bottom, float top, float near, float far) {
    // formerly M4x4Orthof
    Matrix a;
    float rl = right - left;
    float tb = top - bottom;
    float fn = far - near;
    a[0] = 2.0f / rl;
    a[12] = -(right + left) / rl;
    a[5] = 2.0f / tb;
    a[13] = -(top + bottom) / tb;
    a[10] = -2.0f / fn;
    a[14] = (far + near) / fn;
    a[15] = 1.0;
    return a;
} /*M4x4ToStandardCubeParallel*/

Matrix M4x4FromStandardCubeParallel(float left, float right, float bottom, float top, float near, float far) {
    Matrix ai;
    float rl = right - left;
    float tb = top - bottom;
    float fn = far - near;
    ai[0] = 0.5f * rl;
    ai[12] = 0.5f * (right + left);
    ai[5] = 0.5f * tb;
    ai[13] = 0.5f * (top + bottom);
    ai[10] = -0.5f * fn;
    ai[14] = 0.5f * (far + near);
    ai[15] = 1.0;
    return ai;
} /*M4x4FromStandardCubeParallel*/

/**
 * @brief
 * @param w
 * @param h
 * @param aspect
 * @param F
 * @param dist
 * @param xv
 * @param yv
 * @param xp
 * @param yp
 * @param near
 * @param far
 * @param left
 * @param right
 * @param bottom
 * @param top
 * @param vm - size 16
 * @param shvm - size 16
 * @param eyepos - size 4
 * @param pm - size 16
 * @param pmi - size 16
 */
//void M4x4SkewFrustum(
//  int w, int h, float aspect, float F, float dist, float xv, float yv, float xp, float yp, float near,
//  float far, float* left, float* right, float* bottom, float* top, const std::vector<GLfloat>& vm,
//  std::vector<GLfloat>& shvm, std::vector<GLfloat>& eyepos, std::vector<GLfloat>& pm, std::vector<GLfloat>& pmi) {
//    if (vm.empty()) {
//        return;
//    }
//    if (shvm.size() != 16) {
//        shvm.clear();
//        shvm.resize(16);
//    }
//    if (eyepos.size() != 4) {
//        eyepos.clear();
//        eyepos.resize(4);
//    }
//    if (pm.size() != 16) {
//        pm.clear();
//        pm.resize(16);
//    }
//    if (pmi.size() != 16) {
//        pmi.clear();
//        pmi.resize(16);
//    }
//    float DF, awh, rl, tb, r, l, t, b, xr, yr;
//    GLfloat am[16], v[4];
//    int p[3];
//
//    DF = std::sqrt(36.f * 36.f + 24.f * 24.f) / F;
//    awh = aspect * (float) w / (float) h;
//    tb = DF * near / std::sqrt(1.0f + awh * awh);
//    rl = tb * awh;
//    xr = xv * near / dist - xp * rl / (float) w;
//    l = -(r = 0.5f * rl);
//    l -= xr;
//    r -= xr;
//    yr = yv * near / dist - yp * tb / (float) h;
//    b = -(t = 0.5f * tb);
//    b -= yr;
//    t -= yr;
//    if (top) {
//        *top = t;
//    }
//    if (left) {
//        *left = l;
//    }
//    if (right) {
//        *right = r;
//    }
//    if (bottom) {
//        *bottom = b;
//    }
//    if (vm) {
//        M4x4Translate(0.0);
//        M4x4Multiplication(shvm, am, vm);
//        if (eyepos) {
//            M4x4PALUDecomposition(am, p, shvm);
//            v[0] = v[1] = v[2] = 0.0;
//            v[3] = 1.0;
//            M4x4LUsolve(eyepos, am, p, v);
//        }
//    } else {
//        M4x4Translate(0.0);
//    }
//    M4x4Frustumf(pm, l, r, b, t, near, far);
//} /*M4x4SkewFrustum*/

/* ////////////////////////////////////////////////////////////////////////// */

std::pair<float, Vector3f> V3ComposeRotations(const Vector3f& v2, float phi2, const Vector3f& v1, float phi1) {
    float s2 = std::sin(0.5f * phi2);
    float c2 = std::cos(0.5f * phi2);
    float s1 = std::sin(0.5f * phi1);
    float c1 = std::cos(0.5f * phi1);
    auto v2v1 = v2.dot(v1);
    auto v2xv1 = v2 * v1;
    float c = c2 * c1 - v2v1 * s2 * s1;
    Vector3f v(v2[0] * s2 * c1 + v1[0] * s1 * c2 + v2xv1[0] * s2 * s1,
               v2[1] * s2 * c1 + v1[1] * s1 * c2 + v2xv1[1] * s2 * s1,
               v2[2] * s2 * c1 + v1[2] * s1 * c2 + v2xv1[2] * s2 * s1);
    auto s = std::sqrt(v.length_sq());
    float phi;
    if (s > 0.0) {
        v[0] /= s, v[1] /= s, v[2] /= s;
        phi = 2.0f * std::atan2(s, c);
    } else {
        v[0] = 1.0, v[1] = v[2] = phi = 0.0;
    }
    return {phi, v};
} /*V3ComposeRotations*/

/* ////////////////////////////////////////////////////////////////////////// */
Vector3GLf M4x4MultMV3(const Matrix& a, const Vector3GLf& v) {
    Vector3GLf av;
    for (int i = 0; i < 3; i++) {
        av[i] = a[IND(i, 0)] * v[0];
        for (int j = 1; j < 3; j++) {
            av[i] += a[IND(i, j)] * v[j];
        }
    }
    return av;
} /*M4x4MultMV3*/

Vector3GLf M4x4MultMP3(const Matrix& a, const Vector3GLf& p) {
    Vector3GLf ap;
    for (int i = 0; i < 3; i++) {
        ap[i] = a[IND(i, 0)] * p[0] + a[IND(i, 3)];
        for (int j = 1; j < 3; j++) {
            ap[i] += a[IND(i, j)] * p[j];
        }
    }
    return ap;
} /*M4x4MultMP3*/

GLuint CompileShaderFiles(GLenum shader_type, const std::vector<const char*>& filenames) {
    std::basic_stringstream<GLchar> shaderData;
    for (auto filename : filenames) {
        std::ifstream shaderFile(filename);
        if (!shaderFile) {
            ExitOnError("CompileShaderFiles: Can't open shader file!");
        }
        shaderData << shaderFile.rdbuf();  //Loads the entire string into a string stream.
    }

    const auto& shaderString = shaderData.str();
    const auto* ptr = shaderString.c_str();
    GLuint shader_id = glCreateShader(shader_type);
    if (shader_id != 0) {
        glShaderSource(shader_id, 1, &ptr, nullptr);
        glCompileShader(shader_id);
        GLint log_size;
        glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &log_size);
        if (log_size > 1) {
            GLchar log[log_size + 1];
            glGetShaderInfoLog(shader_id, log_size, &log_size, log);
            std::cerr << log << std::endl;
        }
    } else {
        ExitIfGLError("CompileShaderStrings");
        ExitOnError("shader id was zero");
    }
    return shader_id;
} /*CompileShaderFiles*/





