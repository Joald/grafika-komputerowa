#ifndef UTILITIES_H
#define UTILITIES_H

#include <string>
#include <vector>
#include <memory>
#include <iostream>

#include "myglheader.h"
#include <GL/freeglut.h>
#include <GL/glu.h>
#include <fstream>
#include <algorithm>

#include "Vectors.h"
#include "Matrix.h"
#include "math.h"

#ifdef ENABLE_SPIRV
typedef void (*PFNGLSPECIALIZESHADERARB) (GLuint shader, const GLchar* pEntryPoint,
                                          GLuint numSpecializationConstants,
                                          const GLuint* pConstantIndex,
                                          const GLuint* pConstantValue);
extern PFNGLSPECIALIZESHADERARB glSpecializeShaderARB;
#endif

#define UNUSED(x) (void)(x)


/**
 * @brief Simple RAII container for shader source code memory management.
 */
struct ShaderSource {
    explicit ShaderSource(size_t size) : data(size) {}

    GLchar* operator[](size_t index) {
        return data[index].data();
    }

    std::vector<GLchar*> to_compile() {
        std::vector<GLchar*> res(data.size());
        std::transform(data.begin(), data.end(), res.begin(), [](const std::basic_string<GLchar>& s) {
            return const_cast<GLchar*>(s.data());
        });
        return res;
    }

    void resize(size_t index, size_t new_size) {
        data[index].resize(new_size);
    }

private:
    std::vector<std::basic_string<GLchar>> data;
};

enum class Axis {
    X, Y, Z
};

/* ////////////////////////////////////////////////////////////////////////// */

void PrintGLVersion();
void ExitOnError(const std::string& msg);
void ExitIfGLError(const std::string& msg);
void GetGLProcAddresses();

GLuint CompileShaderStrings(GLenum shader_type, const std::vector<GLchar*>& src_lines);

GLuint CompileShaderFiles(GLenum shader_type, const std::vector<const char*>& filenames);

GLuint LinkShaderProgram(int shader_count, const GLuint* shaders);

#ifdef ENABLE_SPIRV
GLuint CreateSPIRVShader ( GLenum shader_type, int size, const GLuint *spirv );
GLuint LoadSPIRVFile ( GLenum shader_type, const char *filename );
#endif

GLuint NewUniformBindingPoint();

void GetAccessToUniformBlock(
  GLuint prog, int n, const GLchar** names,
  GLuint* ind, GLint* size, GLint* ofs, GLuint* binding_point);

GLuint NewUniformBlockObject(GLint size, GLuint binding_point);

void AttachUniformBlockToBindingPoint(GLuint program_id, const GLchar* name, GLuint binding_point);

void GetAccessToStorageBlock(
  GLuint prog, int n, const GLchar** names,
  GLuint* ind, GLint* size, GLint* ofs, GLuint* bpoint);

/* ////////////////////////////////////////////////////////////////////////// */
Matrix M4x4Identity();
Matrix M4x4Translate(Vector3GLf translation_vector);
Matrix M4x4Scale(Vector3f scale_vector);
Matrix M4x4Rotation(Axis axis, float phi);
Matrix M4x4CustomRotation(Vector3f v, float phi);
Matrix M4x4InvertAffineIsometry(const Matrix& a);

void M4x4Transposef(GLfloat at[16], const GLfloat a[16]);
void M4x4MultMVf(GLfloat av[4], const GLfloat a[16], const GLfloat v[4]);
void M4x4MultMTVf(GLfloat av[4], const GLfloat a[16], const GLfloat v[4]);

/* ////////////////////////////////////////////////////////////////////////// */
Matrix M4x4ToStandardCubeFrustum(float left, float right, float bottom, float top, float near, float far);
Matrix M4x4FromStandardCubeFrustum(float left, float right, float bottom, float top, float near, float far);
Matrix M4x4ToStandardCubeParallel(float left, float right, float bottom, float top, float near, float far);
Matrix M4x4FromStandardCubeParallel(float left, float right, float bottom, float top, float near, float far);

//void M4x4SkewFrustum(
//  int w, int h, float aspect, float F, float dist, float xv, float yv, float xp, float yp, float near,
//  float far, float* left, float* right, float* bottom, float* top, const std::vector<GLfloat>& vm,
//  std::vector<GLfloat>& shvm, std::vector<GLfloat>& eyepos, std::vector<GLfloat>& pm, std::vector<GLfloat>& pmi);

/* ////////////////////////////////////////////////////////////////////////// */
float V3DotProductf(const float v1[3], const float v2[3]);
void V3CrossProductf(float v1xv2[3], const float v1[3], const float v2[3]);
std::pair<float, Vector3f> V3ComposeRotations(const Vector3f& v2, float phi2, const Vector3f& v1, float phi1);

/* ////////////////////////////////////////////////////////////////////////// */
Vector3GLf M4x4MultMV3(const Matrix& a, const Vector3GLf& v);
Vector3GLf M4x4MultMP3(const Matrix& a, const Vector3GLf& p);


#endif // UTILITIES_H