#include <cstring>
#include "App.h"
#include "utilities.h"
#include "bezpatches.h"
#include "timer.h"
#include "teapot.h"

App* current_app;
namespace {

App& app() {
    return *current_app;
}

auto error_handler = [](int error, const char* description) {
    throw AppError{"GLFW error: "s + description};
};

}

void App::LoadMyShaders() {
    std::cerr<<"LoadMyShaders\n";
    static const char* filename[] = {
      "shaders/app2.glsl.vert", "shaders/app2.glsl.tesc", "shaders/app2.glsl.tese",
      "shaders/app2.glsl.geom", "shaders/app2.glsl.frag",
      "shaders/app2a1.glsl.vert", "shaders/app2a1.glsl.frag"
    };
    static const GLuint shtype[7] = {
      GL_VERTEX_SHADER, GL_TESS_CONTROL_SHADER, GL_TESS_EVALUATION_SHADER,
      GL_GEOMETRY_SHADER, GL_FRAGMENT_SHADER,
      GL_VERTEX_SHADER, GL_FRAGMENT_SHADER
    };
    static const GLchar* UTBNames[] = {
      "TransBlock", "TransBlock.mm", "TransBlock.mmti", "TransBlock.vm",
      "TransBlock.pm", "TransBlock.mvpm", "TransBlock.eyepos"
    };
    static const GLchar* ULSNames[] = {
      "LSBlock", "LSBlock.nls", "LSBlock.mask",
      "LSBlock.ls[0].ambient", "LSBlock.ls[0].direct", "LSBlock.ls[0].position",
      "LSBlock.ls[0].attenuation", "LSBlock.ls[1].ambient"
    };
    static const GLchar* UCPNames[] = {"CPoints", "CPoints.cp"};
    static const GLchar* UCPINames[] = {"CPIndices", "CPIndices.cpi"};
    static const GLchar* UBezPatchNames[] = {
      "BezPatch", "BezPatch.dim", "BezPatch.udeg", "BezPatch.vdeg",
      "BezPatch.stride_u", "BezPatch.stride_v",
      "BezPatch.stride_p", "BezPatch.stride_q", "BezPatch.nq",
      "BezPatch.use_ind", "BezPatch.Colour"
    };
    static const GLchar* UVarNames[] = {"BezNormals", "BezTessLevel"};
    GLint i;
    for (i = 0; i < 7; i++) {
        shader_id[i] = CompileShaderFiles(shtype[i], std::vector<const char*>{1, filename[i]});
    }
    program_id[0] = LinkShaderProgram(5, shader_id);
    program_id[1] = LinkShaderProgram(2, &shader_id[5]);
    GetAccessToUniformBlock(program_id[0], 6, &UTBNames[0], &trbi, &trbsize, trbofs, &trbbp);
    GetAccessToUniformBlock(program_id[0], 7, &ULSNames[0], &lsbi, &lsbsize, lsbofs, &lsbbp);
    GetAccessToUniformBlock(program_id[0], 1, &UCPNames[0], &cpbi, &i, cpbofs, &cpbbp);
    GetAccessToUniformBlock(program_id[0], 1, &UCPINames[0], &cpibi, &i, cpibofs, &cpibbp);
    GetAccessToUniformBlock(program_id[0], 10, &UBezPatchNames[0], &bezpbi, &bezpbsize, bezpbofs, &bezpbbp);
    ubeznloc = glGetUniformLocation(program_id[0], UVarNames[0]);
    ubeztloc = glGetUniformLocation(program_id[0], UVarNames[1]);

    trbuf = NewUniformBlockObject(trbsize, trbbp);
    lsbuf = NewUniformBlockObject(lsbsize, lsbbp);

    AttachUniformBlockToBindingPoint(program_id[1], UTBNames[0], trbbp);
    AttachUniformBlockToBindingPoint(program_id[1], UCPNames[0], cpbbp);
    AttachUniformBlockToBindingPoint(program_id[1], UCPINames[0], cpibbp);
    AttachUniformBlockToBindingPoint(program_id[1], UBezPatchNames[0], bezpbbp);
    ExitIfGLError("LoadMyShaders");
}

void App::SetupModelMatrix(Vector3f axis, float angle) {
    std::cerr<<"SetupModelMatrix\n";
    const float SCALE_FACTOR = 0.33f;
    Matrix mt = Matrix::translation({0.0, 0.0, -0.6f});
    Matrix mr = M4x4CustomRotation(axis, angle);
    trans.mm = mr * (mt * M4x4Scale({SCALE_FACTOR, SCALE_FACTOR, SCALE_FACTOR * 4.0f / 3.0f}));
    glBindBuffer(GL_UNIFORM_BUFFER, trbuf);
    glBufferSubData(GL_UNIFORM_BUFFER, trbofs[0], 16 * sizeof(GLfloat), trans.mm);
    trans.mmti = mr * M4x4Scale({1.0f / SCALE_FACTOR, 1.0f / SCALE_FACTOR, 1.0f / SCALE_FACTOR * 3.0f / 4.0f});
    glBufferSubData(GL_UNIFORM_BUFFER, trbofs[1], 16 * sizeof(GLfloat), trans.mmti);
    ExitIfGLError("SetupModelMatrix");
    SetupMVPMatrix();
}

void App::InitMyObject() {
    std::cerr<<"InitMyObject\n";
    TimerInit();
    trans = TransBl{};
    light = LightBl{};
    SetupModelMatrix(model_rot_axis, model_rot_angle);
    InitViewMatrix();
    ConstructBezierPatchDomain();
    ConstructMyTeapot();
    InitLights();
} /*InitMyObject*/


void App::SetupMVPMatrix() {
    std::cerr<<"SetupMVPMatrix\n";
    Matrix mvp = trans.pm * (trans.vm * trans.mm);
    glBindBuffer(GL_UNIFORM_BUFFER, trbuf);
    glBufferSubData(GL_UNIFORM_BUFFER, trbofs[4], 16 * sizeof(GLfloat), mvp);
    ExitIfGLError("SetupMVPMatrix");
}


void App::onReshape(GLFWwindow* win, int width, int height) {
    UNUSED(win);
    std::cerr << "onReshape\n";
    glViewport(0, 0, width, height);
    float lr = 0.5533f * width / height;
    trans.pm = M4x4ToStandardCubeFrustum({-lr, lr, -0.5533f, 0.5533f, 5.0f, 15.0f});
    glBindBuffer(GL_UNIFORM_BUFFER, trbuf);
    glBufferSubData(GL_UNIFORM_BUFFER, trbofs[3], 16 * sizeof(GLfloat), trans.pm);
    win_width = width;
    win_height = height;
    auto[left, right, bottom, top, near, far] = params.tuple();
    left = -(right = lr);
    bottom = -(top = 0.5533);
    near = 5.0;
    far = 15.0;
    params.tuple() = std::tie(left, right, bottom, top, near, far);
    ExitIfGLError("ReshapeFunc");
    SetupMVPMatrix();
    redraw = true;
}

void App::onRefresh(GLFWwindow* win) {
    std::cerr<<"onRefresh\n";
    redraw = true;
}

void App::onKeyPress(GLFWwindow* win, int key, int scancode, int action, int mode) {
    std::cerr<<"onKeyPress\n";
    if (action == GLFW_PRESS || action == GLFW_REPEAT) {
        switch (key) {
            case GLFW_KEY_ESCAPE:
                glfwSetWindowShouldClose(mywindow, 1);
                break;
            default:
                break;
        }
    }

}

void App::onCharPress(GLFWwindow* win, unsigned int charcode) {
    std::cerr<<"onCharPress\n";
    switch (charcode) {
        case '+':
            if (TessLevel < MAX_TESS_LEVEL) {
                SetBezierPatchOptions(program_id[0], BezNormals, ++TessLevel);
                redraw = true;
            }
            break;
        case '-':
            if (TessLevel > MIN_TESS_LEVEL) {
                SetBezierPatchOptions(program_id[0], BezNormals, --TessLevel);
                redraw = true;
            }
            break;
        case 'N':
        case 'n':
            BezNormals = BezNormals == 0;
            SetBezierPatchOptions(program_id[0], BezNormals, TessLevel);
            redraw = true;
            break;
        case ' ':
            ToggleAnimation();
            break;
        case 'C':
        case 'c':
            cnet = !cnet;
            redraw = true;
            break;
        case 'L':
        case 'l':
            skeleton = true;
            redraw = true;
            break;
        case 'S':
        case 's':
            skeleton = false;
            redraw = true;
            break;
        default:    /* ignorujemy wszystkie inne klawisze */
            break;
    }

}

void App::onMousePress(GLFWwindow* win, int button, int action, int mods) {
    std::cerr<<"onMousePress\n";
    switch (app_state) {
        case AppState::STATE_NOTHING:
            if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
                glfwGetCursorPos(win, &last_xi, &last_eta);
                app_state = AppState::STATE_TURNING;
            }
            break;
        case AppState::STATE_TURNING:
            if (button == GLFW_MOUSE_BUTTON_LEFT && action != GLFW_PRESS) {
                app_state = AppState::STATE_NOTHING;
            }
            break;
        default:
            break;
    }
}

void App::onMouseMove(GLFWwindow* win, double x, double y) {
    std::cerr<<"onMouseMove\n";
    switch (app_state) {
        case AppState::STATE_TURNING:
            if (x != last_xi || y != last_eta) {
                RotateViewer(x - last_xi, y - last_eta);
                last_xi = x, last_eta = y;
                redraw = true;
            }
            break;
        default:
            break;
    }
}

void onReshape(GLFWwindow* win, int width, int height) {
    app().onReshape(win, width, height);
}

void onRefresh(GLFWwindow* win) {
    app().onRefresh(win);
}

void onKeyPress(GLFWwindow* win, int key, int scancode, int action, int mode) {
    app().onKeyPress(win, key, scancode, action, mode);
}

void onCharPress(GLFWwindow* win, unsigned int charcode) {
    app().onCharPress(win, charcode);
}

void onMousePress(GLFWwindow* win, int button, int action, int mods) {
    app().onMousePress(win, button, action, mods);
}

void onMouseMove(GLFWwindow* win, double x, double y) {
    app().onMouseMove(win, x, y);
}


App::App() {
    std::cerr<<"App ctor\n";
    current_app = this;
    glfwSetErrorCallback(error_handler);
    if (!glfwInit()) {
        throw AppError{"Error: glfwInit failed\n"};
    }
    mywindow = glfwCreateWindow(480, 360, "OpenGL4fun", nullptr, nullptr);
    if (!mywindow) {
        glfwTerminate();
        throw AppError{"Error: glfwCreateWindow failed\n"};
    }
    glfwMakeContextCurrent(mywindow);
    GetGLProcAddresses();
#ifdef USE_GL3W
    if (!gl3wIsSupported(4, 2)) {
        ExitOnError("Initialise: OpenGL version 4.2 not supported\n");
    }
#endif
    PrintGLVersion();
    glfwSetWindowSizeCallback(mywindow, ::onReshape);
    glfwSetWindowRefreshCallback(mywindow, ::onRefresh);
    glfwSetKeyCallback(mywindow, ::onKeyPress);
    glfwSetCharCallback(mywindow, ::onCharPress);
    glfwSetMouseButtonCallback(mywindow, ::onMousePress);
    glfwSetCursorPosCallback(mywindow, ::onMouseMove);
    LoadMyShaders();
    InitMyObject();
    onReshape(mywindow, 480, 360);
    redraw = true;
}

void IdleFunc() {
    app().IdleFunc();
}

void App::SetIdleFunc(std::function<void()> f) {
    std::cerr<<"SetIdleFunc\n";
    idlefunc = f;
}


void App::ToggleAnimation() {
    std::cerr<<"ToggleAnimation\n";
    animate = !animate;
    if (animate) {
        TimerTic();
        SetIdleFunc(::IdleFunc);
    } else {
        model_rot_angle0 = model_rot_angle;
        SetIdleFunc(nullptr);
    }
}

void App::IdleFunc() {
    std::cerr<<"IdleFunc\n";
    model_rot_angle = model_rot_angle0 + 0.78539816f * static_cast<float>(TimerToc());
    SetupModelMatrix(model_rot_axis, model_rot_angle);
    redraw = true;
}

void App::RotateViewer(double delta_xi, double delta_eta) {
    std::cerr<<"RotateViewer\n";
    if (delta_xi == 0 && delta_eta == 0) {
        return;
    }  /* natychmiast uciekamy - nie chcemy dzielic przez 0 */
    Vector3f vi{
      (float) delta_eta * (params.right - params.left) / win_height,
      (float) delta_xi * (params.top - params.bottom) / win_width
    };

    vi.normalize();
    float angi = -0.052359878f;  /* -3 stopnie */

    std::tie(viewer_rangle, viewer_rvec) = V3ComposeRotations(viewer_rvec, viewer_rangle, vi, angi);
    auto tm = Matrix::translation(-viewer_pos0.to_vector3());
    auto rm = M4x4CustomRotation(viewer_rvec, -viewer_rangle);
    trans.vm = tm * rm;
    glBindBuffer(GL_UNIFORM_BUFFER, trbuf);
    glBufferSubData(GL_UNIFORM_BUFFER, trbofs[2], 16 * sizeof(GLfloat), trans.vm);
    trans.eyepos = rm.transpose() * viewer_pos0;
    glBufferSubData(GL_UNIFORM_BUFFER, trbofs[5], 4 * sizeof(GLfloat), trans.eyepos);
    ExitIfGLError("RotateViewer");
    SetupMVPMatrix();
}

void App::MessageLoop() {
    std::cerr<<"MessageLoop\n";
    do {
        if (idlefunc) {
            idlefunc();
            glfwPollEvents();
        } else {
            glfwWaitEvents();
        }
        if (redraw) {
            Redraw(mywindow);
        }
    } while (!glfwWindowShouldClose(mywindow));
}

void App::Redraw(GLFWwindow* win) {
    std::cerr<<"Redraw\n";
    glClearColor(1.0, 1.0, 1.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    DrawMyTeapot();
    if (cnet) {
        DrawMyTeapotCNet();
    }
    glUseProgram(0);
    glFlush();
    glfwSwapBuffers(win);
    ExitIfGLError("Redraw");
    redraw = false;
}

void App::DrawMyTeapot() {
    std::cerr << "DrawMyTeapot\n";
    if (skeleton) {
        glLineWidth(2.0);
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    } else {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }
    glUseProgram(program_id[0]);
    DrawBezierPatches(myteapot);
}

void App::DrawMyTeapotCNet() {
    std::cerr<<"DrawMyTeapotCNet\n";
    glUseProgram(program_id[1]);
    glLineWidth(1.0);
    DrawBezierNets(myteapot);
}

void App::ConstructMyTeapot() {
    std::cerr<<"ConstructMyTeapot\n";
    const GLfloat MyColour[4] = {1.0, 1.0, 1.0, 1.0};

    myteapot = ConstructTheTeapot(MyColour);
    SetBezPatchOptions(BezNormals, TessLevel);
}

void App::SetBezPatchOptions(GLint normals, GLint tess_level) {
    std::cerr<<"SetBezPatchOptions\n";
    glUseProgram(program_id[0]);
    glUniform1i(ubeznloc, normals);
    glUniform1i(ubeztloc, tess_level);
    ExitIfGLError("SetBezPatchOptions");
}

void App::InitViewMatrix() {
    std::cerr<<"InitViewMatrix\n";
    trans.eyepos = viewer_pos0;
    trans.vm = Matrix::translation(-viewer_pos0.to_vector3());
    glBindBuffer(GL_UNIFORM_BUFFER, trbuf);
    glBufferSubData(GL_UNIFORM_BUFFER, trbofs[2], 16 * sizeof(GLfloat), trans.vm);
    glBufferSubData(GL_UNIFORM_BUFFER, trbofs[5], 4 * sizeof(GLfloat), trans.eyepos);
    ExitIfGLError("InitViewMatrix");
    SetupMVPMatrix();
}

void App::InitLights() {
    std::cerr<<"InitLights\n";
    SetLightAmbient(0, {0.2, 0.2, 0.3, 1.0});
    SetLightDiffuse(0, {0.8, 0.8, 0.8, 1.0});
    SetLightPosition(0, {0.0, 1.0, 1.0, 0.0});
    SetLightAttenuation(0, {1.0, 0.0, 0.0});
    SetLightOnOff(0, 1);
}

void App::SetLightAmbient(int l, Vector4GLf amb) {
    std::cout << "SLA\n";
    if (l < 0 || l >= MAX_NLIGHTS) {
        return;
    }
    light.ls[l].ambient = amb;
    GLint ofs = l * (lsbofs[6] - lsbofs[2]) + lsbofs[2];
    glBindBuffer(GL_UNIFORM_BUFFER, lsbuf);
    ExitIfGLError("SetLightAmbient - glBindBuffer");
    glBufferSubData(GL_UNIFORM_BUFFER, ofs, 4 * sizeof(GLfloat), amb);
    ExitIfGLError("SetLightAmbient - glBufferSubData");
} /*SetLightAmbient*/

void App::SetLightDiffuse(int l, Vector4GLf dif) {
    GLint ofs;
    std::cerr<<"SetLightDiffuse\n";
    if (l < 0 || l >= MAX_NLIGHTS) {
        return;
    }
    light.ls[l].diffuse = dif;
    ofs = l * (lsbofs[6] - lsbofs[2]) + lsbofs[3];
    glBindBuffer(GL_UNIFORM_BUFFER, lsbuf);
    glBufferSubData(GL_UNIFORM_BUFFER, ofs, 4 * sizeof(GLfloat), dif);
    ExitIfGLError("SetLightDiffuse");
} /*SetLightDiffuse*/

void App::SetLightPosition(int l, Vector4GLf pos) {
    GLint ofs;
    std::cerr<<"SetLightPosition\n";
    if (l < 0 || l >= MAX_NLIGHTS) {
        return;
    }
    light.ls[l].position = pos;
    ofs = l * (lsbofs[6] - lsbofs[2]) + lsbofs[4];
    glBindBuffer(GL_UNIFORM_BUFFER, lsbuf);
    glBufferSubData(GL_UNIFORM_BUFFER, ofs, 4 * sizeof(GLfloat), pos);
    ExitIfGLError("SetLightPosition");
} /*SetLightPosition*/

void App::SetLightAttenuation(int l, Vector3GLf atn) {
    std::cerr<<"SetLightAttenuation\n";
    if (l < 0 || l >= MAX_NLIGHTS) {
        return;
    }
    light.ls[l].attenuation = atn;
    GLint ofs = l * (lsbofs[6] - lsbofs[2]) + lsbofs[5];
    glBindBuffer(GL_UNIFORM_BUFFER, lsbuf);
    glBufferSubData(GL_UNIFORM_BUFFER, ofs, 3 * sizeof(GLfloat), atn);
    ExitIfGLError("SetLightAttenuation");
} /*SetLightAttenuation*/

void App::SetLightOnOff(int l, char on) {
    std::cerr<<"SetLightOnOff\n";
    if (l < 0 || l >= MAX_NLIGHTS) {
        return;
    }
    int mask = 0x01 << l;
    if (on) {
        light.mask |= mask;
        if (l >= light.nls) {
            light.nls = l + 1;
        }
    } else {
        light.mask &= ~mask;
        for (mask = 0x01 << (light.nls - 1); mask; mask >>= 1) {
            if (light.mask & mask) {
                break;
            } else {
                light.nls--;
            }
        }
    }
    glBindBuffer(GL_UNIFORM_BUFFER, lsbuf);
    glBufferSubData(GL_UNIFORM_BUFFER, lsbofs[0], sizeof(GLuint), &light.nls);
    glBufferSubData(GL_UNIFORM_BUFFER, lsbofs[1], sizeof(GLuint), &light.mask);
    ExitIfGLError("SetLightOnOff");
} /*SetLightOnOff*/

