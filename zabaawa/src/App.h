#pragma once

#include <iostream>
#include <functional>
#include "utilities.h"
#include "Matrix.h"
#include "bezpatches.h"

using namespace std::string_literals;

class AppError : std::logic_error {
public:
    explicit AppError(const std::string& msg) : std::logic_error(msg) {}
};

struct TransBl {
    Matrix mm, mmti, vm, pm;
    Vector4GLf eyepos;
};

#define MAX_NLIGHTS 8

struct LSPar {
    Vector4GLf ambient;
    Vector4GLf diffuse;
    Vector4GLf position;
    Vector3GLf attenuation;
};

struct LightBl { // uniform variable block
    GLuint nls, mask;
    LSPar ls[MAX_NLIGHTS];
};

class App {

    static const int MAX_TESS_LEVEL = 32;
    static const int MIN_TESS_LEVEL = 3;

    enum class AppState {
        STATE_NOTHING, STATE_TURNING
    };

    GLFWwindow* mywindow;
    bool redraw;
    TransBl trans;
    LightBl light;
    GLuint trbuf, trbi, trbbp; // transform access


    GLuint lsbi; // uniform variable block index
    GLuint lsbuf; // UBO identifier
    GLuint lsbbp; // binding point number
    GLint lsbofs[7]; // field offsets inside block
    // nls, mask, ls[0].ambient, ls[0].direct, ls[0].position, ls[0].attenuation,  ls[1].ambient

    GLint lsbsize; // block size


    GLuint ubeznloc, ubeztloc;
    GLint trbofs[6], trbsize;
    int win_width, win_height;
    ProjectionParams params;
    AppState app_state;
    double last_xi, last_eta;
    bool skeleton, cnet, animate;
    float model_rot_angle0, model_rot_angle;
    Vector3f viewer_rvec, model_rot_axis;
    float viewer_rangle;
    Vector4f viewer_pos0;
    GLint TessLevel = 10;
    GLint BezNormals = GL_TRUE;
    GLuint program_id[2];
    GLuint shader_id[7];
    std::function<void()> idlefunc;
    BezierPatchObjf *myteapot;
public:
    App();
    ~App() = default;

    void SetupMVPMatrix();

    // OpenGL callbacks:
    void onReshape(GLFWwindow* win, int width, int height);

    void onRefresh(GLFWwindow* win);

    void onKeyPress(GLFWwindow* win, int key, int scancode, int action, int mode);

    void onCharPress(GLFWwindow* win, unsigned int charcode);

    void onMousePress(GLFWwindow* win, int button, int action, int mods);

    void onMouseMove(GLFWwindow* win, double x, double y);

    void ToggleAnimation();
    void IdleFunc();
    void RotateViewer(double delta_xi, double delta_eta);
    void LoadMyShaders();
    void InitMyObject();
    void SetupModelMatrix(Vector3f axis, float angle);
    void SetIdleFunc(std::function<void()> f);
    void MessageLoop();
    void Redraw(GLFWwindow* win);
    void DrawMyTeapot();
    void DrawMyTeapotCNet();
    void ConstructMyTeapot();
    void SetBezPatchOptions(GLint normals, GLint tess_level);
    void InitViewMatrix();
    void InitLights();



    void SetLightAmbient(int l, Vector4GLf amb);
    void SetLightDiffuse(int l, Vector4GLf dif);
    void SetLightPosition(int l, Vector4GLf pos);
    void SetLightAttenuation(int l, Vector3GLf at3);
    void SetLightOnOff(int l, char on);


};
