
#include <iostream>
#include "../src/Matrix.h"

int irand(int max = 1000000) {
    return rand() % max;
}

float frand(int max = 1000000) {
    return rand() % max;
}

Vector4f get_random_vector() {
    return Vector4f{frand(), frand(), frand(), frand()};
}

Matrix get_random_matrix() {
    return Matrix{get_random_vector(), get_random_vector(), get_random_vector(), get_random_vector()};
}


int main() {
    srand(2137);  // NOLINT(cert-msc32-c,cert-msc51-cpp)

    /// Identity tests
    auto eye = Matrix::identity();
    assert(eye == eye.transpose());
    for (int i = 0; i < 4; ++i) {
        assert(Vector4GLf{eye.column(i)} == Vector4GLf::e(i + 1));
    }

    for (int i = 0; i < 2137; ++i) {
        auto v = get_random_vector();
        assert(eye * v == v);
        assert(eye * eye * v == v);
        assert(eye * eye.transpose() * eye * v == v);
    }

    /// Multiplication tests

    for (int i = 0; i < 2137; ++i) {
        auto v = get_random_vector();
        auto m = get_random_matrix();
        auto mv = m * v;
        auto minv_op = m.invert();
        if (minv_op.has_value()) {
            auto minv = minv_op.value();
            assert(minv * mv == v);
        }
    }

    /// other matrix statics


    std::cout << "All passed!";
}